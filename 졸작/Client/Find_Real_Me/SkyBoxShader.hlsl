#include "Shaders.hlsl"

struct VS_SKYBOX_CUBEMAP_INPUT
{
	float3 position : POSITION;
};

struct VS_SKYBOX_CUBEMAP_OUTPUT
{
	float3	positionL : POSITION;
	float4	position : SV_POSITION;
};

VS_SKYBOX_CUBEMAP_OUTPUT VSSkyBox(VS_SKYBOX_CUBEMAP_INPUT input)
{
	VS_SKYBOX_CUBEMAP_OUTPUT output;
	output.positionL = input.position;

	float4 posW = mul(float4(input.position, 1.0f), gmtxObjectWorld);

	// Always center sky about camera.
	posW.xyz += gvCameraPosition;

	output.position = mul(posW, gmtxViewProjection).xyww;

	return(output);
}

float4 PSSkyBox(VS_SKYBOX_CUBEMAP_OUTPUT input) : SV_TARGET
{
	float4 cColor = gtxtSkyCubeTexture.Sample(gSamplerState, input.positionL);

	return(cColor);
}