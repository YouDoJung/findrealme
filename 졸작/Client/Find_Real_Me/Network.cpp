#include "stdafx.h"
#include "Network.h"
#include "ModelShader.h"

unsigned short CNetwork::m_usId = 0;
concurrency::concurrent_unordered_set<unsigned short>CNetwork::m_ccusViewList[NETWORK_LOCK_NUM];

CNetwork::CNetwork()
{
	if (!Initalize()) {
		printf("Network Init Fail\n");
	}
	else {
		printf("Network Init!\n");
	}
}

CNetwork::~CNetwork()
{
}

BOOL CNetwork::Initalize()
{
	WSADATA wsa;
	WSAStartup(MAKEWORD(2, 2), &wsa);

	m_bLoginSuccess = false;

	this->m_sGamesock = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, NULL, WSA_FLAG_OVERLAPPED);

	m_wbRecvwsabuf.buf = m_cRecvbuf;
	m_wbRecvwsabuf.len = BUFSIZE;

	m_wbSendwsabuf.buf = m_cSendbuf;
	m_wbSendwsabuf.len = BUFSIZE;

	for (char c = 0; c < NETWORK_LOCK_NUM; ++c) {
		m_ccusViewList[c].clear();
	}


	return(ConnectServer());
}

BOOL CNetwork::CreateEventSelect()
{
	int retval;
	m_hEvent = WSACreateEvent();

	retval = WSAEventSelect(m_sGamesock, m_hEvent, FD_READ || FD_CLOSE);
	if (retval != SOCKET_ERROR) {
		std::cout << "CNetwork::CreateSelectEvent() Success" << std::endl;
		return true;
	}
	else {
		retval = WSAGetLastError();
		std::cout << retval << '\t' << "CNetwork::CreateSelectEvent() ERR" << std::endl;
		return false;
	}
}

BOOL CNetwork::ConnectServer()
{
	int retval;
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(SOCKADDR_IN));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(SERVER_PORT);
	serveraddr.sin_addr.s_addr = inet_addr(SERVERIP);
	retval = WSAConnect(m_sGamesock, reinterpret_cast<sockaddr*>(&serveraddr), sizeof(serveraddr), NULL, NULL, NULL, NULL);
	if (retval == SOCKET_ERROR) {
		printf("소켓에러, Connect Fail\n");
		while (1) {
			printf("IP: ");
			std::cin >> SERVERIP;
			volatile bool reconnect = ConnectServer();
			if (reconnect)
				break;
		}
	}
	else {
		printf("연결\n");
		if (CreateEventSelect()) {
			return true;
		}
		else {
			return false;
		}
	}

	return true;
}

void CNetwork::DisConnect()
{
	closesocket(m_sGamesock);
	WSACloseEvent(m_hEvent);
	WSACleanup();
	std::cout << "DisConnect() Run" << std::endl;
}

void CNetwork::SendMapSwapPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_MAP_SWAP_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_MAP_SWAP_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_MAP_SWAP_PACKET);

	//이건 나중에 지우기(포탈이 생기면 지우기)
	char map = m_cMap + 1;
	map %= 4;

	switch (map)
	{
	case MAP_VILLAGE:
		sample_packet->m_bType = CS_MAP_SWAP_TO_VILLAGE;
		break;
	case MAP_LOBY:
		sample_packet->m_bType = CS_MAP_SWAP_TO_LOBY;
		break;
	case MAP_DUNGEON:
		sample_packet->m_bType = CS_MAP_SWAP_TO_DUNGEON;
		break;
	case MAP_BOSS:
		sample_packet->m_bType = CS_MAP_SWAP_TO_BOSS;
		break;
	}

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_MAP_SWAP_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendForDevMapSwapPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_MAP_SWAP_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_MAP_SWAP_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_MAP_SWAP_PACKET);
	sample_packet->m_bType = CS_DEV_MAP_SWAP;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_MAP_SWAP_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendHPPortionUsePacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ITEM_PACKET_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ITEM_PACKET_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);
	sample_packet->m_bType = CS_HP_PORTION_USE;

	m_wbSendwsabuf.len = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendMPPortionUsePacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ITEM_PACKET_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ITEM_PACKET_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);
	sample_packet->m_bType = CS_MP_PORTION_USE;

	m_wbSendwsabuf.len = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendRepatriationUsePacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ITEM_PACKET_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ITEM_PACKET_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);
	sample_packet->m_bType = CS_RETURN_USE;

	m_wbSendwsabuf.len = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendRepatriationGetPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ITEM_PACKET_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ITEM_PACKET_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);
	sample_packet->m_bType = CS_RETURN_GET;

	m_wbSendwsabuf.len = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendHPPortionGetPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ITEM_PACKET_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ITEM_PACKET_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);
	sample_packet->m_bType = CS_HP_PORTION_GET;

	m_wbSendwsabuf.len = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendMPPortionGetPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ITEM_PACKET_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ITEM_PACKET_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);
	sample_packet->m_bType = CS_MP_PORTION_GET;

	m_wbSendwsabuf.len = sizeof(s_pCS_PLAYER_ITEM_PACKET_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendForDevCharacterInvincibility()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;


	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_MAP_SWAP_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_MAP_SWAP_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_MAP_SWAP_PACKET);
	sample_packet->m_bType = CS_DEV_CHARACTER;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_MAP_SWAP_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendPosPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	DirectX::XMFLOAT3 sample_pos = GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetPosition();
	DirectX::XMFLOAT3 sample_dir = GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetLookVector();
	s_pCS_POS_PACKET sample_packet = reinterpret_cast<s_pCS_POS_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_POS_PACKET);
	sample_packet->m_bType = CS_POS;
	sample_packet->m_fX = sample_pos.x;
	sample_packet->m_fY = sample_pos.y;
	sample_packet->m_fZ = sample_pos.z;
	sample_packet->m_Dir = sample_dir;


	m_wbSendwsabuf.len = sizeof(s_CS_POS_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendDirPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_DIR_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_DIR_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_DIR_PACKET);
	sample_packet->m_bType = CS_LOOK;

	//여기서 값 다시 변경
	sample_packet->m_Dir = GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetLookVector();

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_DIR_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendAttackPacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_ATTACK;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendAttack2Packet()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_ATTACK2;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendSkill1Packet()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_SKILL1;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendSkill2Packet()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_SKILL2;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendSkill3Packet()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_SKILL3;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendSkill4Packet()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_SKILL4;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendSkill5Packet()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_SKILL5;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendIdlePacket()
{
	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_PLAYER_ANIMATION_PACKET sample_packet = reinterpret_cast<s_pCS_PLAYER_ANIMATION_PACKET>(m_cSendbuf);

	sample_packet->m_bSize = sizeof(s_CS_PLAYER_ANIMATION_PACKET);
	sample_packet->m_bType = CS_IDLE;

	m_wbSendwsabuf.len = sizeof(s_CS_PLAYER_ANIMATION_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::SendLoginPacket(wchar_t* wcPlayerID, wchar_t* wcPlayerPassword)
{
	int retval, err_code = 0;
	DWORD ipo_byte, flag = 0;

	s_pCS_LOGIN_PACKET login_packet = reinterpret_cast<s_pCS_LOGIN_PACKET>(m_cSendbuf);

	login_packet->m_bType = CS_LOGIN;
	login_packet->m_bSize = sizeof(s_CS_LOGIN_PACKET);

	wcsncpy_s(login_packet->m_wcID, MAX_STR_LEN, wcPlayerID, (UINT32)wcslen(wcPlayerID));
	wcsncpy_s(login_packet->m_wcPASS, MAX_STR_LEN, wcPlayerPassword, (UINT32)wcslen(wcPlayerPassword));

	//memcpy(login_packet->m_wcID, &player_id, MAX_STR_LEN);
	//memcpy(login_packet->m_wcPASS, &player_password, MAX_STR_LEN);


	m_wbSendwsabuf.len = sizeof(s_CS_LOGIN_PACKET);

	retval = WSASend(m_sGamesock, &m_wbSendwsabuf, 1, &ipo_byte, flag, NULL, NULL);

	if (retval != 0) {
		err_code = WSAGetLastError();
		printf("Error while sending packet [%d]", err_code);
	}
}

void CNetwork::Recvpacket()
{
	std::cout << "RunNetWork" << std::endl;
	while (!m_bClientClose) {
		//First is Total Socket Num
		m_iIndex = WSAWaitForMultipleEvents(1, &m_hEvent, FALSE, 1000, FALSE);
		if ((m_iIndex == WSA_WAIT_FAILED) /*|| (m_iIndex == WSA_WAIT_TIMEOUT)*/) {
			std::cout << "WSAWaitForMultipleEvents() WSA_WAIT_TIMEOUT or WSA_WAIT_FAILED" << std::endl;
			return;
		}
		int retval = WSAEnumNetworkEvents(m_sGamesock, m_hEvent, &m_weEvent);
		if (retval == SOCKET_ERROR) {
			std::cout << "WSAEnumNetworkEvents Err" << std::endl;
			return;
		}

		if (m_weEvent.lNetworkEvents & FD_READ) {
			if (m_weEvent.iErrorCode[FD_READ_BIT] != 0) {
				std::cout << "m_weEvent.iErrorCode" << std::endl;
				return;
			}
			DWORD size, flag = 0;
			int retval2 = WSARecv(m_sGamesock, &m_wbRecvwsabuf, 1, &size, &flag, NULL, NULL);
			if (retval2 == SOCKET_ERROR) {
				std::cout << "WSARecv SOCKET_ERROR" << std::endl;
				return;
			}

			BYTE* packet = reinterpret_cast<BYTE*>(m_cRecvbuf);

			while (size != 0)
			{
				if (m_dwPacketsize == 0) m_dwPacketsize = packet[0];
				if (size + m_dwSavepacketsize >= m_dwPacketsize) {
					memcpy(m_cPacketbuf + m_dwSavepacketsize, packet, m_dwPacketsize - m_dwSavepacketsize);
					ProcessPacket(m_cPacketbuf);
					packet += m_dwPacketsize - m_dwSavepacketsize;
					size -= m_dwPacketsize - m_dwSavepacketsize;
					m_dwPacketsize = 0;
					m_dwSavepacketsize = 0;
				}
				else
				{
					memcpy(m_cPacketbuf + m_dwSavepacketsize, packet, size);
					m_dwSavepacketsize += size;
					size = 0;
				}
			}
		}
		if (m_weEvent.lNetworkEvents & FD_CLOSE) {
			std::cout << "FD_CLOSE" << std::endl;
			m_bClientClose = true;
			return;
		}
	}
}

void CNetwork::ProcessPacket(char* PACKET)
{
	switch (PACKET[1])
	{
	case SC_LOGIN_FAIL:
	{
		printf_s("LoginFail ReInput ");
		SetLoginSend(false);
		break;
	}
	case SC_LOGIN_SUCCESS:
	{
		printf_s("LoginSuccess!\n");
		SetLoginSend(true);
		SetIsLogin(true);
		break;
	}
	case SC_LOGIN:
	{
		s_pSC_FIRST_STATUS_PACKET packet = reinterpret_cast<s_pSC_FIRST_STATUS_PACKET>(PACKET);
		m_usId = packet->m_usId;
		gnCurrentMapIndex = m_cMap = packet->m_cMap;
		
		GameFramework::m_ccumPlayer[m_usId]->SetObjectType(packet->m_cObjectType);
		GameFramework::m_ccumPlayer[m_usId]->SetPosition(DirectX::XMFLOAT3(packet->m_fX, packet->m_fY, packet->m_fZ));

		GameFramework::m_ccumPlayer[m_usId]->SetMaxMP(packet->m_iMaxMP);
		GameFramework::m_ccumPlayer[m_usId]->SetMaxEXP(packet->m_iMaxEXP);
		GameFramework::m_ccumPlayer[m_usId]->SetMaxHP(packet->m_iMaxHP);

		GameFramework::m_ccumPlayer[m_usId]->SetHP(packet->m_iHP);
		GameFramework::m_ccumPlayer[m_usId]->SetMP(packet->m_iMP);
		GameFramework::m_ccumPlayer[m_usId]->SetEXP(packet->m_iExp);

		GameFramework::m_ccumPlayer[m_usId]->SetLevel(packet->m_iLevel);
		GameFramework::m_ccumPlayer[m_usId]->SetMoney(packet->m_iMoney);

		s_POSSESSION_ITEM item{ packet->m_cHpPortion, packet->m_cMpPortion, packet->m_cRepatriation };
		GameFramework::m_ccumPlayer[m_usId]->SetPossessionItem(item);

		InsertListElement(PLAYER_LIST, m_usId);
		std::cout << m_usId << std::endl;

		SetLoopStart(true);
		break;
	}
	case SC_HP_PORTION_CHANGE:
	{
		s_pSC_CHANGE_ITEM_PACKET packet = reinterpret_cast<s_pSC_CHANGE_ITEM_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetHPPortion(packet->m_cAny);
		break;
	}
	case SC_MP_PORTION_CHANGE:
	{
		s_pSC_CHANGE_ITEM_PACKET packet = reinterpret_cast<s_pSC_CHANGE_ITEM_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetMPPortion(packet->m_cAny);
		break;
	}
	case SC_REPATRIATION_CHANGE:
	{
		s_pSC_CHANGE_ITEM_PACKET packet = reinterpret_cast<s_pSC_CHANGE_ITEM_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetRepatriation(packet->m_cAny);
		break;
	}
	case SC_MAP_SWAP:
	{
		s_pSC_PLAYER_MAP_SWAP_PACKET packet = reinterpret_cast<s_pSC_PLAYER_MAP_SWAP_PACKET>(PACKET);
		ClearListElement(PLAYER_LIST);
		ClearListElement(MONSTER_LIST);
		gnCurrentMapIndex = m_cMap = packet->m_bMapType;
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetPosition(XMFLOAT3(5000.f, 516.f, 5000.f));
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->SetMonsterWakeUp(true);
		}
		else {
			GameFramework::m_pBoss->SetMonsterWakeUp(false);
		}
		GameFramework::GetInstance()->StopBGM();
		GameFramework::GetInstance()->SetNewBGM();
		break;
	}
	case SC_PLAYER_RESPAWN:
	{
		s_pSC_PLAYER_RESPAWN_PACKET packet = reinterpret_cast<s_pSC_PLAYER_RESPAWN_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetNowAnimation(std::string("idle"));
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetPosition(DirectX::XMFLOAT3(5000.0f, 516.0f, 5000.0f));
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetIsDie(false);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetHP(packet->m_iHP);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetMP(packet->m_iMP);
		GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetEXP(packet->m_iExp);
		break;
	}
	case SC_CONNECT:
	{
		s_pSC_ACCEPT_PACKET accept_packet = reinterpret_cast<s_pSC_ACCEPT_PACKET>(PACKET);
		if (accept_packet->m_usId >= 5) break;
		if (!ExistListElement(PLAYER_LIST, accept_packet->m_usId)) {
			InsertListElement(PLAYER_LIST, accept_packet->m_usId);
		}
		//리스폰을 위해서 룩벡터와 좌표를 다시 받게 해놓았는데 여기서 가끔 터짐 -> 룩벡터만 다시 안받게 수정 해보고 계속 테스트 해보기 
		if (m_usId != accept_packet->m_usId) {
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetObjectType(accept_packet->m_cObjectType);
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetPosXZ(accept_packet->m_fX, accept_packet->m_fZ);
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetPosY(accept_packet->m_fY);
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetPlayerLookVec(accept_packet->m_Dir);
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetLevel(accept_packet->m_iLevel);
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetMaxHP(accept_packet->m_iMaxHP);
			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetHP(accept_packet->m_iHP);

			//wchar_t other_id[MAX_STR_LEN];
			//wcsncpy_s(other_id, MAX_STR_LEN, accept_packet->m_wcID, (UINT32)wcslen(accept_packet->m_wcID));

			//GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetPlayerID(other_id);

			/*wchar_t other_id[MAX_STR_LEN];
			wcsncpy_s(other_id, MAX_STR_LEN, accept_packet->m_wcID, (UINT32)wcslen(accept_packet->m_wcID));

			GameFramework::m_ccumPlayer[accept_packet->m_usId]->SetPlayerID(other_id);*/

		}

		break;

	}
	case SC_POS:
	{
		s_pSC_POS_PACKET packet = reinterpret_cast<s_pSC_POS_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (packet->m_usId != m_usId) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetPosXZ(packet->m_fX, packet->m_fZ);
			GameFramework::m_ccumPlayer[packet->m_usId]->SetPosY(packet->m_fY);
			GameFramework::m_ccumPlayer[packet->m_usId]->SetPlayerLookVec(packet->m_Dir);
		}
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("walk")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("walk"));
		}
		break;
	}

	case SC_CHANGE_STATUS_PLAYER_HP:
	{
		s_pSC_CHANGE_STATUS_PACKET packet = reinterpret_cast<s_pSC_CHANGE_STATUS_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[packet->m_usId]->SetHP(packet->m_iAny);
		//std::cout << "HP: " << packet->m_iAny << "\n";
		break;
	}
	case SC_CHANGE_STATUS_PLAYER_EXP:
	{
		s_pSC_CHANGE_STATUS_PACKET packet = reinterpret_cast<s_pSC_CHANGE_STATUS_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[packet->m_usId]->SetEXP(packet->m_iAny);
		break;
	}
	case SC_CHANGE_STATUS_PLAYER_MP:
	{
		s_pSC_CHANGE_STATUS_PACKET packet = reinterpret_cast<s_pSC_CHANGE_STATUS_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[packet->m_usId]->SetMP(packet->m_iAny);
		break;
	}
	case SC_CHANGE_STATUS_PLAYER_MONEY:
	{
		s_pSC_CHANGE_STATUS_PACKET packet = reinterpret_cast<s_pSC_CHANGE_STATUS_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[packet->m_usId]->SetMoney(packet->m_iAny);
		break;
	}
	case SC_CHANGE_LEVEL_UP:
	{
		s_pSC_LEVEL_UP_PACKET packet = reinterpret_cast<s_pSC_LEVEL_UP_PACKET>(PACKET);
		GameFramework::m_ccumPlayer[m_usId]->SetMaxMP(packet->m_iMaxMP);
		GameFramework::m_ccumPlayer[m_usId]->SetMaxEXP(packet->m_iMaxEXP);
		GameFramework::m_ccumPlayer[m_usId]->SetMaxHP(packet->m_iMaxHP);

		GameFramework::m_ccumPlayer[m_usId]->SetHP(packet->m_iHP);
		GameFramework::m_ccumPlayer[m_usId]->SetMP(packet->m_iMP);
		GameFramework::m_ccumPlayer[m_usId]->SetEXP(packet->m_iExp);

		GameFramework::m_ccumPlayer[m_usId]->SetLevel(packet->m_iLevel);

		break;
	}
	//////////////////////////////
	//FIXX: DIR은 SC_POS에 한번에?
	//////////////////////////////
	case SC_DIR:
	{
		s_pSC_DIR_PACKET packet = reinterpret_cast<s_pSC_DIR_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (packet->m_usId != m_usId) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetPlayerLookVec(packet->m_Dir);
		}
		break;
	}
	case SC_DISCONNECT:
	{
		s_pSC_DISCONNECT_PACKET packet = reinterpret_cast<s_pSC_DISCONNECT_PACKET>(PACKET);
		DeleteListElement(PLAYER_LIST, packet->m_usId);
		break;
	}
	case SC_ATTACK:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("attack1")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("attack1"));
		}
		break;
	}
	case SC_ATTACK2:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("attack2")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("attack2"));
		}
		break;
	}
	case SC_SKILL1:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("skill1")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("skill1"));
		}
		break;
	}
	case SC_SKILL2:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("skill2")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("skill2"));
		}
		break;
	}
	case SC_SKILL3:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("skill3")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("skill3"));
		}
		break;
	}
	case SC_SKILL4:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("skill4")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("skill4"));
		}
		break;
	}
	case SC_SKILL5:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("skill5")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("skill5"));
		}
		break;
	}
	case SC_IDLE:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("idle")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("idle"));
		}
		break;
	}
	case SC_NPC_LOG_IN:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_NPC_LOGIN_PACKET packet = reinterpret_cast<s_pSC_NPC_LOGIN_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterWakeUp(true);
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_NPC_LOGIN_PACKET packet = reinterpret_cast<s_pSC_NPC_LOGIN_PACKET>(PACKET);
			if (packet->m_usId == 48) {
				GameFramework::m_pBoss->SetMonsterWakeUp(true);
				break;
			}
			GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetMonsterWakeUp(true);
		}
		break;
	}
	case SC_NPC_STATUS:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_NPC_STATUS_PACKET packet = reinterpret_cast<s_pSC_NPC_STATUS_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterPos(DirectX::XMFLOAT3(packet->m_fX, 516.0f, packet->m_fZ));
			GameFramework::m_ccumMonster[packet->m_usId]->SetMaxHP(packet->m_iMaxHP);
			GameFramework::m_ccumMonster[packet->m_usId]->SetHP(packet->m_iHP);
			GameFramework::m_ccumMonster[packet->m_usId]->SetLevel(packet->m_iLevel);
			if (packet->m_usId < 96) {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(8.0f, packet->m_xmf3Dir);
			}
			else if (96 <= packet->m_usId&& packet->m_usId < 181) {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(10.0f, packet->m_xmf3Dir);
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(12.0f, packet->m_xmf3Dir);
			}
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_NPC_STATUS_PACKET packet = reinterpret_cast<s_pSC_NPC_STATUS_PACKET>(PACKET);
			if (packet->m_usId == BOSS_ID) break;
			int id = packet->m_usId + NOMALDUNGEON_MONSTER_NUM;
			GameFramework::m_ccumMonster[id]->SetMonsterPos(DirectX::XMFLOAT3(packet->m_fX, 516.0f, packet->m_fZ));
			GameFramework::m_ccumMonster[id]->SetMaxHP(packet->m_iMaxHP);
			GameFramework::m_ccumMonster[id]->SetHP(packet->m_iHP);
			GameFramework::m_ccumMonster[id]->SetLevel(packet->m_iLevel);
			if (id < NOMALDUNGEON_MONSTER_NUM + 24) {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(8.0f, packet->m_xmf3Dir);
			}
			else if (NOMALDUNGEON_MONSTER_NUM + 24 <= id && id < 38 + NOMALDUNGEON_MONSTER_NUM) {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(10.0f, packet->m_xmf3Dir);
			}
			else {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(12.0f, packet->m_xmf3Dir);
			}
		}
		break;
	}
	case SC_NPC_REMOVE:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_DISCONNECT_PACKET packet = reinterpret_cast<s_pSC_DISCONNECT_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetNowAnimationForMonster(std::string("idle"));
			GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterWakeUp(false);
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_DISCONNECT_PACKET packet = reinterpret_cast<s_pSC_DISCONNECT_PACKET>(PACKET);
			if (packet->m_usId == 48) {
				GameFramework::m_pBoss->SetMonsterWakeUp(false);
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetNowAnimationForMonster(std::string("idle"));
				GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetMonsterWakeUp(false);
			}
		}
		break;
	}
	case SC_NPC_POS:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_NPC_POS_PACKET packet = reinterpret_cast<s_pSC_NPC_POS_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterPos(DirectX::XMFLOAT3(packet->m_fX, 516.0f, packet->m_fZ));
			if (packet->m_usId < 96) {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(8.0f, packet->m_xmf3Dir);
			}
			else if (96 <= packet->m_usId&& packet->m_usId < 181) {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(10.0f, packet->m_xmf3Dir);
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(12.0f, packet->m_xmf3Dir);
			}
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_NPC_POS_PACKET packet = reinterpret_cast<s_pSC_NPC_POS_PACKET>(PACKET);
			if (packet->m_usId == BOSS_ID) break;
			int id = packet->m_usId + NOMALDUNGEON_MONSTER_NUM;
			GameFramework::m_ccumMonster[id]->SetMonsterPos(DirectX::XMFLOAT3(packet->m_fX, 516.0f, packet->m_fZ));
			if (id < NOMALDUNGEON_MONSTER_NUM + 24) {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(8.0f, packet->m_xmf3Dir);
			}
			else if (NOMALDUNGEON_MONSTER_NUM + 24 <= id && id < 38 + NOMALDUNGEON_MONSTER_NUM) {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(10.0f, packet->m_xmf3Dir);
			}
			else {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(12.0f, packet->m_xmf3Dir);
			}
		}
		break;
	}
	/*
	case SC_NPC_DIR:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_NPC_DIR_PACKET packet = reinterpret_cast<s_pSC_NPC_DIR_PACKET>(PACKET);
			if (packet->m_usId < 96) {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(8.0f, packet->m_xmf3Dir);
			}
			else if (96 <= packet->m_usId&& packet->m_usId < 181) {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(10.0f, packet->m_xmf3Dir);
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId]->SetMonsterLook(12.0f, packet->m_xmf3Dir);
			}
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_NPC_DIR_PACKET packet = reinterpret_cast<s_pSC_NPC_DIR_PACKET>(PACKET);
			if (packet->m_usId == 48) {
				break;
			}
			int id = packet->m_usId + NOMALDUNGEON_MONSTER_NUM;
			if (id < NOMALDUNGEON_MONSTER_NUM + 24) {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(8.0f, packet->m_xmf3Dir);
			}
			else if (NOMALDUNGEON_MONSTER_NUM + 24 <= id && id < 38 + NOMALDUNGEON_MONSTER_NUM) {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(10.0f, packet->m_xmf3Dir);
			}
			else {
				GameFramework::m_ccumMonster[id]->SetMonsterLook(12.0f, packet->m_xmf3Dir);
			}
		}
		break;

	}*/
	case SC_NPC_ATTACK:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetNowAnimationForMonster(std::string("attack1"));
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetNowAnimationForMonster(std::string("attack1"));
		}
		break;
	}
	case SC_NPC_FOLLOW:
	{
		// 여기서 터졌었음.
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetNowAnimationForMonster(std::string("run"));
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetNowAnimationForMonster(std::string("run"));
		}

		break;
	}
	case SC_NPC_IDLE:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetNowAnimationForMonster(std::string("idle"));
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			if (packet->m_usId == 48) {
				/*DirectX::XMFLOAT3 pos = GameFramework::m_pBoss->GetPosition();
				pos.y = 800.f;
				GameFramework::m_pBoss->SetPosition(pos);*/
				GameFramework::m_pBoss->SetNowAnimation(std::string("idle"));
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetNowAnimationForMonster(std::string("idle"));
			}
		}

		break;
	}
	case SC_PLAYER_DAMAGED:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("stun")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("stun"));
		}
		break;
	}
	case SC_PLAYER_DOWN:
	{
		s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
		if (packet->m_usId >= 5) break;
		if (GameFramework::m_ccumPlayer[packet->m_usId]->GetNowAnimation() != std::string("die")) {
			GameFramework::m_ccumPlayer[packet->m_usId]->SetNowAnimation(std::string("die"));
		}
		if (packet->m_usId == CNetwork::m_usId)
			GameFramework::m_ccumPlayer[packet->m_usId]->SetIsDie(true);
		break;
	}
	case SC_NPC_DOWN:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetNowAnimationForMonster(std::string("die"));
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			if (packet->m_usId == 48) {
				GameFramework::m_pBoss->SetNowAnimation(std::string("die"));
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetNowAnimationForMonster(std::string("die"));
			}

		}

		break;
	}
	case SC_NPC_DAMAGED:
	{

		if (m_cMap == MAP_DUNGEON) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetNowAnimationForMonster(std::string("hit"));
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_ANIMATION_PACKET packet = reinterpret_cast<s_pSC_ANIMATION_PACKET>(PACKET);
			if (packet->m_usId == 48) {
				break;
			}
			else {
				GameFramework::m_ccumMonster[packet->m_usId + NOMALDUNGEON_MONSTER_NUM]->SetNowAnimationForMonster(std::string("hit"));
			}

		}
		break;
	}
	case SC_CHANGE_STATUS_NPC_HP:
	{
		if (m_cMap == MAP_DUNGEON) {
			s_pSC_CHANGE_STATUS_PACKET packet = reinterpret_cast<s_pSC_CHANGE_STATUS_PACKET>(PACKET);
			GameFramework::m_ccumMonster[packet->m_usId]->SetHP(packet->m_iAny);
		}
		else if (m_cMap == MAP_BOSS) {
			s_pSC_CHANGE_STATUS_PACKET packet = reinterpret_cast<s_pSC_CHANGE_STATUS_PACKET>(PACKET);
			unsigned short id = packet->m_usId + NOMALDUNGEON_MONSTER_NUM;
			if (packet->m_usId == 48) {
				GameFramework::m_pBoss->SetHP(packet->m_iAny);
			}
			else {
				GameFramework::m_ccumMonster[id]->SetHP(packet->m_iAny);
			}

		}

		break;
	}
	case SC_BOSS_FAST_FLY:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("fire_breath")) {
				GameFramework::m_pBoss->Rotate(45.0f, 0.0f, 0.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("fast_fly"));
		}
		break;
	}
	case SC_BOSS_LEG_ATTACK:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("tail_attack")) {
				GameFramework::m_pBoss->Rotate(0.0f, 0.0f, 180.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("leg_attack"));
		}
		break;
	}
	case SC_BOSS_TAIL_ATTACK:
	{
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->Rotate(0.0f, 0.0f, -180.0f);
			GameFramework::m_pBoss->SetNowAnimation(std::string("tail_attack"));
		}
		break;
	}
	case SC_BOSS_FLY:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("tail_attack")) {
				GameFramework::m_pBoss->Rotate(0.0f, 0.0f, 180.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("fast_fly"));
			s_pSC_BOSS_FLY_PACKET packet = reinterpret_cast<s_pSC_BOSS_FLY_PACKET>(PACKET);
			DirectX::XMFLOAT3 pos = GameFramework::m_pBoss->GetPosition();
			pos.y = packet->m_fY;
			GameFramework::m_pBoss->SetPosition(pos);
		}
		break;
	}
	case SC_BOSS_FLY_DOWN:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("fire_breath")) {
				GameFramework::m_pBoss->Rotate(45.0f, 0.0f, 0.0f);
			}
			if (GameFramework::m_pBoss->GetNowAnimation() != std::string("turn_die")) {
				GameFramework::m_pBoss->SetNowAnimation(std::string("fast_fly"));
			}
			s_pSC_BOSS_FLY_PACKET packet = reinterpret_cast<s_pSC_BOSS_FLY_PACKET>(PACKET);
			DirectX::XMFLOAT3 pos = GameFramework::m_pBoss->GetPosition();
			pos.y = packet->m_fY;
			GameFramework::m_pBoss->SetPosition(pos);
		}
		break;
	}
	case SC_BOSS_TURN:
	{
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->SetNowAnimation(std::string("turn_walk"));
		}
		break;
	}
	case SC_BOSS_RUN:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("tail_attack")) {
				GameFramework::m_pBoss->Rotate(0.0f, 0.0f, -180.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("run"));
		}
		break;
	}
	case SC_BOSS_FLY_ATTACK:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("fire_breath")) {
				GameFramework::m_pBoss->Rotate(45.0f, 0.0f, 0.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("fly_attack"));
		}
		break;
	}
	case SC_BOSS_FIRE_BREATH:
	{
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->Rotate(-45.0f, 0.0f, 0.0f);
			GameFramework::m_pBoss->SetNowAnimation(std::string("fire_breath"));
		}
		break;
	}
	case SC_BOSS_FLY_DEAD:
	{
		if(m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("fire_breath")) {
				GameFramework::m_pBoss->Rotate(45.0f, 0.0f, 0.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("turn_die"));
		}
		break;
	}
	case SC_BOSS_STATUS:
	{
		if (m_cMap == MAP_BOSS) {
			s_pSC_BOSS_STATUS_PACKET packet = reinterpret_cast<s_pSC_BOSS_STATUS_PACKET>(PACKET);
			GameFramework::m_pBoss->SetPosition(packet->m_xmf3Pos);
			GameFramework::m_pBoss->SetBossLook(packet->m_xmf3Dir);
			GameFramework::m_pBoss->SetMaxHP(packet->m_iMaxHP);
			GameFramework::m_pBoss->SetHP(packet->m_iHP);
			GameFramework::m_pBoss->SetLevel(packet->m_iLevel);
		}
		break;
	}
	case SC_BOSS_POS:
	{
		if (m_cMap == MAP_BOSS) {
			s_pSC_BOSS_POS_PACKET packet = reinterpret_cast<s_pSC_BOSS_POS_PACKET>(PACKET);
			GameFramework::m_pBoss->SetPosition(packet->m_xmf3Pos);
			GameFramework::m_pBoss->SetBossLook(packet->m_xmf3Dir);
		}
		break;
	}
	case SC_BOSS_RAID:
	{
		if (m_cMap == MAP_BOSS) {
			if (GameFramework::m_pBoss->GetNowAnimation() == std::string("fire_breath")) {
				GameFramework::m_pBoss->Rotate(45.0f, 0.0f, 0.0f);
			}
			else if (GameFramework::m_pBoss->GetNowAnimation() == std::string("tail_attack")) {
				GameFramework::m_pBoss->Rotate(0.0f, 0.0f, 180.0f);
			}
			GameFramework::m_pBoss->SetNowAnimation(std::string("idle2"));
		}
		break;
	}
	case SC_BOSS_LEFT_HIT:
	{
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->SetNowAnimation(std::string("left_hit"));
		}
		break;
	}
	case SC_BOSS_RIGHT_HIT:
	{
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->SetNowAnimation(std::string("right_hit"));
		}
		break;
	}
	case SC_BOSS_DOWN_HIT:
	{
		if (m_cMap == MAP_BOSS) {
			GameFramework::m_pBoss->SetNowAnimation(std::string("down_hit"));
		}
		break;
	}

	default:
		break;
	}
}

//
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle", CombinedAnimationClip(start_time, 339.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("jump", CombinedAnimationClip(340.f, 365.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("leg_attack", CombinedAnimationClip(366.f, 410.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fire_breath", CombinedAnimationClip(411.f, 475.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("tail_attack", CombinedAnimationClip(475.f, 515.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("left_hit", CombinedAnimationClip(516.f, 533.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("right_hit", CombinedAnimationClip(534.f, 551.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("die", CombinedAnimationClip(552.f, 611.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("walk", CombinedAnimationClip(612.f, 644.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("run", CombinedAnimationClip(645.f, 660.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fly", CombinedAnimationClip(661.f, 707.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fly_attack", CombinedAnimationClip(708.f, 768, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fast_fly", CombinedAnimationClip(862.f, 884.f, 15.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("down_hit", CombinedAnimationClip(885.f, 905.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("turn_die", CombinedAnimationClip(906.f, 970.f, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle2", CombinedAnimationClip(1005.f, 1100, 10.f)));
//m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("turn_walk", CombinedAnimationClip(1101.f, end_time, 10.f)));