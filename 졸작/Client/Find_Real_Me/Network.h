#pragma once
#include "stdafx.h"
#include "GameFramework.h"
#include "Player.h"
#include "Scene.h"

constexpr char NETWORK_LOCK_NUM = 2;

constexpr char PLAYER_LIST = 0;
constexpr char MONSTER_LIST = 1;

constexpr char LOGIN_SUCCESS_LOCK = 0;
constexpr char LOOP_START_LOCK = 1;
constexpr char LOGIN_PACKET_SEND_LOCK = 2;

class CNetwork : public CTemplateSingleton<CNetwork> {
private:
	SOCKET m_sGamesock;

	WSAEVENT m_hEvent;
	WSANETWORKEVENTS m_weEvent;

	WSABUF m_wbSendwsabuf;
	WSABUF m_wbRecvwsabuf;

	char m_cSendbuf[BUFSIZE];
	char m_cRecvbuf[BUFSIZE];
	char m_cPacketbuf[MAX_PACKET_SIZE];

	DWORD m_dwPacketsize = 0;
	DWORD m_dwSavepacketsize = 0;

	int m_iIndex;

	std::recursive_mutex m_rmListMutex[NETWORK_LOCK_NUM];

	std::recursive_mutex m_rmLoginStateLock[3];

	volatile bool m_bLoginPacketSend = false;
	volatile bool m_bLoginSuccess = false;
	volatile bool m_bLoopStart = false;
	bool m_bPushKey = false;
	bool m_bClientClose = false;
	char m_cMap = MAP_VILLAGE;
	bool m_bCollision = false;
public:
	explicit CNetwork();
	~CNetwork();

public:
	static unsigned short m_usId;

	BOOL Initalize();
	BOOL ConnectServer();
	BOOL CreateEventSelect();


	void DisConnect();
	void Recvpacket();
	void ProcessPacket(char* PACKET);

	void SendHPPortionUsePacket();
	void SendMPPortionUsePacket();
	void SendRepatriationUsePacket();

	void SendHPPortionGetPacket();
	void SendMPPortionGetPacket();
	void SendRepatriationGetPacket();

	void SendPosPacket();
	void SendDirPacket();
	void SendAttackPacket();
	void SendAttack2Packet();
	void SendSkill1Packet();
	void SendSkill2Packet();
	void SendSkill3Packet();
	void SendSkill4Packet();
	void SendSkill5Packet();
	void SendIdlePacket();

	void SendLoginPacket(wchar_t* wcPlayerID, wchar_t* wcPlayerPassword);

	void SendMapSwapPacket();

	void SendForDevMapSwapPacket();
	void SendForDevCharacterInvincibility();

	void SetPushKey(bool bPush) { m_bPushKey = bPush; }
	bool GetPushKey() { return m_bPushKey; }

	bool GetLoginSend() { std::lock_guard<std::recursive_mutex>lock(m_rmLoginStateLock[LOGIN_PACKET_SEND_LOCK]); return m_bLoginPacketSend; }
	void SetLoginSend(bool bStart) { std::lock_guard<std::recursive_mutex>lock(m_rmLoginStateLock[LOGIN_PACKET_SEND_LOCK]); m_bLoginPacketSend = bStart; }

	void SetLoopStart(bool bStart) { std::lock_guard<std::recursive_mutex>lock(m_rmLoginStateLock[LOOP_START_LOCK]); m_bLoopStart = bStart; }
	bool GetLoopStart() { std::lock_guard<std::recursive_mutex>lock(m_rmLoginStateLock[LOOP_START_LOCK]); return m_bLoopStart; }
	void SetClient(bool bStart) { m_bClientClose = bStart; }
	char GetCurrentMap() { return m_cMap; }
	void SetCurrentMap() { ++m_cMap %= 4; }
	void SetCollision(bool bCollision) { m_bCollision = bCollision; }
	bool GetCollision() { return m_bCollision; }
	void SetIsLogin(bool bLogin) { std::lock_guard<std::recursive_mutex>lock(m_rmLoginStateLock[LOGIN_SUCCESS_LOCK]); m_bLoginSuccess = bLogin; }
	bool GetIsLogin() { std::lock_guard<std::recursive_mutex>lock(m_rmLoginStateLock[LOGIN_SUCCESS_LOCK]); return m_bLoginSuccess; }
public:
	static concurrency::concurrent_unordered_set<unsigned short>m_ccusViewList[NETWORK_LOCK_NUM];

	void CopyBefore(char cWhat, concurrency::concurrent_unordered_set<unsigned short>& usCopyList)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		usCopyList = m_ccusViewList[cWhat];
	}
	void DeleteListElement(char cWhat, unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		m_ccusViewList[cWhat].unsafe_erase(bId);
	}
	void InsertListElement(char cWhat, unsigned short bId)
	{
		m_ccusViewList[cWhat].insert(bId);
	}
	BOOL ExistListElement(char cWhat, unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		if (m_ccusViewList[cWhat].count(bId) != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	void ClearListElement(char cWhat)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		m_ccusViewList[cWhat].clear();
	}
};