//-----------------------------------------------------------------------------
// File: CPlayer.cpp
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "Player.h"
#include "Shader.h"
#include "ModelShader.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPlayer
CSkinnedObjectShader* CPlayer::m_pSkinnedObjectShader = nullptr;

CPlayer::CPlayer(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, void* pContext) : CSkinAndDivisionObject()
{
	m_pd3dDevice = pd3dDevice;
	m_pd3dCommandList = pd3dCommandList;
	m_pd3dGraphicsRootSignature = pd3dGraphicsRootSignature;
}

CPlayer::~CPlayer()
{
	if (m_pCamera) {
		delete m_pCamera;
		m_pCamera = nullptr;
	}
	if (m_vCrackEffectObject.size() > 0) {
		for (auto& obj : m_vCrackEffectObject) {
			if (obj) {
				delete obj;
				obj = nullptr;
			}
		}
		m_vCrackEffectObject.clear();
	}
	if (m_vSword.size() > 0) {
		for (auto& obj : m_vSword) {
			delete obj;
			obj = nullptr;
		}
		m_vSword.clear();
	}
	if (m_vThunderEffectObject.size() > 0) {
		for (auto& obj : m_vThunderEffectObject) {
			if (obj) {
				delete obj;
				obj = nullptr;
			}
		}
		m_vThunderEffectObject.clear();
	}
	if (m_vShieldEffectObject.size() > 0) {
		for (auto& obj : m_vShieldEffectObject) {
			if (obj) {
				delete obj;
				obj = nullptr;
			}
		}
		m_vShieldEffectObject.clear();
	}
}

void CPlayer::SetSword(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size)
{
	for (int i = 0; i < 60; ++i)
	{
		m_vSword.emplace_back(new CSwordSkillEffectObject(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, xmf2Size, m_vChilds[0]->GetMesh(0), i, this, m_xmi2TexIndex, m_ManSwordSkillObjectCBIndex[i], m_ManSwordSkillObjectChildObjectCBIndex[i]));
	}

}

void CPlayer::Move(DWORD dwDirection, float fDistance, bool bUpdateVelocity)
{
	if (dwDirection)
	{
		XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
		//if (dwDirection & DIR_FORWARD) xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Look, fDistance);
		//if (dwDirection & DIR_BACKWARD) xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Look, -fDistance);
		//if (dwDirection & DIR_RIGHT) xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Right, fDistance);
		//if (dwDirection & DIR_LEFT) xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Right, -fDistance);
		//if (dwDirection & DIR_UP) xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Up, fDistance);
		//if (dwDirection & DIR_DOWN) xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Up, -fDistance);
		XMFLOAT3 xmf3Look = { 0, 0, 0 };
		if (dwDirection & DIR_FORWARD) {
			if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(0, 0, 1))))
				xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(0, 0, 1));
		}
		if (dwDirection & DIR_BACKWARD) {
			if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(0, 0, -1))))
				xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(0, 0, -1));
		}
		if (dwDirection & DIR_RIGHT) {
			if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(1, 0, 0))))
				xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(1, 0, 0));
		}
		if (dwDirection & DIR_LEFT) {
			if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(-1, 0, 0))))
				xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(-1, 0, 0));
		}

		m_xmf3Look = Vector3::Normalize(xmf3Look);
		m_xmf3Right = Vector3::CrossProduct(m_xmf3Up, m_xmf3Look);

		xmf3Shift = Vector3::Add(xmf3Shift, m_xmf3Look, fDistance);
		Move(xmf3Shift, bUpdateVelocity);
	}
}

void CPlayer::Move(const XMFLOAT3& xmf3Shift, bool bUpdateVelocity)
{
	if (bUpdateVelocity)
	{
		m_xmf3Velocity = Vector3::Add(m_xmf3Velocity, xmf3Shift);
	}
	else
	{
		m_xmf3Position = Vector3::Add(m_xmf3Position, xmf3Shift);
		m_pCamera->Move(xmf3Shift);
	}
}

void CPlayer::Rotate(float x, float y, float z)
{
	DWORD nCurrentCameraMode = m_pCamera->GetMode();
	if ((nCurrentCameraMode == FIRST_PERSON_CAMERA) || (nCurrentCameraMode == THIRD_PERSON_CAMERA))
	{
		if (x != 0.0f)
		{
			m_fPitch += x;
			if (m_fPitch > +89.0f) { x -= (m_fPitch - 89.0f); m_fPitch = +89.0f; }
			if (m_fPitch < -89.0f) { x -= (m_fPitch + 89.0f); m_fPitch = -89.0f; }
		}
		if (y != 0.0f)
		{
			m_fYaw += y;
			if (m_fYaw > 360.0f) m_fYaw -= 360.0f;
			if (m_fYaw < 0.0f) m_fYaw += 360.0f;
		}
		if (z != 0.0f)
		{
			m_fRoll += z;
			if (m_fRoll > +20.0f) { z -= (m_fRoll - 20.0f); m_fRoll = +20.0f; }
			if (m_fRoll < -20.0f) { z -= (m_fRoll + 20.0f); m_fRoll = -20.0f; }
		}
		m_pCamera->Rotate(x, y, z);
		if (y != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_xmf3Up), XMConvertToRadians(y));
			m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmmtxRotate);
			m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmmtxRotate);
		}
	}
	else if (nCurrentCameraMode == SPACESHIP_CAMERA)
	{
		m_pCamera->Rotate(x, y, z);
		if (x != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_xmf3Right), XMConvertToRadians(x));
			m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmmtxRotate);
			m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmmtxRotate);
		}
		if (y != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_xmf3Up), XMConvertToRadians(y));
			m_xmf3Look = Vector3::TransformNormal(m_xmf3Look, xmmtxRotate);
			m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmmtxRotate);
		}
		if (z != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&m_xmf3Look), XMConvertToRadians(z));
			m_xmf3Up = Vector3::TransformNormal(m_xmf3Up, xmmtxRotate);
			m_xmf3Right = Vector3::TransformNormal(m_xmf3Right, xmmtxRotate);
		}
	}

	m_xmf3Look = Vector3::Normalize(m_xmf3Look);
	m_xmf3Right = Vector3::CrossProduct(m_xmf3Up, m_xmf3Look, true);
	m_xmf3Up = Vector3::CrossProduct(m_xmf3Look, m_xmf3Right, true);
}

void CPlayer::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
	m_fElapsedTime = fTimeElapsed;

	if (m_strNowClip == "skill1" || m_strNowClip == "skill2" || m_strNowClip == "skill3" ||
		m_strNowClip == "attack1" || m_strNowClip == "attack2") {
		m_bCoolTime = true;
	}
	else {
		m_bCoolTime = false;
	}

	if (this->GetObjectType() == e_ObjectType::e_Player_Man) {
		// 갈라지기
		if (m_strNowClip == "skill2" && m_fNowTime > 0.8f && m_fNowTime < 0.9f) {
			cout << "이펙트 시작" << endl;
			if (!m_vCrackEffectObject[0]->GetIsEffect()) {
				for (auto& obj : m_vCrackEffectObject) {
					XMFLOAT3 pos = Vector3::Add(m_xmf3Position, Vector3::Multiply(100.f, m_xmf3Look));
					pos.y += 5.f;
					obj->SetEffectStart(pos, true);
				}
			}
		}
		for (auto& obj : m_vCrackEffectObject) {
			obj->Animate(fTimeElapsed);
		}
		// 툼스톤
		if (m_strNowClip == "skill1") {
			if (!m_isSwordEffect) {
				m_isSwordEffect = true;
			}
		}
		if (m_isSwordEffect) {
			m_fSwordFrameTime += fTimeElapsed;

			for (auto& obj : m_vSword) {
				obj->Animate(fTimeElapsed);
			}
			if (m_fSwordFrameTime >= 3.f) {
				m_fSwordFrameTime = 0.f;
				m_isSwordEffect = false;
			}
		}
	}
	// 번개스킬
	if (m_strNowClip == "skill3" && m_fNowTime > 0.4f) {
		m_fThunderSkillTime += fTimeElapsed;
		int i = 0;
		for (auto& obj : m_vThunderEffectObject) {
			XMFLOAT3 pos = m_xmf3Position;
			if (i % 2)
				pos.x += i * 50;
			else {
				pos.x -= i * 50;
			}
			pos = Vector3::Add(pos, Vector3::Multiply(300.f, m_xmf3Look));
			pos.y += 400.f;
			if (obj->GetStartTime() < m_fThunderSkillTime) {
				obj->SetEffectStart(pos, true);
				
			}
			++i;
		}
	}
	else {
		m_fThunderSkillTime = 0;
	}

	// 방패스킬
	if (m_strNowClip == "skill2" && m_fNowTime > 0.15f) {
		m_fShieldSkillTime += fTimeElapsed;
		float i = 0.f;
		for (auto& obj : m_vShieldEffectObject) {
			XMFLOAT3 pos = m_xmf3Position;
			pos.x += 300.f * cos(i / m_vShieldEffectObject.size() * 3.14f * 2.f);
			pos.z += 300.f * sin(i / m_vShieldEffectObject.size() * 3.14f * 2.f);
			pos.y += 200.f;
			if (obj->GetStartTime() < m_fShieldSkillTime) {
				obj->SetEffectStart(pos, true);
			}
			++i;
		}
	}
	else {
		m_fShieldSkillTime = 0;
	}

	// 평타 사운드
	if (m_cObjectType) {
		if (m_strNowClip == "attack1" && m_fNowTime > 0.3f && m_fNowTime < 0.6f) {
			if (!m_IsAttack) {
				if(m_pSound)
					m_pSound->play(EffectSound::SLASH, 10);
				m_IsAttack = true;
			}
		}
		else if (m_strNowClip == "attack1" && m_fNowTime > 0.6f) {
			if (m_IsAttack) {
				if (m_pSound)
					m_pSound->play(EffectSound::SLASH, 10);
				m_IsAttack = false;
			}
		}
		else {
			m_IsAttack = false;
		}
	}
	else {
		if (m_strNowClip == "attack1" && m_fNowTime > 0.2f && m_fNowTime < 0.4f) {
			if (!m_IsAttack) {
				if (m_pSound)m_pSound->play(EffectSound::SLASH, 10);
				m_IsAttack = true;
			}
		}
		else if (m_strNowClip == "attack1" && m_fNowTime > 0.4f && m_fNowTime < 0.6f) {
			if (m_IsAttack) {
				if (m_pSound)m_pSound->play(EffectSound::SLASH, 10);
				m_IsAttack = false;
			}
		}
		else if (m_strNowClip == "attack1" && m_fNowTime > 0.6f) {
			if (!m_IsAttack) {
				if (m_pSound)m_pSound->play(EffectSound::SLASH, 10);
				m_IsAttack = true;
			}
		}
		else {
			m_IsAttack = false;
		}
	}

	// 맞는 사운드
	if (m_strNowClip == "stun") {
		if (!m_IsDamage) {
			if(GetObjectType() == e_ObjectType::e_Player_Man)
				if (m_pSound)m_pSound->play(EffectSound::MAN_DAMAGE, 11);
			else
				if (m_pSound)m_pSound->play(EffectSound::WOMAN_DAMAGE, 11);
			m_IsDamage = true;
		}
	}
	else {
		m_IsDamage = false;
	}

	for (auto& obj : m_vThunderEffectObject) {
		obj->Animate(fTimeElapsed);
	}

	for (auto& obj : m_vShieldEffectObject) {
		obj->Animate(fTimeElapsed);
	}
}

void CPlayer::UpdateCB(FrameResource* pFrameResource)
{
	m_pCurrResource = pFrameResource;
	m_xmf4x4World._11 = m_xmf3Right.x; m_xmf4x4World._12 = m_xmf3Right.y; m_xmf4x4World._13 = m_xmf3Right.z;
	m_xmf4x4World._21 = m_xmf3Up.x; m_xmf4x4World._22 = m_xmf3Up.y; m_xmf4x4World._23 = m_xmf3Up.z;
	m_xmf4x4World._31 = m_xmf3Look.x; m_xmf4x4World._32 = m_xmf3Look.y; m_xmf4x4World._33 = m_xmf3Look.z;
	m_xmf4x4World._41 = m_xmf3Position.x; m_xmf4x4World._42 = m_xmf3Position.y; m_xmf4x4World._43 = m_xmf3Position.z;

	XMMATRIX mtxScale = XMMatrixScaling(100.f, 100.f, 100.f);
	m_xmf4x4World = Matrix4x4::Multiply(mtxScale, m_xmf4x4World);

	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(0.f, XMConvertToRadians(180.0f), 0.0f);
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);

	if (m_nNumFrameDirty > 0) {
		auto currObjectCB = m_pCurrResource->ObjectCB.get();
		ObjectConstants objConstants;

		XMStoreFloat4x4(&objConstants.m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4World)));
		objConstants.m_xmf4DiffuseAlbedo = m_ppMaterials[0]->GetDiffuseAlbedo();
		objConstants.m_xmf3FresnelR0 = m_ppMaterials[0]->GetFresneIRO();
		objConstants.Shininess = 1.f - m_ppMaterials[0]->GetRoughness();
		// 어떤 텍스처를 사용할지 알려주는 텍스처인덱스변수도 넘겨주고,.
		objConstants.m_xmi2TexIndex = m_xmi2TexIndex;
		objConstants.m_fFrameTime = m_fFrameTime;
		currObjectCB->CopyData(m_ObjectCBIndex, objConstants);
		// 다음 프레임으로 넘어간다.
		m_nNumFrameDirty--;
	}

	for (auto& obj : m_vCrackEffectObject) {
		obj->UpdateCB(pFrameResource);
	}

	if (m_isSwordEffect) {
		for (auto& obj : m_vSword) {
			obj->UpdateCB(pFrameResource);
			obj->SetFrameDirty(::gNumFrameResources);
		}
	}

	for (auto& obj : m_vThunderEffectObject) {
		obj->UpdateCB(pFrameResource);
	}

	for (auto& obj : m_vShieldEffectObject) {
		obj->UpdateCB(pFrameResource);
	}

	/*if (m_pCollisionBox) {
		m_pCollisionBox->SetWorld(m_xmf4x4World);
		m_pCollisionBox->SetPosition(m_xmOOBB.Center);
		m_pCollisionBox->UpdateCB(pFrameResource);
		m_pCollisionBox->SetFrameDirty(::gNumFrameResources);
	}*/
}

void CPlayer::Update(float fTimeElapsed)
{
	m_xmf3Velocity = Vector3::Add(m_xmf3Velocity, Vector3::ScalarProduct(m_xmf3Gravity, fTimeElapsed, false));
	float fLength = sqrtf(m_xmf3Velocity.x * m_xmf3Velocity.x + m_xmf3Velocity.z * m_xmf3Velocity.z);
	float fMaxVelocityXZ = m_fMaxVelocityXZ * fTimeElapsed;
	if (fLength > m_fMaxVelocityXZ)
	{
		m_xmf3Velocity.x *= (fMaxVelocityXZ / fLength);
		m_xmf3Velocity.z *= (fMaxVelocityXZ / fLength);
	}
	float fMaxVelocityY = m_fMaxVelocityY * fTimeElapsed;
	fLength = sqrtf(m_xmf3Velocity.y * m_xmf3Velocity.y);
	if (fLength > m_fMaxVelocityY) m_xmf3Velocity.y *= (fMaxVelocityY / fLength);

	Move(m_xmf3Velocity, false);

	if (m_pPlayerUpdatedContext) OnPlayerUpdateCallback(fTimeElapsed);
	else m_xmf3Position.y = 516.f;

	DWORD nCurrentCameraMode = m_pCamera->GetMode();
	if (nCurrentCameraMode == THIRD_PERSON_CAMERA) {
		m_pCamera->Update(m_xmf3Position, fTimeElapsed);

	}
	if (m_pCameraUpdatedContext) OnCameraUpdateCallback(fTimeElapsed);

	if (nCurrentCameraMode == THIRD_PERSON_CAMERA) m_pCamera->SetLookAt(m_xmf3Position);
	m_pCamera->RegenerateViewMatrix();

	fLength = Vector3::Length(m_xmf3Velocity);
	float fDeceleration = (m_fFriction * fTimeElapsed);
	if (fDeceleration > fLength) fDeceleration = fLength;
	m_xmf3Velocity = Vector3::Add(m_xmf3Velocity, Vector3::ScalarProduct(m_xmf3Velocity, -fDeceleration, true));
}

CCamera* CPlayer::OnChangeCamera(DWORD nNewCameraMode, DWORD nCurrentCameraMode)
{
	CCamera *pNewCamera = NULL;
	switch (nNewCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		pNewCamera = new CFirstPersonCamera(m_pCamera);
		break;
	case THIRD_PERSON_CAMERA:
		pNewCamera = new CThirdPersonCamera(m_pCamera);
		break;
	case SPACESHIP_CAMERA:
		pNewCamera = new CSpaceShipCamera(m_pCamera);
		break;
	}
	if (nCurrentCameraMode == SPACESHIP_CAMERA)
	{
		m_xmf3Right = Vector3::Normalize(XMFLOAT3(m_xmf3Right.x, 0.0f, m_xmf3Right.z));
		m_xmf3Up = Vector3::Normalize(XMFLOAT3(0.0f, 1.0f, 0.0f));
		m_xmf3Look = Vector3::Normalize(XMFLOAT3(m_xmf3Look.x, 0.0f, m_xmf3Look.z));

		m_fPitch = 0.0f;
		m_fRoll = 0.0f;
		m_fYaw = Vector3::Angle(XMFLOAT3(0.0f, 0.0f, 1.0f), m_xmf3Look);
		if (m_xmf3Look.x < 0.0f) m_fYaw = -m_fYaw;
	}
	else if ((nNewCameraMode == SPACESHIP_CAMERA) && m_pCamera)
	{
		m_xmf3Right = m_pCamera->GetRightVector();
		m_xmf3Up = m_pCamera->GetUpVector();
		m_xmf3Look = m_pCamera->GetLookVector();
	}

	if (pNewCamera)
	{
		pNewCamera->SetMode(nNewCameraMode);
		pNewCamera->SetPlayer(this);
	}

	if (m_pCamera) delete m_pCamera;

	return(pNewCamera);
}

void CPlayer::OnPrepareRender()
{
}

void CPlayer::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	DWORD nCameraMode = (pCamera) ? pCamera->GetMode() : 0x00;
	if (nCameraMode == THIRD_PERSON_CAMERA) {

		CPlayer::m_pSkinnedObjectShader->Render(pd3dCommandList, pCamera, is_Shadow);
		CSkinAndDivisionObject::Render(pd3dCommandList, pCamera, is_Shadow);
		
		if (m_isSwordEffect) {
			for (auto& obj : m_vSword) {
				CPlayer::m_pSkinnedObjectShader->Render(pd3dCommandList, pCamera, is_Shadow);
				obj->Render(pd3dCommandList, pCamera, is_Shadow);
			}
		}

		for (auto& obj : m_vCrackEffectObject) {
			obj->Render(pd3dCommandList, pCamera, false);
		}
		for (auto& obj : m_vThunderEffectObject) {
			obj->Render(pd3dCommandList, pCamera, false);
		}

		for (auto& obj : m_vShieldEffectObject) {
			obj->Render(pd3dCommandList, pCamera, false);
		}
	}
}

void CPlayer::SetCharacterInfos(CMesh* pParentMesh, vector<CSkinnedModelMesh*> vChildsMeshes, vector<string>& vMeshNames, unordered_map<string, DivisionAnimationClip>& clip, XMFLOAT3 xmf3LocalScale, bool is_Man)
{
	SetMesh(0, pParentMesh);
	SetChilds(m_pd3dDevice, vChildsMeshes, vMeshNames, xmf3LocalScale);
	SetAnimClip(clip);
	SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));

	//CreateCollisionBox(m_pd3dDevice, m_pd3dCommandList, m_pd3dGraphicsRootSignature);

	if (is_Man) {
		if (m_vSword.size() == 0)
			SetSword(m_pd3dDevice, m_pd3dCommandList, m_pd3dGraphicsRootSignature, XMFLOAT2(250.f, 300.f));
		if (m_vCrackEffectObject.size() == 0) {
			m_vCrackEffectObject.resize(2);
			m_vCrackEffectObject[0] = new CCrackObject(m_pd3dDevice, m_pd3dCommandList, m_pd3dGraphicsRootSignature, XMFLOAT2(800.f, 1000.f), m_ManEffectSkillObjectCBIndex[0]);
			m_vCrackEffectObject[1] = new CEffectObject(m_pd3dDevice, m_pd3dCommandList, m_pd3dGraphicsRootSignature, XMFLOAT2(800.f, 1000.f), m_ManEffectSkillObjectCBIndex[1]);
		}
	}
	else {
		SetTextureIndex(XMUINT2(74, 75));
		SetSwordGirlChildsTexIndex();

		m_vThunderEffectObject.resize(5);
		for (int i = 0; i < m_vThunderEffectObject.size(); ++i) {
			m_vThunderEffectObject[i] = new CThunderObject(m_pd3dDevice, m_pd3dCommandList, m_pd3dGraphicsRootSignature, XMFLOAT2(500.f, 1000.f), 114 + i ,m_GirlThunderSkillObjectCBIndex[i]);
			m_vThunderEffectObject[i]->SetStartTime(i * 0.25f);
		}
		m_vShieldEffectObject.resize(16);
		for (int i = 0; i < m_vShieldEffectObject.size(); ++i) {
			m_vShieldEffectObject[i] = new CShieldObject(m_pd3dDevice, m_pd3dCommandList, m_pd3dGraphicsRootSignature, XMFLOAT2(300.f, 300.f), 119, m_GirlShieldSkillObjectCBIndex[i]);
			m_vShieldEffectObject[i]->SetStartTime(i * 0.07f);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTerrainPlayer
//
CTerrainPlayer::CTerrainPlayer(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, void* pContext) : CPlayer(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, pContext)
{
	m_pCamera = ChangeCamera(THIRD_PERSON_CAMERA, 0.0f);

	SetTextureIndex(XMUINT2(2, 3));
	CMaterial* pMaterial = new CMaterial();

	SetMaterial(0, pMaterial);

	CHeightMapTerrain* pTerrain = (CHeightMapTerrain*)pContext;
	SetPosition(XMFLOAT3(pTerrain->GetWidth()*0.5f, 0.f, pTerrain->GetLength()*0.5f));
	SetPlayerUpdatedContext(pTerrain);
	//SetCameraUpdatedContext(pTerrain);
	// 상수버퍼 인덱스 저장.
	m_ObjectCBIndex = ::gnObjCBindex++;
	m_ChildObjectCBIndex[0] = ::gnObjCBindex++;
	m_ChildObjectCBIndex[1] = ::gnObjCBindex++;
	
	for (int i = 0; i < 60; ++i) {
		m_ManSwordSkillObjectCBIndex[i] = ::gnObjCBindex++;
		for (int j = 0; j < 2; ++j) {
			m_ManSwordSkillObjectChildObjectCBIndex[i][j] = ::gnObjCBindex++;
		}
	}
	for(int i = 0; i < 2; ++i) {
		m_ManEffectSkillObjectCBIndex[i] = ::gnObjCBindex++;
	}

	for (int i = 0; i < 5; ++i) {
		m_GirlThunderSkillObjectCBIndex[i] = ::gnObjCBindex++;
	}
	for (int i = 0; i < 16; ++i) {
		m_GirlShieldSkillObjectCBIndex[i] = ::gnObjCBindex++;
	}
	

	// 한번만 만들게.
	if (CPlayer::m_pSkinnedObjectShader == nullptr) {
		CPlayer::m_pSkinnedObjectShader = new CSkinnedObjectShader();
		CPlayer::m_pSkinnedObjectShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, true);
	}

	m_CollisionBoxCBIndex = ::gnObjCBindex++;
}

CTerrainPlayer::~CTerrainPlayer()
{
}

CCamera* CTerrainPlayer::ChangeCamera(DWORD nNewCameraMode, float fTimeElapsed)
{
	DWORD nCurrentCameraMode = (m_pCamera) ? m_pCamera->GetMode() : 0x00;
	if (nCurrentCameraMode == nNewCameraMode) return(m_pCamera);
	switch (nNewCameraMode)
	{
		case FIRST_PERSON_CAMERA:
			SetFriction(250.0f);
			SetGravity(XMFLOAT3(0.0f, -400.0f, 0.0f));
			SetMaxVelocityXZ(300.0f);
			SetMaxVelocityY(400.0f);
			m_pCamera = OnChangeCamera(FIRST_PERSON_CAMERA, nCurrentCameraMode);
			m_pCamera->SetTimeLag(0.0f);
			m_pCamera->SetOffset(XMFLOAT3(0.0f, 20.0f, 0.0f));
			m_pCamera->GenerateProjectionMatrix(1.01f, 10000.0f, ASPECT_RATIO, 60.0f);
			break;
		case SPACESHIP_CAMERA:
			SetFriction(125.0f);
			SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
			SetMaxVelocityXZ(300.0f);
			SetMaxVelocityY(400.0f);
			m_pCamera = OnChangeCamera(SPACESHIP_CAMERA, nCurrentCameraMode);
			m_pCamera->SetTimeLag(0.0f);
			m_pCamera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
			m_pCamera->GenerateProjectionMatrix(1.01f, 10000.f, ASPECT_RATIO, 60.0f);
			break;
		case THIRD_PERSON_CAMERA:
			SetFriction(250.0f);
			SetGravity(XMFLOAT3(0.0f, -250.0f, 0.0f));
			SetMaxVelocityXZ(300.0f);
			SetMaxVelocityY(400.0f);
			m_pCamera = OnChangeCamera(THIRD_PERSON_CAMERA, nCurrentCameraMode);
			m_pCamera->SetTimeLag(0.25f);
			m_pCamera->SetOffset(XMFLOAT3(0.0f, 1300.0f, -1000.0f));
			m_pCamera->SetLookVector(XMFLOAT3(m_pCamera->GetLookVector().x, m_pCamera->GetLookVector().y, 1.f));
			m_pCamera->RegenerateViewMatrix();
			m_pCamera->GenerateProjectionMatrix(1.01f, 5000.f, ASPECT_RATIO, 60.0f);
			break;
		default:
			break;
	}
	Update(fTimeElapsed);
	return(m_pCamera);
}

void CTerrainPlayer::OnPlayerUpdateCallback(float fTimeElapsed)
{
	CHeightMapTerrain* pTerrain = (CHeightMapTerrain*)m_pPlayerUpdatedContext;
	XMFLOAT3 xmf3Scale = pTerrain->GetScale();
	XMFLOAT3 xmf3PlayerPosition = GetPosition();
	int z = (int)(xmf3PlayerPosition.z / xmf3Scale.z);
	// 격자는 홀수번째가 리버스형태라서?
	bool bReverseQuad = ((z % 2) != 0);
	float fHeight = pTerrain->GetHeight(xmf3PlayerPosition.x, xmf3PlayerPosition.z, bReverseQuad) + 6.0f;
	if (xmf3PlayerPosition.y < fHeight)
	{
		XMFLOAT3 xmf3PlayerVelocity = GetVelocity();
		xmf3PlayerVelocity.y = 0.0f;
		SetVelocity(xmf3PlayerVelocity);
		xmf3PlayerPosition.y = fHeight;
		SetPosition(xmf3PlayerPosition);
	}
}

void CTerrainPlayer::OnCameraUpdateCallback(float fTimeElapsed)
{
	CHeightMapTerrain* pTerrain = (CHeightMapTerrain*)m_pCameraUpdatedContext;
	XMFLOAT3 xmf3Scale = pTerrain->GetScale();
	XMFLOAT3 xmf3CameraPosition = m_pCamera->GetPosition();
	int z = (int)(xmf3CameraPosition.z / xmf3Scale.z);
	bool bReverseQuad = ((z % 2) != 0);
	float fHeight = pTerrain->GetHeight(xmf3CameraPosition.x, xmf3CameraPosition.z, bReverseQuad) + 5.0f;
	if (xmf3CameraPosition.y <= fHeight)
	{
		xmf3CameraPosition.y = fHeight;
		m_pCamera->SetPosition(xmf3CameraPosition);
		if (m_pCamera->GetMode() == THIRD_PERSON_CAMERA)
		{
			CThirdPersonCamera* p3rdPersonCamera = (CThirdPersonCamera*)m_pCamera;

			p3rdPersonCamera->SetLookAt(GetPosition());
		}
	}
}
