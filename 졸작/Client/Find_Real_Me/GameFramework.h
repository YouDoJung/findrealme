#pragma once
#include "GameTimer.h"
#include "Scene.h"
#include "Player.h"
#include "Camera.h"
#include "FrameResource.h"
#include "ShadowMap.h"
#include "CSound.h"

class GameFramework : public CTemplateSingleton<GameFramework>
{
public:
	GameFramework();
	~GameFramework();

	bool OnCreate(HINSTANCE hInstance, HWND hMainWnd);
	void OnDestroy();

	void CreateDirect3DDevice();
	void CreateCommandQueueAndList();
	void CreateSwapChain();

	void CreateRtvAndDsvDescriptorHeaps();
	void CreateRenderTargetViews();
	void CreateDepthStencilView();

	void OnResizeBackBuffers();

	void ModelLoad();
	void BuildMonsters();
	void BuildNpcs();
	void BuildFrameResources();
	void BuildObjects();

	void Update();
	void ProcessInput();
	void AnimateObjects();

	void DrawSceneToShadowMap();
	void FrameAdvance();

	void FlushCommandQueue();
	void CalculateFrameStats();

	void PickMousePoint(int x, int y);
	bool CheckPoint(float inputX, float inputY, float targetMinX, float targetMaxX, float targetMinY, float targetMaxY);

	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

public:
	ID3D12Resource* GetCurrentBackBuffer() const;
	D3D12_CPU_DESCRIPTOR_HANDLE GetCurrentBackBufferView() const;
	D3D12_CPU_DESCRIPTOR_HANDLE GetDepthStencilView() const;

	float     AspectRatio() const;

private:
	HINSTANCE m_hAppInst = nullptr; // application instance handle
	HWND      m_hMainWnd = nullptr; // main window handle

	// Set true to use 4X MSAA (?.1.8).  The default is false.
	bool      m_is4xMsaaState = false;    // 4X MSAA enabled
	UINT      m_4xMsaaQuality = 0;      // quality level of 4X MSAA

	// Used to keep track of the �delta-time?and game time (?.4).
	CGameTimer m_Timer;

	ComPtr<IDXGIFactory4> m_pDxgiFactory = nullptr;
	ComPtr<IDXGISwapChain> m_pSwapChain = nullptr;
	ComPtr<ID3D12Device> m_pD3dDevice = nullptr;

	ComPtr<ID3D12Fence> m_pFence = nullptr;
	UINT64 m_CurrentFence = 0;

	ComPtr<ID3D12CommandQueue> m_pCommandQueue = nullptr;
	ComPtr<ID3D12CommandAllocator> m_pDirectCmdListAlloc = nullptr;
	ComPtr<ID3D12GraphicsCommandList> m_pCommandList;

	static const int m_SwapChainBufferCount = 2;
	int m_CurrBackBuffer = 0;
	ComPtr<ID3D12Resource> m_pSwapChainBuffer[m_SwapChainBufferCount] = {};
	ComPtr<ID3D12Resource> m_pDepthStencilBuffer = nullptr;

	ComPtr<ID3D12DescriptorHeap> m_pRtvHeap = nullptr;
	ComPtr<ID3D12DescriptorHeap> m_pDsvHeap = nullptr;

	D3D12_VIEWPORT m_ScreenViewport = {};
	D3D12_RECT m_ScissorRect = {};

	UINT m_RtvDescriptorSize = 0;
	UINT m_DsvDescriptorSize = 0;
	UINT m_CbvSrvUavDescriptorSize = 0;

	CInGameUIShader* m_pUIShader = nullptr;

	CMoneyUIShader* m_pMoneyUIShader = nullptr;
	// Derived class should set these in derived constructor to customize starting values.
	std::wstring m_MainWndCaption = L"FindRealMe";
	D3D_DRIVER_TYPE m_d3dDriverType = D3D_DRIVER_TYPE_HARDWARE;
	DXGI_FORMAT m_BackBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	DXGI_FORMAT m_DepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

private:
	int							m_WndClientWidth = 0;
	int							m_WndClientHeight = 0;
	POINT						m_OldCursorPos = {};

	CScene* m_pScene = nullptr;
	CPlayer* m_pPlayer = nullptr;
	CCamera* m_pCamera = nullptr;

	int m_nNormalDungeonObjects = 0;
	int m_nBossDungeonObjects = 0;
	CShader* m_pSkinnedObjectShader = nullptr;

	vector<unique_ptr<FrameResource>> mFrameResources;
	FrameResource* mCurrFrameResource = nullptr;
	int mCurrFrameResourceIndex = 0;

	UINT m_nTextures = 0;
	TextureManager* m_TextureMgr = nullptr;
	unordered_map<string, ModelData*> m_mModelData;

	unique_ptr<ShadowMap> mShadowMap;

	// NPC ( �ϴ� ��� npc���� �������� ������ ����. ) 5������..
	int m_nNpcObjects = 0;
	CGameObject** m_ppNpcObjects = nullptr;

	// sound
	CSound* m_pSound = nullptr;
	int m_eMaplist = 0;

	//////
	int m_nPlayers = 0;
	CMiniMapShader* m_pMinimapRectShader = nullptr;
	CShader* m_pMinimapObjectShader = nullptr;
	CGameObject** m_ppMinimapPlayers = nullptr;
	CGameObject** m_ppMinimapMonsters = nullptr;
	CGameObject* m_pMinimapBoss = nullptr;

	CShader* m_pTmp = nullptr;

	CUIObject* m_pLoginScreenObj = nullptr;
	CUIShader* m_pLoginUIShader = nullptr;

	CNumberUIShader* m_pNumberUIShader = nullptr;
private:
	bool m_bIsRealeseDirect2D = false;
	bool m_bSetPlayer = false;
	std::shared_ptr<std::thread>m_tNetworkThread = nullptr;
	//std::thread m_tNetworkThread;
public:
	//static s_CS_PLAYER_DIR_PACKET m_sPlayerDir;
	static concurrency::concurrent_unordered_set<unsigned short>m_ccusLoginList;
	static concurrency::concurrent_unordered_map<unsigned short, CPlayer*>m_ccumPlayer;
	static std::recursive_mutex m_mMutex;
	static concurrency::concurrent_unordered_map<unsigned short, CGameObject*>m_ccumMonster;
	static CBossDragonObject* m_pBoss;
public:
	void InitNetWork();
	void RunNetWork();
	bool GetStartLoop();
	void PlayerTerrainSet();
	void SetPlayer();
	void CheckMove(DWORD dwDir);
	void SetCharacterType(char cType, unsigned short usID);
	void StopBGM();
	void SetNewBGM();
	void FirstSet();

	//bool CheckPoint(float InputX, float inputY, float targetMinX, float targetMaxX, float targetMinY, float targetMaxY);

private:
	//////////////////////////////////
	ID3D11On12Device			*m_pd3d11On12Device = NULL;
	ID3D11DeviceContext			*m_pd3d11DeviceContext = NULL;
	ID2D1Factory3				*m_pd2dFactory = NULL;
	IDWriteFactory				*m_pdWriteFactory = NULL;
	ID2D1Device2				*m_pd2dDevice = NULL;
	ID2D1DeviceContext2			*m_pd2dDeviceContext = NULL;

	ID3D11Resource				*m_ppd3d11WrappedBackBuffers[m_SwapChainBufferCount];
	ID2D1Bitmap1				*m_ppd2dRenderTargets[m_SwapChainBufferCount];

	ID2D1SolidColorBrush		*m_pd2dbrBackground = NULL;
	ID2D1SolidColorBrush		*m_pd2dbrBorder = NULL;
	IDWriteTextFormat			*m_pdwFont = NULL;
	IDWriteTextFormat			*m_pdwFailureFont = NULL;
	IDWriteTextLayout			*m_pdwTextLayout = NULL;
	ID2D1SolidColorBrush		*m_pd2dbrText = NULL;

	wchar_t m_IdInput[MAX_STR_LEN] = { '\0' };
	wchar_t m_PasswordInput[MAX_STR_LEN] = { '\0' };

	wchar_t m_InfoWords[100] = L"ID �Ǵ� ��й�ȣ�� �߸��ưų� �Էµ��� �ʾҽ��ϴ�.";
	int m_IdCnt = 0;
	int m_PasswordCnt = 0;
	XMUINT2 m_IdPickPoint = { 0, 0 };
	XMUINT2 m_PasswordPickPoint = { 0, 0 };
	bool m_IsGameStart = false;
	bool m_isIdChatOk = false;
	bool m_isPasswordChatOk = false;

	bool m_isLoginFailure = false;

	D3D12_VIEWPORT m_d3dViewport = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT, 0.0f, 1.0f };
	D3D12_RECT m_d3dScissorRect = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT };
	XMFLOAT4X4 m_xmf4x4Projection = Matrix4x4::PerspectiveFovLH(XMConvertToRadians(60.f), ASPECT_RATIO, 1.01f, 10000.f);
public:
	void CreateDirect2D();
	void RealeseDirect2D();
};

