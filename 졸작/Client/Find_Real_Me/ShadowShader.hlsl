#include "ModelShader.hlsl"

struct VS_SHADOW_OUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TERRAIN_INPUT
{
	float3 position : POSITION;
	float4 color : COLOR;
	float2 uv0 : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
	float3 normal : NORMAL;
};

struct VS_TERRAIN_SHADOW_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv0 : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
};

VS_SHADOW_OUT VSSkiningShadow(VS_SKINING_INPUT input) {

	VS_SHADOW_OUT output = (VS_SHADOW_OUT)0.f;

	float3 posL = float3(0.0f, 0.0f, 0.0f);

	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = input.weight.x;
	weights[1] = input.weight.y;
	weights[2] = input.weight.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	if (input.index[0] == 0 && input.index[1] == 0 && input.weight.x == 0 && input.weight.y == 0)
		posL = input.position;
	else {
		for (int i = 0; i < 4; ++i) {
			posL += weights[i] * mul(float4(input.position, 1.0f),
				gmtxBoneTransforms[input.index[i]]).xyz;

		}
	}
	float3 posW = mul(float4(posL, 1.0f), gmtxObjectWorld).xyz;
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;

	return output;
}

VS_SHADOW_OUT VSModelShadow(VS_MODEL_INPUT input) {

	VS_SHADOW_OUT output = (VS_SHADOW_OUT)0.f;

	float3 posW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;

	return output;
}

VS_TERRAIN_SHADOW_OUTPUT VSTerrainShadow(VS_TERRAIN_INPUT input)
{
	VS_TERRAIN_SHADOW_OUTPUT output;
	float3 posW = mul(input.position, (float3x3)gmtxObjectWorld);
	output.position = mul(mul(float4(posW, 1.0f), gmtxView), gmtxProjection);
	output.uv0 = input.uv0;
	output.uv1 = input.uv1;
	return(output);
}

///////////////////////////////////////////////////////////////////////
