#include "Shaders.hlsl"
struct VS_INPUT
{
	float3 position : POSITION;
	float4 color : COLOR;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;

	float3 posW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);
	output.color = input.color;
	return(output);
}

float4 PS(VS_OUTPUT input) : SV_TARGET
{
	
	float4 cColor = input.color;

	return(cColor);
}