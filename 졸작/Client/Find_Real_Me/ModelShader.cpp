#include "stdafx.h"
#include "ModelShader.h"
#include "Network.h"
#include "GameFramework.h"

CFBXModelShader::CFBXModelShader()
{

}

CFBXModelShader::~CFBXModelShader()
{

}

D3D12_INPUT_LAYOUT_DESC CFBXModelShader::CreateInputLayout()
{
	UINT nInputElementDescs = 4;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_BLEND_DESC CFBXModelShader::CreateBlendState()
{
	D3D12_BLEND_DESC d3dBlendDesc;
	::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
	d3dBlendDesc.AlphaToCoverageEnable = true;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	return(d3dBlendDesc);
}

D3D12_SHADER_BYTECODE CFBXModelShader::CreateVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"ModelShader.hlsl", "VSModel", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CFBXModelShader::CreatePixelShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"ModelShader.hlsl", "PSModel", "ps_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CFBXModelShader::CreateShadowVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"ShadowShader.hlsl", "VSModelShadow", "vs_5_1", ppd3dShaderBlob));
}

void CFBXModelShader::CreateShader(ID3D12Device* pd3dDevice, ID3D12RootSignature* pd3dGraphicsRootSignature, bool is_Shadow)
{
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature, true);
}


/////////////////////////
CFBXVillageShader::CFBXVillageShader()
{
}

CFBXVillageShader::~CFBXVillageShader()
{
}

void CFBXVillageShader::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void *pContext)
{
	ID3D12RootSignature* pRootSignature = (ID3D12RootSignature*)pContext;

	ifstream in("../MapData/MapData-Village.txt");
	if (!in.is_open()) {
		cout << "파일 못읽음" << endl;
		return;
	}
	string Token;
	int TotalTextures = 0;
	in >> Token >> m_nObjects;		// <TotalObjects>
	in >> Token >> TotalTextures;	// <TotalTextures>
	in >> Token;					// <TexturePath>
	for (int i = 0; i < TotalTextures; ++i) {
		in >> Token;
		wstring Token_w;
		Token_w.assign(Token.begin(), Token.end());

	}

	CMaterial* pMaterial = new CMaterial();

	m_ppObjects = new CGameObject*[m_nObjects];
	string ModelList[] = {
		"../Model/Map/Portal/Portal.fbx",
		"../Model/Map/Bridge/Bridge.fbx",
		"../Model/Map/Fence/Fence.fbx",
		"../Model/Map/Building/Building_a.fbx",
		"../Model/Map/Building/Building_b.fbx",
		"../Model/Map/Building/Building_c.fbx",
		"../Model/Map/Mushroom/Mushroom_a.fbx",
		"../Model/Map/Mushroom/Mushroom_c.fbx",
		"../Model/Map/Tent/Tent.fbx",
		"../Model/Map/Logs/Logs_a.fbx",
		"../Model/Map/Barrel/Barrel.fbx",
		"../Model/Map/Cart/Cart_a.fbx",
		"../Model/Map/Cart/Cart_b.fbx",
		"../Model/Map/Rock/Rock_a.fbx",
		"../Model/Map/Rock/Rock_f.fbx",
		"../Model/Map/Tree/Pine_tree.fbx",
		"../Model/Map/Tree/pine2b.fbx"
	};

	string ChangemModelList[] = {
		"../Model/Village/Portal/Portal.fbx",
		"../Model/Village/Bridge/Bridge.fbx",
		"../Model/Village/Fence/Fence.fbx",
		"../Model/Village/Building/Building_a.fbx",
		"../Model/Village/Building/Building_b.fbx",
		"../Model/Village/Building/Building_c.fbx",
		"../Model/Village/Mushroom/Mushroom_a.fbx",
		"../Model/Village/Mushroom/Mushroom_c.fbx",
		"../Model/Village/Tent/Tent.fbx",
		"../Model/Village/Logs/Logs_a.fbx",
		"../Model/Village/Barrel/Barrel.fbx",
		"../Model/Village/Cart/Cart_a.fbx",
		"../Model/Village/Cart/Cart_b.fbx",
		"../Model/Village/Rock/Rock_a.fbx",
		"../Model/Village/Rock/Rock_f.fbx",
		"../Model/Village/Tree/Pine_tree.fbx",
		"../Model/Village/Tree/Pine_tree2.fbx"
	};

	const aiScene* pFile[17];
	CModelMesh* pModelMesh[17];
	for (int i = 0; i < 17; ++i) {
		pFile[i] = ReadModelFile(ChangemModelList[i]);
		pModelMesh[i] = new CModelMesh(pd3dDevice, pd3dCommandList, pFile[i]->mMeshes[0]);
	}

	XMFLOAT3 pos; XMFLOAT3 rot;
	XMUINT2 tex; XMFLOAT3 center; XMFLOAT3 extent;
	in >> Token;					// <ModelDataStart>
	for (int i = 0; i < m_nObjects; ++i) {
		in >> Token;				// ModelPath
		CStaticModelObject* pStaticModelObject = new CStaticModelObject();
		for (int i = 0; i < 17; ++i) {
			if (ModelList[i] == Token) {
				pStaticModelObject->SetMesh(0, pModelMesh[i]);
				break;
			}
		}
		in >> Token;				// <Name>
		in >> Token;
		pStaticModelObject->m_strObjectName = Token;
		in >> Token;				// <Position>
		in >> pos.x >> pos.y >> pos.z;
		pStaticModelObject->SetPosition(pos);
		in >> Token;				// <Rotation>
		in >> rot.x >> rot.y >> rot.z;
		pStaticModelObject->Rotate(rot.x, rot.y, rot.z);
		in >> Token;				// <TextureIndex>
		in >> tex.x >> tex.y;
		tex.x += 6;
		tex.y += 6;
		pStaticModelObject->SetTextureIndex(tex);
		in >> Token;				// <Pivot>
		in >> center.x >> center.y >> center.z;
		in >> Token;				// <Extent>
		in >> extent.x >> extent.y >> extent.z;
		pStaticModelObject->SetOOBB(center, extent, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pStaticModelObject->SetMaterial(0, pMaterial);
		pStaticModelObject->SetObjectCBIndex(::gnObjCBindex++);

		//pStaticModelObject->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[i] = pStaticModelObject;
	}

	
	/*int sample = 0;
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]->m_strObjectName != "Mushroom" && m_ppObjects[i]->m_strObjectName != "Portal")
			++sample;
		else
			continue;
	}
	std::ofstream out("VillageObject.txt");
	out << sample << '\t';
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]->m_strObjectName != "Mushroom" && m_ppObjects[i]->m_strObjectName != "Portal")
			out << m_ppObjects[i]->m_strObjectName << '\t' << m_ppObjects[i]->m_xmOOBB.Center.x << '\t' << m_ppObjects[i]->m_xmOOBB.Center.y << '\t' << m_ppObjects[i]->m_xmOOBB.Center.z << '\t' << m_ppObjects[i]->m_xmOOBB.Extents.x << '\t' << m_ppObjects[i]->m_xmOOBB.Extents.y << '\t' << m_ppObjects[i]->m_xmOOBB.Extents.z << '\t'<< m_ppObjects[i]->GetPosition().x << '\t' << m_ppObjects[i]->GetPosition().y << '\t' << m_ppObjects[i]->GetPosition().z << '\t'<< m_ppObjects[i]->m_xmf3Rotkey.x << '\t' << m_ppObjects[i]->m_xmf3Rotkey.y << '\t' << m_ppObjects[i]->m_xmf3Rotkey.z << '\t';
		else
			continue;
	}*/
	
}

void CFBXVillageShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++) if (m_ppObjects[j]) delete m_ppObjects[j];
		delete[] m_ppObjects;
	}
}

void CFBXVillageShader::AnimateObjects(float fTimeElapsed)
{
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			m_ppObjects[i]->Animate(fTimeElapsed);
			if (m_ppObjects[i]->m_xmOOBB.Intersects(m_pPlayer->m_xmOOBB)) {
				XMFLOAT3 pos = m_pPlayer->GetPosition();
				if (m_ppObjects[i]->m_strObjectName == "Bridge0") {
					if (m_ppObjects[i]->HeightCollisionCheck(pos, true)) {
						m_pPlayer->SetPosition(pos);

						//////////////////////////
						//TODO: FOR NETWORK
						//////////////////////////
						CNetwork::GetInstance()->SendPosPacket();
					}
					else {
						m_pPlayer->Move(m_pPlayer->GetDirection(), -PLAYER_DISTANCE * 1.f * fTimeElapsed, false);
						m_pPlayer->SetPosition(XMFLOAT3(m_pPlayer->GetPosition().x, pos.y, m_pPlayer->GetPosition().z));

						//////////////////////////
						//TODO: FOR NETWORK
						//////////////////////////
						CNetwork::GetInstance()->SendPosPacket();
					}
				}
				else if (m_ppObjects[i]->m_strObjectName == "Bridge1") {
					if (m_ppObjects[i]->HeightCollisionCheck(pos, false)) {
						m_pPlayer->SetPosition(pos);

						//////////////////////////
						//TODO: FOR NETWORK
						//////////////////////////
						CNetwork::GetInstance()->SendPosPacket();
					}
					else {
						m_pPlayer->Move(m_pPlayer->GetDirection(), -PLAYER_DISTANCE * 1.f * fTimeElapsed, false);
						m_pPlayer->SetPosition(XMFLOAT3(m_pPlayer->GetPosition().x, pos.y, m_pPlayer->GetPosition().z));

						//////////////////////////
						//TODO: FOR NETWORK
						//////////////////////////
						CNetwork::GetInstance()->SendPosPacket();
					}
				}
				else if (m_ppObjects[i]->m_strObjectName != "Mushroom" && m_ppObjects[i]->m_strObjectName != "Portal") {
					m_pPlayer->Move(m_pPlayer->GetDirection(), -PLAYER_DISTANCE * fTimeElapsed, false);

					//////////////////////////
					//TODO: FOR NETWORK
					//////////////////////////
					CNetwork::GetInstance()->SendPosPacket();
				}
			}
		}
	}
}

void CFBXVillageShader::UpdateObjectsCB(FrameResource* pFrameResource, float fTime)
{
	for (int i = 0; i < m_nObjects; ++i) {
		m_ppObjects[i]->UpdateCB(pFrameResource);
	}
}

void CFBXVillageShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	//CShader::Render(pd3dCommandList, pCamera, is_Shadow);

	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j] && m_ppObjects[j]->m_xmOOBB.Intersects(pCamera->GetFrustum())) {
			CShader::Render(pd3dCommandList, pCamera, is_Shadow);
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);
		}
	}
}

const aiScene* CFBXVillageShader::ReadModelFile(const string& filename)
{
	const aiScene* m_pModel = aiImportFile(filename.c_str(), aiProcess_JoinIdenticalVertices |        // 동일한 꼭지점 결합, 인덱싱 최적화
		aiProcess_ValidateDataStructure |        // 로더의 출력을 검증
		aiProcess_ImproveCacheLocality |        // 출력 정점의 캐쉬위치를 개선
		aiProcess_RemoveRedundantMaterials |    // 중복된 매터리얼 제거
		aiProcess_GenUVCoords |                    // 구형, 원통형, 상자 및 평면 매핑을 적절한 UV로 변환
		aiProcess_TransformUVCoords |            // UV 변환 처리기 (스케일링, 변환...)
		aiProcess_FindInstances |                // 인스턴스된 매쉬를 검색하여 하나의 마스터에 대한 참조로 제거
		aiProcess_LimitBoneWeights |            // 정점당 뼈의 가중치를 최대 4개로 제한
		aiProcess_OptimizeMeshes |                // 가능한 경우 작은 매쉬를 조인
		aiProcess_GenSmoothNormals |            // 부드러운 노말벡터(법선벡터) 생성
		aiProcess_SplitLargeMeshes |            // 거대한 하나의 매쉬를 하위매쉬들로 분활(나눔)
		aiProcess_Triangulate |                    // 3개 이상의 모서리를 가진 다각형 면을 삼각형으로 만듬(나눔)
		aiProcess_ConvertToLeftHanded |            // D3D의 왼손좌표계로 변환
		aiProcess_PreTransformVertices |			//버텍스미리계산?
		aiProcess_SortByPType);

	return m_pModel;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

CFBXTopInsideShader::CFBXTopInsideShader()
{
}
CFBXTopInsideShader::~CFBXTopInsideShader()
{
}

void CFBXTopInsideShader::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void *pContext)
{
	ID3D12RootSignature* pRootSignature = (ID3D12RootSignature*)pContext;
	m_nObjects = 104;

	CMaterial* pMaterial = new CMaterial();

	m_ppObjects = new CGameObject*[m_nObjects];
	int index = 0;

	string Column = "../Model/TopInside/Column/Column.fbx";
	string Floor = "../Model/TopInside/Floor/Floor.fbx";
	string Wall = "../Model/TopInside/Wall/Wall.fbx";
	string Door = "../Model/TopInside/Door/Door.fbx";

	// 기둥 6
	const aiScene* pFile = ReadModelFile(Column);
	CModelMesh * pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);

	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 2; ++j) {
			CStaticModelObject* pColumn = new CStaticModelObject();
			pColumn->SetMesh(0, pModelMesh);
			pColumn->SetPosition(3950.f + j * 2100.f, 516.f, 3950.f + i * 2100.f);
			pColumn->m_strObjectName = "Column";
			pColumn->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pColumn->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pColumn->SetMaterial(0, pMaterial);
			pColumn->SetTextureIndex(XMUINT2(18, 19));
			pColumn->SetObjectCBIndex(::gnObjCBindex++);

			//pColumn->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pColumn;
		}
	}

	for (int i = 0; i < 2; ++i) {
		CStaticModelObject* pColumn = new CStaticModelObject();
		pColumn->SetMesh(0, pModelMesh);
		pColumn->SetPosition(4550.f + i * 900.f, 516.f, 6050.f);
		pColumn->m_strObjectName = "Column";
		pColumn->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pColumn->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pColumn->SetMaterial(0, pMaterial);
		pColumn->SetTextureIndex(XMUINT2(18, 19));
		pColumn->SetObjectCBIndex(::gnObjCBindex++);

		//pColumn->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pColumn;
	}

	// 바닥 61
	pFile = ReadModelFile(Floor);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);

	for (int i = 0; i < 7; ++i) {
		for (int j = 0; j < 7; ++j) {
			CStaticModelObject* pFloor = new CStaticModelObject();
			pFloor->SetMesh(0, pModelMesh);
			pFloor->SetPosition(4100.f + j * 300.f, 516.f, 4100.f + i * 300.f);
			pFloor->m_strObjectName = "Floor";
			pFloor->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pFloor->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pFloor->SetMaterial(0, pMaterial);
			pFloor->SetTextureIndex(XMUINT2(18, 19));
			pFloor->SetObjectCBIndex(::gnObjCBindex++);

			//pFloor->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pFloor;
		}
	}
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 3; ++j) {
			CStaticModelObject* pFloor = new CStaticModelObject();
			pFloor->SetMesh(0, pModelMesh);
			pFloor->SetPosition(4700.f + j * 300.f, 516.f, 6200.f + i * 300.f);
			pFloor->m_strObjectName = "Floor";
			pFloor->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pFloor->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pFloor->SetMaterial(0, pMaterial);
			pFloor->SetTextureIndex(XMUINT2(18, 19));
			pFloor->SetObjectCBIndex(::gnObjCBindex++);

			//pFloor->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pFloor;
		}
	}

	// 벽 = 14
	pFile = ReadModelFile(Wall);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 7; ++j) {
			CStaticModelObject* pWall = new CStaticModelObject();
			pWall->SetMesh(0, pModelMesh);
			if (j > 4 || j < 2) {
				pWall->SetPosition(4100.f + j * 300.f, 516.f, 3950.f + i * 2100.f);
			}
			else {
				pWall->SetPosition(4100.f + j * 300.f, 516.f, 3950.f + i * 3300.f);
			}
			pWall->m_strObjectName = "Wall";
			pWall->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pWall->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pWall->SetMaterial(0, pMaterial);
			pWall->SetTextureIndex(XMUINT2(18, 19));
			pWall->SetObjectCBIndex(::gnObjCBindex++);

			//pWall->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pWall;
		}
	}
	// 벽 || 14
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 7; ++j) {
			CStaticModelObject* pWall = new CStaticModelObject();
			pWall->SetMesh(0, pModelMesh);
			pWall->SetPosition(3950.f + i * 2100.f, 516.f, 4100.f + j * 300.f);
			pWall->Rotate(0.f, 90.f, 0.f);
			pWall->m_strObjectName = "Wall";
			pWall->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pWall->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pWall->SetMaterial(0, pMaterial);
			pWall->SetTextureIndex(XMUINT2(18, 19));
			pWall->SetObjectCBIndex(::gnObjCBindex++);

			//pWall->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pWall;
		}
	}
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 4; ++j) {
			CStaticModelObject* pWall = new CStaticModelObject();
			pWall->SetMesh(0, pModelMesh);
			pWall->SetPosition(4550.f + i * 900.f, 516.f, 6200.f + j * 300.f);
			pWall->Rotate(0.f, 90.f, 0.f);
			pWall->m_strObjectName = "Wall";
			pWall->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pWall->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pWall->SetMaterial(0, pMaterial);
			pWall->SetTextureIndex(XMUINT2(18, 19));
			pWall->SetObjectCBIndex(::gnObjCBindex++);

			//pWall->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pWall;
		}
	}

	// 문 1
	pFile = ReadModelFile(Door);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	CStaticModelObject* pDoor = new CStaticModelObject();
	pDoor->SetMesh(0, pModelMesh);
	pDoor->SetPosition(5000.f, 510.f, 7230.f);
	pDoor->Rotate(0.f, 0.f, 0.f);
	pDoor->m_strObjectName = "Door";
	pDoor->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pDoor->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	pDoor->SetMaterial(0, pMaterial);
	pDoor->SetTextureIndex(XMUINT2(20, 21));
	pDoor->SetObjectCBIndex(::gnObjCBindex++);

	//pDoor->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
	m_ppObjects[index++] = pDoor;

	int sample = 0;
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]->m_strObjectName == "Floor")
			++sample;
		else
			continue;
	}
	
}

bool CFBXTopInsideShader::CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox)
{
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			if (m_ppObjects[i]->m_xmOOBB.Intersects(oobbAfterBox)) {
				if (m_ppObjects[i]->m_strObjectName != "Floor") {
					return true;
				}
				else continue;
			}
			else continue;
		}
	}
	return false;
}
void CFBXTopInsideShader::AnimateObjects(float fTimeElapsed)
{
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			m_ppObjects[i]->Animate(fTimeElapsed);
		}
	}
}

void CFBXTopInsideShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	//CShader::Render(pd3dCommandList, pCamera, is_Shadow);

	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j] && m_ppObjects[j]->m_xmOOBB.Intersects(pCamera->GetFrustum())) {
			CShader::Render(pd3dCommandList, pCamera, is_Shadow);
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CFBXDungeon::CFBXDungeon()
{
}
CFBXDungeon::~CFBXDungeon()
{
}

void CFBXDungeon::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void *pContext)
{
	ID3D12RootSignature* pRootSignature = (ID3D12RootSignature*)pContext;
	m_nObjects = 702;

	CMaterial* pMaterial = new CMaterial();

	m_ppObjects = new CGameObject*[m_nObjects];
	int index = 0;

	string Castle = "../Model/Dungeon/Floor/Rect.fbx";
	string Test = "../Model/Dungeon/Wall/Wall.fbx";
	string Crate = "../Model/Dungeon/Cage/Cage.fbx";
	string Chain = "../Model/Dungeon/Chain/Chain.fbx";

	ifstream in("test/Cage.txt");
	XMFLOAT3 Pos;
	const aiScene* pFile = ReadModelFile(Crate);
	CModelMesh * pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	for (int i = 0; i < 7; ++i) {
		CStaticModelObject* pCage = new CStaticModelObject();
		pCage->SetMesh(0, pModelMesh);
		in >> Pos.x >> Pos.y >> Pos.z;
		pCage->SetPosition(Pos);
		pCage->m_strObjectName = "Cage";
		pCage->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pCage->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pCage->SetMaterial(0, pMaterial);
		pCage->SetTextureIndex(XMUINT2(24, 25));
		pCage->SetObjectCBIndex(::gnObjCBindex++);

		//pCage->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pCage;
	}
	in.close();

	in.open("test/Chain.txt");
	pFile = ReadModelFile(Chain);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	for (int i = 0; i < 125; ++i) {
		CStaticModelObject* pChain = new CStaticModelObject();
		pChain->SetMesh(0, pModelMesh);
		in >> Pos.x >> Pos.y >> Pos.z;
		pChain->SetPosition(Pos);
		pChain->m_strObjectName = "Chain";
		pChain->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pChain->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pChain->SetMaterial(0, pMaterial);
		pChain->SetTextureIndex(XMUINT2(24, 25));
		pChain->SetObjectCBIndex(::gnObjCBindex++);

		//pChain->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pChain;
	}
	in.close();

	in.open("test/Floor.txt");
	// 바닥 175
	pFile = ReadModelFile(Castle);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	for (int i = 0; i < 35; ++i) {
		for (int j = 0; j < 5; ++j) {
			CStaticModelObject* pFloor = new CStaticModelObject();
			pFloor->SetMesh(0, pModelMesh);
			in >> Pos.x >> Pos.y >> Pos.z;
			pFloor->SetPosition(Pos);
			pFloor->m_strObjectName = "Floor";
			pFloor->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pFloor->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			pFloor->SetMaterial(0, pMaterial);
			pFloor->SetTextureIndex(XMUINT2(22, 23));
			pFloor->SetObjectCBIndex(::gnObjCBindex++);

			//pFloor->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
			m_ppObjects[index++] = pFloor;
		}
	}
	in.close();

	in.open("test/Wall.txt");
	// 벽
	pFile = ReadModelFile(Test);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	for (int i = 0; i < 255; ++i) {
		CStaticModelObject* pWall = new CStaticModelObject();
		pWall->SetMesh(0, pModelMesh);
		in >> Pos.x >> Pos.y >> Pos.z;
		pWall->SetPosition(Pos);
		pWall->m_strObjectName = "Wall";
		pWall->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pWall->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pWall->SetMaterial(0, pMaterial);
		pWall->SetTextureIndex(XMUINT2(26, 27));
		pWall->SetObjectCBIndex(::gnObjCBindex++);

		//pWall->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pWall;
	}
	in.close();

	for (int i = 0; i < 70; ++i) {
		CStaticModelObject* pWall = new CStaticModelObject();
		pWall->SetMesh(0, pModelMesh);
		pWall->SetPosition(-420, 500, 1000 * i + 500);
		pWall->m_strObjectName = "Wall";
		pWall->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pWall->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pWall->SetMaterial(0, pMaterial);
		pWall->SetTextureIndex(XMUINT2(26, 27));
		pWall->SetObjectCBIndex(::gnObjCBindex++);

		//pWall->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pWall;
	}

	for (int i = 0; i < 70; ++i) {
		CStaticModelObject* pWall = new CStaticModelObject();
		pWall->SetMesh(0, pModelMesh);
		pWall->SetPosition(10500, 500, 1000 * i + 500);
		pWall->m_strObjectName = "Wall";
		pWall->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pWall->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pWall->SetMaterial(0, pMaterial);
		pWall->SetTextureIndex(XMUINT2(26, 27));
		pWall->SetObjectCBIndex(::gnObjCBindex++);

		//pWall->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pWall;
	}

	int sample = 0;
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]->m_strObjectName == "Chain"|| m_ppObjects[i]->m_strObjectName == "Floor")
			++sample;
		else
			continue;
	}
	
}

void CFBXDungeon::AnimateObjects(float fTimeElapsed) {
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			m_ppObjects[i]->Animate(fTimeElapsed);
		}
	}
}

bool CFBXDungeon::CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox)
{
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			if (m_ppObjects[i]->m_xmOOBB.Intersects(oobbAfterBox)) {
				if (m_ppObjects[i]->m_strObjectName != "Floor" && m_ppObjects[i]->m_strObjectName != "Chain") {
					return true;
				}
				else continue;
			}
			else continue;
		}
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////////////////////
CFBXBossDungeon::CFBXBossDungeon()
{
}
CFBXBossDungeon::~CFBXBossDungeon()
{
}
void CFBXBossDungeon::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void *pContext) {

	ID3D12RootSignature* pRootSignature = (ID3D12RootSignature*)pContext;
	m_nObjects = 138;

	CMaterial* pMaterial = new CMaterial();

	m_ppObjects = new CGameObject*[m_nObjects];
	int index = 0;

	string Rect = "../Model/BossDungeon/Floor/Rect.fbx";
	string FloorGate = "../Model/BossDungeon/Bars/Bars.fbx";
	string Test = "../Model/BossDungeon/Wall/Wall.fbx";

	ifstream in("test/Floor.txt");
	XMFLOAT3 Pos;

	/*const aiScene* pFile = ReadModelFile(Rect);
	CModelMesh* pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	*/// 바닥 75
	//for (int i = 0; i < 15; ++i) {
	//	for (int j = 0; j < 5; ++j) {
	//		CStaticModelObject* pFloor = new CStaticModelObject();
	//		pFloor->SetMesh(0, pModelMesh);
	//		in >> Pos.x >> Pos.y >> Pos.z;
	//		pFloor->SetPosition(Pos.x, Pos.y - 700.f, Pos.z);
	//		pFloor->m_strObjectName = "LavaFloor";
	//		pFloor->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pFloor->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	//		pFloor->SetMaterial(0, pMaterial);
	//		pFloor->SetTextureIndex(XMUINT2(28, 29));
	//		pFloor->SetObjectCBIndex(::gnObjCBindex++);
	//		m_ppObjects[index++] = pFloor;
	//	}
	//}
	in.close();

	in.open("test/FloorGate.txt");
	const aiScene* pFile = ReadModelFile(FloorGate);
	CModelMesh* pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	// 철창 28
	for (int i = 0; i < 28; ++i) {
		CStaticModelObject* pFloorGate = new CStaticModelObject();
		pFloorGate->SetMesh(0, pModelMesh);
		in >> Pos.x >> Pos.y >> Pos.z;
		pFloorGate->SetPosition(Pos.x + 500.f, Pos.y - 200.f, Pos.z);
		pFloorGate->m_strObjectName = "Bars";
		pFloorGate->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pFloorGate->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pFloorGate->SetMaterial(0, pMaterial);
		pFloorGate->SetTextureIndex(XMUINT2(30, 31));
		pFloorGate->SetObjectCBIndex(::gnObjCBindex++);

		//pFloorGate->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pFloorGate;
	}
	in.close();
	in.open("test/Road.txt");
	pFile = ReadModelFile(Test);
	pModelMesh = new CModelMesh(pd3dDevice, pd3dCommandList, pFile->mMeshes[0]);
	// 길 110
	for (int i = 0; i < 110; ++i) {
		CStaticModelObject* pBridge = new CStaticModelObject();
		pBridge->SetMesh(0, pModelMesh);
		in >> Pos.x >> Pos.y >> Pos.z;
		pBridge->SetPosition(Pos.x, Pos.y - 200.f, Pos.z);
		pBridge->m_strObjectName = "Road";
		pBridge->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pBridge->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pBridge->SetMaterial(0, pMaterial);
		pBridge->SetTextureIndex(XMUINT2(32, 33));
		pBridge->SetObjectCBIndex(::gnObjCBindex++);

		//pBridge->CreateCollisionBox(pd3dDevice, pd3dCommandList, pRootSignature);
		m_ppObjects[index++] = pBridge;
	}
}
void CFBXBossDungeon::AnimateObjects(float fTimeElapsed) {
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			m_ppObjects[i]->Animate(fTimeElapsed);
		}
	}
}

bool CFBXBossDungeon::CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox)
{
	oobbAfterBox.Extents.x = 0.05f;
	oobbAfterBox.Extents.z = 0.05f;
	for (int i = 0; i < m_nObjects; ++i) {
		if (m_ppObjects[i]) {
			if (m_ppObjects[i]->m_strObjectName == "Bars") {
				oobbAfterBox.Center.y = 500.0f;
				if (m_ppObjects[i]->m_xmOOBB.Intersects(oobbAfterBox)) {
					return false;
				}
			}
			else if (m_ppObjects[i]->m_strObjectName == "Road") {
				oobbAfterBox.Center.y = -200.0f;
				if (m_ppObjects[i]->m_xmOOBB.Intersects(oobbAfterBox)) {
					return false;
				}
			}
			
		}
	}
	return true;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
CSkinnedObjectShader::CSkinnedObjectShader()
{
}

CSkinnedObjectShader::~CSkinnedObjectShader()
{
}

D3D12_INPUT_LAYOUT_DESC CSkinnedObjectShader::CreateInputLayout()
{
	UINT nInputElementDescs = 6;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "BONEINDEX", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[5] = { "WEIGHT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 60, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE CSkinnedObjectShader::CreateVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"ModelShader.hlsl", "VSSkining", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CSkinnedObjectShader::CreatePixelShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"ModelShader.hlsl", "PSSkining", "ps_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CSkinnedObjectShader::CreateShadowVertexShader(ID3DBlob** ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"ShadowShader.hlsl", "VSSkiningShadow", "vs_5_1", ppd3dShaderBlob));
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//CNormalDungeonMonsterShader::CNormalDungeonMonsterShader()
//{
//}
//
//CNormalDungeonMonsterShader::~CNormalDungeonMonsterShader()
//{
//}
//
//D3D12_INPUT_LAYOUT_DESC CNormalDungeonMonsterShader::CreateInputLayout()
//{
//	UINT nInputElementDescs = 6;
//	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
//
//	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
//	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
//	pd3dInputElementDescs[2] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
//	pd3dInputElementDescs[3] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
//	pd3dInputElementDescs[4] = { "BONEINDEX", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
//	pd3dInputElementDescs[5] = { "WEIGHT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 60, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
//
//	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
//	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
//	d3dInputLayoutDesc.NumElements = nInputElementDescs;
//
//	return(d3dInputLayoutDesc);
//}
//
//D3D12_SHADER_BYTECODE CNormalDungeonMonsterShader::CreateVertexShader(ID3DBlob** ppd3dShaderBlob)
//{
//	return(CShader::CompileShaderFromFile(L"ModelShader.hlsl", "VSSkining", "vs_5_1", ppd3dShaderBlob));
//}
//
//D3D12_SHADER_BYTECODE CNormalDungeonMonsterShader::CreatePixelShader(ID3DBlob** ppd3dShaderBlob)
//{
//	return(CShader::CompileShaderFromFile(L"ModelShader.hlsl", "PSSkining", "ps_5_1", ppd3dShaderBlob));
//}
//
//D3D12_SHADER_BYTECODE CNormalDungeonMonsterShader::CreateShadowVertexShader(ID3DBlob** ppd3dShaderBlob)
//{
//	return(CShader::CompileShaderFromFile(L"ShadowShader.hlsl", "VSSkiningShadow", "vs_5_1", ppd3dShaderBlob));
//}
//
//void CNormalDungeonMonsterShader::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void *pContext)
//{
//	m_nObjects = 244;
//	m_ppObjects = new CGameObject*[m_nObjects];
//	int index = 0;
//	CMaterial* pMaterial = new CMaterial();
//
//	string strModelFilenames[] = {
//		"../Model/Monster/Peon/peon.fbx", "../Model/Monster/Grunt/Grunt.fbx", "../Model/Monster/Lord/Lord.fbx"
//	};
//	string strAnimFilenames[] = {
//		"../Model/Monster/Peon/peon@idle.fbx", "../Model/Monster/Grunt/Grunt@idle.fbx", "../Model/Monster/Lord/Lord@idle.fbx"
//	};
//	string strMonsternames[] = { "Peon", "Grunt", "Lord" };
//	for (int i = 0; i < 3; ++i)
//	{
//		ModelData* pData = new ModelData();
//		pData->ModelLoadAndMeshLoad(strModelFilenames[i], pd3dDevice, pd3dCommandList);
//		pData->AnimationLoad(strAnimFilenames[i], "walk", 0.8f, true);
//
//		m_mModelData.insert(pair<string, ModelData*>(strMonsternames[i], pData));
//	}
//
//	ifstream in("test/peon.txt");
//	XMFLOAT3 pos;
//
//	for (int i = 0; i < 96; ++i) {
//		CSkinAndDivisionObject* pPeon = new CSkinAndDivisionObject();
//		pPeon->SetTextureIndex(XMUINT2(38, 39));
//		pPeon->SetMesh(0, m_mModelData["Peon"]->m_pParentMesh);
//		pPeon->SetChilds(pd3dDevice, m_mModelData["Peon"]->m_vChildMesh, m_mModelData["Peon"]->m_vNames);
//		pPeon->SetAnimClip(m_mModelData["Peon"]->m_mDivisionAnimationClip);
//		in >> pos.x >> pos.y >> pos.z;
//		pPeon->SetPosition(pos);
//		pPeon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(4.f, pPeon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//		pPeon->SetMaterial(0, pMaterial);
//		pPeon->m_strObjectName = "Peon";
//		pPeon->SetObjectCBIndex(::gnObjCBindex++);
//		pPeon->SetSkinCBIndex(::gnSkinCBindex++);
//		m_ppObjects[index++] = pPeon;
//	}
//	in.close();
//
//	in.open("test/grunt.txt");
//	for (int i = 0; i < 85; ++i) {
//		CSkinAndDivisionObject* pGrunt = new CSkinAndDivisionObject();
//		pGrunt->SetTextureIndex(XMUINT2(34, 35));
//		pGrunt->SetMesh(0, m_mModelData["Grunt"]->m_pParentMesh);
//		pGrunt->SetChilds(pd3dDevice, m_mModelData["Grunt"]->m_vChildMesh, m_mModelData["Grunt"]->m_vNames);
//		pGrunt->SetAnimClip(m_mModelData["Grunt"]->m_mDivisionAnimationClip);
//		in >> pos.x >> pos.y >> pos.z;
//		pGrunt->SetPosition(pos);
//		pGrunt->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(5.f, pGrunt->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//		pGrunt->SetMaterial(0, pMaterial);
//		pGrunt->m_strObjectName = "Grunt";
//		pGrunt->SetObjectCBIndex(::gnObjCBindex++);
//		pGrunt->SetSkinCBIndex(::gnSkinCBindex++);
//		m_ppObjects[index++] = pGrunt;
//	}
//	in.close();
//
//	in.open("test/lord.txt");
//	for (int i = 0; i < 63; ++i) {
//		CSkinAndDivisionObject* pLord = new CSkinAndDivisionObject();
//		pLord->SetTextureIndex(XMUINT2(42, 43));
//		pLord->SetMesh(0, m_mModelData["Lord"]->m_pParentMesh);
//		pLord->SetChilds(pd3dDevice, m_mModelData["Lord"]->m_vChildMesh, m_mModelData["Lord"]->m_vNames);
//		pLord->SetAnimClip(m_mModelData["Lord"]->m_mDivisionAnimationClip);
//		in >> pos.x >> pos.y >> pos.z;
//		pLord->SetPosition(pos);
//		pLord->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(6.f, pLord->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//		pLord->SetMaterial(0, pMaterial);
//		pLord->m_strObjectName = "Lord";
//		pLord->SetObjectCBIndex(::gnObjCBindex++);
//		pLord->SetSkinCBIndex(::gnSkinCBindex++);
//		m_ppObjects[index++] = pLord;
//	}
//	in.close();
//}
//
//void CNormalDungeonMonsterShader::ReleaseObjects()
//{
//
//	if (m_ppObjects)
//	{
//		for (int j = 0; j < m_nObjects; j++) if (m_ppObjects[j]) delete m_ppObjects[j];
//		delete[] m_ppObjects;
//	}
//
//	for (auto& data : m_mModelData) {
//		if (data.second) {
//			delete data.second;
//			data.second = nullptr;
//		}
//	}
//	m_mModelData.clear();
//
//}
//
//void CNormalDungeonMonsterShader::AnimateObjects(float fTimeElapsed)
//{
//	for (int j = 0; j < m_nObjects; j++)
//	{
//		m_ppObjects[j]->Animate(fTimeElapsed);
//	}
//}
//
//void CNormalDungeonMonsterShader::UpdateObjectsCB(FrameResource* pFrameResource, float fTime)
//{
//	for (int i = 0; i < m_nObjects; ++i) {
//		m_ppObjects[i]->SetFrameDirty(::gNumFrameResources);
//		XMFLOAT4X4 world = m_ppObjects[i]->GetWorld();
//		if (m_ppObjects[i]->m_strObjectName == string("Peon")) {
//			world._11 = 8.f, world._22 = 8.f, world._33 = 8.f;
//		}
//		else if (m_ppObjects[i]->m_strObjectName == string("Grunt")) {
//			world._11 = 10.f, world._22 = 10.f, world._33 = 10.f;
//		}
//		else if (m_ppObjects[i]->m_strObjectName == string("Lord")) {
//			world._11 = 12.f, world._22 = 12.f, world._33 = 12.f;
//		}
//		m_ppObjects[i]->SetWorld(world);
//		m_ppObjects[i]->UpdateCB(pFrameResource);
//		m_ppObjects[i]->UpdateSkinCB(pFrameResource, fTime);
//	}
//}
//
//void CNormalDungeonMonsterShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
//{
//	CShader::Render(pd3dCommandList, pCamera, is_Shadow);
//	int cnt = 0;
//	for (int j = 0; j < m_nObjects; j++)
//	{
//		if (m_ppObjects[j] && m_ppObjects[j]->m_xmOOBB.Intersects(pCamera->GetFrustum())) {
//			cnt++;
//			m_ppObjects[j]->Render(pd3dCommandList, pCamera);
//		}
//	}
//	//cout << cnt << "마리 그림" << endl;
//}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CBossDungeonMonsterShader::CBossDungeonMonsterShader()
//{
//}
//
//CBossDungeonMonsterShader::~CBossDungeonMonsterShader()
//{
//}
//
//void CBossDungeonMonsterShader::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void *pContext)
//{
//	m_nObjects = 48;
//	m_ppObjects = new CGameObject*[m_nObjects];
//	int index = 0;
//	CMaterial* pMaterial = new CMaterial();
//
//	string strModelFilenames[] = {
//		"../Model/Monster/Peon/peon.fbx", "../Model/Monster/Grunt/Grunt.fbx", "../Model/Monster/Lord/Lord.fbx"
//	};
//	string strAnimFilenames[] = {
//		"../Model/Monster/Peon/peon@idle.fbx", "../Model/Monster/Grunt/Grunt@idle.fbx", "../Model/Monster/Lord/Lord@idle.fbx"
//	};
//	string strMonsternames[] = { "Peon", "Grunt", "Lord" };
//	for (int i = 0; i < 3; ++i)
//	{
//		ModelData* pData = new ModelData();
//		pData->ModelLoadAndMeshLoad(strModelFilenames[i], pd3dDevice, pd3dCommandList);
//		pData->AnimationLoad(strAnimFilenames[i], "walk", 0.8f, true);
//
//		m_mModelData.insert(pair<string, ModelData*>(strMonsternames[i], pData));
//	}
//
//	ifstream in("test/peonAlt.txt");
//	XMFLOAT3 pos;
//
//	for (int i = 0; i < 24; ++i) {
//		CSkinAndDivisionObject* pPeon = new CSkinAndDivisionObject();
//		pPeon->SetTextureIndex(XMUINT2(50, 51));
//		pPeon->SetMesh(0, m_mModelData["Peon"]->m_pParentMesh);
//		pPeon->SetChilds(pd3dDevice, m_mModelData["Peon"]->m_vChildMesh, m_mModelData["Peon"]->m_vNames);
//		pPeon->SetAnimClip(m_mModelData["Peon"]->m_mDivisionAnimationClip);
//		in >> pos.x >> pos.y >> pos.z;
//		pPeon->SetPosition(pos);
//		pPeon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(4.f, pPeon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//		pPeon->SetMaterial(0, pMaterial);
//		pPeon->m_strObjectName = "Peon";
//		pPeon->SetObjectCBIndex(::gnObjCBindex++);
//		pPeon->SetSkinCBIndex(::gnSkinCBindex++);
//		m_ppObjects[index++] = pPeon;
//	}
//	in.close();
//
//	in.open("test/gruntAlt.txt");
//	for (int i = 0; i < 14; ++i) {
//		CSkinAndDivisionObject* pGrunt = new CSkinAndDivisionObject();
//		pGrunt->SetTextureIndex(XMUINT2(46, 47));
//		pGrunt->SetMesh(0, m_mModelData["Grunt"]->m_pParentMesh);
//		pGrunt->SetChilds(pd3dDevice, m_mModelData["Grunt"]->m_vChildMesh, m_mModelData["Grunt"]->m_vNames);
//		pGrunt->SetAnimClip(m_mModelData["Grunt"]->m_mDivisionAnimationClip);
//		in >> pos.x >> pos.y >> pos.z;
//		pGrunt->SetPosition(pos);
//		pGrunt->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(5.f, pGrunt->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//		pGrunt->SetMaterial(0, pMaterial);
//		pGrunt->m_strObjectName = "Grunt";
//		pGrunt->SetObjectCBIndex(::gnObjCBindex++);
//		pGrunt->SetSkinCBIndex(::gnSkinCBindex++);
//		m_ppObjects[index++] = pGrunt;
//	}
//	in.close();
//
//	in.open("test/lordAlt.txt");
//	for (int i = 0; i < 10; ++i) {
//		CSkinAndDivisionObject* pLord = new CSkinAndDivisionObject();
//		pLord->SetTextureIndex(XMUINT2(54, 55));
//		pLord->SetMesh(0, m_mModelData["Lord"]->m_pParentMesh);
//		pLord->SetChilds(pd3dDevice, m_mModelData["Lord"]->m_vChildMesh, m_mModelData["Lord"]->m_vNames);
//		pLord->SetAnimClip(m_mModelData["Lord"]->m_mDivisionAnimationClip);
//		in >> pos.x >> pos.y >> pos.z;
//		pLord->SetPosition(pos);
//		pLord->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(6.f, pLord->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//		pLord->SetMaterial(0, pMaterial);
//		pLord->m_strObjectName = "Lord";
//		pLord->SetObjectCBIndex(::gnObjCBindex++);
//		pLord->SetSkinCBIndex(::gnSkinCBindex++);
//		m_ppObjects[index++] = pLord;
//	}
//	in.close();
//
//	m_pBossDragon = new CBossDragonObject();
//	m_pBossDragon->LoadModelAndInitCombinedAnimationClip("../Model/Monster/Dragon/DragonBoss.fbx", pd3dDevice, pd3dCommandList);
//	m_pBossDragon->SetTextureIndex(XMUINT2(58, 59));
//	m_pBossDragon->SetPosition(XMFLOAT3(5000.f, 1000.f, 26500.f));
//	m_pBossDragon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, m_pBossDragon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
//	m_pBossDragon->SetMaterial(0, pMaterial);
//	m_pBossDragon->m_strObjectName = "Boss";
//	m_pBossDragon->SetScale(5.f, 5.f, 5.f);
//	m_pBossDragon->Rotate(90.f, 0, 0.f);
//	m_pBossDragon->SetObjectCBIndex(::gnObjCBindex++);
//	m_pBossDragon->SetSkinCBIndex(::gnSkinCBindex++);
//}
//
//void CBossDungeonMonsterShader::AnimateObjects(float fTimeElapsed)
//{
//	for (int j = 0; j < m_nObjects; j++)
//	{
//		m_ppObjects[j]->Animate(fTimeElapsed);
//	}
//	m_pBossDragon->Animate(fTimeElapsed);
//}
//
//void CBossDungeonMonsterShader::UpdateObjectsCB(FrameResource* pFrameResource, float fTime)
//{
//	for (int i = 0; i < m_nObjects; ++i) {
//		m_ppObjects[i]->SetFrameDirty(::gNumFrameResources);
//		XMFLOAT4X4 world = m_ppObjects[i]->GetWorld();
//		if (m_ppObjects[i]->m_strObjectName == string("Peon")) {
//			world._11 = 8.f, world._22 = 8.f, world._33 = 8.f;
//		}
//		else if (m_ppObjects[i]->m_strObjectName == string("Grunt")) {
//			world._11 = 10.f, world._22 = 10.f, world._33 = 10.f;
//		}
//		else if (m_ppObjects[i]->m_strObjectName == string("Lord")) {
//			world._11 = 12.f, world._22 = 12.f, world._33 = 12.f;
//		}
//		m_ppObjects[i]->SetWorld(world);
//		m_ppObjects[i]->UpdateCB(pFrameResource);
//		m_ppObjects[i]->UpdateSkinCB(pFrameResource, fTime);
//	}
//
//	m_pBossDragon->SetFrameDirty(::gNumFrameResources);
//	m_pBossDragon->UpdateCB(pFrameResource);
//	m_pBossDragon->UpdateSkinCB(pFrameResource, fTime);
//}
//
//
//void CBossDungeonMonsterShader::ReleaseObjects()
//{
//
//	if (m_ppObjects)
//	{
//		for (int j = 0; j < m_nObjects; j++) if (m_ppObjects[j]) delete m_ppObjects[j];
//		delete[] m_ppObjects;
//	}
//
//	if (m_pBossDragon) {
//		delete m_pBossDragon;
//		m_pBossDragon = nullptr;
//	}
//
//	for (auto& data : m_mModelData) {
//		if (data.second) {
//			delete data.second;
//			data.second = nullptr;
//		}
//	}
//	m_mModelData.clear();
//}
//
//void CBossDungeonMonsterShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
//{
//	CShader::Render(pd3dCommandList, pCamera, is_Shadow);
//
//	for (int j = 0; j < m_nObjects; j++)
//	{
//		if (m_ppObjects[j] && m_ppObjects[j]->m_xmOOBB.Intersects(pCamera->GetFrustum())) {
//			m_ppObjects[j]->Render(pd3dCommandList, pCamera);
//		}
//	}
//	m_pBossDragon->Render(pd3dCommandList, pCamera);
//}