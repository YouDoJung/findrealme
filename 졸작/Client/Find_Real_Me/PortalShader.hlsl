#include "Shaders.hlsl"

struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VS_TEXTURED_OUTPUT VS(VS_TEXTURED_INPUT input)
{
	VS_TEXTURED_OUTPUT output;

	float3 posW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);

	//int time = gFrameTime;
	//output.uv = input.uv / 4.f;
	//output.uv.x += time % 4 / 4.f;
	//
	//output.uv.y += (time / 4) / 4.f;

	int time = gFrameTime;
	output.uv = input.uv;
	output.uv.x /= 7.f;
	output.uv.y /= 7.f;

	output.uv.x += time % 7 / 7.f;

	if(time % 7 < 5)
		output.uv.y += (time / 7) / 7.f;

	return(output);
}

float4 PS(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
	float4 diffuseAlbedo = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);

	float4 ambient = gAmbientLight * diffuseAlbedo;
	float4 cColor = ambient;

	return(cColor);
}

////////////////////// portal
/////////////////////////////////////////////////////////////
struct VS_PARTICLE_INPUT
{
	float3 posL			: POSITION;
	float2 sizeW		: SIZE;
	float2 start_life	: STARTLIFE;
	float value : VALUE;
};

struct VS_PARTICLE_OUTPUT
{
	float3 centerW		: POSITION;
	float2 sizeW		: SIZE;
};

struct GS_PARTICLE_OUTPUT
{
	float4 positionH	: SV_POSITION;
	float4 shadowPosition : POSITION0;
	float3 positionW	: POSITION1;
	float3 normalW		: NORMAL;
	float2 uv			: TEXCOORD;
	uint primID			: SV_PrimitiveID;
};

VS_PARTICLE_OUTPUT VS_PortalParticle(VS_PARTICLE_INPUT input)
{
	VS_PARTICLE_OUTPUT output;

	float3 newPos = input.posL;
	float startTime = input.start_life.x;
	float lifeTime = input.start_life.y;
	float newTime = gfTotalTime * 100.f;

	newTime = fmod(newTime, lifeTime);

	//newPos = float3(100 * sin(input.value * 3.14f * 2.f), 0.f, 100 * cos(input.value * 3.14f * 2.f));
	//if(newTime / 10.f > startTime)
	newPos = float3(200 * sin(input.value * newTime / 30.f), 0.f, 200 * cos(input.value * newTime / 30.f));
	/*else
		newPos = float3(100 * sin(input.value * 3.14f * 2.f), 0.f, 100 * cos(input.value * 3.14f * 2.f));*/
		//newPos.x += cos(newTime / 50.f);
	newPos.y += newTime * 0.08f * startTime;
	//newPos.z += sin(newTime / 50.f);

	//newPos += 0.05f * newTime * newTime * gravity;

	output.centerW = mul(float4(newPos, 1.0f), gmtxObjectWorld).xyz;
	output.sizeW = input.sizeW;


	return output;
}

[maxvertexcount(4)]
void GS_PortalParticle(point VS_PARTICLE_OUTPUT input[1], uint primID : SV_PrimitiveID, inout TriangleStream<GS_PARTICLE_OUTPUT> outStream)
{
	float3 vUp = float3(0.0f, 1.0f, 0.0f);
	float3 vLook = gvCameraPosition.xyz - input[0].centerW;
	vLook = normalize(vLook);
	float3 vRight = cross(vUp, vLook);

	float fHalfW = input[0].sizeW.x * 0.5f;
	float fHalfH = input[0].sizeW.y * 0.5f;

	// 사각형 정점들을 월드변환행렬로 변환하고, 그것들을 하나의 삼각형으로 출력
	float4 pVertices[4];
	pVertices[0] = float4(input[0].centerW + fHalfW * vRight - fHalfH * vUp, 1.0f);
	pVertices[1] = float4(input[0].centerW + fHalfW * vRight + fHalfH * vUp, 1.0f);
	pVertices[2] = float4(input[0].centerW - fHalfW * vRight - fHalfH * vUp, 1.0f);
	pVertices[3] = float4(input[0].centerW - fHalfW * vRight + fHalfH * vUp, 1.0f);

	float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
	int time = gFrameTime;
	GS_PARTICLE_OUTPUT output;
	[unroll]
	for (int i = 0; i < 4; ++i)
	{
		output.positionW = pVertices[i].xyz;
		output.shadowPosition = mul(float4(output.positionW, 1.f), gmtxShadowTransform);
		output.positionH = mul(pVertices[i], gmtxViewProjection);
		output.normalW = vLook;
		output.uv = pUVs[i];

		output.primID = primID;
		outStream.Append(output);
	}
}

float4 PS_PortalParticle(GS_PARTICLE_OUTPUT input) : SV_Target
{
	float4 diffuseAlbedo = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].Sample(gSamplerState, input.uv);

	float4 cColor = diffuseAlbedo;

	return cColor;
}