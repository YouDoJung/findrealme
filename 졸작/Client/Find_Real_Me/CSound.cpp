#include "stdafx.h"
#include "CSound.h"
#include "../Fmod/inc/fmod_errors.h"

CSound::CSound()
{
	system->release();
	system->close();
}


CSound::~CSound()
{
}

//CSound* CSound::sharedManager()
//{
//	if (instance == nullptr) instance = new CSound;
//	return instance;
//}

void CSound::init()
{
	result = System_Create(&system);
	ErrorCheck(result);
	result = system->init(100, FMOD_INIT_NORMAL, nullptr);
	ErrorCheck(result);
}

void CSound::loading()
{
	result = system->createSound("../Sound/village.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::VILLAGE]);
	ErrorCheck(result);
	result = system->createSound("../Sound/top_inside.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::TOP_INSIDE]);
	ErrorCheck(result);
	result = system->createSound("../Sound/dungeon.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::DUNGEON]);
	ErrorCheck(result);
	result = system->createSound("../Sound/boss_dungeon.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::BOSS_DUNGEON]);
	ErrorCheck(result);
	result = system->createSound("../Sound/thump.wav", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::THUMP]);
	ErrorCheck(result);

	result = system->createSound("../Sound/explosion.mp3", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::EXPLOSION]);
	ErrorCheck(result);
	result = system->createSound("../Sound/firebreath.flac", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::FIREBREATH]);
	ErrorCheck(result);
	result = system->createSound("../Sound/thunder.wav", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::THUNDER]);
	ErrorCheck(result);
	result = system->createSound("../Sound/slash.wav", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::SLASH]);
	ErrorCheck(result);
	result = system->createSound("../Sound/player_damage.wav", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::MAN_DAMAGE]);
	ErrorCheck(result);

	result = system->createSound("../Sound/dragon-die.wav", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::DRAGON_DIE]);
	ErrorCheck(result);
	result = system->createSound("../Sound/dragon-fly.wav", FMOD_LOOP_NORMAL, nullptr, &sound[EffectSound::DRAGON_FLY]);
	ErrorCheck(result);
	result = system->createSound("../Sound/girl_damage.wav", FMOD_LOOP_OFF, nullptr, &sound[EffectSound::WOMAN_DAMAGE]);
	ErrorCheck(result);
}

void CSound::play(int type, int chNum)
{
	system->update();
	result = system->playSound(sound[type], 0, false, &channel[chNum]);
	ErrorCheck(result);
}

void CSound::stop(int chNum)
{
	channel[chNum]->stop();
}

void CSound::ErrorCheck(FMOD_RESULT r)
{
	if (r != FMOD_OK) {
		TCHAR str[256] = { 0 };
		MultiByteToWideChar(CP_ACP, NULL, FMOD_ErrorString(result), -1, str, 256);
		MessageBox(NULL, str, L"Sound Error", MB_OK);
	}
}