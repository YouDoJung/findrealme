#include "stdafx.h"
#include "Object.h"
#include "Shader.h"
#include "ModelShader.h"

//CCollisionBoxShader* CGameObject::m_pCollisionBoxShader = nullptr;
CHpShader* CGameObject::m_pHpBarShader = nullptr;
CGameObject::CGameObject(int nMeshes, int nMaterials)
{
	m_xmf4x4World = Matrix4x4::Identity();

	m_nMeshes = nMeshes;
	if (m_nMeshes > 0)
	{
		m_ppMeshes = new CMesh*[m_nMeshes];
		for (int i = 0; i < m_nMeshes; i++)	m_ppMeshes[i] = nullptr;
	}

	m_nMaterials = nMaterials;
	if (m_nMaterials > 0)
	{
		m_ppMaterials = new CMaterial*[m_nMaterials];
		for (int i = 0; i < m_nMaterials; i++) m_ppMaterials[i] = nullptr;
	}
}

CGameObject::CGameObject(int num)
{
	m_nMeshes = num;
	m_nMaterials = num;
	m_xmf4x4World = Matrix4x4::Identity();

	if (m_nMeshes > 0)
	{
		m_ppMeshes = new CMesh*[m_nMeshes];
		for (int i = 0; i < m_nMeshes; i++) {
			m_ppMeshes[i] = nullptr;
		}
		m_ppMaterials = new CMaterial*[m_nMaterials];
		for (int i = 0; i < m_nMaterials; i++) {
			m_ppMaterials[i] = nullptr;
		}
	}
}

CGameObject::~CGameObject()
{
	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; ++i)
		{
			if (m_ppMeshes[i]) {
				m_ppMeshes[i]->Release();
				m_ppMeshes[i] = nullptr;
			}
		}
		delete[] m_ppMeshes;
	}
	if (m_ppMaterials)
	{
		for (int i = 0; i < m_nMaterials; ++i)
		{
			if (m_ppMaterials[i]) {
				m_ppMaterials[i]->Release();
				m_ppMaterials[i] = nullptr;
			}
		}
		delete[] m_ppMaterials;
	}

	for (int i = 0; i < m_vChilds.size(); ++i)
	{
		delete m_vChilds[i];
		m_vChilds[i] = nullptr;
	}

	/*if (m_pCollisionBox) {
		delete m_pCollisionBox;
		m_pCollisionBox = nullptr;
	}
	if (m_pCollisionBoxShader) {
		delete m_pCollisionBoxShader;
		m_pCollisionBoxShader = nullptr;
	}*/

	if (m_pHpBar) {
		delete m_pHpBar;
		m_pHpBar = nullptr;
	}
	if (m_pHpBGBar) {
		delete m_pHpBGBar;
		m_pHpBGBar = nullptr;
	}
	if (m_pHpBarShader) {
		delete m_pHpBarShader;
		m_pHpBarShader = nullptr;
	}
}

void CGameObject::SetMesh(int nIndex, CMesh* pMesh)
{
	if (m_ppMeshes)
	{
		if (m_ppMeshes[nIndex]) m_ppMeshes[nIndex]->Release();
		m_ppMeshes[nIndex] = pMesh;
		if (pMesh) pMesh->AddRef();
	}
}

void CGameObject::SetChilds(ID3D12Device* pd3dDevice, vector<CSkinnedModelMesh*> pMesh, vector<string>& meshName, XMFLOAT3 xmf3LocalScale) {

	for (int i = 0; i < m_nMeshes; ++i) {
		m_ppMeshes[i]->SetChildNames(meshName);
	}
	m_vChilds.resize(pMesh.size());
	for (int i = 0; i < pMesh.size(); ++i) {

		m_vChilds[i] = new CStaticModelObject(1);
		m_vChilds[i]->SetMesh(0, pMesh[i]);
		m_vChilds[i]->m_strObjectName = meshName[i];

		// 각 자식들에 Material 셋, 상수버퍼인덱스, 텍스처인덱스 셋
		CMaterial* pMaterial = new CMaterial();
		m_vChilds[i]->SetMaterial(0, pMaterial);
		if (m_ChildObjectCBIndex[0] != 0)
			m_vChilds[i]->SetObjectCBIndex(m_ChildObjectCBIndex[i]);
		else m_vChilds[i]->SetObjectCBIndex(gnObjCBindex++);
		XMUINT2 index = m_xmi2TexIndex;
		index.x += 2;
		index.y += 2;
		m_vChilds[i]->SetTextureIndex(index);
		m_vChilds[i]->m_xmf3LocalScale = xmf3LocalScale;
	}
}

void CGameObject::SetShader(CShader* pShader)
{
	if (!m_ppMaterials[0]) {
		CMaterial *pMaterial = new CMaterial();
		SetMaterial(0, pMaterial);
	}
	m_ppMaterials[0]->SetShader(pShader);
}

void CGameObject::SetShader(int nIndex, CShader *pShader)
{
	if (!m_ppMaterials[nIndex])
		m_ppMaterials[nIndex] = new CMaterial();
	m_ppMaterials[nIndex]->SetShader(pShader);
}

void CGameObject::SetMaterial(int nMaterial, CMaterial *pMaterial)
{
	if (m_ppMaterials[nMaterial]) m_ppMaterials[nMaterial]->Release();
	m_ppMaterials[nMaterial] = pMaterial;
	if (m_ppMaterials[nMaterial]) m_ppMaterials[nMaterial]->AddRef();
}

void CGameObject::SetTextureIndex(XMUINT2 nIndex)
{
	m_xmi2TexIndex = nIndex;
}

void CGameObject::UpdateCB(FrameResource* pFrameResource)
{
	m_pCurrResource = pFrameResource;

	if (m_nNumFrameDirty > 0) {
		auto currObjectCB = m_pCurrResource->ObjectCB.get();
		ObjectConstants objConstants;

		XMStoreFloat4x4(&objConstants.m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4World)));
		objConstants.m_xmf4DiffuseAlbedo = m_ppMaterials[0]->GetDiffuseAlbedo();
		objConstants.m_xmf3FresnelR0 = m_ppMaterials[0]->GetFresneIRO();
		objConstants.Shininess = 1.f - m_ppMaterials[0]->GetRoughness();
		// 어떤 텍스처를 사용할지 알려주는 텍스처인덱스변수도 넘겨주고,.
		objConstants.m_xmi2TexIndex = m_xmi2TexIndex;
		objConstants.m_fFrameTime = m_fFrameTime;
		objConstants.m_fStartTime = m_fStartTime;
		objConstants.m_iMaxHP = m_iMaxHP;
		objConstants.m_iHP = m_iHP;
		objConstants.m_iHpSize = m_iHPsize;

		currObjectCB->CopyData(m_ObjectCBIndex, objConstants);
		// 다음 프레임으로 넘어간다.
		m_nNumFrameDirty--;

	}

	for (auto& obj : m_vFireBreathObject) {
		obj->UpdateCB(pFrameResource);
	}

	/*if (m_pCollisionBox) {
		m_pCollisionBox->SetWorld(m_xmf4x4World);
		m_pCollisionBox->SetPosition(m_xmOOBB.Center);
		m_pCollisionBox->UpdateCB(pFrameResource);
		m_pCollisionBox->SetFrameDirty(::gNumFrameResources);
	}*/
	if (m_pHpBar) {
		m_pHpBar->SetWorld(m_xmf4x4World);
		m_pHpBar->SetHP(m_iHP);
		m_pHpBar->SetMaxHP(m_iMaxHP);
		m_pHpBar->SetHpSize(m_iHPsize);
		m_pHpBar->SetPosition(GetPosition().x, GetPosition().y + GetWidthHeightDepth().y * 10, GetPosition().z);
		m_pHpBar->UpdateCB(pFrameResource);
		m_pHpBar->SetFrameDirty(::gNumFrameResources);
	}
	if (m_pHpBGBar) {
		m_pHpBGBar->SetWorld(m_xmf4x4World);
		m_pHpBGBar->SetHP(m_iMaxHP);
		m_pHpBGBar->SetMaxHP(m_iMaxHP);
		m_pHpBGBar->SetHpSize(m_iHPsize);
		m_pHpBGBar->SetPosition(GetPosition().x, GetPosition().y + GetWidthHeightDepth().y * 10, GetPosition().z);
		m_pHpBGBar->UpdateCB(pFrameResource);
		m_pHpBGBar->SetFrameDirty(::gNumFrameResources);
	}
}

void CGameObject::UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime)
{
}

void CGameObject::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
}

void CGameObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	UINT objCBByteSize = (sizeof(ObjectConstants) + 255) & ~255;

	OnPrepareRender();

	for (int i = 0; i < m_nMaterials; i++)
	{
		if (m_ppMaterials[i])
		{
			if (m_ppMaterials[i]->m_pShader)
			{
				m_ppMaterials[i]->m_pShader->Render(pd3dCommandList, pCamera, is_Shadow);
			}
		}
	}

	// b2 정보전달
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pCurrResource->ObjectCB->Resource()->GetGPUVirtualAddress() + m_ObjectCBIndex * objCBByteSize;
	pd3dCommandList->SetGraphicsRootConstantBufferView(1, d3dGpuVirtualAddress);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList);
		}
	}

	/*if (!is_Shadow)
		CollisionBoxRender(pd3dCommandList, pCamera);*/
}

void CGameObject::SetPosition(float x, float y, float z)
{
	m_xmf4x4World._41 = x;
	m_xmf4x4World._42 = y;
	m_xmf4x4World._43 = z;
}

void CGameObject::SetPosition(XMFLOAT3 xmf3Position)
{
	SetPosition(xmf3Position.x, xmf3Position.y, xmf3Position.z);
	for (auto& obj : m_vChilds)
		obj->SetPosition(xmf3Position);
}

XMFLOAT3 CGameObject::GetPosition() const
{
	return(XMFLOAT3(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43));
}

XMFLOAT3 CGameObject::GetLook() const
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._31, m_xmf4x4World._32, m_xmf4x4World._33)));
}

XMFLOAT3 CGameObject::GetUp() const
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23)));
}

XMFLOAT3 CGameObject::GetRight() const
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._11, m_xmf4x4World._12, m_xmf4x4World._13)));
}

XMFLOAT4X4 CGameObject::GetWorld() const
{
	return m_xmf4x4World;
}

void CGameObject::MoveStrafe(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Right = GetRight();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Right, fDistance);
	CGameObject::SetPosition(xmf3Position);
}

void CGameObject::MoveUp(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Up = GetUp();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Up, fDistance);
	CGameObject::SetPosition(xmf3Position);
}

void CGameObject::MoveForward(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Look = GetLook();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Look, fDistance);
	CGameObject::SetPosition(xmf3Position);
}

void CGameObject::Rotate(float fPitch, float fYaw, float fRoll)
{
	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(fPitch), XMConvertToRadians(fYaw), XMConvertToRadians(fRoll));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);

	m_xmf3Rotkey = XMFLOAT3(fPitch, fYaw, fRoll);
}

void CGameObject::Rotate(XMFLOAT3* pxmf3Axis, float fAngle)
{
	XMMATRIX mtxRotate = XMMatrixRotationAxis(XMLoadFloat3(pxmf3Axis), XMConvertToRadians(fAngle));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

XMFLOAT3 CGameObject::GetWidthHeightDepth(XMFLOAT3 scale) const {
	if (m_ppMeshes[0] == nullptr)
		return XMFLOAT3(0, 0, 0);
	XMFLOAT3 Max = { 0.f, 0.f, 0.f };
	XMFLOAT3 Min = { FLT_MAX, FLT_MAX, FLT_MAX };
	for (int i = 0; i < m_nMeshes; ++i) {
		if (Max.x < m_ppMeshes[i]->GetMax().x)
			Max.x = m_ppMeshes[i]->GetMax().x;
		if (Max.y < m_ppMeshes[i]->GetMax().y)
			Max.y = m_ppMeshes[i]->GetMax().y;
		if (Max.z < m_ppMeshes[i]->GetMax().z)
			Max.z = m_ppMeshes[i]->GetMax().z;
		if (Min.x > m_ppMeshes[i]->GetMin().x)
			Min.x = m_ppMeshes[i]->GetMin().x;
		if (Min.y > m_ppMeshes[i]->GetMin().y)
			Min.y = m_ppMeshes[i]->GetMin().y;
		if (Min.z > m_ppMeshes[i]->GetMin().z)
			Min.z = m_ppMeshes[i]->GetMin().z;
	}
	return Vector3::Multiply(scale, Vector3::Subtract(Max, Min));
}

bool CGameObject::HeightCollisionCheck(XMFLOAT3& position, bool is_rot) const {
	bool _Result = false;
	for (int i = 0; i < m_nMeshes; ++i) {
		XMFLOAT3 Max = m_ppMeshes[i]->GetMax();
		XMFLOAT3 Min = m_ppMeshes[i]->GetMin();
		XMStoreFloat3(&Max, XMVector3TransformCoord(XMLoadFloat3(&Max), XMLoadFloat4x4(&m_xmf4x4World)));
		XMStoreFloat3(&Min, XMVector3TransformCoord(XMLoadFloat3(&Min), XMLoadFloat4x4(&m_xmf4x4World)));
		if (is_rot) {
			// 1번 다리는 회전이 안들어갔기 때문에 z값이 다리의 높이를 좌우함.
			// 결국 x가 다리의 너비부분이기 때문에 양쪽 기둥과는 충돌처리가 달라야 하니까 범위의 오차를 둬서 기둥이 아닌곳 판별
			// y값은 기둥에 충돌하나 다리에 충돌하나 맞춰준다.(단, 밑에서 위로 충돌하는 경우 빼고)
			if (position.x > Min.x && position.x < Max.x && position.y + 15.f > m_xmf4x4World._42) {
				// 1번 다리는 z가 6000.f 일때 최대 높이값 나옴.
				if (position.z < 6000.f)
					position.y = position.z / 10.f;
				else
					position.y = -(position.z - 6000.f) / 10.f + 600.f;

				// 다리에 들어와있는 경우에는 true, 기둥쪽일 경우 false
				if (position.x > Min.x + 80 && position.x < Max.x - 80)
					_Result = true;
				else
					_Result = false;
			}

		}
		else {
			// 2번 다리는 y축으로 90도 회전이 들어감. x와 z가 반전되기 때문에 반대로 구해야됨.
			// 결국 z가 다리의 너비부분이기 때문에 양쪽 기둥과는 충돌처리가 달라야 하니까 범위의 오차를 둬서 기둥이 아닌곳 판별
			// y값은 기둥에 충돌하나 다리에 충돌하나 맞춰준다.(단, 밑에서 위로 충돌하는 경우 빼고)
			if (position.z > Max.z && position.z < Min.z && position.y + 15.f > m_xmf4x4World._42) {
				// 2번 다리는 x가 2800.f 일때 최대 높이값 나옴.
				if (position.x < 2800.f)
					position.y = (position.x - 2100.f) / 10.f + 530.f;
				else
					position.y = -(position.x - 2800.f) / 10.f + 600.f;
				// 다리에 들어와있는 경우에는 true, 기둥쪽일 경우 false
				if (position.z > Max.z + 80 && position.z < Min.z - 80)
					_Result = true;
				else
					_Result = false;
			}
		}
	}
	return _Result;
}

void CGameObject::SetAnimationTime(float fTime)
{
	m_fNowTime = fTime;
}

void CGameObject::SetNowAnimation(string& s)
{
}

std::string CGameObject::GetNowAnimation()
{
	return m_strNowClip;
}

//void CGameObject::CreateCollisionBox(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList,
//	ID3D12RootSignature* pd3dGraphicsRootSignature)
//{
//	XMFLOAT3 w_h_d = Vector3::Multiply(m_xmOOBB.Extents, m_xmf3LocalScale);
//	CMesh* pMesh = new CCubeMeshDiffused(pd3dDevice, pd3dCommandList, w_h_d.x * 2, w_h_d.y * 2, w_h_d.z * 2);
//	m_pCollisionBox = new CGameObject(1);
//
//	m_pCollisionBox->SetMesh(0, pMesh);
//
//	CMaterial *pMaterial = new CMaterial();
//	m_pCollisionBox->SetMaterial(0, pMaterial);
//
//	if (CGameObject::m_pCollisionBoxShader == nullptr) {
//		m_pCollisionBoxShader = new CCollisionBoxShader();
//		m_pCollisionBoxShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);
//	}
//	if (m_CollisionBoxCBIndex != 0)
//		m_pCollisionBox->SetObjectCBIndex(m_CollisionBoxCBIndex);
//	else
//		m_pCollisionBox->SetObjectCBIndex(::gnObjCBindex++);
//}
//
//void CGameObject::CollisionBoxRender(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera)
//{
//	if (m_pCollisionBox) {
//		if(m_pCollisionBoxShader)
//			m_pCollisionBoxShader->Render(pd3dCommandList, pCamera, false);
//		m_pCollisionBox->Render(pd3dCommandList, pCamera, false);
//	}
//}

void CGameObject::CreateHpBar(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList,
	ID3D12RootSignature* pd3dGraphicsRootSignature)
{
	XMFLOAT2 xmf2Size = { 100, 15 };
	CMesh* pHpMesh = new CHpMesh(pd3dDevice, pd3dCommandList, xmf2Size, false);
	CMesh* pHpBGMesh = new CHpMesh(pd3dDevice, pd3dCommandList, xmf2Size, true);
	CMaterial *pMaterial = new CMaterial();
	m_pHpBar = new CGameObject(1);
	m_pHpBar->SetMesh(0, pHpMesh);
	m_pHpBar->SetMaterial(0, pMaterial);
	m_pHpBar->SetHP(m_iHP);
	m_pHpBar->SetMaxHP(m_iMaxHP);
	m_pHpBar->SetHpSize(m_iHPsize);

	m_pHpBGBar = new CGameObject(1);
	m_pHpBGBar->SetMesh(0, pHpBGMesh);
	m_pHpBGBar->SetMaterial(0, pMaterial);
	m_pHpBGBar->SetHP(m_iMaxHP);
	m_pHpBGBar->SetMaxHP(m_iMaxHP);
	m_pHpBGBar->SetHpSize(m_iHPsize);

	if (CGameObject::m_pHpBarShader == nullptr) {
		m_pHpBarShader = new CHpShader();
		m_pHpBarShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);
	}

	/*if (m_HpBarIndex != 0)
		m_pHpBar->SetObjectCBIndex(m_HpBarIndex);
	else*/
	m_pHpBar->SetObjectCBIndex(::gnObjCBindex++);
	m_pHpBGBar->SetObjectCBIndex(::gnObjCBindex++);
}
void CGameObject::HpBarRender(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera)
{
	if (m_pHpBar) {
		m_pHpBarShader->Render(pd3dCommandList, pCamera, false);
		m_pHpBar->Render(pd3dCommandList, pCamera, false);
		m_pHpBGBar->Render(pd3dCommandList, pCamera, false);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CHeightMapTerrain::CHeightMapTerrain(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color) : CGameObject(0, 1)
{
	SetOOBB(XMFLOAT3(5000.f, 516.f / 2.f, 5000.f), XMFLOAT3(5000.f, 516.f / 2.f, 5000.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	m_nWidth = nWidth;
	m_nLength = nLength;

	int cxQuadsPerBlock = nBlockWidth;
	int czQuadsPerBlock = nBlockLength;

	m_xmf3Scale = xmf3Scale;

	m_pHeightMapImage = new CHeightMapImage(pFileName, nWidth, nLength, xmf3Scale);

	long cxBlocks = (m_nWidth) / cxQuadsPerBlock;
	long czBlocks = (m_nLength) / czQuadsPerBlock;
	m_nMeshes = cxBlocks * czBlocks;
	m_ppMeshes = new CMesh*[m_nMeshes];
	for (int i = 0; i < m_nMeshes; i++)	m_ppMeshes[i] = NULL;

	CHeightMapGridMesh* pHeightMapGridMesh = NULL;
	for (int z = 0, zStart = 0; z < czBlocks; z++)
	{
		for (int x = 0, xStart = 0; x < cxBlocks; x++)
		{
			xStart = x * (nBlockWidth);
			zStart = z * (nBlockLength);
			pHeightMapGridMesh = new CHeightMapGridMesh(pd3dDevice, pd3dCommandList, xStart, zStart, nBlockWidth, nBlockLength, xmf3Scale, xmf4Color, m_pHeightMapImage);
			SetMesh(x + (z*cxBlocks), pHeightMapGridMesh);
		}
	}

	CTerrainShader *pTerrainShader = new CTerrainShader();
	pTerrainShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, true);

	m_xmi2TexIndex = { 0, 1 };

	CMaterial* pTerrainMaterial = new CMaterial();

	pTerrainMaterial->SetAlbedo(XMFLOAT4(0.2f, 0.2f, 0.2f, 1.f));
	pTerrainMaterial->SetRoughness(0.125f);
	SetMaterial(0, pTerrainMaterial);

	SetShader(pTerrainShader);
	// 상수버퍼 인덱스 저장.
	m_ObjectCBIndex = ::gnObjCBindex++;
}

CHeightMapTerrain::~CHeightMapTerrain(void)
{
	if (m_pHeightMapImage) delete m_pHeightMapImage;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
CSkyBox::CSkyBox(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature) : CGameObject(1)
{
	CSkyBoxMesh *pSkyBoxMesh = new CSkyBoxMesh(pd3dDevice, pd3dCommandList, 20.0f, 20.0f, 2.0f);
	SetMesh(0, pSkyBoxMesh);

	CSkyBoxShader *pSkyBoxShader = new CSkyBoxShader();
	pSkyBoxShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);

	CMaterial *pSkyBoxMaterial = new CMaterial();
	SetMaterial(0, pSkyBoxMaterial);
	SetShader(pSkyBoxShader);

	XMStoreFloat4x4(&m_xmf4x4World, XMMatrixScaling(5000.0f, 5000.0f, 5000.0f));

	m_ObjectCBIndex = ::gnObjCBindex++;
}

CSkyBox::~CSkyBox()
{
}

void CSkyBox::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera, bool is_Shadow)
{
	CGameObject::Render(pd3dCommandList, pCamera, is_Shadow);
}

//
void CUIObject::UpdateCB(FrameResource* pFrameResource)
{
	m_pCurrResource = pFrameResource;

	if (m_nNumFrameDirty > 0) {
		auto currObjectCB = m_pCurrResource->ObjectCB.get();
		ObjectConstants objConstants;

		objConstants.m_xmf2XYScale = m_xmf2XYScale;
		objConstants.m_xmf2CenterPosition = m_xmf2CenterPosition;

		// 어떤 텍스처를 사용할지 알려주는 텍스처인덱스변수도 넘겨주고,.
		objConstants.m_xmi2TexIndex = m_xmi2TexIndex;
		objConstants.m_fFrameTime = m_fFrameTime;
		currObjectCB->CopyData(m_ObjectCBIndex, objConstants);
		// 다음 프레임으로 넘어간다.
		m_nNumFrameDirty--;
	}
}

void CUIObject::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera, bool is_Shadow)
{
	UINT objCBByteSize = (sizeof(ObjectConstants) + 255) & ~255;
	// b2 정보전달
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pCurrResource->ObjectCB->Resource()->GetGPUVirtualAddress() + m_ObjectCBIndex * objCBByteSize;
	pd3dCommandList->SetGraphicsRootConstantBufferView(1, d3dGpuVirtualAddress);

	pd3dCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pd3dCommandList->DrawInstanced(6, 1, 0, 0);
}