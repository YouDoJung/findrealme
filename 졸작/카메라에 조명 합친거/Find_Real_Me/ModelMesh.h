#pragma once
#include "Mesh.h"
#define SKINNED_ANIMATION_BONES		96
struct CModelVertex : public CVertex
{
	XMFLOAT3 m_xmf3Normal;
	XMFLOAT3 m_xmf3Tangent;
	XMFLOAT2 m_xmf2TexCoord;

	CModelVertex() : CVertex(), m_xmf3Normal{ 0.f, 0.f, 0.f }, m_xmf2TexCoord{ 0.f, 0.f }, m_xmf3Tangent{ 1.f, 1.f, 1.f } {}
	CModelVertex(XMFLOAT3& pos, XMFLOAT3& normal, XMFLOAT2& tex, XMFLOAT3& tan) :
		CVertex(pos), m_xmf3Normal(normal), m_xmf2TexCoord(tex), m_xmf3Tangent(tan) {}
};
struct CSkinnedVertex : public CModelVertex
{

	XMUINT4 m_nBoneIndex;
	XMFLOAT3 m_xmf3BoneWeight;

	CSkinnedVertex() : CModelVertex::CModelVertex(), m_nBoneIndex{ 0, 0, 0, 0 }, m_xmf3BoneWeight{ 0.f, 0.f, 0.f } {}
	
	CSkinnedVertex(XMFLOAT3& pos, XMFLOAT3& normal, XMFLOAT2& tex, XMFLOAT3& tan) : CModelVertex::CModelVertex(pos, normal, tex, tan)
	{
		m_xmf3BoneWeight = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_nBoneIndex = XMUINT4(0, 0, 0, 0);
	}

	void AddBoneData(UINT index, float weight) {

		if (m_xmf3BoneWeight.x == 0.0f) {
			m_nBoneIndex.x = index;
			m_xmf3BoneWeight.x = weight;
		}
		else if (m_xmf3BoneWeight.y == 0.0f) {
			m_nBoneIndex.y = index;
			m_xmf3BoneWeight.y = weight;
		}
		else if (m_xmf3BoneWeight.z == 0.0f) {
			m_nBoneIndex.z = index;
			m_xmf3BoneWeight.z = weight;
		}
		else {
			m_nBoneIndex.w = index;
		}
	}
};

struct Bone
{
	XMMATRIX BoneOffset;
	XMMATRIX FinalTransformation;

	Bone() {
		BoneOffset = XMMatrixIdentity();
		FinalTransformation = XMMatrixIdentity();
	}
};

class CSkinnedModelMesh : public CMesh
{
public:
	CSkinnedModelMesh();
	virtual ~CSkinnedModelMesh();

private:
	ComPtr<ID3D12Resource> m_pd3dcbBoneTransforms = nullptr;
	XMFLOAT4X4* m_pcbxmf4x4BoneTransforms = nullptr;

private:
	const aiScene* m_pScene = nullptr;
	vector<pair<string, Bone>> m_vBones;		//�� ����
	UINT m_nBones = 0;

public:
	virtual void LoadMesh(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const aiMesh* pMesh, const aiScene* pModel = nullptr);

	void BoneTransform(float fTime);

	void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const XMMATRIX& ParentTransform);
	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const string& NodeName);

	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);

	unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);

	virtual void CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList, float fAnimationtime);
};