#pragma once
#include "Shader.h"

class ShadowShader : public CShader
{
public:
	ShadowShader(UINT width, UINT height);
	~ShadowShader();

	UINT Width()const;
	UINT Height()const;
	ID3D12Resource* Resource();

	D3D12_VIEWPORT Viewport()const;
	D3D12_RECT ScissorRect()const;

	void SetCameraConstantBuffer(CCamera* pCamera) {
		m_pd3dcbShadow = pCamera->GetConstantBuffer(); 
		m_pcbMappedShadow = pCamera->GetPassInfo();
	}
	void SetDcvHandle(D3D12_CPU_DESCRIPTOR_HANDLE handle) { m_d3dDsvCPUDescriptorStartHandle = handle; }
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDsvDescriptorStartHandle() const { return(m_d3dDsvCPUDescriptorStartHandle); }

	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);
	virtual void AnimateObjects(float fTimeElapsed);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);

private:
	ComPtr<ID3D12Resource> m_pd3dcbShadow = nullptr;
	VS_CB_PASS_INFO* m_pcbMappedShadow = nullptr;
	D3D12_CPU_DESCRIPTOR_HANDLE	m_d3dDsvCPUDescriptorStartHandle = {};

	CTexture* m_pTexture = nullptr;

	D3D12_VIEWPORT mViewport;
	D3D12_RECT mScissorRect;

	UINT mWidth = 0;
	UINT mHeight = 0;
	DXGI_FORMAT mFormat = DXGI_FORMAT_R24G8_TYPELESS;

	BoundingSphere mSceneBounds;

	float mLightNearZ = 0.0f;
	float mLightFarZ = 0.0f;
	XMFLOAT3 mLightPosW;
	XMFLOAT4X4 mLightView = Matrix4x4::Identity();
	XMFLOAT4X4 mLightProj = Matrix4x4::Identity();
	XMFLOAT4X4 mShadowTransform = Matrix4x4::Identity();

	float mLightRotationAngle = 0.0f;
	XMFLOAT3 mBaseLightDirections[3] = {
		XMFLOAT3(0.57735f, -0.57735f, 0.57735f),
		XMFLOAT3(-0.57735f, -0.57735f, 0.57735f),
		XMFLOAT3(0.0f, -0.707f, -0.707f)
	};
public:
	XMFLOAT3 mRotatedLightDirections[3];

};

