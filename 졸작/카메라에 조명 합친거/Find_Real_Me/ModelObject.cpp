#include "stdafx.h"
#include "ModelObject.h"
#include "Material.h"
#include "Shader.h"

CSkinnedObject::CSkinnedObject() : CGameObject(0, 0)
{

}

void CSkinnedObject::InitAnimationClip() {
	float start_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[0].mTime;
	float end_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[m_pModel->mAnimations[0]->mChannels[0]->mNumPositionKeys - 1].mTime - 1.0f;
	m_mAnimationClip["idle"] = AnimationClip(start_time, 59.f, 10.f);
	m_mAnimationClip["walk"] = AnimationClip(60.f, 90.f, 15.f);
	m_mAnimationClip["run"] = AnimationClip(184.f, 214.f, 30.f);
	m_mAnimationClip["leave"] = AnimationClip(91.f, end_time, 10.f);
}

void CSkinnedObject::SetNowAnimation(string& s) {
	if (s == "walk") {
		m_fNowTime = 60.f;
	}
	else if (s == "run") {
		m_fNowTime = 184.f;
	}
	else if (s == "leave") {
		m_fNowTime = 91.f;
	}
	else m_fNowTime = 0.f;
	m_strNowClip = s;
}

void CSkinnedObject::LoadModel(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const string& filename)
{
	m_Importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	m_pModel = m_Importer.ReadFile(filename.c_str(), aiProcess_JoinIdenticalVertices |		// join identical vertices/ optimize indexing
		aiProcess_ValidateDataStructure |		// perform a full validation of the loader's output
		aiProcess_ImproveCacheLocality |		// improve the cache locality of the output vertices
		aiProcess_RemoveRedundantMaterials |	// remove redundant materials
		aiProcess_GenUVCoords |					// convert spherical, cylindrical, box and planar mapping to proper UVs
		aiProcess_TransformUVCoords |			// pre-process UV transformations (scaling, translation ...)
		aiProcess_FindInstances |				// search for instanced meshes and remove them by references to one master
		aiProcess_LimitBoneWeights |			// limit bone weights to 4 per vertex
		aiProcess_OptimizeMeshes |				// join small meshes, if possible;
		//aiProcess_PreTransformVertices |
		aiProcess_GenSmoothNormals |			// generate smooth normal vectors if not existing
		aiProcess_SplitLargeMeshes |			// split large, unrenderable meshes into sub-meshes
		aiProcess_Triangulate |					// triangulate polygons with more than 3 edges
		aiProcess_ConvertToLeftHanded |			// convert everything to D3D left handed space
		aiProcess_SortByPType);

	if (m_pModel) {
		if (m_pModel->HasMeshes()) {
			m_nMeshes = m_pModel->mNumMeshes;
			m_ppMeshes = new CMesh*[m_nMeshes];
			for (int i = 0; i < m_nMeshes; ++i) {
				const aiMesh* pMesh = m_pModel->mMeshes[i];
				m_ppMeshes[i] = new CSkinnedModelMesh();

				m_ppMeshes[i]->LoadMesh(pd3dDevice, pd3dCommandList, pMesh, m_pModel);
			}
		}

		if (m_pModel->HasMaterials()) {
			m_nMaterials = m_pModel->mNumMaterials;
			m_ppMaterials = new CMaterial*[m_nMaterials];
			for (int i = 0; i < m_nMaterials; ++i) {
				m_ppMaterials[i] = nullptr;
			}
			//for (int i = 0; i < m_nMaterials; ++i) {
			//	m_ppMaterials[i] = new CMaterial();
			//	m_ppMaterials[i]->LoadMaterial(pd3dDevice, pd3dCommandList, m_pModel->mMaterials[i]);
			//}
		}
	}
	else {
		cout << "못읽었는디" << endl;
	}
}

void CSkinnedObject::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
	//cout << "플레이어 : " << m_xmOOBB.Center.x << ", " << m_xmOOBB.Center.y << ", " << m_xmOOBB.Center.z << endl;

	m_fElapsedTime = fTimeElapsed;
}

void CSkinnedObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	OnPrepareRender();

	// 재질을 오브젝트가 갖고있으면, 오브젝트 내부에서 상수버퍼를 수정한다고 생각하자.

	for (int i = 0; i < m_nMaterials; i++)
	{
		if (m_ppMaterials[i])
		{

			if (m_ppMaterials[i]->m_pShader)
			{
				m_ppMaterials[i]->m_pShader->Render(pd3dCommandList, pCamera, is_Shadow);
				UpdateShaderVariables(pd3dCommandList);
			}
			if (m_ppMaterials[i]->m_pTexture)
			{
				m_ppMaterials[i]->m_pTexture->UpdateShaderVariables(pd3dCommandList);
			}
		}
	}
	// 서술자 테이블로 썼을 경우에 테이블을 셋해줘야 됨.
	pd3dCommandList->SetGraphicsRootDescriptorTable(1, m_d3dCbvGPUDescriptorHandle);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				//cout << m_mAnimationClip["Main"].m_fStartTime << ", " << m_mAnimationClip["Main"].m_fEndTime << endl;
				m_ppMeshes[i]->UpdateShaderVariables(pd3dCommandList, m_fNowTime);
				m_fNowTime += m_fElapsedTime * m_mAnimationClip[m_strNowClip].m_fSpeed;
				if (m_fNowTime > m_mAnimationClip[m_strNowClip].m_fEndTime)
					m_fNowTime = m_mAnimationClip[m_strNowClip].m_fStartTime;

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}
}