#ifndef NUM_DIR_LIGHTS
#define NUM_DIR_LIGHTS 3
#endif

#ifndef NUM_POINT_LIGHTS
#define NUM_POINT_LIGHTS 0
#endif

#ifndef NUM_SPOT_LIGHTS
#define NUM_SPOT_LIGHTS 0
#endif
#include "LightingUtil.hlsl"

#define SKINNED_ANIMATION_BONES		96


cbuffer cbCameraInfo : register(b0)
{
	matrix		gmtxView : packoffset(c0);
	matrix		gmtxInverseView : packoffset(c4);
	matrix		gmtxProjection : packoffset(c8);
	matrix		gmtxInverseProjection : packoffset(c12);
	matrix		gmtxViewProjection : packoffset(c16);
	matrix		gmtxShadowTransform : packoffset(c20);
	float3		gvCameraPosition : packoffset(c24);

	float4 gAmbientLight : packoffset(c25);
	Light gLights[MaxLights] : packoffset(c26);
};

cbuffer cbObjectInfo : register(b1)
{
	matrix	gmtxObjectWorld : packoffset(c0);
	Material gMaterial : packoffset(c4);
	uint2 gTextureIndex : packoffset(c6);
};

cbuffer cbSkinnedInfo : register(b2)
{
	matrix		gmtxBoneTransforms[SKINNED_ANIMATION_BONES];
}

// 플레이어 텍스처
Texture2D gtxtTexture : register(t0);
Texture2D gtxtNormalTexture : register(t1);

// 터레인 텍스처
Texture2D gtxtTerrainBaseTexture : register(t2);
Texture2D gtxtTerrainDetailTexture : register(t3);

// 쉐도우 맵
Texture2D gtxtShadow : register(t4);

SamplerState gSamplerState : register(s0);
SamplerComparisonState gShadowSamplerState : register(s1);

float3 NormalSampleToWorldSpace(float3 normalMapSample, float3 unitNormalW, float3 tangentW)
{
	// Uncompress each component from [0,1] to [-1,1].
	float3 normalT = 2.0f*normalMapSample - 1.0f;

	// Build orthonormal basis.
	float3 N = unitNormalW;
	float3 T = normalize(tangentW - dot(tangentW, N) * N);
	float3 B = cross(N, T);

	float3x3 TBN = float3x3(T, B, N);

	// Transform from tangent space to world space.
	float3 bumpedNormalW = mul(normalT, TBN);

	return bumpedNormalW;
}

//---------------------------------------------------------------------------------------
// PCF for shadow mapping.
//---------------------------------------------------------------------------------------
//#define SMAP_SIZE = (2048.0f)
//#define SMAP_DX = (1.0f / SMAP_SIZE)
float CalcShadowFactor(float4 shadowPosH)
{
	// Complete projection by doing division by w.
	shadowPosH.xyz /= shadowPosH.w;

	// Depth in NDC space.
	float depth = shadowPosH.z;

	uint width, height, numMips;
	gtxtShadow.GetDimensions(0, width, height, numMips);

	// Texel size.
	float dx = 1.0f / (float)width;

	float percentLit = 0.0f;
	const float2 offsets[9] =
	{
		float2(-dx,  -dx), float2(0.0f,  -dx), float2(dx,  -dx),
		float2(-dx, 0.0f), float2(0.0f, 0.0f), float2(dx, 0.0f),
		float2(-dx,  +dx), float2(0.0f,  +dx), float2(dx,  +dx)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		percentLit += gtxtShadow.SampleCmpLevelZero(gShadowSamplerState,
			shadowPosH.xy + offsets[i], depth).r;
	}

	return percentLit / 9.0f;
}