#pragma once
#include "Shader.h"
#include "Player.h"
//struct MATERIAL
//{
//	XMFLOAT4 m_xmf4DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
//	XMFLOAT3 m_xmf3FresnelR0 = { 0.01f, 0.01f, 0.01f };
//	float m_fRoughness = 0.25f;
//	XMFLOAT4X4 m_xmf4x4MatTransform = Matrix4x4::Identity();
//};

class CScene
{
public:
    CScene();
    ~CScene();

	bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	void ReleaseObjects();

	void CreateGraphicsRootSignature(ID3D12Device* pd3dDevice);
	ID3D12RootSignature* GetGraphicsRootSignature() const { return(m_pd3dGraphicsRootSignature.Get()); }
	void SetGraphicsRootSignature(ID3D12GraphicsCommandList* pd3dCommandList) { pd3dCommandList->SetGraphicsRootSignature(m_pd3dGraphicsRootSignature.Get()); }

	virtual void CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void ReleaseShaderVariables();

	bool ProcessInput(UCHAR* pKeysBuffer);
    void AnimateObjects(float fTimeElapsed);
	void UpdateLightsInfo(XMFLOAT3 xmf3Lightdirections[3]);
    void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera=nullptr, bool is_Shadow = false);

	void ReleaseUploadBuffers();

	CHeightMapTerrain* GetTerrain() const { return(m_pTerrain); }

	CPlayer* m_pPlayer = nullptr;

	void SetPlayer()
	{
		for (int i = 0; i < m_nShaders; ++i) {
			m_ppShaders[i]->SetPlayer(m_pPlayer);
		}
	}

protected:
	UINT m_eMaplist = MAP_LIST::BILLAGE;

	CShader** m_ppShaders = nullptr;
	int m_nShaders = 0;

	CHeightMapTerrain* m_pTerrain = nullptr;
	ComPtr<ID3D12RootSignature>	m_pd3dGraphicsRootSignature = nullptr;

private:

	D3D12_VIEWPORT mShadowViewport;
	D3D12_RECT mShadowScissorRect;
};
