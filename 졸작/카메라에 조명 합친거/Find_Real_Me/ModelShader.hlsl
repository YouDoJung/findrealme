#include "Shaders.hlsl"

struct VS_MODEL_OUTPUT
{
	float4 position : SV_POSITION;
	float4 shadowPosition : POSITION0;
	float3 positionW : POSITION1;
	float3 normalW : NORMAL;
	float3 tangentW : TANGENT;
	float2 uv : TEXCOORD;
};

struct VS_SKINING_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float2 uv : TEXCOORD;
	uint4 index : BONEINDEX;
	float3 weight : WEIGHT;
};

VS_MODEL_OUTPUT VSSkining(VS_SKINING_INPUT input) {
	VS_MODEL_OUTPUT output = (VS_MODEL_OUTPUT)0.f;

	float3 posL = float3(0.0f, 0.0f, 0.0f);
	float3 normalL = float3(0.0f, 0.0f, 0.0f);
	float3 tangentL = float3(0.f, 0.f, 0.f);
	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = input.weight.x;
	weights[1] = input.weight.y;
	weights[2] = input.weight.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	for (int i = 0; i < 4; ++i) {
		posL += weights[i] * mul(float4(input.position, 1.0f),
			gmtxBoneTransforms[input.index[i]]).xyz;
		normalL += weights[i] * mul(input.normal,
			(float3x3)gmtxBoneTransforms[input.index[i]]).xyz;
		tangentL += weights[i] * mul(input.tangent,
			(float3x3)gmtxBoneTransforms[input.index[i]]).xyz;
	}
	//output.positionW = (float3)mul(float4(posL, 1.0f), gmtxObjectWorld);
	output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.normalW = mul(normalL, (float3x3)gmtxObjectWorld);
	output.tangentW = mul(tangentL, (float3x3)gmtxObjectWorld);
	output.position = mul(float4(output.positionW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;
	output.shadowPosition = mul(float4(output.positionW, 1.f), gmtxShadowTransform);
	return output;
}

float4 PSSkining(VS_MODEL_OUTPUT input) : SV_TARGET{

	input.normalW = normalize(input.normalW);
	float4 normalColor = gtxtNormalTexture.Sample(gSamplerState, input.uv);
	float3 BumpNormalW = NormalSampleToWorldSpace(normalColor.rgb, input.normalW, input.tangentW);

	float3 toEyeW = normalize(gvCameraPosition - input.positionW);

	float3 shadowFactor = 1.0f;
	//shadowFactor[0] = CalcShadowFactor(input.shadowPosition);

	float4 directLight = ComputeLighting(gLights, gMaterial, input.positionW, BumpNormalW, toEyeW, shadowFactor);

	float4 diffuseAlbedo = gtxtTexture.Sample(gSamplerState, input.uv);

	float4 ambient = gAmbientLight * diffuseAlbedo;
	float4 cColor = ambient + directLight;

	cColor.a = diffuseAlbedo.a;
	return cColor;
}