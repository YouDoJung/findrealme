#pragma once
#include "Object.h"

struct AnimationClip
{
	float m_fStartTime;
	float m_fEndTime;
	float m_fSpeed;

	AnimationClip() : m_fStartTime{ 0.f }, m_fEndTime{ 0.f }, m_fSpeed{ 0.f } {}
	AnimationClip(float start, float end, float speed) : m_fStartTime{ start }, m_fEndTime{ end }, m_fSpeed{ speed } {}
};

class CSkinnedObject : public CGameObject
{
public:
	CSkinnedObject();
	void InitAnimationClip();
	void SetNowAnimation(string& s);
	virtual void LoadModel(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const string& filename);
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);

	virtual ~CSkinnedObject() {}

protected:
	float m_fElapsedTime = 0.f;
	unordered_map<string, AnimationClip> m_mAnimationClip;
	string m_strNowClip = "walk";
	float m_fNowTime = 0.f;
};