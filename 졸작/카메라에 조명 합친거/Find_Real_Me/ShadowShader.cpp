#include "stdafx.h"
#include "ShadowShader.h"


ShadowShader::ShadowShader(UINT width, UINT height)
{

	mWidth = width;
	mHeight = height;

	mViewport = { 0.0f, 0.0f, (float)width, (float)height, 0.0f, 1.0f };
	mScissorRect = { 0, 0, (int)width, (int)height };

	mSceneBounds.Center = XMFLOAT3(0.f, 0.f, 0.0f);
	mSceneBounds.Radius = sqrtf(5000.0f*5000.0f + 5000.0f*5000.0f);
}

ShadowShader::~ShadowShader()
{
}

UINT ShadowShader::Width()const
{
	return mWidth;
}

UINT ShadowShader::Height()const
{
	return mHeight;
}

ID3D12Resource* ShadowShader::Resource()
{
	return m_pTexture->GetTexture(0);
}

D3D12_VIEWPORT ShadowShader::Viewport()const
{
	return mViewport;
}

D3D12_RECT ShadowShader::ScissorRect()const
{
	return mScissorRect;
}

void ShadowShader::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext)
{
	m_pTexture = new CTexture(1, RESOURCE_TEXTURE2D, 0);

	// build resource
	D3D12_CLEAR_VALUE optClear;
	optClear.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	optClear.DepthStencil.Depth = 1.0f;
	optClear.DepthStencil.Stencil = 0;

	m_pTexture->CreateTexture(pd3dDevice, pd3dCommandList, mWidth, mHeight,
		mFormat, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL, D3D12_RESOURCE_STATE_GENERIC_READ,
		&optClear, 0);

	CreateCbvAndSrvAndUavDescriptorHeaps(pd3dDevice, pd3dCommandList, 0, 1, 0);

	// build descriptor
	// Create SRV to resource so we can sample the shadow map in a shader program.
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	srvDesc.Texture2D.PlaneSlice = 0;
	pd3dDevice->CreateShaderResourceView(m_pTexture->GetTexture(0), &srvDesc, m_d3dSrvCPUDescriptorStartHandle);
	m_pTexture->SetRootArgument(0, 6, m_d3dSrvGPUDescriptorStartHandle);

	// Create DSV to resource so we can render to the shadow map.
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvDesc.Texture2D.MipSlice = 0;
	pd3dDevice->CreateDepthStencilView(m_pTexture->GetTexture(0), &dsvDesc, m_d3dDsvCPUDescriptorStartHandle);
}

void ShadowShader::AnimateObjects(float fTimeElapsed)
{
	mLightRotationAngle += 0.1f*fTimeElapsed;

	XMMATRIX R = XMMatrixRotationY(mLightRotationAngle);
	for (int i = 0; i < 3; ++i)
	{
		XMVECTOR lightDir = XMLoadFloat3(&mBaseLightDirections[i]);
		lightDir = XMVector3TransformNormal(lightDir, R);
		XMStoreFloat3(&mRotatedLightDirections[i], lightDir);
	}

	// Only the first "main" light casts a shadow.
	XMVECTOR lightDir = XMLoadFloat3(&mRotatedLightDirections[0]);
	XMVECTOR lightPos = -2.0f*mSceneBounds.Radius*lightDir;
	XMVECTOR targetPos = XMLoadFloat3(&mSceneBounds.Center);
	XMVECTOR lightUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMMATRIX lightView = XMMatrixLookAtLH(lightPos, targetPos, lightUp);

	XMStoreFloat3(&mLightPosW, lightPos);

	// Transform bounding sphere to light space.
	XMFLOAT3 sphereCenterLS;
	XMStoreFloat3(&sphereCenterLS, XMVector3TransformCoord(targetPos, lightView));

	// Ortho frustum in light space encloses scene.
	float l = sphereCenterLS.x - mSceneBounds.Radius;
	float b = sphereCenterLS.y - mSceneBounds.Radius;
	float n = sphereCenterLS.z - mSceneBounds.Radius;
	float r = sphereCenterLS.x + mSceneBounds.Radius;
	float t = sphereCenterLS.y + mSceneBounds.Radius;
	float f = sphereCenterLS.z + mSceneBounds.Radius;

	mLightNearZ = n;
	mLightFarZ = f;
	XMMATRIX lightProj = XMMatrixOrthographicOffCenterLH(l, r, b, t, n, f);

	// Transform NDC space [-1,+1]^2 to texture space [0,1]^2
	XMMATRIX T(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, -0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.0f, 1.0f);

	XMMATRIX S = lightView * lightProj*T;
	XMStoreFloat4x4(&mLightView, lightView);
	XMStoreFloat4x4(&mLightProj, lightProj);
	XMStoreFloat4x4(&mShadowTransform, S);
}

void ShadowShader::UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList)
{
	XMMATRIX view = XMLoadFloat4x4(&mLightView);
	XMMATRIX proj = XMLoadFloat4x4(&mLightProj);

	XMMATRIX viewProj = XMMatrixMultiply(view, proj);
	XMMATRIX invView = XMMatrixInverse(&XMMatrixDeterminant(view), view);
	XMMATRIX invProj = XMMatrixInverse(&XMMatrixDeterminant(proj), proj);
	XMMATRIX invViewProj = XMMatrixInverse(&XMMatrixDeterminant(viewProj), viewProj);

	// 여기에서 전송하긴 하지만 카메라에서 또다시 업데이트 해줄거임.
	XMMATRIX shadowTransform = XMLoadFloat4x4(&mShadowTransform);

	XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4View, XMMatrixTranspose(view));
	XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4InverseView, XMMatrixTranspose(invView));

	XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4Projection, XMMatrixTranspose(proj));
	XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4InverseProjection, XMMatrixTranspose(invProj));

	XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4ViewProjection, XMMatrixTranspose(viewProj));

	// 여기에서 전송하긴 하지만 카메라에서 또다시 업데이트 해줄거임.
	XMStoreFloat4x4(&m_pcbMappedShadow->m_xmf4x4ShadowTransform, XMMatrixTranspose(shadowTransform));
	m_pcbMappedShadow->m_xmf3CameraPosition = mLightPosW;

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pd3dcbShadow->GetGPUVirtualAddress();
	pd3dCommandList->SetGraphicsRootConstantBufferView(0, d3dGpuVirtualAddress);

}

void ShadowShader::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	ID3D12DescriptorHeap* descriptorHeaps[] = { m_pd3dCbvSrvDescriptorHeap.Get() };
	pd3dCommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);
	if (is_Shadow) {
		UpdateShaderVariables(pd3dCommandList);
	}
	else {

		ID3D12DescriptorHeap* descriptorHeaps[] = { m_pd3dCbvSrvDescriptorHeap.Get() };
		pd3dCommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);

		m_pTexture->UpdateShaderVariables(pd3dCommandList);
	}
}
