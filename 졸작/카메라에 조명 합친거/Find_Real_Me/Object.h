#pragma once
#include "ModelMesh.h"
#include "Mesh.h"
#include "Camera.h"
#include "Texture.h"
#include "Material.h"
#define DIR_FORWARD					0x01
#define DIR_BACKWARD				0x02
#define DIR_LEFT					0x04
#define DIR_RIGHT					0x08
#define DIR_UP						0x10
#define DIR_DOWN					0x20
class CShader;
class CGameObject
{
public:
	CGameObject(int nMeshes = 1, int nMaterials = 1);
	virtual ~CGameObject();

protected:
	XMFLOAT4X4 m_xmf4x4World;

	CMesh** m_ppMeshes = nullptr;
	int m_nMeshes = 0;
	CMaterial** m_ppMaterials = nullptr;
	int m_nMaterials = 0;

	XMUINT2 m_xmi2TexIndex = { 0, 0 };

	// 만약에 상수버퍼를 서술자 테이블로 만들었을 경우에 사용해야 될 것. 핸들값으로 서술자테이블안에서의 서술자의 위치를 뽑아냄.
	D3D12_GPU_DESCRIPTOR_HANDLE		m_d3dCbvGPUDescriptorHandle;

	// 객체 하나만 만들 때 사용.
	ComPtr<ID3D12Resource> m_pd3dcbGameObject = nullptr;
	CB_GAMEOBJECT_INFO* m_pcbMappedGameObject = nullptr;
public:
	string m_strName = "";
	BoundingOrientedBox m_xmOOBB;
	BoundingOrientedBox m_xmOOBBTransformed;
	void SetOOBB(XMFLOAT3& xmCenter, XMFLOAT3& xmExtents, XMFLOAT4& xmOrientation) {
		m_xmOOBBTransformed = m_xmOOBB = BoundingOrientedBox(xmCenter, xmExtents, xmOrientation);
	}


public:
	void SetMesh(int nIndex, CMesh* pMesh);
	void SetShader(CShader* pShader);
	void SetShader(int nIndex, CShader* pShader);
	void SetMaterial(int nIndex, CMaterial* pMaterial);
	void SetTextureIndex(XMUINT2 nIndex);
	void SetCbvGPUDescriptorHandle(D3D12_GPU_DESCRIPTOR_HANDLE d3dCbvGPUDescriptorHandle) { m_d3dCbvGPUDescriptorHandle = d3dCbvGPUDescriptorHandle; }
	void SetCbvGPUDescriptorHandlePtr(UINT64 nCbvGPUDescriptorHandlePtr) { m_d3dCbvGPUDescriptorHandle.ptr = nCbvGPUDescriptorHandlePtr; }
	D3D12_GPU_DESCRIPTOR_HANDLE GetCbvGPUDescriptorHandle() const { return(m_d3dCbvGPUDescriptorHandle); }

	virtual void CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void ReleaseShaderVariables();
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList);

	virtual void Animate(float fTimeElapsed);
	virtual void OnPrepareRender() { }
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);
	virtual void BuildMaterials(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) { }
	virtual void ReleaseUploadBuffers();

	XMFLOAT3 GetPosition() const;
	XMFLOAT3 GetLook() const;
	XMFLOAT3 GetUp() const;
	XMFLOAT3 GetRight() const;
	XMFLOAT4X4 GetWorld() const;

	CMaterial* GetMaterial(int nIndex) const { return m_ppMaterials[nIndex]; }
	XMUINT2 GetTextureIndex() const { return m_xmi2TexIndex; }

	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 xmf3Position);

	void MoveStrafe(float fDistance = 1.0f);
	void MoveUp(float fDistance = 1.0f);
	void MoveForward(float fDistance = 1.0f);

	void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);
	void Rotate(XMFLOAT3* pxmf3Axis, float fAngle);

	virtual void LoadModel(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const string& filename);
	void SetScale(float x, float y, float z) {
		m_xmf4x4World._11 *= x;
		m_xmf4x4World._22 *= y;
		m_xmf4x4World._33 *= z;
	}

	XMFLOAT3 GetWidthHeightDepth(XMFLOAT3 scale = XMFLOAT3(1.f, 1.f, 1.f)) const;
	bool HeightCollisionCheck(XMFLOAT3& position, bool is_rot) const;
protected:
	const aiScene* m_pModel = nullptr;
	Assimp::Importer m_Importer;

public:
	string m_strFilename = "";
	XMFLOAT3 m_xmf3Rotkey = { 0.f, 0.f, 0.f };
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CHeightMapTerrain : public CGameObject
{
public:
	CHeightMapTerrain(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color);
	virtual ~CHeightMapTerrain();

private:
	CHeightMapImage* m_pHeightMapImage;

	int m_nWidth;
	int m_nLength;

	XMFLOAT3 m_xmf3Scale;

public:
	float GetHeight(float x, float z, bool bReverseQuad = false) const { return(m_pHeightMapImage->GetHeight(x, z, bReverseQuad) * m_xmf3Scale.y); } //World
	XMFLOAT3 GetNormal(float x, float z) const { return(m_pHeightMapImage->GetHeightMapNormal(int(x / m_xmf3Scale.x), int(z / m_xmf3Scale.z))); }

	int GetHeightMapWidth() const { return(m_pHeightMapImage->GetHeightMapWidth()); }
	int GetHeightMapLength() const { return(m_pHeightMapImage->GetHeightMapLength()); }

	XMFLOAT3 GetScale() const { return(m_xmf3Scale); }
	float GetWidth() const { return(m_nWidth * m_xmf3Scale.x); }
	float GetLength() const { return(m_nLength * m_xmf3Scale.z); }
};