#include "stdafx.h"
#include "Object.h"
#include "Shader.h"
CGameObject::CGameObject(int nMeshes, int nMaterials)
{
	m_xmf4x4World = Matrix4x4::Identity();

	m_nMeshes = nMeshes;
	m_ppMeshes = NULL;
	if (m_nMeshes > 0)
	{
		m_ppMeshes = new CMesh*[m_nMeshes];
		for (int i = 0; i < m_nMeshes; i++)	m_ppMeshes[i] = NULL;
	}

	m_nMaterials = nMaterials;
	if (m_nMaterials > 0)
	{
		m_ppMaterials = new CMaterial*[m_nMaterials];
		for (int i = 0; i < m_nMaterials; i++) m_ppMaterials[i] = NULL;
	}
}

CGameObject::~CGameObject()
{
	ReleaseShaderVariables();

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; ++i)
		{
			if (m_ppMeshes[i]) m_ppMeshes[i]->Release();
			m_ppMeshes[i] = nullptr;
		}
		delete[] m_ppMeshes;
	}
	if (m_ppMaterials)
	{
		for (int i = 0; i < m_nMaterials; ++i)
		{
			if (m_ppMaterials[i]) m_ppMaterials[i]->Release();
		}
		delete[] m_ppMaterials;
	}
}

void CGameObject::SetMesh(int nIndex, CMesh* pMesh)
{
	if (m_ppMeshes)
	{
		if (m_ppMeshes[nIndex]) m_ppMeshes[nIndex]->Release();
		m_ppMeshes[nIndex] = pMesh;
		if (pMesh) pMesh->AddRef();
	}
}

void CGameObject::SetShader(CShader* pShader)
{
	if (!m_ppMaterials[0]) {
		CMaterial *pMaterial = new CMaterial();
		SetMaterial(0, pMaterial);
	}
	m_ppMaterials[0]->SetShader(pShader);
}

void CGameObject::SetShader(int nIndex, CShader *pShader)
{
	if(!m_ppMaterials[nIndex])
		m_ppMaterials[nIndex] = new CMaterial();
	m_ppMaterials[nIndex]->SetShader(pShader);
}

void CGameObject::SetMaterial(int nMaterial, CMaterial *pMaterial)
{
	if (m_ppMaterials[nMaterial]) m_ppMaterials[nMaterial]->Release();
	m_ppMaterials[nMaterial] = pMaterial;
	if (m_ppMaterials[nMaterial]) m_ppMaterials[nMaterial]->AddRef();
}

void CGameObject::SetTextureIndex(XMUINT2 nIndex)
{
	m_xmi2TexIndex = nIndex;
}

void CGameObject::CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList)
{
	UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	m_pd3dcbGameObject = ::CreateBufferResource(pd3dDevice, pd3dCommandList, nullptr, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

	m_pd3dcbGameObject->Map(0, nullptr, (void **)&m_pcbMappedGameObject);
}

void CGameObject::ReleaseShaderVariables()
{
	if (m_pd3dcbGameObject)
	{
		m_pd3dcbGameObject->Unmap(0, nullptr);
	}
	if (m_ppMaterials) {
		for (int i = 0; i < m_nMaterials; ++i) {
			m_ppMaterials[i]->ReleaseShaderVariables();
		}
	}
}

void CGameObject::UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList)
{
	UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255);


	for (int i = 0; i < m_nMaterials; ++i) {
		CB_GAMEOBJECT_INFO* pbMappedcbGameObject = (CB_GAMEOBJECT_INFO*)((UINT8*)m_pcbMappedGameObject + (i * ncbElementBytes));
		XMStoreFloat4x4(&pbMappedcbGameObject->m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4World)));
		pbMappedcbGameObject->m_xmf4DiffuseAlbedo = m_ppMaterials[i]->GetDiffuseAlbedo();
		pbMappedcbGameObject->m_xmf3FresnelR0 = m_ppMaterials[i]->GetFresneIRO();
		pbMappedcbGameObject->Shininess = 1.f - m_ppMaterials[i]->GetRoughness();
	}
}

void CGameObject::Animate(float fTimeElapsed)
{

	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
}

void CGameObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	OnPrepareRender();

	// 재질을 오브젝트가 갖고있으면, 오브젝트 내부에서 상수버퍼를 수정한다고 생각하자.

	for (int i = 0; i < m_nMaterials; i++)
	{
		if (m_ppMaterials[i])
		{
			if (m_ppMaterials[i]->m_pShader)
			{
				m_ppMaterials[i]->m_pShader->Render(pd3dCommandList, pCamera, is_Shadow);
				UpdateShaderVariables(pd3dCommandList);
			}
			if (m_ppMaterials[i]->m_pTexture)
			{
				m_ppMaterials[i]->m_pTexture->UpdateShaderVariables(pd3dCommandList);
			}
		}
	}

	// 서술자 테이블로 썼을 경우에 테이블을 셋해줘야 됨.
	pd3dCommandList->SetGraphicsRootDescriptorTable(1, m_d3dCbvGPUDescriptorHandle);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i]) m_ppMeshes[i]->Render(pd3dCommandList);
		}
	}
}

void CGameObject::ReleaseUploadBuffers()
{
	for (int i = 0; i < m_nMaterials; ++i)
	{
		if (m_ppMaterials[i]) m_ppMaterials[i]->ReleaseUploadBuffers();
	}
}

void CGameObject::SetPosition(float x, float y, float z)
{
	m_xmf4x4World._41 = x;
	m_xmf4x4World._42 = y;
	m_xmf4x4World._43 = z;
}

void CGameObject::SetPosition(XMFLOAT3 xmf3Position)
{
	SetPosition(xmf3Position.x, xmf3Position.y, xmf3Position.z);
}

XMFLOAT3 CGameObject::GetPosition() const
{
	return(XMFLOAT3(m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43));
}

XMFLOAT3 CGameObject::GetLook() const
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._31, m_xmf4x4World._32, m_xmf4x4World._33)));
}

XMFLOAT3 CGameObject::GetUp() const
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23)));
}

XMFLOAT3 CGameObject::GetRight() const
{
	return(Vector3::Normalize(XMFLOAT3(m_xmf4x4World._11, m_xmf4x4World._12, m_xmf4x4World._13)));
}

XMFLOAT4X4 CGameObject::GetWorld() const
{
	return m_xmf4x4World;
}

void CGameObject::MoveStrafe(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Right = GetRight();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Right, fDistance);
	CGameObject::SetPosition(xmf3Position);
}

void CGameObject::MoveUp(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Up = GetUp();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Up, fDistance);
	CGameObject::SetPosition(xmf3Position);
}

void CGameObject::MoveForward(float fDistance)
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Look = GetLook();
	xmf3Position = Vector3::Add(xmf3Position, xmf3Look, fDistance);
	CGameObject::SetPosition(xmf3Position);
}

void CGameObject::Rotate(float fPitch, float fYaw, float fRoll)
{
	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(fPitch), XMConvertToRadians(fYaw), XMConvertToRadians(fRoll));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);

	m_xmf3Rotkey = XMFLOAT3(fPitch, fYaw, fRoll);
}

void CGameObject::Rotate(XMFLOAT3* pxmf3Axis, float fAngle)
{
	XMMATRIX mtxRotate = XMMatrixRotationAxis(XMLoadFloat3(pxmf3Axis), XMConvertToRadians(fAngle));
	m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
}

void CGameObject::LoadModel(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const string& filename)
{
}

XMFLOAT3 CGameObject::GetWidthHeightDepth(XMFLOAT3 scale) const {
	XMFLOAT3 Max = { 0.f, 0.f, 0.f };
	XMFLOAT3 Min = { FLT_MAX, FLT_MAX, FLT_MAX };
	for (int i = 0; i < m_nMeshes; ++i) {
		if (Max.x < m_ppMeshes[i]->GetMax().x)
			Max.x = m_ppMeshes[i]->GetMax().x;
		if (Max.y < m_ppMeshes[i]->GetMax().y)
			Max.y = m_ppMeshes[i]->GetMax().y;
		if (Max.z < m_ppMeshes[i]->GetMax().z)
			Max.z = m_ppMeshes[i]->GetMax().z;
		if (Min.x > m_ppMeshes[i]->GetMin().x)
			Min.x = m_ppMeshes[i]->GetMin().x;
		if (Min.y > m_ppMeshes[i]->GetMin().y)
			Min.y = m_ppMeshes[i]->GetMin().y;
		if (Min.z > m_ppMeshes[i]->GetMin().z)
			Min.z = m_ppMeshes[i]->GetMin().z;
	}
	return Vector3::Subtract(Max, Min);
}

bool CGameObject::HeightCollisionCheck(XMFLOAT3& position, bool is_rot) const {
	bool _Result = false;
	for (int i = 0; i < m_nMeshes; ++i) {
		XMFLOAT3 Max = m_ppMeshes[i]->GetMax();
		XMFLOAT3 Min = m_ppMeshes[i]->GetMin();
		XMStoreFloat3(&Max, XMVector3TransformCoord(XMLoadFloat3(&Max), XMLoadFloat4x4(&m_xmf4x4World)));
		XMStoreFloat3(&Min, XMVector3TransformCoord(XMLoadFloat3(&Min), XMLoadFloat4x4(&m_xmf4x4World)));
		if (is_rot) {
			// 1번 다리는 회전이 안들어갔기 때문에 z값이 다리의 높이를 좌우함.
			// 결국 x가 다리의 너비부분이기 때문에 양쪽 기둥과는 충돌처리가 달라야 하니까 범위의 오차를 둬서 기둥이 아닌곳 판별
			// y값은 기둥에 충돌하나 다리에 충돌하나 맞춰준다.(단, 밑에서 위로 충돌하는 경우 빼고)
			if (position.x > Min.x && position.x < Max.x && position.y + 15.f > m_xmf4x4World._42) {
				// 1번 다리는 z가 6000.f 일때 최대 높이값 나옴.
				if (position.z < 6000.f)
					position.y = position.z / 10.f;
				else
					position.y = -(position.z - 6000.f) / 10.f + 600.f;

				// 다리에 들어와있는 경우에는 true, 기둥쪽일 경우 false
				if (position.x > Min.x + 80 && position.x < Max.x - 80)
					_Result = true;
				else
					_Result = false;
			}
			
		}
		else {
			// 2번 다리는 y축으로 90도 회전이 들어감. x와 z가 반전되기 때문에 반대로 구해야됨.
			// 결국 z가 다리의 너비부분이기 때문에 양쪽 기둥과는 충돌처리가 달라야 하니까 범위의 오차를 둬서 기둥이 아닌곳 판별
			// y값은 기둥에 충돌하나 다리에 충돌하나 맞춰준다.(단, 밑에서 위로 충돌하는 경우 빼고)
			if (position.z > Max.z && position.z < Min.z && position.y + 15.f > m_xmf4x4World._42) {
				// 2번 다리는 x가 2800.f 일때 최대 높이값 나옴.
				if (position.x < 2800.f)
					position.y = (position.x - 2100.f) / 10.f + 530.f;
				else
					position.y = -(position.x - 2800.f) / 10.f + 600.f;
				// 다리에 들어와있는 경우에는 true, 기둥쪽일 경우 false
				if (position.z > Max.z + 80 && position.z < Min.z - 80)
					_Result = true;
				else
					_Result = false;
			}
		}
	}
	return _Result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CHeightMapTerrain::CHeightMapTerrain(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color) : CGameObject(0, 1)
{
	m_nWidth = nWidth;
	m_nLength = nLength;

	int cxQuadsPerBlock = nBlockWidth;
	int czQuadsPerBlock = nBlockLength;

	m_xmf3Scale = xmf3Scale;

	m_pHeightMapImage = new CHeightMapImage(pFileName, nWidth, nLength, xmf3Scale);

	long cxBlocks = (m_nWidth) / cxQuadsPerBlock;
	long czBlocks = (m_nLength) / czQuadsPerBlock;
	m_nMeshes = cxBlocks * czBlocks;
	m_ppMeshes = new CMesh*[m_nMeshes];
	for (int i = 0; i < m_nMeshes; i++)	m_ppMeshes[i] = NULL;

	CHeightMapGridMesh* pHeightMapGridMesh = NULL;
	for (int z = 0, zStart = 0; z < czBlocks; z++)
	{
		for (int x = 0, xStart = 0; x < cxBlocks; x++)
		{
			xStart = x * (nBlockWidth);
			zStart = z * (nBlockLength);
			pHeightMapGridMesh = new CHeightMapGridMesh(pd3dDevice, pd3dCommandList, xStart, zStart, nBlockWidth, nBlockLength, xmf3Scale, xmf4Color, m_pHeightMapImage);
			SetMesh(x + (z*cxBlocks), pHeightMapGridMesh);
		}
	}

	CreateShaderVariables(pd3dDevice, pd3dCommandList);

	CTexture *pTerrainTexture = new CTexture(2, RESOURCE_TEXTURE2D_ARRAY, 0);

	pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Terrain/earth.dds", 0);
	pTerrainTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Terrain/GrassyRocks.dds", 1);

	UINT ncbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수

	CTerrainShader *pTerrainShader = new CTerrainShader();
	pTerrainShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, true);
	pTerrainShader->CreateCbvAndSrvAndUavDescriptorHeaps(pd3dDevice, pd3dCommandList, 1, 2);
	pTerrainShader->CreateConstantBufferViews(pd3dDevice, pd3dCommandList, 1, m_pd3dcbGameObject.Get(), ncbElementBytes);
	pTerrainShader->CreateShaderResourceViews(pd3dDevice, pd3dCommandList, pTerrainTexture, 4, false);

	CMaterial* pTerrainMaterial = new CMaterial();
	pTerrainMaterial->SetTexture(pTerrainTexture);
	pTerrainMaterial->SetAlbedo(XMFLOAT4(0.5f, 0.5f, 0.5f, 1.f));
	pTerrainMaterial->SetRoughness(0.125f);
	SetMaterial(0, pTerrainMaterial);

	// 마찬가지야. 서술자테이블을 안쓰니깐 없애야지.
	SetCbvGPUDescriptorHandle(pTerrainShader->GetGPUCbvDescriptorStartHandle());

	SetShader(pTerrainShader);
}

CHeightMapTerrain::~CHeightMapTerrain(void)
{
	if (m_pHeightMapImage) delete m_pHeightMapImage;
}