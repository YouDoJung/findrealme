#include "stdafx.h"
#include "GameFramework.h"
#include <chrono>
using namespace chrono;
GameFramework::GameFramework()
{
	m_WndClientWidth = FRAME_BUFFER_WIDTH;
	m_WndClientHeight = FRAME_BUFFER_HEIGHT;
}

float GameFramework::AspectRatio()const
{
	return static_cast<float>(FRAME_BUFFER_WIDTH) / FRAME_BUFFER_HEIGHT;
}

bool GameFramework::OnCreate(HINSTANCE hInstance, HWND hMainWnd)
{
	m_hAppInst = hInstance;
	m_hMainWnd = hMainWnd;

	CreateDirect3DDevice();
	CreateCommandQueueAndList();
	CreateRtvAndDsvDescriptorHeaps();
	CreateSwapChain();
	CreateDepthStencilView();

	BuildObjects();
	return(true);
}

void GameFramework::OnDestroy()
{
	if (m_pPlayer) {
		delete m_pPlayer;
		//m_pPlayer = nullptr;
	}
	if (m_pScene) {
		m_pScene->ReleaseObjects();
		delete m_pScene;
		//m_pScene = nullptr;
	}
}

void GameFramework::CreateDirect3DDevice()
{
#if defined(DEBUG) || defined(_DEBUG) 
	// 디버그 층 활성화
	{
		ComPtr<ID3D12Debug> debugController;

		ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)));
		debugController->EnableDebugLayer();
	}
#endif

	ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&m_pDxgiFactory)));

	// 하드웨어 어댑터를 나타내는 장치를 생성해 본다.
	HRESULT hardwareResult = D3D12CreateDevice(
		nullptr,             // 기본.
		D3D_FEATURE_LEVEL_11_0,
		IID_PPV_ARGS(&m_pD3dDevice));

	// 실패했으면 WARP device생성
	if (FAILED(hardwareResult))
	{
		ComPtr<IDXGIAdapter> pWarpAdapter;
		ThrowIfFailed(m_pDxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&pWarpAdapter)));

		ThrowIfFailed(D3D12CreateDevice(
			pWarpAdapter.Get(),
			D3D_FEATURE_LEVEL_11_0,
			IID_PPV_ARGS(&m_pD3dDevice)));
	}

	ThrowIfFailed(m_pD3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE,
		IID_PPV_ARGS(&m_pFence)));

	m_RtvDescriptorSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_DsvDescriptorSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	gnCbvSrvUavDescriptorIncrementSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	// Check 4X MSAA quality support for our back buffer format.
	// All Direct3D 11 capable devices support 4X MSAA for all render 
	// target formats, so we only need to check quality support.
	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msQualityLevels;
	msQualityLevels.Format = m_BackBufferFormat;
	msQualityLevels.SampleCount = 4;
	msQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	msQualityLevels.NumQualityLevels = 0;
	ThrowIfFailed(m_pD3dDevice->CheckFeatureSupport(
		D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS,
		&msQualityLevels,
		sizeof(msQualityLevels)));

	m_4xMsaaQuality = msQualityLevels.NumQualityLevels;
	assert(m_4xMsaaQuality > 0 && "Unexpected MSAA quality level.");

	//m_pD3dDevice->QueryInterface(IID_PPV_ARGS(&device));
	//device->ReportLiveDeviceObjects(D3D12_RLDO_DETAIL);
}

void GameFramework::CreateSwapChain()
{
	// 새 스왑체인을 만들기 전에 먼저 기존 스왑체인을 해제.
	m_pSwapChain.Reset();

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = m_WndClientWidth;
	sd.BufferDesc.Height = m_WndClientHeight;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = m_BackBufferFormat;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.SampleDesc.Count = m_is4xMsaaState ? 4 : 1;
	sd.SampleDesc.Quality = m_is4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = m_SwapChainBufferCount;
	sd.OutputWindow = m_hMainWnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	// Note: 스왑체인은  명령큐를 이용해 방출(flush)를 수행한다.
	ThrowIfFailed(m_pDxgiFactory->CreateSwapChain(
		m_pCommandQueue.Get(),
		&sd,	
		m_pSwapChain.GetAddressOf()));
}

void GameFramework::CreateCommandQueueAndList()
{
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	ThrowIfFailed(m_pD3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue)));

	ThrowIfFailed(m_pD3dDevice->CreateCommandAllocator(
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		IID_PPV_ARGS(m_pDirectCmdListAlloc.GetAddressOf())));

	ThrowIfFailed(m_pD3dDevice->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		m_pDirectCmdListAlloc.Get(), // 연관된 명령 할당자.
		nullptr,                   // 초기 파이프라인 상태 객체
		IID_PPV_ARGS(m_pCommandList.GetAddressOf())));

	// 닫힌 상태로 시작한다. 이후 명령 리스트를 처음 참조할 때 Reset을 호출하는데
	// Reset을 호출하려면 명령 리스트가 닫혀 있어야 한다.
	ThrowIfFailed(m_pCommandList->Close());
}

void GameFramework::CreateRtvAndDsvDescriptorHeaps()
{
	// 다중렌더타겟할라면 NumDescriptors 수정하셈.
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
	rtvHeapDesc.NumDescriptors = m_SwapChainBufferCount + 1;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	rtvHeapDesc.NodeMask = 0;
	ThrowIfFailed(m_pD3dDevice->CreateDescriptorHeap(
		&rtvHeapDesc, IID_PPV_ARGS(m_pRtvHeap.GetAddressOf())));

	// 그림자 때문에.
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
	dsvHeapDesc.NumDescriptors = 1;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dsvHeapDesc.NodeMask = 0;
	ThrowIfFailed(m_pD3dDevice->CreateDescriptorHeap(
		&dsvHeapDesc, IID_PPV_ARGS(m_pDsvHeap.GetAddressOf())));
}

void GameFramework::CreateRenderTargetViews()
{
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_pRtvHeap->GetCPUDescriptorHandleForHeapStart());
	for (UINT i = 0; i < m_SwapChainBufferCount; ++i)
	{
		ThrowIfFailed(m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(&m_pSwapChainBuffer[i])));
		m_pD3dDevice->CreateRenderTargetView(m_pSwapChainBuffer[i].Get(), nullptr, rtvHeapHandle);
		rtvHeapHandle.Offset(1, m_RtvDescriptorSize);
	}
}

void GameFramework::CreateDepthStencilView()
{
	// Create the depth/stencil buffer and view.
	D3D12_RESOURCE_DESC depthStencilDesc;
	depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	depthStencilDesc.Alignment = 0;
	depthStencilDesc.Width = m_WndClientWidth;
	depthStencilDesc.Height = m_WndClientHeight;
	depthStencilDesc.DepthOrArraySize = 1;
	depthStencilDesc.MipLevels = 1;

	depthStencilDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;

	depthStencilDesc.SampleDesc.Count = m_is4xMsaaState ? 4 : 1;
	depthStencilDesc.SampleDesc.Quality = m_is4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE optClear;
	optClear.Format = m_DepthStencilFormat;
	optClear.DepthStencil.Depth = 1.0f;
	optClear.DepthStencil.Stencil = 0;
	ThrowIfFailed(m_pD3dDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthStencilDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&optClear,
		IID_PPV_ARGS(m_pDepthStencilBuffer.GetAddressOf())));

	// Create descriptor to mip level 0 of entire resource using the format of the resource.
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = m_DepthStencilFormat;
	dsvDesc.Texture2D.MipSlice = 0;
	m_pD3dDevice->CreateDepthStencilView(m_pDepthStencilBuffer.Get(), &dsvDesc, GetDepthStencilView());
}

void GameFramework::OnResizeBackBuffers()
{
	assert(m_pD3dDevice);
	assert(m_pSwapChain);
	assert(m_pDirectCmdListAlloc);

	// Flush before changing any resources.
	FlushCommandQueue();

	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

	// Release the previous resources we will be recreating.
	for (int i = 0; i < m_SwapChainBufferCount; ++i)
		m_pSwapChainBuffer[i].Reset();
	m_pDepthStencilBuffer.Reset();

	// Resize the swap chain.
	ThrowIfFailed(m_pSwapChain->ResizeBuffers(
		m_SwapChainBufferCount,
		m_WndClientWidth, m_WndClientHeight,
		m_BackBufferFormat,
		DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));

	m_CurrBackBuffer = 0;

	CreateRenderTargetViews();
	CreateDepthStencilView();
	//CreateShadowDepthStencilView();
	// Execute the resize commands.
	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	// Wait until resize is complete.
	FlushCommandQueue();

}

void GameFramework::FlushCommandQueue()
{
	// 현재 펜스 지점까지의 명령들을 표시하도록 펜스 값을 증가시킨다.
	m_CurrentFence++;

	// 새 펜스 지점을 설정하는 명령(Signal)을 명령 대기열에 추가한다.
	// 지금 우리는 gpu 시간선에 있으므로, 새 펜스 지점은 gpu가 Signal 명령까지의
	// 모든 명령을 처리하기 전까지는 설정되지 않는다.
	ThrowIfFailed(m_pCommandQueue->Signal(m_pFence.Get(), m_CurrentFence));

	// GPU가 이 펜스 지점까지의 명령들을 완료할 때까지 기다린다.
	if (m_pFence->GetCompletedValue() < m_CurrentFence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);

		// 도달했으면 이벤트를 발생시킨다. 
		ThrowIfFailed(m_pFence->SetEventOnCompletion(m_CurrentFence, eventHandle));

		// GPU가 현재 펜스 지점에 도달했음을 뜻하는 이벤트를 기다린다.
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}
}

ID3D12Resource* GameFramework::GetCurrentBackBuffer()const
{
	return m_pSwapChainBuffer[m_CurrBackBuffer].Get();
}

D3D12_CPU_DESCRIPTOR_HANDLE GameFramework::GetCurrentBackBufferView()const
{
	return CD3DX12_CPU_DESCRIPTOR_HANDLE(
		m_pRtvHeap->GetCPUDescriptorHandleForHeapStart(),
		m_CurrBackBuffer,
		m_RtvDescriptorSize);
}

D3D12_CPU_DESCRIPTOR_HANDLE GameFramework::GetDepthStencilView()const
{
	return m_pDsvHeap->GetCPUDescriptorHandleForHeapStart();
}

void GameFramework::CalculateFrameStats()
{
	// Code computes the average frames per second, and also the 
	// average time it takes to render one frame.  These stats 
	// are appended to the window caption bar.

	static int frameCnt = 0;
	static float timeElapsed = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if ((m_Timer.GetTotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;

		wstring fpsStr = to_wstring(fps);
		wstring mspfStr = to_wstring(mspf);

		wstring windowText = m_MainWndCaption +
			L"    fps: " + fpsStr +
			L"   mspf: " + mspfStr + 
			L"   " + to_wstring(m_pPlayer->GetPosition().x) +
			L"," + to_wstring(m_pPlayer->GetPosition().y) +
			L"," + to_wstring(m_pPlayer->GetPosition().z) + L"FrameTime : " + to_wstring(m_Timer.GetDeltaTime());

		SetWindowText(m_hMainWnd, windowText.c_str());

		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}

void GameFramework::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		::SetCapture(hWnd);
		::GetCursorPos(&m_OldCursorPos);
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		::ReleaseCapture();
		break;
	case WM_MOUSEMOVE:
		break;
	default:
		break;
	}
}

void GameFramework::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_KEYUP:
		switch (wParam)
		{
		case VK_ESCAPE:
			::PostQuitMessage(0);
			break;
		case VK_RETURN:
			break;
		case VK_F1:
		case VK_F2:
		case VK_F3:
			m_pCamera = m_pPlayer->ChangeCamera((DWORD)(wParam - VK_F1 + 1), m_Timer.GetDeltaTime());
			break;
		case VK_F9:
		{
			BOOL bFullScreenState = FALSE;
			m_pSwapChain->GetFullscreenState(&bFullScreenState, NULL);
			m_pSwapChain->SetFullscreenState(!bFullScreenState, NULL);

			OnResizeBackBuffers();
			break;
		}
		case VK_F10:
			break;
		default:
			m_pScene->OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
			break;
		}
		break;
	default:
		break;
	}
}

LRESULT CALLBACK GameFramework::OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_ACTIVATE:
	{
		if (LOWORD(wParam) == WA_INACTIVE)
			m_Timer.Stop();
		else
			m_Timer.Start();
		break;
	}
	case WM_SIZE:
	{
		m_WndClientWidth = LOWORD(lParam);
		m_WndClientHeight = HIWORD(lParam);

		if(m_WndClientWidth != 0 && m_WndClientHeight != 0)
			OnResizeBackBuffers();
		break;
	}
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam);
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
		break;
	}
	return(0);
}

void GameFramework::BuildObjects()
{
	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));
	m_pScene = new CScene();
	m_pScene->BuildObjects(m_pD3dDevice.Get(), m_pCommandList.Get());

	m_pScene->m_pPlayer = m_pPlayer = new CTerrainPlayer(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature(), m_pScene->GetTerrain(), 1);
	m_pPlayer->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, m_pPlayer->GetWidthHeightDepth(XMFLOAT3(0.4f, 0.6f, 0.4f))), XMFLOAT4(0.f, 0.f, 0.f, 1.f));

	m_pCamera = m_pPlayer->GetCamera();
	m_pScene->SetPlayer();

	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	FlushCommandQueue();
	if (m_pScene) m_pScene->ReleaseUploadBuffers();

	m_Timer.Reset();
}

void GameFramework::AnimateObjects()
{
	if (m_pScene) {
		m_pScene->AnimateObjects(m_Timer.GetDeltaTime());
	}
	m_pPlayer->Animate(m_Timer.GetDeltaTime());

}

void GameFramework::FrameAdvance()
{
	//auto t = high_resolution_clock::now();

	m_Timer.Tick();

	ProcessInput();
	AnimateObjects();

	ThrowIfFailed(m_pDirectCmdListAlloc->Reset());
	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

	m_pScene->SetGraphicsRootSignature(m_pCommandList.Get());
	m_pCamera->SetViewportsAndScissorRects(m_pCommandList.Get());

	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	m_pCommandList->ClearRenderTargetView(GetCurrentBackBufferView(), Colors::AliceBlue, 0, nullptr);
	m_pCommandList->ClearDepthStencilView(GetDepthStencilView(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	m_pCommandList->OMSetRenderTargets(1, &GetCurrentBackBufferView(), true, &GetDepthStencilView());

	m_pCamera->UpdateShaderVariables(m_pCommandList.Get());
	m_pScene->Render(m_pCommandList.Get(), m_pCamera, false);
    m_pPlayer->Render(m_pCommandList.Get(), m_pCamera, false);
		
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	ThrowIfFailed(m_pSwapChain->Present(0, 0));
	m_CurrBackBuffer = (m_CurrBackBuffer + 1) % m_SwapChainBufferCount;

	CalculateFrameStats();
	//auto d = high_resolution_clock::now() - t;
	//t = high_resolution_clock::now();
	FlushCommandQueue();
	//auto s = high_resolution_clock::now() - t;

	//cout << "cpu : " << duration_cast<milliseconds>(d).count() << endl;
	//cout << "gpu : " << duration_cast<milliseconds>(s).count() << endl;


}

void GameFramework::ReleaseObjects()
{
	if (m_pPlayer) delete m_pPlayer;

	if (m_pScene) m_pScene->ReleaseObjects();
	if (m_pScene) delete m_pScene;
}

void GameFramework::ProcessInput()
{
	static UCHAR pKeysBuffer[256];
	bool bProcessedByScene = false;
	if (GetKeyboardState(pKeysBuffer) && m_pScene) 
		bProcessedByScene = m_pScene->ProcessInput(pKeysBuffer);

	if (!bProcessedByScene)
	{
		DWORD dwDirection = 0;
		if (pKeysBuffer[VK_UP] & 0xF0 || pKeysBuffer[0x57] & 0xF0) dwDirection |= DIR_FORWARD;
		if (pKeysBuffer[VK_DOWN] & 0xF0 || pKeysBuffer[0x53] & 0xF0) dwDirection |= DIR_BACKWARD;
		if (pKeysBuffer[VK_LEFT] & 0xF0 || pKeysBuffer[0x41] & 0xF0) dwDirection |= DIR_LEFT;
		if (pKeysBuffer[VK_RIGHT] & 0xF0 || pKeysBuffer[0x44] & 0xF0) dwDirection |= DIR_RIGHT;
		if (pKeysBuffer[VK_PRIOR] & 0xF0 || pKeysBuffer[0x45] & 0xF0) dwDirection |= DIR_UP;
		if (pKeysBuffer[VK_NEXT] & 0xF0 || pKeysBuffer[0x51] & 0xF0) dwDirection |= DIR_DOWN;

		if (pKeysBuffer[0x5A] & 0xF0) //z
			m_pPlayer->SetNowAnimation(string("idle"));
		if (pKeysBuffer[0x58] & 0xF0) //x
			m_pPlayer->SetNowAnimation(string("walk"));
		if (pKeysBuffer[0x43] & 0xF0) //c
			m_pPlayer->SetNowAnimation(string("leave"));
		if (pKeysBuffer[0x56] & 0xF0) //v
			m_pPlayer->SetNowAnimation(string("run"));
		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos;
		if (GetCapture() == m_hMainWnd)
		{
			SetCursor(NULL);
			GetCursorPos(&ptCursorPos);
			cxDelta = (float)(ptCursorPos.x - m_OldCursorPos.x) / 3.0f;
			cyDelta = (float)(ptCursorPos.y - m_OldCursorPos.y) / 3.0f;
			SetCursorPos(m_OldCursorPos.x, m_OldCursorPos.y);
		}

		if ((dwDirection != 0) || (cxDelta != 0.0f) || (cyDelta != 0.0f))
		{
			if (cxDelta || cyDelta)
			{
				if (pKeysBuffer[VK_RBUTTON] & 0xF0)
					m_pPlayer->Rotate(cyDelta, 0.0f, -cxDelta);
				else
					m_pPlayer->Rotate(cyDelta, cxDelta, 0.0f);
			}
			if (dwDirection) {
				//cout << "무브중" << endl;
				m_pPlayer->Move(dwDirection, PLAYER_WORKSPEED * m_Timer.GetDeltaTime(), false);
				m_pPlayer->SetDirection(dwDirection);
			}
		}
		//if (!dwDirection)
		//	cout << "안무브중" << endl;
	}
	m_pPlayer->Update(m_Timer.GetDeltaTime());
}

GameFramework::~GameFramework()
{
	if (m_pD3dDevice != nullptr)
		FlushCommandQueue();
}
