#pragma once
#include "stdafx.h"
#include "monsterpool.h"
#include "playerpool.h"

constexpr char g_cProcessLockNum = 4;

//concurrency_priority_queue 연산자
class CEventOperator {
public:
	CEventOperator() {}
	~CEventOperator() {}
	bool operator()(const s_ObjectEvent& eLeftEvent, const s_ObjectEvent& eRightEvent) {
		return (eLeftEvent.m_hrcTimePoint > eRightEvent.m_hrcTimePoint);
	}
};

class CProcess {
private:
	std::recursive_mutex m_rmProcessMutex[g_cProcessLockNum];

public:
	explicit CProcess();
	virtual ~CProcess();
public:
	static class CPlayerPool* m_pPlayerPool;
	static class CMonsterPool* m_pMonsterPool;
	static concurrency::concurrent_unordered_set<unsigned short>m_ccusLoginList[4];	//MAP_NUM == 4
	static std::function<void(char&, unsigned short&)>m_fpAttackEvent[2][7];
	static concurrency::concurrent_priority_queue<s_ObjectEvent, CEventOperator>m_ccpqEventQueue;
	
public:
	inline void BindPlayerAttackEvent()
	{
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Attack_1] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerAttack1Monster(map, player_id);};
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Attack_2] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerAttack2Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Skill_1] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerSkill1Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Skill_2] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerSkill2Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Skill_3] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerSkill3Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Skill_4] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerSkill4Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Man][e_PlayerAttackSkillType::e_Skill_5] = [&](char& map, unsigned short& player_id) {
			return this->PostEventManPlayerSkill5Monster(map, player_id); };

		m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Attack_1] = [&](char& map, unsigned short& player_id) {
			return this->PostEventWoManPlayerAttack1Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Attack_2] = [&](char& map, unsigned short& player_id) {
			return this->PostEventWoManPlayerAttack2Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Skill_1] = [&](char& map, unsigned short& player_id) {
			return this->PostEventWoManPlayerSkill1Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Skill_2] = [&](char& map, unsigned short& player_id) {
			return this->PostEventWoManPlayerSkill2Monster(map, player_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Skill_3] = [&](char& map, unsigned short& player_id) {
			return this->PostEventWoManPlayerSkill3Monster(map, player_id); };

		//여캐는 4번 5번 애니메이션이 존재하지 않는다.
		/*m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Skill_4] = [&](char& map, unsigned short& player_id, unsigned short& monster_id) {
			return this->CheckCollisionManPlayerSkill4(map, player_id, monster_id); };
		m_fpAttackEvent[e_ObjectType::e_Player_Woman][e_PlayerAttackSkillType::e_Skill_5] = [&](char& map, unsigned short& player_id, unsigned short& monster_id) {
			return this->CheckCollisionManPlayerSkill5(map, player_id, monster_id); };*/
	}
	void InitProcessData();
	concurrency::concurrent_unordered_set<unsigned short>& GetLonginList(char cMapNum)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		auto au = m_ccusLoginList[cMapNum];
		return au;
	}
	void CopyBefore(char cMapNum, concurrency::concurrent_unordered_set<unsigned short>& ccusCopyList)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		ccusCopyList = m_ccusLoginList[cMapNum];
	/*	auto au = m_ccusLoginList[cMapNum];
		ccusCopyList = au;*/
		//if (!m_ccusLoginList[cMapNum].empty()) ccusCopyList = m_ccusLoginList[cMapNum];
	}
	void DeleteListElement(char cMapNum, unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		m_ccusLoginList[cMapNum].unsafe_erase(bId);
	}
	void InsertListElement(char cMapNum, unsigned short bId)
	{
		m_ccusLoginList[cMapNum].insert(bId);
	}
	BOOL ExistListElement(char cMapNum, unsigned short bId)
	{
		//std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		if (m_ccusLoginList[cMapNum].count(bId) != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	static void InitializeBeforeStart()
	{
		CProcess::m_ccpqEventQueue.clear();
		for (int i = 0; i < 4; ++i) {
			m_ccusLoginList[i].clear();
		}
	}

	static void PostEvent(char cMap, unsigned short usId, unsigned short usOther, char cOverEvent,char cEvent, const std::chrono::high_resolution_clock::time_point& hrcTimePoint)
	{
		m_ccpqEventQueue.push(s_ObjectEvent{ cMap, hrcTimePoint, cEvent, cOverEvent, usId,usOther });
	}
	static bool TryPopEvent(s_ObjectEvent& eEvent){	return m_ccpqEventQueue.try_pop(eEvent);}
	static bool StateEventQueue() { return m_ccpqEventQueue.empty(); }
	static bool CheckEventStart(s_ObjectEvent& eEvent)
	{
		if (m_ccpqEventQueue.try_pop(eEvent)) {
			if (eEvent.m_hrcTimePoint > std::chrono::high_resolution_clock::now()) {
				CProcess::PostEvent(eEvent.m_cMap, eEvent.m_usID, eEvent.m_usOtherID, eEvent.m_cOverType, eEvent.m_cEventType, eEvent.m_hrcTimePoint);
				return false;
			}
			else return true;
		}
		else return false;
	}

public:
	void PostEventManPlayerAttack1Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventManPlayerAttack2Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventManPlayerSkill1Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventManPlayerSkill2Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventManPlayerSkill3Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventManPlayerSkill4Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventManPlayerSkill5Monster(char& cMap, unsigned short& usPlayerID);

	void PostEventWoManPlayerAttack1Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventWoManPlayerAttack2Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventWoManPlayerSkill1Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventWoManPlayerSkill2Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventWoManPlayerSkill3Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventWoManPlayerSkill4Monster(char& cMap, unsigned short& usPlayerID);
	void PostEventWoManPlayerSkill5Monster(char& cMap, unsigned short& usPlayerID);

	
	bool CheckPortalByPlayerInVillage(unsigned short& usPlayerID);
	bool CheckPortalByPlayerInLoby(unsigned short& usPlayerID);
	bool CheckPortalByPlayerInDungeon(unsigned short& usPlayerID);
	bool CheckPortalByPlayerInBoss(unsigned short& usPlayerID);



	bool CheckCollisionManPlayerAttack1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionManPlayerAttack2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionManPlayerSkill1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);	//칼 떨어지는거
	bool CheckCollisionManPlayerSkill2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);	//땅 갈라지는거
	bool CheckCollisionManPlayerSkill3(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionManPlayerSkill4(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionManPlayerSkill5(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);

	bool CheckCollisionWoManPlayerAttack1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionWoManPlayerAttack2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionWoManPlayerSkill1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionWoManPlayerSkill2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionWoManPlayerSkill3(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionWoManPlayerSkill4(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionWoManPlayerSkill5(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID);

	bool CheckCollisionPlayerByBossLegAttack(unsigned short& usPlayerID);
	bool CheckCollisionPlayerByBossTailAttack(unsigned short& usPlayerID);
	bool CheckCollisionPlayerByBossFlyAttack(unsigned short& usPlayerID);
	bool CheckCollisionPlayerByBossFireBreath(unsigned short& usPlayerID);
};