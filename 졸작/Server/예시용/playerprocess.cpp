#include "stdafx.h"
#include "playerprocess.h"
#include "network.h"
#include "manager.h"
#include "monsterprocess.h"

CPlayerProcess::CPlayerProcess()
{
	if (!CProcess::m_pPlayerPool) {
		CProcess::m_pPlayerPool = new class CPlayerPool();
		BindPacketProcess();
		BindUpdateViewListFuntion();
	}
}

CPlayerProcess::~CPlayerProcess()
{
	if (m_pPlayerPool)
		SAFE_DELETE(m_pPlayerPool);
}

void CPlayerProcess::AcceptClient(const SOCKET& sSOCKET, unsigned short usID)
{
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPlayerSocket(sSOCKET);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPlayerID(usID);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetRecvState();

	/*/////////////////////////////
	DataBase X
	////////////////////////////*/
	/*s_DataBaseEvent sample_event;
	sample_event.charcter_ = 0;
	sample_event.client_num_ = usID;
	sample_event.exp_ = 100;
	sample_event.hp_ = 1000;
	sample_event.level_ = 5;
	sample_event.map_ = 0;
	sample_event.money_ = 1000;
	sample_event.mp_ = 1000;
	sample_event.posx_ = 5000.0f;
	sample_event.posy_ = 516.0f;
	sample_event.posz_ = 5000.0f;
	sample_event.hp_portion_ = 10;
	sample_event.mp_portion_ = 10;
	sample_event.repatriation_ = 10;
	PlayerLogIn(sample_event);*/
}

void CPlayerProcess::Recv(unsigned short usID, DWORD dwSize, UCHAR* ucPacket)
{
	UCHAR* packet = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->RecvEvent(dwSize, ucPacket);
	if (packet != NULL) {
		m_fpPacketProcess[ucPacket[1]](usID, ucPacket);
		CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetRecvState();
	}
	else
		return;
}

void CPlayerProcess::PlayerMapSwap(unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ClearListElement();

	DeleteListElement(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap(), usID);
	
	for (auto& au : viewlist) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPlayerConnection();
		if (!is_connect) continue;
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
		}
		CManager::SendPlayerRemoveData(usID, au);
	}
}

void CPlayerProcess::PlayerDisconnect(unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPlayerConnect(false);

	/*/////////////////////////////
	DataBase
	////////////////////////////*/
	if (!CData_Base::GetInstance()->GetIsLogin(usID)) {
		return;
	}

	DirectX::XMFLOAT3 pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	char map = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap();

	/*/////////////////////////////
	DataBase 
	////////////////////////////*/
	s_POSSESSION_ITEM player_item_data = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPossessionItem();
	s_DataBaseEvent data_event{ e_Data_Base_State::e_LogOut,usID,CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel(),CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp(), CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHP() ,CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMP(), pos.x , pos.y, pos.z, map,  CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMoney(),  CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetIsPlayer(),player_item_data.m_cHpPortion,player_item_data.m_cMpPortion,player_item_data.m_cRepatriation };
	CData_Base::GetInstance()->InsertToDataBaseQueue(data_event);


	if(ExistListElement(map, usID))
		DeleteListElement(map, usID);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CManager::SendPlayerDisconnect(usID);
	closesocket(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerSokcet());
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InitClientData();

}

void CPlayerProcess::PlayerLogInFail(unsigned short usID)
{
	s_SC_LOGIN_STATE_PACKET packet;
	packet.m_bSize = sizeof(s_SC_LOGIN_STATE_PACKET);
	packet.m_bType = SC_LOGIN_FAIL;
	CManager::SendEvent(usID, &packet);
}

void CPlayerProcess::PlayerLogIn(s_DataBaseEvent& event)
{
	s_SC_LOGIN_STATE_PACKET packet;
	packet.m_bSize = sizeof(s_SC_LOGIN_STATE_PACKET);
	packet.m_bType = SC_LOGIN_SUCCESS;
	CManager::SendEvent(event.client_num_, &packet);

	concurrency::concurrent_unordered_set<unsigned short>sample_list;

	InsertListElement(event.map_, event.client_num_);

	s_POSSESSION_ITEM sample_item_data;
	sample_item_data.m_cHpPortion = event.hp_portion_;
	sample_item_data.m_cMpPortion = event.mp_portion_;
	sample_item_data.m_cRepatriation = event.repatriation_;

	s_SC_FIRST_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_FIRST_STATUS_PACKET);
	status_packet.m_bType = SC_LOGIN;
	status_packet.m_usId = event.client_num_;
	status_packet.m_cMap = event.map_;
	status_packet.m_fX = event.posx_;
	status_packet.m_fY = event.posy_;
	status_packet.m_fZ = event.posz_;
	status_packet.m_iExp = event.exp_;
	status_packet.m_iMaxEXP = event.level_ * 1000;
	status_packet.m_iMaxHP = event.level_ * 1000;
	status_packet.m_iMaxMP = event.level_ * 1000;
	status_packet.m_iHP = event.hp_;
	status_packet.m_iLevel = event.level_;
	status_packet.m_iMoney = event.money_;
	status_packet.m_iMP = event.mp_;
	status_packet.m_cObjectType = event.charcter_;
	status_packet.m_cHpPortion = event.hp_portion_;
	status_packet.m_cMpPortion = event.mp_portion_;
	status_packet.m_cRepatriation = event.repatriation_;

	CManager::SendEvent(event.client_num_, &status_packet);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetPos(DirectX::XMFLOAT3(event.posx_, event.posy_, event.posz_));
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLevel(event.level_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetMP(event.mp_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetHP(event.hp_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetMap(event.map_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetMoney(event.money_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetExp(event.exp_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLimitEXP(status_packet.m_iMaxEXP);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLimitHP(status_packet.m_iMaxHP);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLimitMP(status_packet.m_iMaxMP);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetOOBB(DirectX::XMFLOAT3(event.posx_, event.posy_, event.posz_));
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetIsPlayer(event.charcter_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetPlayerConnect(true);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetPossessionItem(sample_item_data);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetAtk(BASIC_PLAYER_ATK * event.level_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetAmr(BASIC_PLAYER_AMR * event.level_);

	CopyBefore(event.map_, sample_list);
	CManager::SendPlayerAcceptData(sample_list, event.client_num_);

	m_fpUpdateViewList[event.map_](event.client_num_);
}

void CPlayerProcess::ClientLoginInput(unsigned short usID, UCHAR* ucPacket)
{
	s_pCS_LOGIN_PACKET packet = reinterpret_cast<s_pCS_LOGIN_PACKET>(ucPacket);
	wchar_t input_id[MAX_STR_LEN];
	wcsncpy_s(input_id, MAX_STR_LEN, packet->m_wcPASS, (UINT32)wcslen(packet->m_wcPASS));
	s_DataBaseEvent data_event{ e_Data_Base_State::e_Login,usID,packet->m_wcID, input_id };
	/*/////////////////////////////
	DataBase
	////////////////////////////*/
	CData_Base::GetInstance()->InsertToDataBaseQueue(data_event);
}

void CPlayerProcess::PlayerAcceptMap(char cMap, unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>sample_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->StatResetForSwapMap();
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMap(cMap);
	CManager::SendMapSwap(cMap, usID);
	InsertListElement(cMap, usID);
	CopyBefore(cMap, sample_list);
	CManager::SendPlayerAcceptData(sample_list, usID);
	m_fpUpdateViewList[cMap](usID);
}

void CPlayerProcess::PlayerPos(unsigned short usID, UCHAR* ucPacket)
{
	s_pCS_POS_PACKET packet = reinterpret_cast<s_pCS_POS_PACKET>(ucPacket);
	DirectX::XMFLOAT3 sample_pos = DirectX::XMFLOAT3(packet->m_fX, packet->m_fY, packet->m_fZ);
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetWorld();

	xmf4x4World._31 = packet->m_Dir.x; xmf4x4World._32 = packet->m_Dir.y; xmf4x4World._33 = packet->m_Dir.z;
	XMFLOAT3 xmf3Right = Vector3::CrossProduct(XMFLOAT3(xmf4x4World._21, xmf4x4World._22, xmf4x4World._23), packet->m_Dir, true);
	XMFLOAT3 xmf3Up = Vector3::CrossProduct(packet->m_Dir, xmf3Right, true);
	xmf4x4World._21 = xmf3Up.x; xmf4x4World._22 = xmf3Up.y; xmf4x4World._23 = xmf3Up.z;
	xmf4x4World._11 = xmf3Right.x; xmf4x4World._12 = xmf3Right.y; xmf4x4World._13 = xmf3Right.z;
	xmf4x4World._41 = sample_pos.x, xmf4x4World._42 = sample_pos.y, xmf4x4World._43 = sample_pos.z;

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetWorld(xmf4x4World);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->TransFormedOOBB();

	m_fpUpdateViewList[CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap()](usID);
}

void CPlayerProcess::PlayerLook(unsigned short usID, UCHAR* ucPacket)
{
	s_pCS_PLAYER_DIR_PACKET packet = reinterpret_cast<s_pCS_PLAYER_DIR_PACKET>(ucPacket);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLookVec(packet->m_Dir);
	m_fpUpdateViewList[CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap()](usID);
}

void CPlayerProcess::PlayerUseHPPortion(unsigned short usID)
{
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down)
		return;

	int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHP();
	int player_limit_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLimitHP();
	if (player_hp >= player_limit_hp || player_hp <= 0) return;

	char player_item_cnt = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHPPortion();
	if (player_item_cnt <= 0) return;

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetHP(player_limit_hp);

	player_item_cnt -= 1;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetHPPortion(player_item_cnt);

	s_SC_CHANGE_STATUS_PACKET hp_packet;
	hp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	hp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
	hp_packet.m_iAny = player_limit_hp;
	hp_packet.m_usId = usID;

	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(player_list);
	for (auto& player : player_list) {
		bool connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerConnection();
		if (!connect) continue;
		CManager::SendEvent(player, &hp_packet);
	}

	s_SC_CHANGE_ITEM_PACKET item_packet;
	item_packet.m_bSize = sizeof(s_SC_CHANGE_ITEM_PACKET);
	item_packet.m_bType = SC_HP_PORTION_CHANGE;
	item_packet.m_cAny = player_item_cnt;
	CManager::SendEvent(usID, &item_packet);
}

void CPlayerProcess::PlayerUseMPPortion(unsigned short usID)
{
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down)
		return;

	int player_mp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMP();
	int player_limit_mp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLimitMP();
	if (player_mp >= player_limit_mp || player_mp <= 0) return;

	char player_item_cnt = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMPPortion();
	if (player_item_cnt <= 0) return;

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMP(player_limit_mp);

	player_item_cnt -= 1;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMPPortion(player_item_cnt);

	s_SC_CHANGE_STATUS_PACKET mp_packet;
	mp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	mp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MP;
	mp_packet.m_iAny = player_limit_mp;
	mp_packet.m_usId = usID;

	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(player_list);
	for (auto& player : player_list) {
		bool connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerConnection();
		if (!connect) continue;
		CManager::SendEvent(player, &mp_packet);
	}

	s_SC_CHANGE_ITEM_PACKET item_packet;
	item_packet.m_bSize = sizeof(s_SC_CHANGE_ITEM_PACKET);
	item_packet.m_bType = SC_MP_PORTION_CHANGE;
	item_packet.m_cAny = player_item_cnt;
	CManager::SendEvent(usID, &item_packet);
}

void CPlayerProcess::PlayerUseRepartriation(unsigned short usID)
{
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down)
		return;

	char player_item_cnt = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetRepatriation();
	if (player_item_cnt <= 0) return;
	player_item_cnt -= 1;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetRepatriation(player_item_cnt);

	s_SC_CHANGE_ITEM_PACKET item_packet;
	item_packet.m_bSize = sizeof(s_SC_CHANGE_ITEM_PACKET);
	item_packet.m_bType = SC_REPATRIATION_CHANGE;
	item_packet.m_cAny = player_item_cnt;
	CManager::SendEvent(usID, &item_packet);

	this->PlayerMapSwap(usID); this->PlayerAcceptMap(MAP_VILLAGE, usID);
}

void CPlayerProcess::PlayerGetHPPortion(unsigned short usID)
{
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap() != MAP_VILLAGE)
		return;

	int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMoney();
	player_money -= HP_PORTION_PRICE;
	if (player_money < 0) return;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMoney(player_money);

	char player_item_cnt = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHPPortion();
	++player_item_cnt;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetHPPortion(player_item_cnt);

	s_SC_CHANGE_ITEM_PACKET item_packet;
	item_packet.m_bSize = sizeof(s_SC_CHANGE_ITEM_PACKET);
	item_packet.m_bType = SC_HP_PORTION_CHANGE;
	item_packet.m_cAny = player_item_cnt;
	CManager::SendEvent(usID, &item_packet);

	s_SC_CHANGE_STATUS_PACKET money_packet;
	money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
	money_packet.m_iAny = player_money;
	money_packet.m_usId = usID;
	CManager::SendEvent(usID, &money_packet);
}

void CPlayerProcess::PlayerGetMPPortion(unsigned short usID)
{
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap() != MAP_VILLAGE)
		return;

	int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMoney();
	player_money -= MP_PORTION_PRICE;
	if (player_money < 0) return;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMoney(player_money);

	char player_item_cnt = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMPPortion();
	++player_item_cnt;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMPPortion(player_item_cnt);

	s_SC_CHANGE_ITEM_PACKET item_packet;
	item_packet.m_bSize = sizeof(s_SC_CHANGE_ITEM_PACKET);
	item_packet.m_bType = SC_MP_PORTION_CHANGE;
	item_packet.m_cAny = player_item_cnt;
	CManager::SendEvent(usID, &item_packet);

	s_SC_CHANGE_STATUS_PACKET money_packet;
	money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
	money_packet.m_iAny = player_money;
	money_packet.m_usId = usID;
	CManager::SendEvent(usID, &money_packet);
}

void CPlayerProcess::PlayerGetRepartriation(unsigned short usID)
{
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap() != MAP_VILLAGE)
		return;

	int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMoney();
	player_money -= REPATRIATION_PRICE;
	if (player_money < 0) return;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMoney(player_money);

	char player_item_cnt = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetRepatriation();
	++player_item_cnt;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetRepatriation(player_item_cnt);

	s_SC_CHANGE_ITEM_PACKET item_packet;
	item_packet.m_bSize = sizeof(s_SC_CHANGE_ITEM_PACKET);
	item_packet.m_bType = SC_REPATRIATION_CHANGE;
	item_packet.m_cAny = player_item_cnt;
	CManager::SendEvent(usID, &item_packet);

	s_SC_CHANGE_STATUS_PACKET money_packet;
	money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
	money_packet.m_iAny = player_money;
	money_packet.m_usId = usID;
	CManager::SendEvent(usID, &money_packet);
}

void CPlayerProcess::PlayerAttack(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Attack_1)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Attack);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendAttackData(usID);
}

void CPlayerProcess::PlayerAttack2(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Attack_2)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Attack2);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendAttack2Data(usID);
}

void CPlayerProcess::PlayerSkill1(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_1)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill1);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill1Data(usID);
}

void CPlayerProcess::PlayerSkill2(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_2)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill2);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill2Data(usID);
}

void CPlayerProcess::PlayerSkill3(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_3)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill3);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill3Data(usID);
}

void CPlayerProcess::PlayerSkill4(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_4)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill4);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill4Data(usID);
}

void CPlayerProcess::PlayerSkill5(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_5)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill5);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill5Data(usID);
}

void CPlayerProcess::PlayerIdle(unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Idle);
	CManager::SendIdleData(usID);
}

void CPlayerProcess::UpdateViewListInVillage(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	CopyBefore(MAP_VILLAGE, login);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::UpdateViewListInLoby(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	CopyBefore(MAP_LOBY, login);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::UpdateViewListInDungeon(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;



	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	CopyBefore(MAP_DUNGEON, login);

	char what_map = DUNGEON_MONSTER;

	for (auto& au : CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map]) {
		if (au.second->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 other_pos = au.second->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, MONSTER_RANGE)) {
			after.insert(au.first + MAX_PLAYER);
			continue;
		}
	}

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			if (au >= MAX_PLAYER) {
				unsigned short monster_id = au - MAX_PLAYER;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetWakeUp()) {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetWakeUp(true);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetState(e_ObjectState::e_Object_Idle);
					CProcess::PostEvent(what_map, monster_id, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					state_packet.m_usId = monster_id;
					CManager::SendEvent(usID, &state_packet);
				}
				CManager::SendNpcLogInInPlayer(usID, monster_id);
				CManager::SendNpcStatusInPlayer(what_map, usID, monster_id);
				continue;
			}
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (au >= MAX_PLAYER) {
				unsigned short monster_id = au - MAX_PLAYER;
				if (CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetFollow() != NO_TARGET_PLAYER) continue;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetWakeUp()) {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetWakeUp(true);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetState(e_ObjectState::e_Object_Idle);
					CProcess::PostEvent(what_map, monster_id, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
				if (Vector3::ObjectRangeCheck(sample_pos, CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetPos(), MONSTER_DETECT_PLAYER)) {
					//Battle Range -> Move
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetFollow(usID);
				}
				continue;
			}
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				if (au >= MAX_PLAYER) {
					unsigned short monster_id = au - MAX_PLAYER;
					CManager::SendNpcRemoveInPlayer(usID, monster_id);
					continue;
				}
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::UpdateViewListInBoss(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;


	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	CopyBefore(MAP_BOSS, login);

	char what_map = BOSSMAP_MONSTER;

	for (auto& au : CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map]) {
		if (au.second->GetState() == e_ObjectState::e_Object_Down) continue;
		if (au.second->GetState() == e_BossState::e_Boss_Down) continue;
		DirectX::XMFLOAT3 other_pos = au.second->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, MONSTER_RANGE)) {
			after.insert(au.first + MAX_PLAYER);
			continue;
		}
	}

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			if (au >= MAX_PLAYER) {
				unsigned short monster_id = au - MAX_PLAYER;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetWakeUp()) {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetWakeUp(true);
					if (monster_id == BOSS_ID) {
						CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetState(e_BossState::e_Boss_Idle);
						CProcess::PostEvent(what_map, monster_id, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
						CManager::SendBossStatusInPlayer(usID);
					}
					else {
						CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetState(e_ObjectState::e_Object_Idle);
						CProcess::PostEvent(what_map, monster_id, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
					state_packet.m_bType = SC_NPC_IDLE;
					state_packet.m_usId = monster_id;
					CManager::SendEvent(usID, &state_packet);
				}
				CManager::SendNpcLogInInPlayer(usID, monster_id);
				if (monster_id == BOSS_ID) {
					CManager::SendBossStatusInPlayer(usID);
				}
				else {
					CManager::SendNpcStatusInPlayer(what_map, usID, monster_id);
				}
				continue;
			}
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (au >= MAX_PLAYER) {
				unsigned short monster_id = au - MAX_PLAYER;
				if (CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetFollow() != NO_TARGET_PLAYER) continue;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetWakeUp()) {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetWakeUp(true);
					if (monster_id == BOSS_ID) {
						CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetState(e_BossState::e_Boss_Idle);
						CProcess::PostEvent(what_map, monster_id, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
					else {
						CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetState(e_ObjectState::e_Object_Idle);
						CProcess::PostEvent(what_map, monster_id, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
				}
				if (Vector3::ObjectRangeCheck(sample_pos, CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->GetPos(), MONSTER_DETECT_PLAYER)) {
					if (monster_id == BOSS_ID) continue;
					//Battle Range -> Move
					CProcess::m_pMonsterPool->m_ccumMonsterPool[what_map][monster_id]->SetFollow(usID);
				}
				continue;
			}
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				if (au >= MAX_PLAYER) {
					unsigned short monster_id = au - MAX_PLAYER;
					CManager::SendNpcRemoveInPlayer(usID, monster_id);
					continue;
				}
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::PlayerUpdate(char cState, unsigned short usID)
{
	switch (cState)
	{
	case e_ObjectState::e_Object_Down:
	{
		PlayerReSpawn(usID);
		break;
	}
	default:
		break;
	}
}

void CPlayerProcess::PlayerReSpawn(unsigned short usID)
{
	char map_num = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap();

	//CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ClearListElement();	//이걸 해야하나?

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ReSpawn();

	s_SC_PLAYER_RESPAWN_PACKET packet;
	packet.m_bSize = sizeof(s_SC_PLAYER_RESPAWN_PACKET);
	packet.m_bType = SC_PLAYER_RESPAWN;
	packet.m_iHP = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHP();
	packet.m_iMP = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMP();
	packet.m_iExp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
	CManager::SendEvent(usID, &packet);

	//원래 뷰리스트 업뎃을 여기다 넣었음
	m_fpUpdateViewList[CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap()](usID);
}

bool CPlayerProcess::CalculateInteraction(unsigned short usID, char cType)
{
	char map = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap();
	if (map != MAP_DUNGEON && map != MAP_BOSS) {
		return true;
	}
	
	map -= 2;

	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyMonsterList(monster_list);

	std::chrono::high_resolution_clock::time_point cool_time = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + std::chrono::milliseconds(2000);
	std::chrono::high_resolution_clock::time_point current_time = std::chrono::high_resolution_clock::now();
	if (cool_time > current_time) return false;

	char is_man = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetIsPlayer();

	int for_skill_mp = 50;
	int mp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMP() - for_skill_mp;
	////////////////////////////////
	//수정할거
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetInvincibility()) mp = 100;
	
	if (mp <= 0) {
		return false;
	}
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMP(mp);
	s_SC_CHANGE_STATUS_PACKET after_mp_packet;
	after_mp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	after_mp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MP;
	after_mp_packet.m_usId = usID;
	after_mp_packet.m_iAny = mp;
	CManager::SendEvent(usID, &after_mp_packet);

	CProcess::m_fpAttackEvent[is_man][cType](map, usID);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	return true;
}
