#pragma once
#include "stdafx.h"

class CMap_Boss : public CTemplateSingleton<CMap_Boss> {

private:
	int m_iNumObject = 0;
	std::multimap<std::string, DirectX::BoundingOrientedBox>m_mmObjectBoundingBox;
public:
	explicit CMap_Boss();
	~CMap_Boss();

public:
	bool CheckMapObjectCollision(DirectX::BoundingOrientedBox& ObjectOOBB);
	bool CheckMapPortal(DirectX::BoundingOrientedBox& ObjectOOBB);
};