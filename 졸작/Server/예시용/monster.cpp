#include "stdafx.h"
#include "monster.h"
#include "randomMt.h"

CMonster::CMonster()
{

}

CMonster::~CMonster()
{

}

void CMonster::Move(const float& fTimeDistance)
{
	this->SetPos(Vector3::Add(this->GetPos(), Vector3::Add(DirectX::XMFLOAT3(0, 0, 0), this->GetLookVector(), MONSTER_IDLE_MOVE_SPEED * fTimeDistance)));
	
}

void CMonster::ReSpawn()
{
	int hp = 0;
	int exp = 0;
	int level = 0;
	int money = 0;
	int atk = 0;
	int amr = 0;

	DirectX::XMFLOAT3 first_pos = GetFirstPos();
	DirectX::XMFLOAT4X4 xmf4World = Matrix4x4::Identity();

	xmf4World._11 = 1.0f; xmf4World._12 = 0.0f; xmf4World._13 = 0.0f;
	xmf4World._21 = 0.0f; xmf4World._22 = 1.0f;xmf4World._23 = 0.0f;
	xmf4World._31 = 0.0f; xmf4World._32 = 0.0f; xmf4World._33 = 1.0f;
	xmf4World._41 = first_pos.x; xmf4World._42 = first_pos.y; xmf4World._43 = first_pos.z;

	if (GetIsPlayer() == e_ObjectType::e_Monster_Peon) {
		level = rand() % 5 + 1;
		exp = level * BASIC_MONSTER_EXP;
		hp = level * BASIC_MONSTER_HP;
		atk = level * BASIC_MONSTER_ATK;
		amr = level * BASIC_MONSTER_AMR;
		money = RAND_MONEY(e_ObjectType::e_Monster_Peon, level);
		SetLevel(level);
		SetMoney(money);
		SetExp(exp);
		SetHP(hp);
		SetLimitHP(hp);
		SetAmr(amr);
		SetAtk(atk);
		SetState(e_ObjectState::e_Object_Idle);
	}
	else if (GetIsPlayer() == e_ObjectType::e_Monster_Lord) {
		level = rand() % 10 + 5;
		exp = level * BASIC_MONSTER_EXP;
		hp = level * BASIC_MONSTER_HP;
		atk = level * BASIC_MONSTER_ATK;
		amr = level * BASIC_MONSTER_AMR;
		money = RAND_MONEY(e_ObjectType::e_Monster_Lord, level);
		SetLevel(level);
		SetMoney(money);
		SetExp(exp);
		SetHP(hp);
		SetLimitHP(hp);
		SetAmr(amr);
		SetAtk(atk);
		SetState(e_ObjectState::e_Object_Idle);
	}
	else if (GetIsPlayer() == e_ObjectType::e_Monster_Grunt) {
		level = rand() % 15 + 10;
		exp = level * BASIC_MONSTER_EXP;
		hp = level * BASIC_MONSTER_HP;
		atk = level * BASIC_MONSTER_ATK;
		amr = level * BASIC_MONSTER_AMR;
		money = RAND_MONEY(e_ObjectType::e_Monster_Grunt, level);
		SetLevel(level);
		SetMoney(money);
		SetExp(exp);
		SetHP(hp);
		SetLimitHP(hp);
		SetAmr(amr);
		SetAtk(atk);
		SetState(e_ObjectState::e_Object_Idle);
	}
	else if (GetIsPlayer() == e_ObjectType::e_Monster_Boss) {
		level = rand() % 20 + 15;
		exp = level * BASIC_MONSTER_EXP;
		hp = level * BASIC_MONSTER_HP;
		atk = level * BASIC_MONSTER_ATK;
		amr = level * BASIC_MONSTER_AMR;
		money = RAND_MONEY(e_ObjectType::e_Monster_Boss, level);
		SetLevel(level);
		SetMoney(money);
		SetExp(exp);
		SetHP(hp);
		SetLimitHP(hp);
		SetAmr(amr);
		SetAtk(atk);
		SetState(e_BossState::e_Boss_Idle);
	}

	SetWorld(xmf4World);
	SetOOBB(first_pos);
	SetWakeUp(false);
	SetFollow(NO_TARGET_PLAYER);
	ClearEvent();
}