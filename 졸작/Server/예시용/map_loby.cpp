#include "stdafx.h"
#include "map_loby.h"

CMap_Loby::CMap_Loby()
{
	std::string sample_id;
	DirectX::XMFLOAT3 sample_center;
	DirectX::XMFLOAT3 sample_extent;
	DirectX::XMFLOAT3 sample_pos;
	DirectX::XMFLOAT3 sample_ori;

	m_mmObjectBoundingBox.clear();

	std::ifstream in("../MapData/TopInsideObject.txt");
	if (!in.is_open()) {
		std::cout << "TopObejct Open Failed" << std::endl;
		return;
	}

	in >> m_iNumObject;
	for (int i = 0; i < m_iNumObject; ++i) {
		if (in.eof())
			break;
		else {
			in >> sample_id;
			in >> sample_center.x;
			in >> sample_center.y;
			in >> sample_center.z;
			in >> sample_extent.z;
			in >> sample_extent.y;
			in >> sample_extent.x;
			in >> sample_pos.x;
			in >> sample_pos.y;
			in >> sample_pos.z;
			in >> sample_ori.x;
			in >> sample_ori.y;
			in >> sample_ori.z;
			m_mmObjectBoundingBox.insert(std::make_pair(sample_id, DirectX::BoundingOrientedBox(sample_pos, sample_extent, DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f))));
		}
	}
	in.close();
}

CMap_Loby::~CMap_Loby()
{
	m_mmObjectBoundingBox.clear();
}

bool CMap_Loby::CheckMapObjectCollision(const DirectX::BoundingOrientedBox& ObjectOOBB)
{
	DirectX::BoundingOrientedBox object = ObjectOOBB;
	for (auto& au : m_mmObjectBoundingBox) {
		if (Vector3::ObjectRangeCheck(object.Center, au.second.Center, 500.f)) {
			if (au.second.Intersects(object)) {
				return true;
			}
		}
	}
	return false;
}

bool CMap_Loby::CheckMapPortal(DirectX::BoundingOrientedBox& ObjectOOBB)
{
	std::multimap<std::string, DirectX::BoundingOrientedBox>::iterator it;

	it = m_mmObjectBoundingBox.find(std::string("Door"));
	if (it != m_mmObjectBoundingBox.end()) {
		if (it->second.Intersects(ObjectOOBB)) {
			return true;
		}
		else return false;
	}
	else return false;
}
