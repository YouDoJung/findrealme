#pragma once
#include "stdafx.h"
#include "network.h"


class CManager{
public:
	static void SendEvent(unsigned short usID, void* pPACKET);

	static void SendMapSwap(char cMap, unsigned short usID);

	static void SendPlayerLogIn(unsigned short usID);
	static void SendPlayerAcceptData(concurrency::concurrent_unordered_set<unsigned short>& list, unsigned short usID);/*FOR FIRST LOADING*/
	static void SendPlayerStatusData(unsigned short usPlayer, unsigned short usOther);
	static void SendPlayerPosData(unsigned short usPlayer, unsigned short usOther);
	static void SendPlayerLook(unsigned short usPlayer);
	static void SendPlayerRemoveData(unsigned short usPlayer, unsigned short usOther);
	static void SendPlayerDisconnect(unsigned short usPlayer);



	static void SendAttackData(unsigned short usPlayer);
	static void SendAttack2Data(unsigned short usPlayer);
	
	static void SendSkill1Data(unsigned short usPlayer);
	static void SendSkill2Data(unsigned short usPlayer);
	static void SendSkill3Data(unsigned short usPlayer);
	static void SendSkill4Data(unsigned short usPlayer);
	static void SendSkill5Data(unsigned short usPlayer);

	static void SendIdleData(unsigned short usPlayer);

	static void SendNpcLogInInPlayer(unsigned short usPlayer, unsigned short usMonster);
	static void SendNpcStatusInPlayer(char cMap, unsigned short usPlayer, unsigned short usMonster);
	static void SendBossStatusInPlayer(unsigned short usPlayer);
	static void SendNpcRemoveInPlayer(unsigned short usPlayer, unsigned short usMonster);




};