#include "stdafx.h"
#include "monsterprocess.h"
#include "manager.h"
#include "map_dungeon.h"
#include "map_boss.h"

CMonsterProcess::CMonsterProcess()
{
	if (!CProcess::m_pMonsterPool) {
		CProcess::m_pMonsterPool = new class CMonsterPool();
		BindMonsterUpdate();
		BindMonsterDamaged();
		BindBossAttackPlayer();
	}
}

CMonsterProcess::~CMonsterProcess()
{
	if (m_pMonsterPool)
		SAFE_DELETE(m_pMonsterPool);
}

void CMonsterProcess::CheckPlayerInRange(char cMap, concurrency::concurrent_unordered_set<unsigned short>& ccusLogin, concurrency::concurrent_unordered_set<unsigned short>& ccusList, unsigned short usID)
{
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	for (auto& au : ccusLogin) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPlayerConnection();
		if (!is_connect) continue;
		//if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetState() == e_ObjectState::e_Object_Down)continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_RANGE)) {
			ccusList.insert(au);
		}
	}
}

bool CMonsterProcess::CheckPlayerInRangeForBossAttack(unsigned short usID, DirectX::XMFLOAT3& xmf3BossPos, float fDistance)
{
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	xmf3BossPos.y = 516.0f;
	if (Vector3::ObjectRangeCheck(player_pos, xmf3BossPos, fDistance)) {
		return true;
	}
	return false;
}

void CMonsterProcess::MoveToTargetPlayer(unsigned short usTargetID)
{
	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	float boss_y = xmf3Position.y;
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	float fNextRotate = 0.0f;
	if (fDotProduct > -0.95) {
		fNextRotate = xmf3CrossProduct.y * BOSS_TURN_SPEED;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	}
	//회전시킨 후의 Look Vec을 설정한다.
	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//여기서 좌표를 설정 한다.

	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * BOSS_TARGET_MOVE_SPEED, boss_y, xmf4x4World._43 + xmf3ToTarget.z * BOSS_TARGET_MOVE_SPEED);
	xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetWorld(xmf4x4World);
	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->TransFormedOOBB();

	//s_MonsterEvent sample_event;

	////월드 행렬을 가져온다.
	//DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	////월드 행렬을 통해서 좌표를 가져온다.
	//DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	//float boss_y = xmf3Position.y;
	//DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	//DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
	//float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	//float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	//DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	//float fNextRotate = 0.0f;
	//if (fDotProduct > -0.95) {
	//	fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
	//	//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
	//	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
	//	//몬스터를 회전시킨다.
	//	xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	//}
	////회전시킨 후의 Look Vec을 설정한다.
	//xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	////여기서 좌표를 설정 한다.
	//xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * BOSS_TARGET_MOVE_SPEED * fUpdateTime, boss_y, xmf4x4World._43 + xmf3ToTarget.z * BOSS_TARGET_MOVE_SPEED * fUpdateTime);

	//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);

	//if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetEvent(sample_event)) {
	//	float monster_rotate;
	//	DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
	//	monster_rotate = sample_event.m_fRotate;
	//	//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
	//	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
	//	//몬스터를 회전시킨다.
	//	xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	//	//회전시킨 후의 Look Vec을 설정한다.
	//	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//	//여기서 좌표를 설정 한다.
	//	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * BOSS_TARGET_MOVE_SPEED * fUpdateTime, boss_y, xmf4x4World._43 + xmf3ToTarget.z * BOSS_TARGET_MOVE_SPEED * fUpdateTime);
	//	xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetWorld(xmf4x4World);
	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->TransFormedOOBB();
	//	std::cout << xmf3Position.x << '\t' << xmf3Position.y << '\t' << xmf3Position.z << '\n';
	//}
}

void CMonsterProcess::MoveToTargetPlayer(char cMap, unsigned short usTargetID, unsigned short usID)
{
	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표
	DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	float fNextRotate = 0.0f;
	if (fDotProduct > -0.95) {
		fNextRotate = xmf3CrossProduct.y * MONSTER_TURN_SPEED;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	}
	//회전시킨 후의 Look Vec을 설정한다.
	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//여기서 좌표를 설정 한다.
	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED);

	xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->TransFormedOOBB();

	//if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
	//	DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
	//	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	//	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	//	DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	//	float fNextRotate = 0.0f;
	//	if (fDotProduct > -0.95) {
	//		fNextRotate = xmf3CrossProduct.y * MONSTER_TURN_SPEED;
	//		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
	//		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
	//		//몬스터를 회전시킨다.
	//		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	//	}
	//	//회전시킨 후의 Look Vec을 설정한다.
	//	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//	//여기서 좌표를 설정 한다.
	//	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED);

	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
	//	
	//}

	//if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
	//	float monster_rotate;
	//	DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
	//	monster_rotate = sample_event.m_fRotate;
	//	//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
	//	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
	//	//몬스터를 회전시킨다.
	//	xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	//	//회전시킨 후의 Look Vec을 설정한다.
	//	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//	//여기서 좌표를 설정 한다.
	//	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED);
	//	xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->TransFormedOOBB();
	//}
}

void CMonsterProcess::MoveToFirstPos(char cMap, unsigned short usID)
{
	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();   // 플레이어 좌표
	DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	float fNextRotate = 0.0f;
	if (fDotProduct > -0.95) {
		fNextRotate = xmf3CrossProduct.y * MONSTER_TURN_SPEED;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	}
	//회전시킨 후의 Look Vec을 설정한다.
	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//여기서 좌표를 설정 한다.
	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED);

	xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->TransFormedOOBB();

	//if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, 10)) {
	//	DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
	//	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	//	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	//	DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	//	float fNextRotate = 0.0f;
	//	if (fDotProduct > -0.95) {
	//		fNextRotate = xmf3CrossProduct.y * MONSTER_TURN_SPEED;
	//		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
	//		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
	//		//몬스터를 회전시킨다.
	//		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	//	}
	//	//회전시킨 후의 Look Vec을 설정한다.
	//	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//	//여기서 좌표를 설정 한다.
	//	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED);

	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
	//}

	//if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
	//	float monster_rotate;
	//	DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
	//	monster_rotate = sample_event.m_fRotate;
	//	//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
	//	XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
	//	//몬스터를 회전시킨다.
	//	xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	//	//회전시킨 후의 Look Vec을 설정한다.
	//	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	//	//여기서 좌표를 설정 한다.
	//	xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED);
	//	xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
	//	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->TransFormedOOBB();
	//}
}

void CMonsterProcess::FollowEvent(char cMap, unsigned short usID, unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>before_login;
	concurrency::concurrent_unordered_set<unsigned short>after_login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	char this_map = cMap + 2;
	unsigned short this_monster_id = usID + MAX_PLAYER;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, before_login);
	CheckPlayerInRange(cMap, before_login, before, usID);

	s_SC_NPC_LOGIN_PACKET login_packet;
	s_SC_NPC_STATUS_PACKET status_packet;
	s_SC_NPC_POS_PACKET npc_pos_packet;
	s_SC_DISCONNECT_PACKET remove_packet;
	s_SC_ANIMATION_PACKET state_packet;

	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;

	npc_pos_packet.m_bSize = sizeof(s_SC_NPC_POS_PACKET);
	npc_pos_packet.m_bType = SC_NPC_POS;
	npc_pos_packet.m_usId = usID;

	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = usID;
	state_packet.m_bType = SC_NPC_FOLLOW;

	//쫓는 와중에 플레이어가 죽으면 어택과 동시에 일어나면 어택부분에서 NO_TARGET으로 바꿔준 상태라 이상한 곳을 참조하게 된다.
	//여기서 체크해주기.
	//그니깐 뭔가 동시성 떄문에? shared_mutex를 쓰니 이렇게 되는 거임 동시에 읽을 수는 있어서.

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();

	if (usTargetID == NO_TARGET_PLAYER || (usTargetID != NO_TARGET_PLAYER && CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down)) {
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	MoveToTargetPlayer(cMap, usTargetID, usID);

	CopyBefore(this_map, after_login);
	CheckPlayerInRange(cMap, after_login, after, usID);

	sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	status_packet.m_fX = npc_pos_packet.m_fX = sample_pos.x;
	status_packet.m_fZ = npc_pos_packet.m_fZ = sample_pos.z;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = npc_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLookVector();
	status_packet.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	for (auto& pu : before) {
		if (after.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->DeleteListElement(this_monster_id);
			}
			CManager::SendEvent(pu, &remove_packet);
		}
	}

	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down) {
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		for (auto& pu : after) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (before.count(pu) == 0) {
				if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->InsertListElement(this_monster_id);
				}
				CManager::SendEvent(pu, &status_packet);
				CManager::SendEvent(pu, &login_packet);
			}
			else {
				CManager::SendEvent(pu, &npc_pos_packet);
			}
			CManager::SendEvent(pu, &state_packet);
		}
		if (after.empty()) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
		}
		else {
			if (!Vector3::ObjectRangeCheck(sample_pos, first_pos, MONSTER_LIMIT_MOVE_RANGE)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else if (Vector3::ObjectRangeCheck(sample_pos, target_pos, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Attack);
				CProcess::PostEvent(cMap, usID, usTargetID, UPDATE_EVENT, e_ObjectState::e_Object_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else if (!Vector3::ObjectRangeCheck(sample_pos, target_pos, MONSTER_DETECT_PLAYER)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::PostEvent(cMap, usID, usTargetID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
		}
	}
}

void CMonsterProcess::IdleEvent(char cMap, unsigned short usID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	if (range_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
		return;
	}

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = usID;
	state_packet.m_bType = SC_NPC_IDLE;

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();

	unsigned short target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow();
	for (auto& pu : range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &state_packet);	//IDLE패킷을 먼저 보내야 죽었을때 동기화가 된다.
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, sample_pos, MONSTER_DETECT_PLAYER) && CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow() == NO_TARGET_PLAYER) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(pu);
			target_id = pu;
		}
	}

	if (target_id != NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Follow);
		CProcess::PostEvent(cMap, usID, target_id, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
	}
}

void CMonsterProcess::AttackEvent(char cMap, unsigned short usID, unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetHP();
	int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetAmr();
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = usID;
	state_packet.m_bType = SC_NPC_ATTACK;

	s_SC_ANIMATION_PACKET player_state_packet;
	player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	player_state_packet.m_usId = usTargetID;

	s_SC_CHANGE_STATUS_PACKET player_status_packet;
	player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
	player_status_packet.m_usId = usTargetID;
	if (usTargetID == NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}
	if (range_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
	}
	else {
		int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAtk();
		
		///////////////////////////////
		//수정할거
		int after_player_hp = player_hp;
		if (player_amr < monster_atk) {
			after_player_hp = player_hp - (monster_atk - player_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetInvincibility()) after_player_hp = 100;

		if (after_player_hp <= 0) {
			
			player_state_packet.m_bType = SC_PLAYER_DOWN;

			player_status_packet.m_iAny = 0;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(0);

			state_packet.m_bType = SC_NPC_IDLE;

			for (auto& pu : range_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_status_packet);
				CManager::SendEvent(pu, &player_state_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);

			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			//여기서 부활 큐 ㄱㄱ
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() != e_ObjectState::e_Object_Down) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetState(e_ObjectState::e_Object_Down);
				CProcess::PostEvent(cMap, usTargetID, NO_TARGET_PLAYER, PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
			}

		}
		else {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(after_player_hp);
			player_state_packet.m_bType = SC_PLAYER_DAMAGED;

			player_status_packet.m_iAny = after_player_hp;

			for (auto& pu : range_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_state_packet);
				CManager::SendEvent(pu, &player_status_packet);
			}

			if (Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Attack);
				CProcess::PostEvent(cMap, usID, usTargetID,UPDATE_EVENT, e_ObjectState::e_Object_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(800));
			}
			else if (!Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_DETECT_PLAYER)) {
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::PostEvent(cMap, usID, usTargetID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(30));
			}
		}
	}
}

void CMonsterProcess::DownEvent(char cMap, unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	s_SC_DISCONNECT_PACKET remove_packet;
	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	//시체가 사라지도록
	for (auto& player : range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(player, &remove_packet);
	}

	CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Respawn, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
}

void CMonsterProcess::RespawnEvent(char cMap, unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	if (range_list.empty())return;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(true);
	unsigned short this_monster_id = usID + MAX_PLAYER;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));

	s_SC_NPC_LOGIN_PACKET login_packet;
	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	s_SC_NPC_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;
	status_packet.m_fX = monster_pos.x;
	status_packet.m_fZ = monster_pos.z;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();
	status_packet.m_xmf3Dir = xmf3Look;

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;
	state_packet.m_usId = usID;

	for (auto& player: range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->ExistListElement(this_monster_id)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->InsertListElement(this_monster_id);
		}
		CManager::SendEvent(player, &login_packet);
		CManager::SendEvent(player, &status_packet);
		CManager::SendEvent(player, &state_packet);
	}

	CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
}

void CMonsterProcess::HealEvent(char cMap, unsigned short usID)
{
	char state = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState();
	if (state == e_ObjectState::e_Object_Down || state == e_ObjectState::e_Object_Attack || state == e_ObjectState::e_Object_Follow) return;
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	unsigned short this_monster_id = usID + MAX_PLAYER;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	int limit_monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();

	//돌아가는 중에 hp 리셋 시키기
	if (monster_hp < limit_monster_hp) {
		double percent_hp = (double)((double)limit_monster_hp * 0.01);
		monster_hp += (int)percent_hp;
	}
	if (monster_hp >= limit_monster_hp) {
		monster_hp = limit_monster_hp;
	}

	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(monster_hp);

	s_SC_CHANGE_STATUS_PACKET npc_hp_packet;
	npc_hp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	npc_hp_packet.m_bType = SC_CHANGE_STATUS_NPC_HP;
	npc_hp_packet.m_usId = usID;
	npc_hp_packet.m_iAny = monster_hp;

	for (auto& pu : range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &npc_hp_packet);
	}

	if (range_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
	}
	else {
		if (monster_hp != limit_monster_hp) {
			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Heal, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(100));
		}
	}

}

void CMonsterProcess::MoveFirstPosEvent(char cMap, unsigned short usID, unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>before_login;
	concurrency::concurrent_unordered_set<unsigned short>after_login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	s_SC_NPC_LOGIN_PACKET login_packet;
	s_SC_NPC_STATUS_PACKET status_packet;
	s_SC_NPC_POS_PACKET npc_pos_packet;
	s_SC_DISCONNECT_PACKET remove_packet;
	s_SC_ANIMATION_PACKET state_packet;
	
	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;

	npc_pos_packet.m_bSize = sizeof(s_SC_NPC_POS_PACKET);
	npc_pos_packet.m_bType = SC_NPC_POS;
	npc_pos_packet.m_usId = usID;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_FOLLOW;
	state_packet.m_usId = usID;

	char this_map = cMap + 2;
	unsigned short this_monster_id = usID + MAX_PLAYER;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, before_login);
	CheckPlayerInRange(cMap, before_login, before, usID);

	MoveToFirstPos(cMap, usID);

	CopyBefore(this_map, after_login);
	CheckPlayerInRange(cMap, after_login, after, usID);

	int limit_monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();
	status_packet.m_fX = npc_pos_packet.m_fX = sample_pos.x;
	status_packet.m_fZ = npc_pos_packet.m_fZ = sample_pos.z;
	status_packet.m_iHP = monster_hp;
	status_packet.m_iMaxHP = limit_monster_hp;
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = npc_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLookVector();

	for (auto& pu : after) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		if (before.count(pu) == 0) {
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->InsertListElement(this_monster_id);
			}
			CManager::SendEvent(pu, &login_packet);
			CManager::SendEvent(pu, &status_packet);
		}
		else {
			CManager::SendEvent(pu, &npc_pos_packet);
		}
		CManager::SendEvent(pu, &state_packet);
	}

	for (auto& pu : before) {
		if (after.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->DeleteListElement(this_monster_id);
			}
			CManager::SendEvent(pu, &remove_packet);
		}
	}

	if (after.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
	}
	else {
		if (Vector3::ObjectRangeCheck(sample_pos, first_pos, 10.0f)) {
			//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
			//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(limit_monster_hp);
			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
}


void CMonsterProcess::BossFollowEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Follow);

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	s_SC_BOSS_POS_PACKET boss_pos_packet;
	s_SC_ANIMATION_PACKET state_packet;

	boss_pos_packet.m_bSize = sizeof(s_SC_BOSS_POS_PACKET);
	boss_pos_packet.m_bType = SC_BOSS_POS;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;
	state_packet.m_bType = SC_BOSS_RUN;

	
	if (usTargetID == NO_TARGET_PLAYER || (usTargetID != NO_TARGET_PLAYER && CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	MoveToTargetPlayer(usTargetID);

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	boss_pos_packet.m_xmf3Pos = sample_pos;
	boss_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &boss_pos_packet);
		CManager::SendEvent(pu, &state_packet);
	}

	int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

	if (boss_hp <= limit_Boss_hp) {
		if (!m_bIs_Boss_Raid) {
			m_cBefore_Boss_State = e_BossState::e_Boss_Follow;
			//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			sample_pos.y = target_pos.y;
			if (m_bBoss_Next_Attack) {
				if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_TAIL_ATTACK_START_RANGE)) {
					//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					return;
				}
			}
			else {
				if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_LEG_ATTACK_START_RANGE)) {
					//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					return;
				}
			}
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
	else {
		sample_pos.y = target_pos.y;
		if (m_bBoss_Next_Attack) {
			if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_TAIL_ATTACK_START_RANGE)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				return;
			}
		}
		else {
			if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_LEG_ATTACK_START_RANGE)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				return;
			}
		}
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
}

void CMonsterProcess::BossIdleEvent()
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Idle);
	concurrency::concurrent_unordered_set<unsigned short>login_list;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	s_SC_ANIMATION_PACKET state_packet;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;
	state_packet.m_bType = SC_NPC_IDLE;

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();

	unsigned short target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFollow();
	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &state_packet);	//IDLE패킷을 먼저 보내야 죽었을때 동기화가 된다.
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, sample_pos, MONSTER_DETECT_PLAYER) && CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFollow() == NO_TARGET_PLAYER) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(pu);
			target_id = pu;
		}
	}

	if (target_id != NO_TARGET_PLAYER) {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_Idle;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, target_id, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, target_id, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
		}
		else {
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, target_id, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
	else {
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER,UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
	}
}

void CMonsterProcess::BossLegAttackEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	m_bBoss_Next_Attack = true;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_Leg_Attack;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Leg_Attack);
				DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
				DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
				DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

				s_SC_ANIMATION_PACKET state_packet;
				state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
				state_packet.m_usId = BOSS_ID;
				state_packet.m_bType = SC_BOSS_LEG_ATTACK;
				for (auto& pu : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(pu, &state_packet);
					if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
				}
				DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
				DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
				++m_cBossAttackCnt;
				if ((Vector3::ObjectRangeCheck(boss_pos, target_pos, BOSS_TAIL_ATTACK_START_RANGE))) {
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1600));
				}
				else {
					//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1600));
				}
			}
		}
		else{
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Leg_Attack);
			DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
			DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
			DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

			s_SC_ANIMATION_PACKET state_packet;
			state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
			state_packet.m_usId = BOSS_ID;
			state_packet.m_bType = SC_BOSS_LEG_ATTACK;
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
			}
			DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
			DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
			++m_cBossAttackCnt;
			if ((Vector3::ObjectRangeCheck(boss_pos, target_pos, BOSS_TAIL_ATTACK_START_RANGE))) {
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1600));
			}
			else {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1600));
			}
		}
	}
}

void CMonsterProcess::BossMoveFirstPosEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	s_SC_BOSS_STATUS_PACKET status_packet;
	s_SC_ANIMATION_PACKET state_packet;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_BOSS_STATUS;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;

	unsigned short this_monster_id = BOSS_ID + MAX_PLAYER;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	MoveToFirstPos(BOSSMAP_MONSTER, BOSS_ID);

	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	int limit_monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP();

	if (m_bIs_Boss_Fly) {
		state_packet.m_bType = SC_BOSS_FAST_FLY;
	}
	else {
		state_packet.m_bType = SC_BOSS_RUN;
	}
	//돌아가는 중에 hp 리셋 시키기
	if (monster_hp < limit_monster_hp) {
		double percent_hp = limit_monster_hp * 0.5;
		monster_hp += (int)percent_hp;
		if (monster_hp >= limit_monster_hp) {
			monster_hp = limit_monster_hp;
		}
	}

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFirstPos();
	status_packet.m_xmf3Pos = sample_pos;
	status_packet.m_iHP = monster_hp;
	status_packet.m_iMaxHP = limit_monster_hp;
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLevel();
	status_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();

	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &status_packet);
		CManager::SendEvent(pu, &state_packet);
	}

	if (Vector3::ObjectRangeCheck(sample_pos, first_pos, 10.0f)) {
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(limit_monster_hp);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(monster_hp);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
}

void CMonsterProcess::BossTailAttackEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	monster_pos.y = 516.0f;
	if (!Vector3::ObjectRangeCheck(monster_pos, player_pos, BOSS_TAIL_ATTACK_START_RANGE)) {
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	m_bBoss_Next_Attack = false;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_Tail_Attack;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Tail_Attack);
				DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
				DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
				DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

				s_SC_ANIMATION_PACKET state_packet;
				state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
				state_packet.m_usId = BOSS_ID;
				state_packet.m_bType = SC_BOSS_TAIL_ATTACK;
				for (auto& pu : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(pu, &state_packet);
					if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(300));
				}
				DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
				DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

				if (m_cBossAttackCnt >= 3) {
					m_cBossAttackCnt = 0;
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
				}
				else {
					player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
					monster_pos.y = 516.0f;
					if (Vector3::ObjectRangeCheck(monster_pos, player_pos, BOSS_LEG_ATTACK_START_RANGE)) {
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
					}
					else {
						//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
					}
				}
			}
		}
		else {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Tail_Attack);
			DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
			DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
			DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

			s_SC_ANIMATION_PACKET state_packet;
			state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
			state_packet.m_usId = BOSS_ID;
			state_packet.m_bType = SC_BOSS_TAIL_ATTACK;
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(300));
			}
			DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
			DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

			if (m_cBossAttackCnt >= 3) {
				m_cBossAttackCnt = 0;
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
			}
			else {
				player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
				monster_pos.y = 516.0f;
				if (Vector3::ObjectRangeCheck(monster_pos, player_pos, BOSS_LEG_ATTACK_START_RANGE)) {
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
				}
				else {
					//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
				}
			}
		}
	}
}

void CMonsterProcess::BossFlyEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly);

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_Fly;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				m_bIs_Boss_Fly = true;
				DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
				float y_pos = boss_pos.y;

				s_SC_BOSS_FLY_PACKET boss_y_packet;
				boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
				boss_y_packet.m_bType = SC_BOSS_FLY;

				y_pos += 1.0f;
				boss_y_packet.m_fY = y_pos;

				for (auto& pu : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(pu, &boss_y_packet);
				}

				boss_pos.y = y_pos;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

				if (m_bIs_Breath) {
					if (y_pos >= 1000.0f) {
						boss_pos.y = 1000.0f;
						CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
						for (auto& pu : login_list) {
							bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
							if (!is_connect) continue;
							CManager::SendEvent(pu, &boss_y_packet);
						}
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
					}
					else {
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
				}
				else {
					if (y_pos >= 1400.0f) {
						boss_pos.y = 1400.0f;
						CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
						for (auto& pu : login_list) {
							bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
							if (!is_connect) continue;
							CManager::SendEvent(pu, &boss_y_packet);
						}
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
					}
					else {
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
				}
			}
		}
		else {
			m_bIs_Boss_Fly = true;
			DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
			float y_pos = boss_pos.y;

			s_SC_BOSS_FLY_PACKET boss_y_packet;
			boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
			boss_y_packet.m_bType = SC_BOSS_FLY;

			y_pos += 1.0f;
			boss_y_packet.m_fY = y_pos;

			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &boss_y_packet);
			}

			boss_pos.y = y_pos;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

			if (m_bIs_Breath) {
				if (y_pos >= 1000.0f) {
					boss_pos.y = 1000.0f;
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
					for (auto& pu : login_list) {
						bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
						if (!is_connect) continue;
						CManager::SendEvent(pu, &boss_y_packet);
					}
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
				}
				else {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly);
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
			}
			else {
				if (y_pos >= 1400.0f) {
					boss_pos.y = 1400.0f;
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
					for (auto& pu : login_list) {
						bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
						if (!is_connect) continue;
						CManager::SendEvent(pu, &boss_y_packet);
					}
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
				}
				else {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly);
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
			}
		}
	}
}


void CMonsterProcess::BossFlyFollowEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Follow);

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	s_SC_BOSS_POS_PACKET boss_pos_packet;
	s_SC_ANIMATION_PACKET state_packet;

	boss_pos_packet.m_bSize = sizeof(s_SC_BOSS_POS_PACKET);
	boss_pos_packet.m_bType = SC_BOSS_POS;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;
	state_packet.m_bType = SC_BOSS_FAST_FLY;

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFirstPos();

	if (usTargetID == NO_TARGET_PLAYER || (usTargetID != NO_TARGET_PLAYER && CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER,UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	MoveToTargetPlayer(usTargetID);

	sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	boss_pos_packet.m_xmf3Pos = sample_pos;
	boss_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &boss_pos_packet);
		CManager::SendEvent(pu, &state_packet);
	}

	int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

	if (boss_hp <= limit_Boss_hp) {
		if (!m_bIs_Boss_Raid) {
			m_cBefore_Boss_State = e_BossState::e_Boss_Fly_Follow;
			//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			sample_pos.y = target_pos.y;
			if (m_bBoss_Next_Attack) {
				if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_FIRE_BREATH_START_RANGE)) {
					//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_FireBreath, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					return;
				}
			}
			else {
				if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_FLY_ATTACK_START_RANGE)) {
					//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					return;
				}
			}
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
	else {
		sample_pos.y = target_pos.y;
		if (m_bBoss_Next_Attack) {
			if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_FIRE_BREATH_START_RANGE)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_FireBreath, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				return;
			}
		}
		else {
			if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_FLY_ATTACK_START_RANGE)) {
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				return;
			}
		}
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
}

void CMonsterProcess::BossFlyAttackEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	m_bBoss_Next_Attack = true;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_Fly_Attack;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				m_bIs_Breath = false;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Attack);
				DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
				DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
				DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

				s_SC_ANIMATION_PACKET state_packet;
				state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
				state_packet.m_usId = BOSS_ID;
				state_packet.m_bType = SC_BOSS_FLY_ATTACK;
				for (auto& pu : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(pu, &state_packet);
					if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
				}
				DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
				DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

				++m_cBossAttackCnt;

				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(2000));
			}
		}
		else {
			m_bIs_Breath = false;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Attack);
			DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
			DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
			DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

			s_SC_ANIMATION_PACKET state_packet;
			state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
			state_packet.m_usId = BOSS_ID;
			state_packet.m_bType = SC_BOSS_FLY_ATTACK;
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
			}
			DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
			DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

			++m_cBossAttackCnt;

			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(2000));
		}
	}
}

void CMonsterProcess::BossFireBreathEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	monster_pos.y = 516.0f;
	if (!Vector3::ObjectRangeCheck(monster_pos, player_pos, BOSS_FIRE_BREATH_START_RANGE)) {
		//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Follow);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	m_bBoss_Next_Attack = false;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);


	if (usTargetID == NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_FireBreath;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				m_bIs_Breath = true;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_FireBreath);
				DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
				DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
				DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

				s_SC_ANIMATION_PACKET state_packet;
				state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
				state_packet.m_usId = BOSS_ID;
				state_packet.m_bType = SC_BOSS_FIRE_BREATH;
				for (auto& pu : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(pu, &state_packet);
					if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Fire_Breath, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
				}
				DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
				DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(6000));
			}
		}
		else {
			m_bIs_Breath = true;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_FireBreath);
			DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();
			DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
			DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

			s_SC_ANIMATION_PACKET state_packet;
			state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
			state_packet.m_usId = BOSS_ID;
			state_packet.m_bType = SC_BOSS_FIRE_BREATH;
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				if (!CheckPlayerInRangeForBossAttack(pu, monster_pos, CAN_DAMAGED_DURING_BOSS_ATTACK)) continue;
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, pu, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Fire_Breath, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
			}
			DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
			DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(6000));
		}
	}
}

void CMonsterProcess::BossRespawnEvent()
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;

	m_cBossHitCnt = 0;
	m_bIs_Boss_Raid = false;
	m_bIs_Boss_Attack_Player = false;
	m_bIs_Breath = false;
	m_bBoss_Next_Attack = false;
	m_bIs_Boss_Fly = false;
	m_cBossAttackCnt = 0;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty())return;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetWakeUp(true);
	unsigned short this_monster_id = BOSS_ID + MAX_PLAYER;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));

	s_SC_NPC_LOGIN_PACKET login_packet;
	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = BOSS_ID;

	s_SC_BOSS_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_BOSS_STATUS_PACKET);
	status_packet.m_bType = SC_BOSS_STATUS;
	status_packet.m_xmf3Pos = monster_pos;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLevel();
	status_packet.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP();
	status_packet.m_xmf3Dir = xmf3Look;

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;
	state_packet.m_usId = BOSS_ID;

	for (auto& player : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->ExistListElement(this_monster_id)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->InsertListElement(this_monster_id);
		}
		CManager::SendEvent(player, &login_packet);
		CManager::SendEvent(player, &status_packet);
		CManager::SendEvent(player, &state_packet);
	}

	CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
}

void CMonsterProcess::BossFlyDownEvent(unsigned short usTargetID)
{
	char current_boss_state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (current_boss_state == e_BossState::e_Boss_Down) return;
	if (current_boss_state == e_BossState::e_Boss_Raid) return;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Down);

	concurrency::concurrent_unordered_set<unsigned short>login_list;
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		int boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
		int limit_Boss_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 2;

		if (boss_hp <= limit_Boss_hp) {
			if (!m_bIs_Boss_Raid) {
				m_cBefore_Boss_State = e_BossState::e_Boss_Fly_Down;
				//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Raid);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				m_bIs_Boss_Fly = false;
				DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
				float y_pos = boss_pos.y;

				s_SC_BOSS_FLY_PACKET boss_y_packet;
				boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
				boss_y_packet.m_bType = SC_BOSS_FLY_DOWN;

				y_pos -= 1.0f;
				boss_y_packet.m_fY = y_pos;

				for (auto& pu : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(pu, &boss_y_packet);
				}

				boss_pos.y = y_pos;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

				if (m_cBossAttackCnt >= 3) {
					if (y_pos <= 900.0f) {
						boss_pos.y = 900.0f;
						CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
						for (auto& pu : login_list) {
							bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
							if (!is_connect) continue;
							CManager::SendEvent(pu, &boss_y_packet);
						}
						m_cBossAttackCnt = 0;
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
					}
					else {
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
				}
				else {
					if (y_pos <= 1000.0f) {
						boss_pos.y = 1000.0f;
						CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
						for (auto& pu : login_list) {
							bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
							if (!is_connect) continue;
							CManager::SendEvent(pu, &boss_y_packet);
						}
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
					}
					else {
						CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
					}
				}
			}
		}
		else {
			m_bIs_Boss_Fly = false;
			DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
			float y_pos = boss_pos.y;

			s_SC_BOSS_FLY_PACKET boss_y_packet;
			boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
			boss_y_packet.m_bType = SC_BOSS_FLY_DOWN;

			y_pos -= 1.0f;
			boss_y_packet.m_fY = y_pos;

			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &boss_y_packet);
			}

			boss_pos.y = y_pos;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

			if (m_cBossAttackCnt >= 3) {
				if (y_pos <= 900.0f) {
					boss_pos.y = 900.0f;
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
					for (auto& pu : login_list) {
						bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
						if (!is_connect) continue;
						CManager::SendEvent(pu, &boss_y_packet);
					}
					m_cBossAttackCnt = 0;
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
				}
				else {
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
			}
			else {
				if (y_pos <= 1000.0f) {
					boss_pos.y = 1000.0f;
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
					for (auto& pu : login_list) {
						bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
						if (!is_connect) continue;
						CManager::SendEvent(pu, &boss_y_packet);
					}
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
				}
				else {
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
			}
		}
	}
}

void CMonsterProcess::BossDownEvent()
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	s_SC_DISCONNECT_PACKET remove_packet;
	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = BOSS_ID;

	//시체가 사라지도록
	for (auto& player : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(player, &remove_packet);
	}

	CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Respawn, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
}

void CMonsterProcess::BossFlyDeadEvent()
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		m_bIs_Boss_Fly = false;
		DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
		float y_pos = boss_pos.y;

		s_SC_BOSS_FLY_PACKET boss_y_packet;
		boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
		boss_y_packet.m_bType = SC_BOSS_FLY_DOWN;

		y_pos -= 1.0f;
		boss_y_packet.m_fY = y_pos;

		for (auto& pu : login_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &boss_y_packet);
		}

		boss_pos.y = y_pos;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

		if (y_pos <= 750.0f) {
			boss_pos.y = 750.0f;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &boss_y_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Down);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Fly_Dead, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}

	}
}

void CMonsterProcess::BossRaidEvent(unsigned short usTarget)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		s_SC_ANIMATION_PACKET monster_state_packet;
		monster_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
		monster_state_packet.m_usId = BOSS_ID;
		monster_state_packet.m_bType = SC_BOSS_RAID;
		if (!m_bIs_Boss_Raid) {
			m_bIs_Boss_Raid = true;
			s_SC_BOSS_FLY_PACKET boss_y_packet;
			boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
			boss_y_packet.m_bType = SC_BOSS_FLY_DOWN;
			boss_y_packet.m_fY = 750.0f;

			for (auto& player : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(player, &boss_y_packet);
				CManager::SendEvent(player, &monster_state_packet);
			}
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTarget, UPDATE_EVENT, e_BossState::e_Boss_Raid, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(7000));
		}
		else {
			int hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
			if (hp <= 0) {
				monster_state_packet.m_bType = SC_NPC_DOWN;
				for (auto& player : login_list) {
					bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
					if (!is_connect) continue;
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Down);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
				return;
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(m_cBefore_Boss_State);
			m_fpMonsterUpdate[m_cBefore_Boss_State](MAP_BOSS, BOSS_ID, usTarget);
		} 
	}
}

void CMonsterProcess::ByPlayerManAttack1(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);

	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerAttack1(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk();
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
	
}

void CMonsterProcess::ByPlayerManAttack2(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);

	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerAttack2(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk();
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerManSkill1(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerSkill1(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 300;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerManSkill2(char cMap, unsigned short usPlayer) 
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerSkill2(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 300;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerManSkill3(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerSkill3(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 200;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerManSkill4(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerSkill4(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 100;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerManSkill5(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);

	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionManPlayerSkill5(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 100;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}


void CMonsterProcess::ByPlayerWoManAttack1(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerAttack1(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk();
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerWoManAttack2(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerAttack2(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk();
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerWoManSkill1(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerSkill1(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 300;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerWoManSkill2(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerSkill2(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 300;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerWoManSkill3(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerSkill3(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk() + 300;
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerWoManSkill4(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerSkill1(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk();
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerWoManSkill5(char cMap, unsigned short usPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->CopyMonsterList(monster_list);
	for (auto& monster : monster_list) {
		unsigned short monster_id = monster - MAX_PLAYER;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
		if (!CheckCollisionWoManPlayerSkill1(cMap, usPlayer, monster_id)) continue;

		int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetAmr();
		int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][monster_id]->GetHP();

		////////////////////////////////
		//수정할거
		int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetAtk();
		int after_monster_hp = monster_hp;
		if (player_atk > monster_amr) {
			after_monster_hp = monster_hp - (player_atk - monster_amr);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayer]->GetInvincibility()) after_monster_hp = 0;

		if (monster_id == BOSS_ID) {
			BossByPlayerLevelUpEvent(usPlayer, after_monster_hp);
		}
		else ByPlayerLevelUpEvent(cMap, monster_id, usPlayer, after_monster_hp);
	}
}

void CMonsterProcess::ByPlayerLevelUpEvent(char& cMap, unsigned short usMonsterId, unsigned short& usPlayerId, int& iAfterHp)
{
	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->CopyPlayerList(player_list);

	s_SC_ANIMATION_PACKET monster_state_packet;
	monster_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	s_SC_CHANGE_STATUS_PACKET npc_status_packet;
	npc_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	npc_status_packet.m_bType = SC_CHANGE_STATUS_NPC_HP;

	//NPC HP변화등
	monster_state_packet.m_usId = npc_status_packet.m_usId = usMonsterId;
	npc_status_packet.m_iAny = iAfterHp;

	if (iAfterHp <= 0) {
		int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetLevel();
		int monster_money = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->GetMoney();
		int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->GetExp();
		int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetMoney();
		int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetExp();

		player_money += monster_money;

		//CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->SetState(e_ObjectState::e_Object_Down);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->SetHP(0);

		monster_state_packet.m_bType = SC_NPC_DOWN;

		npc_status_packet.m_iAny = 0;

		s_SC_CHANGE_STATUS_PACKET player_money_packet;
		player_money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
		player_money_packet.m_usId = usPlayerId;
		player_money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
		player_money_packet.m_iAny = player_money;
		CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetMoney(player_money);

		int after_exp = player_exp + monster_exp;

		if (after_exp >= player_level * 1000) {

			s_SC_LEVEL_UP_PACKET level_up_packet;
			level_up_packet.m_bSize = sizeof(s_SC_LEVEL_UP_PACKET);
			level_up_packet.m_bType = SC_CHANGE_LEVEL_UP;

			after_exp -= (player_level * 1000);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			player_level += 1;
			int limit_stat = player_level * 1000;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitMP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitEXP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLevel(player_level);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetMP(limit_stat);

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAtk(player_level * 10);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAmr(player_level * 10);

			level_up_packet.m_usId = usPlayerId;
			level_up_packet.m_iExp = after_exp;
			level_up_packet.m_iHP = limit_stat;
			level_up_packet.m_iMP = limit_stat;
			level_up_packet.m_iLevel = player_level;
			level_up_packet.m_iMaxEXP = limit_stat;
			level_up_packet.m_iMaxHP = limit_stat;
			level_up_packet.m_iMaxMP = limit_stat;

			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &level_up_packet);
				CManager::SendEvent(player, &player_money_packet);
			}
		}
		else {
			s_SC_CHANGE_STATUS_PACKET player_exp_packet;
			player_exp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
			player_exp_packet.m_usId = usPlayerId;
			player_exp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_EXP;
			player_exp_packet.m_iAny = after_exp;

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &player_exp_packet);
				CManager::SendEvent(player, &player_money_packet);
			}
		}
		CProcess::PostEvent(cMap, usMonsterId, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
	}
	else {
		monster_state_packet.m_bType = SC_NPC_DAMAGED;
		for (auto& player : player_list) {
			CManager::SendEvent(player, &monster_state_packet);
			CManager::SendEvent(player, &npc_status_packet);
		}
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->SetHP(iAfterHp);
	}
}


void CMonsterProcess::BossByPlayerLevelUpEvent(unsigned short& usPlayerId, int& iAfterHp)
{
	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->CopyPlayerList(player_list);

	s_SC_ANIMATION_PACKET monster_state_packet;
	monster_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	monster_state_packet.m_usId = BOSS_ID;

	s_SC_CHANGE_STATUS_PACKET npc_status_packet;
	npc_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	npc_status_packet.m_bType = SC_CHANGE_STATUS_NPC_HP;

	//NPC HP변화등
	monster_state_packet.m_usId = npc_status_packet.m_usId = BOSS_ID;
	npc_status_packet.m_iAny = iAfterHp;

	//////////////////
	//수정할거
	iAfterHp -= CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP() / 10;

	if (iAfterHp <= 0) {
		int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetLevel();
		int monster_money = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetMoney();
		int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetExp();
		int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetMoney();
		int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetExp();

		player_money += monster_money;

		//CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(0);

		int next_event_time = 1300;
		char boss_state = e_BossState::e_Boss_Down;
		monster_state_packet.m_bType = SC_NPC_DOWN;
		if (m_bIs_Boss_Fly) {
			monster_state_packet.m_bType = SC_BOSS_FLY_DEAD;
			boss_state = e_BossState::e_Boss_Fly_Dead;
			next_event_time = 800;
		}
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(boss_state);

		npc_status_packet.m_iAny = 0;

		int after_exp = player_exp + monster_exp;

		s_SC_CHANGE_STATUS_PACKET player_money_packet;
		player_money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
		player_money_packet.m_usId = usPlayerId;
		player_money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
		player_money_packet.m_iAny = player_money;
		CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetMoney(player_money);

		if (after_exp >= player_level * 1000) {

			s_SC_LEVEL_UP_PACKET level_up_packet;
			level_up_packet.m_bSize = sizeof(s_SC_LEVEL_UP_PACKET);
			level_up_packet.m_bType = SC_CHANGE_LEVEL_UP;

		

			after_exp -= (player_level * 1000);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			player_level += 1;
			int limit_stat = player_level * 1000;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitMP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitEXP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLevel(player_level);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetMP(limit_stat);

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAtk(player_level * 10);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAmr(player_level * 10);

			level_up_packet.m_usId = usPlayerId;
			level_up_packet.m_iExp = after_exp;
			level_up_packet.m_iHP = limit_stat;
			level_up_packet.m_iMP = limit_stat;
			level_up_packet.m_iLevel = player_level;
			level_up_packet.m_iMaxEXP = limit_stat;
			level_up_packet.m_iMaxHP = limit_stat;
			level_up_packet.m_iMaxMP = limit_stat;

			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &level_up_packet);
				CManager::SendEvent(player, &player_money_packet);
			}
		}
		else {
			s_SC_CHANGE_STATUS_PACKET player_exp_packet;
			player_exp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
			player_exp_packet.m_usId = usPlayerId;
			player_exp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_EXP;
			player_exp_packet.m_iAny = after_exp;
			
			
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &player_exp_packet);
				CManager::SendEvent(player, &player_money_packet);
			}
		}
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, boss_state, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(next_event_time));
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(iAfterHp);
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Raid){
			++m_cBossHitCnt %= 3;
			monster_state_packet.m_bType = m_cBossHitCnt + 50;
			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
			}
		}
		else {
			for (auto& player : player_list) {
				CManager::SendEvent(player, &npc_status_packet);
			}
		}
	}
}

void CMonsterProcess::BossTailAttackPlayerUpdate(unsigned short usTarget)
{
	char target_state = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetState();
	char state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (state == e_BossState::e_Boss_Down || state != e_BossState::e_Boss_Tail_Attack || target_state == e_ObjectState::e_Object_Down) return;
	if (!CheckCollisionPlayerByBossTailAttack(usTarget)) {
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTarget, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}
	ByBossPlayerDamagedEvent(usTarget, 10);
}

void CMonsterProcess::BossLegAttackPlayerUpdate(unsigned short usTarget)
{
	char target_state = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetState();
	char state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (state == e_BossState::e_Boss_Down || state != e_BossState::e_Boss_Leg_Attack || target_state == e_ObjectState::e_Object_Down) return;
	if (!CheckCollisionPlayerByBossLegAttack(usTarget)) {
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTarget, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}
	ByBossPlayerDamagedEvent(usTarget, 10);
}

void CMonsterProcess::BossFlyAttackPlayerUpdate(unsigned short usTarget)
{
	char target_state = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetState();
	char state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (state == e_BossState::e_Boss_Down || state != e_BossState::e_Boss_Fly_Attack || target_state == e_ObjectState::e_Object_Down) return;
	if (!CheckCollisionPlayerByBossFlyAttack(usTarget)) {
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTarget, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}
	ByBossPlayerDamagedEvent(usTarget, 10);
}

void CMonsterProcess::BossFireBreathPlayerUpdate(unsigned short usTarget)
{
	char target_state = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetState();
	char state = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState();
	if (state == e_BossState::e_Boss_Down || state != e_BossState::e_Boss_FireBreath || target_state == e_ObjectState::e_Object_Down) return;
	if (CheckCollisionPlayerByBossFireBreath(usTarget))
		ByBossPlayerDamagedEvent(usTarget, 0);
	CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTarget, PLAYER_BY_MONSTER_EVENT, e_BossAttackType::e_Fire_Breath, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(100));
}

void CMonsterProcess::ByBossPlayerDamagedEvent(unsigned short& usTarget, int iDamage)
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	CopyBefore(MAP_BOSS, login_list);

	int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetHP();
	int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAmr();

	s_SC_ANIMATION_PACKET player_state_packet;
	player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	s_SC_CHANGE_STATUS_PACKET player_status_packet;
	player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
	player_status_packet.m_usId = usTarget;

	int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetAtk() + iDamage;

	/////////////////////////////////
	//수정할거
	int after_player_hp = player_hp;
	if (player_amr < monster_atk) {
		after_player_hp = player_hp - (monster_atk - player_amr);
	}
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_player_hp = 100;

	if (after_player_hp <= 0) {

		player_state_packet.m_bType = SC_PLAYER_DOWN;
		player_state_packet.m_usId = usTarget;
		player_status_packet.m_iAny = 0;

		CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->SetHP(0);

		for (auto& pu : login_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &player_status_packet);
			CManager::SendEvent(pu, &player_state_packet);
		}
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetState() != e_ObjectState::e_Object_Down) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->SetState(e_ObjectState::e_Object_Down);
			CProcess::PostEvent(MAP_BOSS, usTarget, NO_TARGET_PLAYER, PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
		}
	}
	else {
		CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->SetHP(after_player_hp);
		player_state_packet.m_bType = SC_PLAYER_DAMAGED;
		player_state_packet.m_usId = usTarget;

		player_status_packet.m_iAny = after_player_hp;

		for (auto& pu : login_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &player_state_packet);
			CManager::SendEvent(pu, &player_status_packet);
		}
	}
}