#include "stdafx.h"
#include "Player.h"
#include "network.h"

CPlayer::CPlayer()
{
	InitClientData();
}

CPlayer::~CPlayer()
{
	InitClientData();
	SAFE_DELETE_CON_SET(m_ccusViewList);
}

UCHAR* CPlayer::RecvEvent(int data_size, UCHAR* io_ptr)
{

	int size = data_size;
	UCHAR* packet = io_ptr;
	int curr = m_sState.m_iCursize;
	int prev = m_sState.m_iPredata;
	UCHAR* sample_packet = m_sState.m_ucPacketBuf;
	while (0 < size) {
		int packe_size;
		if (0 != curr)
			packe_size = curr;
		else {
			packe_size = packet[0];
			curr = packe_size;
		}
		int need_size = packe_size - prev;
		if (need_size <= size) {
			memcpy(sample_packet
				+ prev, packet, need_size);
			prev = 0;
			curr = 0;
			size -= need_size;
			packet += need_size;
			return sample_packet;

		}
		else {
			memcpy(sample_packet + prev, packet, size);
			prev += size;
			size = -size;
			packet += size;
		}
	}
	return NULL;
}

void CPlayer::SetRecvState()
{
	DWORD flag = 0;
	ZeroMemory(&m_sState.m_sContext.m_sOverlapped, sizeof(WSAOVERLAPPED));
	//커널에 등록
	int retval = WSARecv(m_sSock, &m_sState.m_sContext.m_sWsabuf, 1, NULL, &flag, &m_sState.m_sContext.m_sOverlapped, NULL);
	if (0 != retval) {
		int err_code = WSAGetLastError();
		if (WSA_IO_PENDING != err_code) {
			CNetwork::Err_display("FirstRecv Err");
		}
	}
}

void CPlayer::InitClientData()
{
	SetPlayerSocket(INVALID_SOCKET);
	SetPlayerConnect(false);
	m_ccusViewList.clear();
	m_sState.m_iCursize = 0;
	m_sState.m_iPredata = 0;

	ZeroMemory(&m_sState.m_sContext, sizeof(s_Context));

	m_sState.m_sContext.m_bType = RECV_EVENT;
	m_sState.m_sContext.m_sWsabuf.buf = reinterpret_cast<char*>(m_sState.m_sContext.m_cBuf);
	m_sState.m_sContext.m_sWsabuf.len = sizeof(m_sState.m_sContext.m_cBuf);
}
