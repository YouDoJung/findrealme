#pragma once
#include "stdafx.h"

#define RAND(type, maxVal)	(type)CRandomCalculate::GetInstance()->rand(maxVal)
#define RAND_MONEY(type, level)	CRandomCalculate::GetInstance()->rand_drop_money(type, level)
#define RAND_PERCENT(percent)	CRandomCalculate::GetInstance()->rand_percent(percent)


class CRandomCalculate : public CTemplateSingleton<CRandomCalculate> {
public:
	std::random_device rand_dv;
	std::default_random_engine dre{ rand_dv() };

	uint64_t rand(int maxVal)
	{
		std::mt19937 engine((uint32_t)time(nullptr) + (uint32_t)std::hash<std::thread::id>()(std::this_thread::get_id()));

		std::uniform_int_distribution<uint64_t>distribution(0, UINT64_MAX);

		auto generator = bind(distribution, engine);

		return (uint64_t)(generator() % maxVal);
	}

	/*bool rand_percent(int percent)
	{
		std::uniform_int_distribution<int> distribution(1, 100);

		auto generator = bind(distribution, rand_dv);

		return (generator() < percent) ? true : false;
	}*/

	int rand_drop_money(char monster_type, int monster_level)
	{
		int minimum = 0;
		int maximum = 0;

		switch (monster_type)
		{
		case e_ObjectType::e_Monster_Peon:
		{
			minimum = 100 * monster_level;
			maximum = 150 * monster_level;

			break;
		}
		case e_ObjectType::e_Monster_Lord:
		{
			minimum = 150 * monster_level;
			maximum = 200 * monster_level;
			break;
		}
		case e_ObjectType::e_Monster_Grunt:
		{
			minimum = 200 * monster_level;
			maximum = 250 * monster_level;
			break;
		}
		case e_ObjectType::e_Monster_Boss:
		{
			minimum = 500 * monster_level;
			maximum = 550 * monster_level;
			break;
		}
		default:
			break;
		}

		std::uniform_int_distribution<int> money_distribution(minimum, maximum);

		auto generator = bind(money_distribution, dre);

		return (int)(generator());
	}

};