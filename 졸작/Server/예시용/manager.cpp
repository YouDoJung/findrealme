#include "stdafx.h"
#include "manager.h"
#include "data_base.h"
#include "network.h"

void CManager::SendEvent(unsigned short usID, void* pPACKET)
{
	int retval;
	int err_code = 0;

	DWORD send_bytes = 0;

	s_pContext sample = new s_Context;

	//0번지가 크기.
	unsigned char* cur_packet = reinterpret_cast<unsigned char*>(pPACKET);

	memcpy(sample->m_cBuf, pPACKET, cur_packet[0]);

	sample->m_bType = SEND_EVENT;

	sample->m_sWsabuf.buf = reinterpret_cast<char*>(sample->m_cBuf);
	sample->m_sWsabuf.len = cur_packet[0];

	ZeroMemory(&sample->m_sOverlapped, sizeof(WSAOVERLAPPED));

	retval = WSASend(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerSokcet(), &sample->m_sWsabuf, 1, &send_bytes, 0, &sample->m_sOverlapped, NULL);

	if (retval == SOCKET_ERROR) {
		err_code = WSAGetLastError();
		if (err_code != WSA_IO_PENDING) {
			std::cout << "err_code - " << err_code;
			CNetwork::Err_display("SEND EVENT() SOCKET_ERROR");
		}
	}
}

void CManager::SendPlayerLogIn(unsigned short usID)
{
	s_SC_LOGIN_PACKET SendPacket;
	SendPacket.m_bSize = sizeof(s_SC_LOGIN_PACKET);
	SendPacket.m_bType = SC_LOGIN;
	SendPacket.m_usId = usID;
	CManager::SendEvent(usID, &SendPacket);
}

void CManager::SendPlayerAcceptData(concurrency::concurrent_unordered_set<unsigned short>& list, unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	s_SC_ACCEPT_PACKET SendPacket;
	SendPacket.m_bSize = sizeof(s_SC_ACCEPT_PACKET);
	SendPacket.m_bType = SC_CONNECT;
	SendPacket.m_usId = usID;
	SendPacket.m_fX = sample_pos.x;
	SendPacket.m_fZ = sample_pos.z;
	SendPacket.m_Dir = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLookVector();
	SendPacket.m_iHP = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHP();
	SendPacket.m_iLevel = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

	/*/////////////////////////////
	DataBase 
	////////////////////////////*/
	/*wchar_t player_id[MAX_STR_LEN];
	CData_Base::GetInstance()->FindIsLogin(usID, player_id);
	wcsncpy_s(SendPacket.m_wcID, MAX_STR_LEN, player_id, (UINT32)wcslen(player_id));*/
	for (auto& au : list) {
		if (!Vector3::ObjectRangeCheck(CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos(), sample_pos, PLAYER_AND_PLAYER_RANGE))
			continue;
		CManager::SendEvent(au, &SendPacket);
	}
}

void CManager::SendMapSwap(char cMap, unsigned short usID)
{
	s_SC_PLAYER_MAP_SWAP_PACKET SendPacket;
	SendPacket.m_bSize = sizeof(SendPacket);
	SendPacket.m_bType = SC_MAP_SWAP;
	SendPacket.m_bMapType = cMap;
	CManager::SendEvent(usID, &SendPacket);
}

void CManager::SendPlayerStatusData(unsigned short bPlayer, unsigned short bOther)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetPos();
	s_SC_ACCEPT_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ACCEPT_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ACCEPT_PACKET);
	new_player_status.m_bType = SC_CONNECT;
	new_player_status.m_usId = bPlayer;
	new_player_status.m_fX = sample_pos.x;
	new_player_status.m_fY = sample_pos.y;
	new_player_status.m_fZ = sample_pos.z;
	new_player_status.m_Dir = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetLookVector();
	new_player_status.m_iHP = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetHP();
	new_player_status.m_iLevel = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetLevel();
	new_player_status.m_cObjectType = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetIsPlayer();

	/*/////////////////////////////
	DataBase 
	////////////////////////////*/
	/*wchar_t player_id[MAX_STR_LEN];
	CData_Base::GetInstance()->FindIsLogin(bPlayer, player_id);
	wcsncpy_s(new_player_status.m_wcID, MAX_STR_LEN, player_id, (UINT32)wcslen(player_id));*/


	CManager::SendEvent(bOther, &new_player_status);
}

void CManager::SendPlayerPosData(unsigned short bPlayer, unsigned short bOther)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetPos();
	s_SC_POS_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_POS_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_POS_PACKET);
	new_player_status.m_bType = SC_POS;
	new_player_status.m_usId = bPlayer; 
	new_player_status.m_fX = sample_pos.x;
	new_player_status.m_fY = sample_pos.y;
	new_player_status.m_fZ = sample_pos.z;
	new_player_status.m_Dir = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetLookVector();
	CManager::SendEvent(bOther, &new_player_status);
}

void CManager::SendPlayerLook(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	s_SC_DIR_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_DIR_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_DIR_PACKET);
	new_player_status.m_bType = SC_DIR;
	new_player_status.m_usId = bPlayer;
	new_player_status.m_Dir = CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->GetLookVector();
	
	for (auto& au : list) {
		if (au == bPlayer)continue;
		CManager::SendEvent(au, &new_player_status);
	}
}

void CManager::SendPlayerDisconnect(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	s_SC_DISCONNECT_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_DISCONNECT_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	new_player_status.m_bType = SC_DISCONNECT;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		if (au == bPlayer)continue;
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(bPlayer)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(bPlayer);
			if(CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPlayerConnection())
				CManager::SendEvent(au, &new_player_status);
		}
	}
}

void CManager::SendPlayerRemoveData(unsigned short bPlayer, unsigned short bOther)
{
	if (bPlayer != bOther) {
		s_SC_DISCONNECT_PACKET new_player_status;
		ZeroMemory(&new_player_status, sizeof(s_SC_DISCONNECT_PACKET));
		new_player_status.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
		new_player_status.m_bType = SC_DISCONNECT;
		new_player_status.m_usId = bPlayer;
		CManager::SendEvent(bOther, &new_player_status);
	}
}

void CManager::SendAttackData(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_ATTACK;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}


void CManager::SendAttack2Data(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_ATTACK2;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}


void CManager::SendSkill1Data(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_SKILL1;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}


void CManager::SendSkill2Data(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_SKILL2;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}


void CManager::SendSkill3Data(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_SKILL3;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}


void CManager::SendSkill4Data(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_SKILL4;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}


void CManager::SendSkill5Data(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	SendPlayerLook(bPlayer);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_SKILL5;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}

void CManager::SendIdleData(unsigned short bPlayer)
{
	concurrency::concurrent_unordered_set<unsigned short>list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[bPlayer]->CopyPlayerList(list);
	s_SC_ANIMATION_PACKET new_player_status;
	ZeroMemory(&new_player_status, sizeof(s_SC_ANIMATION_PACKET));
	new_player_status.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	new_player_status.m_bType = SC_IDLE;
	new_player_status.m_usId = bPlayer;
	for (auto& au : list) {
		CManager::SendEvent(au, &new_player_status);
	}
}

void CManager::SendNpcLogInInPlayer(unsigned short usPlayer, unsigned short usMonster)
{
	s_SC_NPC_LOGIN_PACKET new_npc_status;
	ZeroMemory(&new_npc_status, sizeof(s_SC_NPC_LOGIN_PACKET));
	new_npc_status.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	new_npc_status.m_bType = SC_NPC_LOG_IN;
	new_npc_status.m_usId = usMonster;
	CManager::SendEvent(usPlayer, &new_npc_status);
}

void CManager::SendNpcStatusInPlayer(char cMap, unsigned short usPlayer, unsigned short usMonster)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonster]->GetPos();
	s_SC_NPC_STATUS_PACKET new_npc_status;
	ZeroMemory(&new_npc_status, sizeof(s_SC_NPC_STATUS_PACKET));
	new_npc_status.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	new_npc_status.m_bType = SC_NPC_STATUS;
	new_npc_status.m_usId = usMonster;
	new_npc_status.m_fX = sample_pos.x;
	new_npc_status.m_fZ = sample_pos.z;
	new_npc_status.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonster]->GetHP();
	new_npc_status.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonster]->GetLevel();
	new_npc_status.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonster]->GetLookVector();
	new_npc_status.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonster]->GetLimitHP();
	CManager::SendEvent(usPlayer, &new_npc_status);
}

void CManager::SendBossStatusInPlayer(unsigned short usPlayer)
{
	s_SC_BOSS_STATUS_PACKET new_npc_status;
	ZeroMemory(&new_npc_status, sizeof(s_SC_BOSS_STATUS_PACKET));
	new_npc_status.m_bSize = sizeof(s_SC_BOSS_STATUS_PACKET);
	new_npc_status.m_bType = SC_BOSS_STATUS;
	new_npc_status.m_xmf3Pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	new_npc_status.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	new_npc_status.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLevel();
	new_npc_status.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();
	new_npc_status.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP();
	CManager::SendEvent(usPlayer, &new_npc_status);
}

void CManager::SendNpcRemoveInPlayer(unsigned short usPlayer, unsigned short usMonster)
{
	s_SC_DISCONNECT_PACKET new_npc_status;
	ZeroMemory(&new_npc_status, sizeof(s_SC_DISCONNECT_PACKET));
	new_npc_status.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	new_npc_status.m_bType = SC_NPC_REMOVE;
	new_npc_status.m_usId = usMonster;
	CManager::SendEvent(usPlayer, &new_npc_status);
}
