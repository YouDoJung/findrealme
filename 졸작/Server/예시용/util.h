#pragma once
#include "stdafx.h"

typedef void(*Function)(void*);

#if _DEBUG
#define CONTEXT_SWITCH Sleep(1)
#else
#define CONTEXT_SWITCH ::SwitchToThread()
#endif


template<typename T>
constexpr void SAFE_DELETE(T& some)
{
	if (some) {
		delete some;
		some = nullptr;
	}
}

template<typename T>
constexpr void SAFE_DELETE_ARRAY(T some)
{
	if (some) {
		delete[] some;
		some = NULL;
	}
}

template<typename T>
constexpr void SAFE_RELEASE(T& some)
{
	if (some) {
		some.Release();
		some = nullptr;
	}
}

template<typename T>
constexpr void SAFE_DELETE_VECTOR(T& some)
{
	for (auto& au : some) {
		delete &au;
	}
	some.clear();
}

template<typename T>
constexpr void SAFE_DELETE_MAP(T& some)
{
	for (auto& au : some) {
		SAFE_DELETE(au.second);
	}
	some.clear();
}

template<typename T>
constexpr void SAFE_DELETE_CON_SET(T& some)
{
	some.clear();
}




