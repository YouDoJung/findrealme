#pragma once
#include "../Fmod/inc/fmod.hpp"
#pragma comment(lib, "fmod64_vc.lib")

enum EffectSound {
	THUMP = 4,
	EXPLOSION,
	FIREBREATH,
	THUNDER,
	SLASH,
	MAN_DAMAGE,
	DRAGON_DIE,
	DRAGON_FLY,
	WOMAN_DAMAGE
};

class CSound
{
public:
	CSound();
	~CSound();

	static CSound* instance;
	//static CSound* sharedManager();
	void init();
	void loading();
	void play(int typem, int chNum);
	void stop(int chNum);
	void ErrorCheck(FMOD_RESULT _r);

private:
	FMOD::System* system = nullptr;
	FMOD::Sound* sound[13];
	FMOD::Channel* channel[100];
	FMOD_RESULT result;
};

