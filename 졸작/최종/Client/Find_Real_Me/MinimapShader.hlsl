#include "Shaders.hlsl"
struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VS_TEXTURED_OUTPUT VS_Minimap(uint nVertexID : SV_VertexID)
{
	// 00	10
	// 01	11
	VS_TEXTURED_OUTPUT output;
	if (gMapIndex == 0) {
		if (nVertexID == 0) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 0.f); }
		if (nVertexID == 1) { output.position = float4(1.f, +1.0f, 0.0f, 1.0f); output.uv = float2(1.f, 0.f); }
		if (nVertexID == 2) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }

		if (nVertexID == 3) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 0.f); }
		if (nVertexID == 4) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }
		if (nVertexID == 5) { output.position = float4(0.5f, +0.5f, 0.0f, 1.0f); output.uv = float2(0.f, 1.f); }
	}
	else if (gMapIndex == 1) {
		if (nVertexID == 0) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 0.f); }
		if (nVertexID == 1) { output.position = float4(1.f, +1.0f, 0.0f, 1.0f); output.uv = float2(1.f, 0.f); }
		if (nVertexID == 2) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }

		if (nVertexID == 3) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 0.f); }
		if (nVertexID == 4) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }
		if (nVertexID == 5) { output.position = float4(0.5f, +0.5f, 0.0f, 1.0f); output.uv = float2(0.f, 1.f); }
	}
	else if (gMapIndex == 2) {
		if (nVertexID == 0) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 1 - 1.f / 7.f); }
		if (nVertexID == 1) { output.position = float4(1.f, +1.0f, 0.0f, 1.0f); output.uv = float2(1.f, 1 - 1.f / 7.f); }
		if (nVertexID == 2) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }

		if (nVertexID == 3) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 1 - 1.f / 7.f); }
		if (nVertexID == 4) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }
		if (nVertexID == 5) { output.position = float4(0.5f, +0.5f, 0.0f, 1.0f); output.uv = float2(0.f, 1.f); }

		output.uv.y -= clamp(((int)gPlayerZ - 5000) / 10000.f / 7.f, 0, output.uv.y);

		if (gPlayerZ >= 65000.f) {
			if (nVertexID == 0) { output.uv = float2(0.f, 0.f); }
			if (nVertexID == 1) { output.uv = float2(1.f, 0.f); }
			if (nVertexID == 2) { output.uv = float2(1.f, 1.f/7.f); }

			if (nVertexID == 3) { output.uv = float2(0.f, 0.f); }
			if (nVertexID == 4) { output.uv = float2(1.f, 1.f/7.f); }
			if (nVertexID == 5) { output.uv = float2(0.f, 1.f/7.f); }
		}
	}
	else {
		if (nVertexID == 0) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 1 - 1.f / 3.f); }
		if (nVertexID == 1) { output.position = float4(1.f, +1.0f, 0.0f, 1.0f); output.uv = float2(1.f, 1 - 1.f / 3.f); }
		if (nVertexID == 2) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }

		if (nVertexID == 3) { output.position = float4(0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 1 - 1.f / 3.f); }
		if (nVertexID == 4) { output.position = float4(1.f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }
		if (nVertexID == 5) { output.position = float4(0.5f, +0.5f, 0.0f, 1.0f); output.uv = float2(0.f, 1.f); }

		output.uv.y -= clamp(((int)gPlayerZ - 5000) / 10000.f / 3.f, 0, output.uv.y);
		if (gPlayerZ >= 25000.f) {
			if (nVertexID == 0) { output.uv = float2(0.f, 0.f); }
			if (nVertexID == 1) { output.uv = float2(1.f, 0.f); }
			if (nVertexID == 2) { output.uv = float2(1.f, 1.f / 3.f); }

			if (nVertexID == 3) { output.uv = float2(0.f, 0.f); }
			if (nVertexID == 4) { output.uv = float2(1.f, 1.f / 3.f); }
			if (nVertexID == 5) { output.uv = float2(0.f, 1.f / 3.f); }
		}
	}

	return output;
}

float4 PS_Minimap(VS_TEXTURED_OUTPUT input) : SV_Target
{
	int index = 110 + gMapIndex;
	float4 cColor = gtxtAllTextures[NonUniformResourceIndex(index)].Sample(gSamplerState, input.uv);

	//return float4(gtxtShadow.Sample(gSamplerState, input.uv).rgb, 1.0f);
	return cColor;
}
/////////////////////////////////////////////////////////////////////////////
struct VS_COLOR_INPUT
{
	float3 position : POSITION;
	float4 color : COLOR;
};

struct VS_COLOR_OUTPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

VS_COLOR_OUTPUT VS_MINIMAP_OBJECT(VS_COLOR_INPUT input)
{
	VS_COLOR_OUTPUT output;
	float3 posW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);

	if (gMapIndex > 1 && gPlayerZ > 5000.f) {
		switch (gMapIndex) {
		case 2:
			if (gPlayerZ < 65000.f)
				posW.z -= ((int)gPlayerZ - 5000);
			else {
				posW.z -= 60000.f;
			}
			break;
		case 3:
			if (gPlayerZ < 25000.f)
				posW.z -= ((int)gPlayerZ - 5000);
			else {
				posW.z -= 20000.f;
			}
			break;
		}
	}

	if (gMapIndex == 1) {
		float WminX = 4000, WmaxX = 6000;
		float WminY = 4000, WmaxY = 7000;

		posW.x = posW.x / WminX - 0.5f;
		posW.z = posW.z / WminY / 2.f;
	}
	else {
		float Wmin = 0.f; float Wmax = 10000.f;
		float Vmin = 0.5f; float Vmax = 1.f;
		posW = posW / (Wmax * 2.f) + 0.5f;
	}
	output.position = float4(posW.x, posW.z, 0.f, 1.f);

	output.color = input.color;
	return output;
}

float4 PS_MINIMAP_OBJECT(VS_COLOR_OUTPUT input) : SV_TARGET
{
	float4 color = input.color;
	return color;
}