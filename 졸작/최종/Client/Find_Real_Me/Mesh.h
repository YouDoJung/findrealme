//------------------------------------------------------- ----------------------
// File: Mesh.h
//-----------------------------------------------------------------------------

#pragma once
#include "FrameResource.h"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 포지션
class CVertex
{
public:
	XMFLOAT3 m_xmf3Position;

public:
	CVertex() { m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); }
	CVertex(XMFLOAT3 xmf3Position) { m_xmf3Position = xmf3Position; }
	~CVertex() { }
};

// 포지션 + 컬러
class CDiffusedVertex : public CVertex
{
public:
	XMFLOAT4 m_xmf4Diffuse;

public:
	CDiffusedVertex() { m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f); }
	CDiffusedVertex(float x, float y, float z, XMFLOAT4 xmf4Diffuse) { m_xmf3Position = XMFLOAT3(x, y, z); m_xmf4Diffuse = xmf4Diffuse; }
	CDiffusedVertex(XMFLOAT3 xmf3Position, XMFLOAT4 xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f)) { m_xmf3Position = xmf3Position; m_xmf4Diffuse = xmf4Diffuse; }
	~CDiffusedVertex() { }
};

// 포지션 + 텍스처좌표
class CTexturedVertex : public CVertex
{
public:
	XMFLOAT2 m_xmf2TexCoord;

public:
	CTexturedVertex() { m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmf2TexCoord = XMFLOAT2(0.0f, 0.0f); }
	CTexturedVertex(float x, float y, float z, XMFLOAT2 xmf2TexCoord) { m_xmf3Position = XMFLOAT3(x, y, z); m_xmf2TexCoord = xmf2TexCoord; }
	CTexturedVertex(XMFLOAT3 xmf3Position, XMFLOAT2 xmf2TexCoord = XMFLOAT2(0.0f, 0.0f)) { m_xmf3Position = xmf3Position; m_xmf2TexCoord = xmf2TexCoord; }
	~CTexturedVertex() { }
};

struct CIlluminatedTexturedVertex
{
	XMFLOAT3 m_xmf3Position;
	XMFLOAT3 m_xmf3Normal;
	XMFLOAT2 m_xmf2TexCoord;
	float m_fValue;
	CIlluminatedTexturedVertex() : m_xmf3Position{ 0.f, 0.f, 0.f }, m_xmf3Normal{ 0.f, 0.f, 0.f }, m_xmf2TexCoord{ 0.f, 0.f }, m_fValue{ 0.f } { }
	CIlluminatedTexturedVertex(XMFLOAT3 xmf3Position, XMFLOAT3 xmf3Normal, XMFLOAT2 xmf2TexCoord, float value)
		: m_xmf3Position{ xmf3Position }, m_xmf3Normal{ xmf3Normal }, m_xmf2TexCoord{ xmf2TexCoord }, m_fValue{ value } {}
	~CIlluminatedTexturedVertex() { }
};

// 포지션 + 컬러 + 텍스처좌표
class CDiffusedTexturedVertex : public CDiffusedVertex
{
public:
	XMFLOAT2 m_xmf2TexCoord0;

public:
	CDiffusedTexturedVertex() { m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f); m_xmf2TexCoord0 = XMFLOAT2(0.0f, 0.0f); }
	CDiffusedTexturedVertex(float x, float y, float z, XMFLOAT4 xmf4Diffuse, XMFLOAT2 xmf2TexCoord) { m_xmf3Position = XMFLOAT3(x, y, z); m_xmf4Diffuse = xmf4Diffuse; m_xmf2TexCoord0 = xmf2TexCoord; }
	CDiffusedTexturedVertex(XMFLOAT3 xmf3Position, XMFLOAT4 xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), XMFLOAT2 xmf2TexCoord = XMFLOAT2(0.0f, 0.0f)) { m_xmf3Position = xmf3Position; m_xmf4Diffuse = xmf4Diffuse; m_xmf2TexCoord0 = xmf2TexCoord; }
	~CDiffusedTexturedVertex() { }
};

class CDetailTexturedVertex : public CDiffusedTexturedVertex
{
public:
	XMFLOAT2 m_xmf2TexCoord1;
	CDetailTexturedVertex() {
		m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		m_xmf2TexCoord0 = XMFLOAT2(0.0f, 0.0f); m_xmf2TexCoord1 = XMFLOAT2(0.0f, 0.0f);
	}
	CDetailTexturedVertex(float x, float y, float z, XMFLOAT4 xmf4Diffuse, XMFLOAT2 xmf2TexCoord, XMFLOAT2 xmf2TexCoord1) {
		m_xmf3Position = XMFLOAT3(x, y, z); m_xmf4Diffuse = xmf4Diffuse;
		m_xmf2TexCoord0 = xmf2TexCoord; m_xmf2TexCoord1 = xmf2TexCoord1;
	}
	CDetailTexturedVertex(XMFLOAT3 xmf3Position, XMFLOAT4 xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f),
		XMFLOAT2 xmf2TexCoord = XMFLOAT2(0.0f, 0.0f), XMFLOAT2 xmf2TexCoord1 = XMFLOAT2(0.0f, 0.0f)) {
		m_xmf3Position = xmf3Position; m_xmf4Diffuse = xmf4Diffuse;
		m_xmf2TexCoord0 = xmf2TexCoord; m_xmf2TexCoord1 = xmf2TexCoord1;
	}
};

class CDetailTexturedilluminatedVertex : public CDetailTexturedVertex
{
public:
	XMFLOAT3 m_xmf3Normal;
	CDetailTexturedilluminatedVertex() {
		m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		m_xmf2TexCoord0 = XMFLOAT2(0.0f, 0.0f); m_xmf2TexCoord1 = XMFLOAT2(0.0f, 0.0f);
		m_xmf3Normal = XMFLOAT3(0.f, 0.f, 0.f);
	}
	CDetailTexturedilluminatedVertex(float x, float y, float z, XMFLOAT4 xmf4Diffuse, XMFLOAT2 xmf2TexCoord, XMFLOAT2 xmf2TexCoord1, XMFLOAT3 xmf3Normal) {
		m_xmf3Position = XMFLOAT3(x, y, z); m_xmf4Diffuse = xmf4Diffuse;
		m_xmf2TexCoord0 = xmf2TexCoord; m_xmf2TexCoord1 = xmf2TexCoord1;
		m_xmf3Normal = xmf3Normal;
	}
	CDetailTexturedilluminatedVertex(XMFLOAT3 xmf3Position, XMFLOAT4 xmf4Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f),
		XMFLOAT2 xmf2TexCoord = XMFLOAT2(0.0f, 0.0f), XMFLOAT2 xmf2TexCoord1 = XMFLOAT2(0.0f, 0.0f), XMFLOAT3 xmf3Normal = XMFLOAT3(0.f, 0.f, 0.f)) {
		m_xmf3Position = xmf3Position; m_xmf4Diffuse = xmf4Diffuse;
		m_xmf2TexCoord0 = xmf2TexCoord; m_xmf2TexCoord1 = xmf2TexCoord1;
		m_xmf3Normal = xmf3Normal;
	}
};

struct GeometryBillboardVertex {
	XMFLOAT3 m_xmf3Position;
	XMFLOAT2 m_xmf2Size;
	XMFLOAT2 m_xmf2StartAndLifeTime;
	float m_fValue;
	GeometryBillboardVertex() : m_xmf3Position{ 0.f, 0.f, 0.f }, m_xmf2Size{ 0.f, 0.f }, m_xmf2StartAndLifeTime{ 0.f, 0.f }, m_fValue{ 0.f } {}
	GeometryBillboardVertex(XMFLOAT3 xmf3Position, XMFLOAT2 xmf2Size, XMFLOAT2 xmf2StartLife, XMFLOAT2 xmf2RatioAmp, float fValue) :
		m_xmf3Position{ xmf3Position }, m_xmf2Size{ xmf2Size },
		m_xmf2StartAndLifeTime{ xmf2StartLife }, m_fValue{ fValue }{}
};

struct HpVertex {
	XMFLOAT3 m_xmf3Position;
	XMFLOAT2 m_xmf2Size;
	float m_fColor;
};

class CLavaVertex : public CVertex
{
public:
	XMFLOAT2						m_xmf2TexCoord;
	float							m_value;

public:
	CLavaVertex() { m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f); m_xmf2TexCoord = XMFLOAT2(0.0f, 0.0f); m_value = 0.f; }
	CLavaVertex(float x, float y, float z, XMFLOAT2 xmf2TexCoord) { m_xmf3Position = XMFLOAT3(x, y, z); m_xmf2TexCoord = xmf2TexCoord; }
	CLavaVertex(XMFLOAT3 xmf3Position, XMFLOAT2 xmf2TexCoord = XMFLOAT2(0.0f, 0.0f), float value = 0.f) { m_xmf3Position = xmf3Position; m_xmf2TexCoord = xmf2TexCoord; m_value = value; }
	~CLavaVertex() { }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CMesh
{
public:
	CMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
	CMesh() {}
	virtual ~CMesh();

private:
	int m_nReferences = 0;

public:
	void AddRef() { m_nReferences++; }
	void Release() { if (--m_nReferences <= 0) delete this; }

protected:
	ComPtr<ID3DBlob> VertexBufferCPU = nullptr;
	ComPtr<ID3DBlob> IndexBufferCPU = nullptr;

	ComPtr<ID3D12Resource> m_pd3dVertexBuffer = nullptr;
	ComPtr<ID3D12Resource> m_pd3dVertexUploadBuffer = nullptr;

	ComPtr<ID3D12Resource> m_pd3dIndexBuffer = nullptr;
	ComPtr<ID3D12Resource> m_pd3dIndexUploadBuffer = nullptr;

	D3D12_VERTEX_BUFFER_VIEW m_d3dVertexBufferView = {};
	D3D12_INDEX_BUFFER_VIEW	m_d3dIndexBufferView = {};

	D3D12_PRIMITIVE_TOPOLOGY m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_LINELIST;
	UINT m_nSlot = 0;
	UINT m_nVertices = 0;
	UINT m_nStride = 0;
	UINT m_nOffset = 0;

	UINT m_nIndices = 0;
	UINT m_nStartIndex = 0;
	int	m_nBaseVertex = 0;
	XMFLOAT3 m_xmf3Max = { 0.f, 0.f, 0.f };
	XMFLOAT3 m_xmf3Min = { 10000.f, 10000.f, 10000.f };

	unordered_map<string, XMFLOAT4X4> m_xmf4x4ChildWorld;
	vector<string> vNames;
	UINT m_nChild = 0;

public:
	ComPtr<ID3DBlob> GetVertexBufferCPU() const { return VertexBufferCPU; }
	ComPtr<ID3DBlob> GetIndexBufferCPU() const { return IndexBufferCPU; }
	UINT GetIndexCount() const { return m_nIndices; }
	UINT GetVertexCount() const { return m_nVertices; }

	XMFLOAT3 GetMax() const { return m_xmf3Max; }
	XMFLOAT3 GetMin() const { return m_xmf3Min; }
	void SetMax(XMFLOAT3& max) { m_xmf3Max = max; }
	void SetMin(XMFLOAT3& min) { m_xmf3Min = min; }

	void SetChildNames(vector<string>& names) { vNames = names; }
	XMFLOAT4X4 GetChildWorlds(const string s) { return m_xmf4x4ChildWorld[s]; }

	UINT GetNumVertices() const { return m_nVertices; }

	virtual void BoneTransform(FrameResource* pFrameResource, const aiScene* pModel, float fTime, UINT nSkinCBindex) {}
	virtual void Animate(float fTimeElapsed) {}
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CHeightMapImage
{
private:
	BYTE* m_pHeightMapPixels;

	int m_nWidth = 0;
	int m_nLength = 0;
	XMFLOAT3 m_xmf3Scale;

public:
	CHeightMapImage(LPCTSTR pFileName, int nWidth, int nLength, XMFLOAT3 xmf3Scale);
	~CHeightMapImage(void);

	float GetHeight(float x, float z, bool bReverseQuad = false) const;
	XMFLOAT3 GetHeightMapNormal(int x, int z) const;
	XMFLOAT3 GetScale() const { return(m_xmf3Scale); }

	BYTE* GetHeightMapPixels() const { return(m_pHeightMapPixels); }
	int GetHeightMapWidth() const { return(m_nWidth); }
	int GetHeightMapLength() const { return(m_nLength); }
};

class CHeightMapGridMesh : public CMesh
{
protected:
	int m_nWidth = 0;
	int	m_nLength = 0;
	XMFLOAT3 m_xmf3Scale;

public:
	CHeightMapGridMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int xStart, int zStart, int nWidth, int nLength, XMFLOAT3 xmf3Scale = XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4 xmf4Color = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f), void *pContext = NULL);
	virtual ~CHeightMapGridMesh();

	XMFLOAT3 GetScale() { return(m_xmf3Scale); }
	int GetWidth() { return(m_nWidth); }
	int GetLength() { return(m_nLength); }

	virtual float OnGetHeight(int x, int z, void *pContext) const;
	virtual XMFLOAT4 OnGetColor(int x, int z, void *pContext) const;
};

class CGeometryParticleMesh : public CMesh
{
public:
	CGeometryParticleMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Size, int nVertices, bool is_Spurt);
	virtual ~CGeometryParticleMesh();
};

class CGeometryPortalParticleMesh : public CMesh
{
public:
	CGeometryPortalParticleMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Size, int nVertices, bool is_Spurt);
	virtual ~CGeometryPortalParticleMesh();
};

class CHpMesh : public CMesh
{
public:
	CHpMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Size, bool is_Back);
	virtual ~CHpMesh();
};

class CTexturedRectMesh : public CMesh
{
public:
	CTexturedRectMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList,
		float fWidth, float fHeight, float fDept, float fxPosition, float fyPosition, float fzPosition);
	~CTexturedRectMesh() {};
};

class CColorRectMesh : public CMesh
{
public:
	CColorRectMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList,
		float fWidth, float fHeight, float fDept, float fxPosition, float fyPosition, float fzPosition, XMFLOAT4 xmf4Color);
	~CColorRectMesh() { }
};


class CTextureGridMesh : public CMesh
{
public:
	CTextureGridMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int nWidth, int nLength, XMFLOAT3 xmf3Scale);
	virtual ~CTextureGridMesh() {};

};

class CLavaMesh : public CMesh
{
public:
	CLavaMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList,
		float fWidth, float fHeight, float fDept, float fxPosition, float fyPosition, float fzPosition);
};

class CSkyBoxMesh : public CMesh
{
public:
	CSkyBoxMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, float fWidth = 20.0f, float fHeight = 20.0f, float fDepth = 20.0f);
	virtual ~CSkyBoxMesh();
};

struct UIVertex
{
	XMFLOAT2 m_xmf2Position = { 0, 0 };
	XMFLOAT2 m_xmf2TexCoord = { 0, 0 };
	UINT m_bIsLeft = true;
};
class CUIMesh : public CMesh
{
public:
	CUIMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Left);
};


// 이거 물구현할라고 해놨던건데 신경 ㄴㄴ
class CWaves : public CMesh
{
public:

	CWaves(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int m, int n, float dx, float dt, float speed, float damping);
	virtual ~CWaves();

	virtual void CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);

	virtual void Animate(float fTimeElapsed);
	void Disturb(int i, int j, float magnitude);
private:
	int m_NumRows = 0;
	int m_NumCols = 0;

	float m_K1 = 0.0f;
	float m_K2 = 0.0f;
	float m_K3 = 0.0f;

	float m_TimeStep = 0.0f;
	float m_SpatialStep = 0.0f;

	UINT m_nElementBytes = 0;

	vector<XMFLOAT3> m_PrevSolution;
	vector<XMFLOAT3> m_CurrSolution;
	vector<XMFLOAT3> m_Normals;
	vector<XMFLOAT3> m_TangentX;

	BYTE* m_MappedData = nullptr;
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CCubeMeshDiffused : public CMesh
{
public:
	CCubeMeshDiffused(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, float fWidth = 2.0f, float fHeight = 2.0f, float fDepth = 2.0f);
	virtual ~CCubeMeshDiffused() {};
};

