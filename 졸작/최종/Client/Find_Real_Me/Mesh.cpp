
#include "stdafx.h"
#include "Mesh.h"

CMesh::CMesh(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList)
{
}

CMesh::~CMesh()
{
}

void CMesh::Render(ID3D12GraphicsCommandList* pd3dCommandList)
{
	pd3dCommandList->IASetPrimitiveTopology(m_d3dPrimitiveTopology);
	pd3dCommandList->IASetVertexBuffers(m_nSlot, 1, &m_d3dVertexBufferView);
	if (m_pd3dIndexBuffer)
	{
		pd3dCommandList->IASetIndexBuffer(&m_d3dIndexBufferView);
		pd3dCommandList->DrawIndexedInstanced(m_nIndices, 1, 0, 0, 0);
	}
	else
	{
		pd3dCommandList->DrawInstanced(m_nVertices, 1, m_nOffset, 0);
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
CHeightMapImage::CHeightMapImage(LPCTSTR pFileName, int nWidth, int nLength, XMFLOAT3 xmf3Scale)
{
	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmf3Scale = xmf3Scale;

	BYTE *pHeightMapPixels = new BYTE[m_nWidth * m_nLength];

	HANDLE hFile = ::CreateFile(pFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_READONLY, NULL);
	DWORD dwBytesRead;
	::ReadFile(hFile, pHeightMapPixels, (m_nWidth * m_nLength), &dwBytesRead, NULL);
	::CloseHandle(hFile);

	m_pHeightMapPixels = new BYTE[m_nWidth * m_nLength];
	for (int y = 0; y < m_nLength; y++)
	{
		for (int x = 0; x < m_nWidth; x++)
		{
			m_pHeightMapPixels[x + ((m_nLength - 1 - y)*m_nWidth)] = pHeightMapPixels[x + (y*m_nWidth)];
		}
	}

	if (pHeightMapPixels) delete[] pHeightMapPixels;
}

CHeightMapImage::~CHeightMapImage()
{
	if (m_pHeightMapPixels) delete[] m_pHeightMapPixels;
	m_pHeightMapPixels = NULL;
}

XMFLOAT3 CHeightMapImage::GetHeightMapNormal(int x, int z) const
{
	if ((x < 0.0f) || (z < 0.0f) || (x >= m_nWidth) || (z >= m_nLength)) return(XMFLOAT3(0.0f, 1.0f, 0.0f));

	int nHeightMapIndex = x + (z * m_nWidth);
	int xHeightMapAdd = (x < (m_nWidth - 1)) ? 1 : -1;
	int zHeightMapAdd = (z < (m_nLength - 1)) ? m_nWidth : -m_nWidth;
	float y1 = (float)m_pHeightMapPixels[nHeightMapIndex] * m_xmf3Scale.y;
	float y2 = (float)m_pHeightMapPixels[nHeightMapIndex + xHeightMapAdd] * m_xmf3Scale.y;
	float y3 = (float)m_pHeightMapPixels[nHeightMapIndex + zHeightMapAdd] * m_xmf3Scale.y;
	XMFLOAT3 xmf3Edge1 = XMFLOAT3(0.0f, y3 - y1, m_xmf3Scale.z);
	XMFLOAT3 xmf3Edge2 = XMFLOAT3(m_xmf3Scale.x, y2 - y1, 0.0f);
	XMFLOAT3 xmf3Normal = Vector3::CrossProduct(xmf3Edge1, xmf3Edge2, true);

	return(xmf3Normal);
}

#define _WITH_APPROXIMATE_OPPOSITE_CORNER

float CHeightMapImage::GetHeight(float fx, float fz, bool bReverseQuad) const
{
	fx = fx / m_xmf3Scale.x;		// 이미지상에서의 x값으로 바꿈
	fz = fz / m_xmf3Scale.z;		// 이미지상에서의 z값으로 바꿈
	if ((fx < 0.0f) || (fz < 0.0f) || (fx >= m_nWidth) || (fz >= m_nLength)) return(0.0f);

	int x = (int)fx;				// int로 형변환. 픽셀값을 읽어야 하기 때문에.
	int z = (int)fz;
	float fxPercent = fx - x;		// float가 int로 바뀌면서 얼마만큼 짤렸는가를 저장.
	float fzPercent = fz - z;

	// 그 점을 둘러싼 픽셀 한개를 도출
	float fBottomLeft = (float)m_pHeightMapPixels[x + (z*m_nWidth)];
	float fBottomRight = (float)m_pHeightMapPixels[(x + 1) + (z*m_nWidth)];
	float fTopLeft = (float)m_pHeightMapPixels[x + ((z + 1)*m_nWidth)];
	float fTopRight = (float)m_pHeightMapPixels[(x + 1) + ((z + 1)*m_nWidth)];
#ifdef _WITH_APPROXIMATE_OPPOSITE_CORNER
	if (bReverseQuad)
	{
		// 만약에 z가 짤린게 더 많다면
		if (fzPercent >= fxPercent)
			fBottomRight = fBottomLeft + (fTopRight - fTopLeft);
		else
			fTopLeft = fTopRight + (fBottomLeft - fBottomRight);
	}
	else
	{
		if (fzPercent < (1.0f - fxPercent))
			fTopRight = fTopLeft + (fBottomRight - fBottomLeft);
		else
			fBottomLeft = fTopLeft + (fBottomRight - fTopRight);
	}
#endif
	float fTopHeight = fTopLeft * (1 - fxPercent) + fTopRight * fxPercent;
	float fBottomHeight = fBottomLeft * (1 - fxPercent) + fBottomRight * fxPercent;
	float fHeight = fBottomHeight * (1 - fzPercent) + fTopHeight * fzPercent;

	return(fHeight);
}

CHeightMapGridMesh::CHeightMapGridMesh(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int xStart, int zStart, int nWidth, int nLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color, void *pContext) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nVertices = nWidth * nLength;
	//	m_nStride = sizeof(CTexturedVertex);
	//	m_nStride = sizeof(CDiffusedTexturedVertex);
	m_nStride = sizeof(CDetailTexturedilluminatedVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	m_nWidth = nWidth;
	m_nLength = nLength;
	m_xmf3Scale = xmf3Scale;

	//	CTexturedVertex *pVertices = new CTexturedVertex[m_nVertices];
		// CDiffusedTexturedVertex *pVertices = new CDiffusedTexturedVertex[m_nVertices];
	CDetailTexturedilluminatedVertex *pVertices = new CDetailTexturedilluminatedVertex[m_nVertices];
	CHeightMapImage *pHeightMapImage = (CHeightMapImage *)pContext;
	int cxHeightMap = pHeightMapImage->GetHeightMapWidth();
	int czHeightMap = pHeightMapImage->GetHeightMapLength();
	XMFLOAT3 Max = { FLT_MIN, FLT_MIN, FLT_MIN };
	XMFLOAT3 Min = { FLT_MAX, FLT_MAX, FLT_MAX };
	float fHeight = 0.0f, fMinHeight = +FLT_MAX, fMaxHeight = -FLT_MAX;
	int index = 0;
	for (int i = 0, z = zStart; z < (zStart + nLength); z++)
	{
		for (int x = xStart; x < (xStart + nWidth); x++, i++)
		{
			fHeight = OnGetHeight(x, z, pContext);
			pVertices[i].m_xmf3Position = XMFLOAT3((x*m_xmf3Scale.x), fHeight, (z*m_xmf3Scale.z));

			pVertices[i].m_xmf4Diffuse = Vector4::Add(OnGetColor(x, z, pContext), xmf4Color);
			//pVertices[i].m_xmf2TexCoord0 = XMFLOAT2(float(x) / float(cxHeightMap - 1), float(czHeightMap - 1 - z) / float(czHeightMap - 1));
			pVertices[i].m_xmf2TexCoord0 = XMFLOAT2(float(x) / float(m_xmf3Scale.x * 0.1f), float(z) / float(m_xmf3Scale.z * 0.1f));
			pVertices[i].m_xmf2TexCoord1 = XMFLOAT2(float(x) / float(m_xmf3Scale.x * 0.05f), float(z) / float(m_xmf3Scale.z * 0.05f));
			pVertices[i].m_xmf3Normal = pHeightMapImage->GetHeightMapNormal(x, z);
			if (fHeight < fMinHeight) fMinHeight = fHeight;
			if (fHeight > fMaxHeight) fMaxHeight = fHeight;
		}
		if (index < i)
			index = i;
	}
	ThrowIfFailed(D3DCreateBlob(m_nStride * m_nVertices, &VertexBufferCPU));
	CopyMemory(VertexBufferCPU->GetBufferPointer(), pVertices, m_nStride * m_nVertices);

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);
	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	delete[] pVertices;

	m_nIndices = (nWidth - 1) * (nLength - 1) * 2 * 3; //((nWidth * 2)*(nLength - 1)) + ((nLength - 1) - 1);
	UINT *pnIndices = new UINT[m_nIndices];

	for (int j = 0, z = 0; z < nLength - 1; z++)
	{
		if ((z % 2) == 0)
		{
			for (int x = 0; x < nWidth; x++)
			{
				if ((x == 0) && (z > 0)) pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)((x + (z * nWidth)) + nWidth);
			}
		}
		else
		{
			for (int x = nWidth - 1; x >= 0; x--)
			{
				if (x == (nWidth - 1)) pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)((x + (z * nWidth)) + nWidth);
			}
		}
	}


	//UINT k = 0;
	//for (UINT i = 0; i < nWidth; ++i) {
	//	for (UINT j = 0; j < nLength; ++j) {
	//		pnIndices[k] = i * nLength + j;
	//		pnIndices[k + 1] = i * nLength + j + 1;
	//		pnIndices[k + 2] = (i + 1) * nLength + j;

	//		pnIndices[k + 3] = (i + 1) * nLength + j;
	//		pnIndices[k + 4] = i * nLength + j + 1;
	//		pnIndices[k + 5] = (i + 1) * nLength + j + 1;

	//		k += 6;
	//	}
	//}
	ThrowIfFailed(D3DCreateBlob(sizeof(UINT) * m_nIndices, &IndexBufferCPU));
	CopyMemory(IndexBufferCPU->GetBufferPointer(), pnIndices, sizeof(UINT) * m_nIndices);

	m_pd3dIndexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pnIndices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	delete[] pnIndices;
}

CHeightMapGridMesh::~CHeightMapGridMesh()
{
}

float CHeightMapGridMesh::OnGetHeight(int x, int z, void *pContext) const
{
	CHeightMapImage *pHeightMapImage = (CHeightMapImage *)pContext;
	BYTE *pHeightMapPixels = pHeightMapImage->GetHeightMapPixels();
	XMFLOAT3 xmf3Scale = pHeightMapImage->GetScale();
	int nWidth = pHeightMapImage->GetHeightMapWidth();
	float fHeight = pHeightMapPixels[x + (z*nWidth)] * xmf3Scale.y;
	return(fHeight);
}

XMFLOAT4 CHeightMapGridMesh::OnGetColor(int x, int z, void *pContext) const
{
	XMFLOAT3 xmf3LightDirection = XMFLOAT3(-1.0f, 1.0f, 1.0f);
	xmf3LightDirection = Vector3::Normalize(xmf3LightDirection);
	CHeightMapImage *pHeightMapImage = (CHeightMapImage *)pContext;
	XMFLOAT3 xmf3Scale = pHeightMapImage->GetScale();
	XMFLOAT4 xmf4IncidentLightColor(0.9f, 0.8f, 0.4f, 1.0f);
	float fScale = Vector3::DotProduct(pHeightMapImage->GetHeightMapNormal(x, z), xmf3LightDirection);
	fScale += Vector3::DotProduct(pHeightMapImage->GetHeightMapNormal(x + 1, z), xmf3LightDirection);
	fScale += Vector3::DotProduct(pHeightMapImage->GetHeightMapNormal(x + 1, z + 1), xmf3LightDirection);
	fScale += Vector3::DotProduct(pHeightMapImage->GetHeightMapNormal(x, z + 1), xmf3LightDirection);
	fScale = (fScale / 4.0f) + 0.05f;
	if (fScale > 1.0f) fScale = 1.0f;
	if (fScale < 0.25f) fScale = 0.25f;
	XMFLOAT4 xmf4Color = Vector4::Multiply(fScale, xmf4IncidentLightColor);
	return(xmf4Color);
}
///////////////////////////////////////////////////////////
CGeometryParticleMesh::CGeometryParticleMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Size, int nVertices, bool is_Spurt) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nStride = sizeof(GeometryBillboardVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;

	m_nVertices = nVertices;
	GeometryBillboardVertex* pVertices = new GeometryBillboardVertex[m_nVertices];

	default_random_engine dre;
	uniform_real_distribution<> urd(3.f, 7.f);

	for (int i = 0; i < m_nVertices; ++i) {
		pVertices[i].m_xmf3Position = XMFLOAT3(0.f, 0.f, 0.f);//XMFLOAT3(100 * sin(i / (float)m_nVertices * 3.14f * 2.f), 100.f, 100 * cos(i / (float)m_nVertices * 3.14f * 2.f));
		pVertices[i].m_xmf2Size = xmf2Size;
		if (is_Spurt)
			pVertices[i].m_xmf2StartAndLifeTime = XMFLOAT2((float)urd(dre), (float)urd(dre) * 50.f);
		else
			pVertices[i].m_xmf2StartAndLifeTime = XMFLOAT2((float)urd(dre), 150.f);
		pVertices[i].m_fValue = float(i) / m_nVertices;
	}

	m_pd3dVertexBuffer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices,
		m_nStride*m_nVertices, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	if (pVertices) delete[] pVertices;
}

CGeometryParticleMesh::~CGeometryParticleMesh()
{
}

///////////////////////////////////////////////////////////
CGeometryPortalParticleMesh::CGeometryPortalParticleMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Size, int nVertices, bool is_Spurt) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nStride = sizeof(GeometryBillboardVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;

	m_nVertices = nVertices;
	GeometryBillboardVertex* pVertices = new GeometryBillboardVertex[m_nVertices];

	default_random_engine dre;
	uniform_real_distribution<> urd(3.f, 10.f);

	for (int i = 0; i < m_nVertices; ++i) {
		pVertices[i].m_xmf3Position = XMFLOAT3(0, 0, 0);//XMFLOAT3(100 * sin(i / (float)m_nVertices * 3.14f * 2.f), 100.f, 100 * cos(i / (float)m_nVertices * 3.14f * 2.f));
		pVertices[i].m_xmf2Size = xmf2Size;
		if (is_Spurt)
			pVertices[i].m_xmf2StartAndLifeTime = XMFLOAT2((float)urd(dre), (float)urd(dre) * 100.f);
		else
			pVertices[i].m_xmf2StartAndLifeTime = XMFLOAT2((float)urd(dre), 150.f);
		pVertices[i].m_fValue = float(i) / m_nVertices;
	}

	m_pd3dVertexBuffer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices,
		m_nStride*m_nVertices, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	if (pVertices) delete[] pVertices;
}

CGeometryPortalParticleMesh::~CGeometryPortalParticleMesh()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CHpMesh::CHpMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Size, bool is_Back) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nStride = sizeof(HpVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;

	m_nVertices = 1;
	HpVertex* pVertex = new HpVertex();

	pVertex->m_xmf3Position = XMFLOAT3(0.f, 0.f, 0.f);
	pVertex->m_xmf2Size = xmf2Size;
	if (is_Back) {
		pVertex->m_fColor = 0.f;
	}
	else {
		pVertex->m_fColor = 1.f;
	}

	m_pd3dVertexBuffer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, pVertex,
		m_nStride*m_nVertices, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	if (pVertex) delete[] pVertex;
}

CHpMesh::~CHpMesh()
{
}
///////////////////////////////////////////////////////////
CTexturedRectMesh::CTexturedRectMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList,
	float fWidth, float fHeight, float fDepth, float fxPosition, float fyPosition, float fzPosition) : CMesh(pd3dDevice, pd3dCommandList) {

	m_nVertices = 6;
	m_nStride = sizeof(CTexturedVertex);
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	CTexturedVertex pVertices[6];
	float fx = (fWidth * 0.5) + fxPosition, fy = (fHeight * 0.5) + fyPosition, fz = (fDepth * 0.5) + fzPosition;
	if (fWidth == 0.f) {
		if (fxPosition > 0.f) {
			pVertices[0] = CTexturedVertex(XMFLOAT3(fx, +fy, -fz), XMFLOAT2(1.f, 0.f));
			pVertices[1] = CTexturedVertex(XMFLOAT3(fx, -fy, -fz), XMFLOAT2(1.f, 1.f));
			pVertices[2] = CTexturedVertex(XMFLOAT3(fx, -fy, +fz), XMFLOAT2(0.f, 1.f));
			pVertices[3] = CTexturedVertex(XMFLOAT3(fx, -fy, +fz), XMFLOAT2(0.f, 1.f));
			pVertices[4] = CTexturedVertex(XMFLOAT3(fx, +fy, +fz), XMFLOAT2(0.f, 0.f));
			pVertices[5] = CTexturedVertex(XMFLOAT3(fx, +fy, -fz), XMFLOAT2(1.f, 0.f));
		}
		else {
			pVertices[0] = CTexturedVertex(XMFLOAT3(fx, +fy, +fz), XMFLOAT2(1.f, 0.f));
			pVertices[1] = CTexturedVertex(XMFLOAT3(fx, -fy, +fz), XMFLOAT2(1.f, 1.f));
			pVertices[2] = CTexturedVertex(XMFLOAT3(fx, -fy, -fz), XMFLOAT2(0.f, 1.f));
			pVertices[3] = CTexturedVertex(XMFLOAT3(fx, -fy, -fz), XMFLOAT2(0.f, 1.f));
			pVertices[4] = CTexturedVertex(XMFLOAT3(fx, +fy, -fz), XMFLOAT2(0.f, 0.f));
			pVertices[5] = CTexturedVertex(XMFLOAT3(fx, +fy, +fz), XMFLOAT2(1.f, 0.f));
		}
	}
	else if (fHeight == 0.f) {
		if (fyPosition > 0.f) {
			pVertices[0] = CTexturedVertex(XMFLOAT3(+fx, fy, -fz), XMFLOAT2(1.f, 0.f));
			pVertices[1] = CTexturedVertex(XMFLOAT3(+fx, fy, +fz), XMFLOAT2(1.f, 1.f));
			pVertices[2] = CTexturedVertex(XMFLOAT3(-fx, fy, +fz), XMFLOAT2(0.f, 1.f));
			pVertices[3] = CTexturedVertex(XMFLOAT3(-fx, fy, +fz), XMFLOAT2(0.f, 1.f));
			pVertices[4] = CTexturedVertex(XMFLOAT3(-fx, fy, -fz), XMFLOAT2(0.f, 0.f));
			pVertices[5] = CTexturedVertex(XMFLOAT3(+fx, fy, -fz), XMFLOAT2(1.f, 0.f));
		}
		else {
			pVertices[0] = CTexturedVertex(XMFLOAT3(+fx, fy, +fz), XMFLOAT2(1.f, 0.f));
			pVertices[1] = CTexturedVertex(XMFLOAT3(+fx, fy, -fz), XMFLOAT2(1.f, 1.f));
			pVertices[2] = CTexturedVertex(XMFLOAT3(-fx, fy, -fz), XMFLOAT2(0.f, 1.f));
			pVertices[3] = CTexturedVertex(XMFLOAT3(-fx, fy, -fz), XMFLOAT2(0.f, 1.f));
			pVertices[4] = CTexturedVertex(XMFLOAT3(-fx, fy, +fz), XMFLOAT2(0.f, 0.f));
			pVertices[5] = CTexturedVertex(XMFLOAT3(+fx, fy, +fz), XMFLOAT2(1.f, 0.f));
		}
	}
	else if (fDepth == 0.f) {
		//if (fzPosition > 0.f) {
		pVertices[0] = CTexturedVertex(XMFLOAT3(+fx, +fy, fz), XMFLOAT2(1.f, 0.f));
		pVertices[1] = CTexturedVertex(XMFLOAT3(+fx, -fy, fz), XMFLOAT2(1.f, 1.f));
		pVertices[2] = CTexturedVertex(XMFLOAT3(-fx, -fy, fz), XMFLOAT2(0.f, 1.f));
		pVertices[3] = CTexturedVertex(XMFLOAT3(-fx, -fy, fz), XMFLOAT2(0.f, 1.f));
		pVertices[4] = CTexturedVertex(XMFLOAT3(-fx, +fy, fz), XMFLOAT2(0.f, 0.f));
		pVertices[5] = CTexturedVertex(XMFLOAT3(+fx, +fy, fz), XMFLOAT2(1.f, 0.f));
		//}
		/*else {
			pVertices[0] = CTexturedVertex(XMFLOAT3(-fx, +fy, fz), XMFLOAT2(1.f, 0.f));
			pVertices[1] = CTexturedVertex(XMFLOAT3(-fx, -fy, fz), XMFLOAT2(1.f, 1.f));
			pVertices[2] = CTexturedVertex(XMFLOAT3(+fx, -fy, fz), XMFLOAT2(0.f, 1.f));
			pVertices[3] = CTexturedVertex(XMFLOAT3(+fx, -fy, fz), XMFLOAT2(0.f, 1.f));
			pVertices[4] = CTexturedVertex(XMFLOAT3(+fx, +fy, fz), XMFLOAT2(0.f, 0.f));
			pVertices[5] = CTexturedVertex(XMFLOAT3(-fx, +fy, fz), XMFLOAT2(1.f, 0.f));
		}*/
	}
	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;
}
//////////////////////////////////////////////////////////////////////
CColorRectMesh::CColorRectMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList,
	float fWidth, float fHeight, float fDepth, float fxPosition, float fyPosition, float fzPosition, XMFLOAT4 xmf4Color) : CMesh(pd3dDevice, pd3dCommandList) {

	m_nVertices = 6;
	m_nStride = sizeof(CDiffusedVertex);
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	CDiffusedVertex pVertices[6];
	float fx = (fWidth * 0.5) + fxPosition, fy = (fHeight * 0.5) + fyPosition, fz = (fDepth * 0.5) + fzPosition;
	if (fWidth == 0.f) {
		if (fxPosition > 0.f) {
			pVertices[0] = CDiffusedVertex(XMFLOAT3(fx, +fy, -fz), xmf4Color);
			pVertices[1] = CDiffusedVertex(XMFLOAT3(fx, -fy, -fz), xmf4Color);
			pVertices[2] = CDiffusedVertex(XMFLOAT3(fx, -fy, +fz), xmf4Color);
			pVertices[3] = CDiffusedVertex(XMFLOAT3(fx, -fy, +fz), xmf4Color);
			pVertices[4] = CDiffusedVertex(XMFLOAT3(fx, +fy, +fz), xmf4Color);
			pVertices[5] = CDiffusedVertex(XMFLOAT3(fx, +fy, -fz), xmf4Color);
		}
		else {
			pVertices[0] = CDiffusedVertex(XMFLOAT3(fx, +fy, +fz), xmf4Color);
			pVertices[1] = CDiffusedVertex(XMFLOAT3(fx, -fy, +fz), xmf4Color);
			pVertices[2] = CDiffusedVertex(XMFLOAT3(fx, -fy, -fz), xmf4Color);
			pVertices[3] = CDiffusedVertex(XMFLOAT3(fx, -fy, -fz), xmf4Color);
			pVertices[4] = CDiffusedVertex(XMFLOAT3(fx, +fy, -fz), xmf4Color);
			pVertices[5] = CDiffusedVertex(XMFLOAT3(fx, +fy, +fz), xmf4Color);
		}
	}
	else if (fHeight == 0.f) {
		if (fyPosition > 0.f) {
			pVertices[0] = CDiffusedVertex(XMFLOAT3(+fx, fy, -fz), xmf4Color);
			pVertices[1] = CDiffusedVertex(XMFLOAT3(+fx, fy, +fz), xmf4Color);
			pVertices[2] = CDiffusedVertex(XMFLOAT3(-fx, fy, +fz), xmf4Color);
			pVertices[3] = CDiffusedVertex(XMFLOAT3(-fx, fy, +fz), xmf4Color);
			pVertices[4] = CDiffusedVertex(XMFLOAT3(-fx, fy, -fz), xmf4Color);
			pVertices[5] = CDiffusedVertex(XMFLOAT3(+fx, fy, -fz), xmf4Color);
		}
		else {
			pVertices[0] = CDiffusedVertex(XMFLOAT3(+fx, fy, +fz), xmf4Color);
			pVertices[1] = CDiffusedVertex(XMFLOAT3(+fx, fy, -fz), xmf4Color);
			pVertices[2] = CDiffusedVertex(XMFLOAT3(-fx, fy, -fz), xmf4Color);
			pVertices[3] = CDiffusedVertex(XMFLOAT3(-fx, fy, -fz), xmf4Color);
			pVertices[4] = CDiffusedVertex(XMFLOAT3(-fx, fy, +fz), xmf4Color);
			pVertices[5] = CDiffusedVertex(XMFLOAT3(+fx, fy, +fz), xmf4Color);
		}
	}
	else if (fDepth == 0.f) {
		if (fzPosition > 0.f) {
			pVertices[0] = CDiffusedVertex(XMFLOAT3(+fx, +fy, fz), xmf4Color);
			pVertices[1] = CDiffusedVertex(XMFLOAT3(+fx, -fy, fz), xmf4Color);
			pVertices[2] = CDiffusedVertex(XMFLOAT3(-fx, -fy, fz), xmf4Color);
			pVertices[3] = CDiffusedVertex(XMFLOAT3(-fx, -fy, fz), xmf4Color);
			pVertices[4] = CDiffusedVertex(XMFLOAT3(-fx, +fy, fz), xmf4Color);
			pVertices[5] = CDiffusedVertex(XMFLOAT3(+fx, +fy, fz), xmf4Color);
		}
		else {
			pVertices[0] = CDiffusedVertex(XMFLOAT3(-fx, +fy, fz), xmf4Color);
			pVertices[1] = CDiffusedVertex(XMFLOAT3(-fx, -fy, fz), xmf4Color);
			pVertices[2] = CDiffusedVertex(XMFLOAT3(+fx, -fy, fz), xmf4Color);
			pVertices[3] = CDiffusedVertex(XMFLOAT3(+fx, -fy, fz), xmf4Color);
			pVertices[4] = CDiffusedVertex(XMFLOAT3(+fx, +fy, fz), xmf4Color);
			pVertices[5] = CDiffusedVertex(XMFLOAT3(-fx, +fy, fz), xmf4Color);
		}
	}
	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;
}

/////////////////////////////////////////////////////////////////
CTextureGridMesh::CTextureGridMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, int nWidth, int nLength, XMFLOAT3 xmf3Scale) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nVertices = nWidth * nLength;
	m_nStride = sizeof(CIlluminatedTexturedVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	CIlluminatedTexturedVertex *pVertices = new CIlluminatedTexturedVertex[m_nVertices];

	uniform_real_distribution<> uid(0, 1);
	uniform_real_distribution<> uid2(0, 0.1f);
	default_random_engine dre;

	for (int i = 0, z = 0; z < nLength; ++z) {
		for (int x = 0; x < nWidth; ++x, ++i) {
			pVertices[i].m_xmf3Position = XMFLOAT3((x*xmf3Scale.x), 60, (z*xmf3Scale.z));
			pVertices[i].m_xmf3Normal = XMFLOAT3(uid(dre), uid(dre), uid(dre));
			pVertices[i].m_xmf2TexCoord = XMFLOAT2(float(x) / float(xmf3Scale.x * 0.1f), float(z) / float(xmf3Scale.z * 0.1f));
			pVertices[i].m_fValue = uid(dre);
		}
	}

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	delete[] pVertices;

	m_nIndices = ((nWidth * 2)*(nLength - 1)) + ((nLength - 1) - 1);
	UINT *pnIndices = new UINT[m_nIndices];

	for (int j = 0, z = 0; z < nLength - 1; z++)
	{
		if ((z % 2) == 0)
		{
			for (int x = 0; x < nWidth; x++)
			{
				if ((x == 0) && (z > 0)) pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)((x + (z * nWidth)) + nWidth);
			}
		}
		else
		{
			for (int x = nWidth - 1; x >= 0; x--)
			{
				if (x == (nWidth - 1)) pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)(x + (z * nWidth));
				pnIndices[j++] = (UINT)((x + (z * nWidth)) + nWidth);
			}
		}
	}

	m_pd3dIndexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pnIndices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	delete[] pnIndices;
}

CSkyBoxMesh::CSkyBoxMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, float fWidth, float fHeight, float fDepth) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nVertices = 36;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	XMFLOAT3* pVertices = new XMFLOAT3[m_nVertices];
	m_nStride = sizeof(XMFLOAT3);

	float fx = fWidth * 0.5f, fy = fHeight * 0.5f, fz = fDepth * 0.5f;
	// Front Quad (quads point inward)
	pVertices[0] = XMFLOAT3(-fx, +fx, +fx);
	pVertices[1] = XMFLOAT3(+fx, +fx, +fx);
	pVertices[2] = XMFLOAT3(-fx, -fx, +fx);
	pVertices[3] = XMFLOAT3(-fx, -fx, +fx);
	pVertices[4] = XMFLOAT3(+fx, +fx, +fx);
	pVertices[5] = XMFLOAT3(+fx, -fx, +fx);
	// Back Quad										
	pVertices[6] = XMFLOAT3(+fx, +fx, -fx);
	pVertices[7] = XMFLOAT3(-fx, +fx, -fx);
	pVertices[8] = XMFLOAT3(+fx, -fx, -fx);
	pVertices[9] = XMFLOAT3(+fx, -fx, -fx);
	pVertices[10] = XMFLOAT3(-fx, +fx, -fx);
	pVertices[11] = XMFLOAT3(-fx, -fx, -fx);
	// Left Quad										
	pVertices[12] = XMFLOAT3(-fx, +fx, -fx);
	pVertices[13] = XMFLOAT3(-fx, +fx, +fx);
	pVertices[14] = XMFLOAT3(-fx, -fx, -fx);
	pVertices[15] = XMFLOAT3(-fx, -fx, -fx);
	pVertices[16] = XMFLOAT3(-fx, +fx, +fx);
	pVertices[17] = XMFLOAT3(-fx, -fx, +fx);
	// Right Quad										
	pVertices[18] = XMFLOAT3(+fx, +fx, +fx);
	pVertices[19] = XMFLOAT3(+fx, +fx, -fx);
	pVertices[20] = XMFLOAT3(+fx, -fx, +fx);
	pVertices[21] = XMFLOAT3(+fx, -fx, +fx);
	pVertices[22] = XMFLOAT3(+fx, +fx, -fx);
	pVertices[23] = XMFLOAT3(+fx, -fx, -fx);
	// Top Quad											
	pVertices[24] = XMFLOAT3(-fx, +fx, -fx);
	pVertices[25] = XMFLOAT3(+fx, +fx, -fx);
	pVertices[26] = XMFLOAT3(-fx, +fx, +fx);
	pVertices[27] = XMFLOAT3(-fx, +fx, +fx);
	pVertices[28] = XMFLOAT3(+fx, +fx, -fx);
	pVertices[29] = XMFLOAT3(+fx, +fx, +fx);
	// Bottom Quad										
	pVertices[30] = XMFLOAT3(-fx, -fx, +fx);
	pVertices[31] = XMFLOAT3(+fx, -fx, +fx);
	pVertices[32] = XMFLOAT3(-fx, -fx, -fx);
	pVertices[33] = XMFLOAT3(-fx, -fx, -fx);
	pVertices[34] = XMFLOAT3(+fx, -fx, +fx);
	pVertices[35] = XMFLOAT3(+fx, -fx, -fx);

	m_pd3dVertexBuffer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, sizeof(XMFLOAT3) * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = sizeof(XMFLOAT3);
	m_d3dVertexBufferView.SizeInBytes = sizeof(XMFLOAT3) * m_nVertices;
	delete[] pVertices;
}

CSkyBoxMesh::~CSkyBoxMesh()
{
}

///////////////////////////////////////////////////////////////
CWaves::CWaves(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int m, int n, float dx, float dt, float speed, float damping) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_NumRows = m;
	m_NumCols = n;

	m_nVertices = m * n;
	m_nIndices = (m - 1) * (n - 1) * 2 * 3;

	m_TimeStep = dt;
	m_SpatialStep = dx;

	float d = damping * dt + 2.0f;
	float e = (speed*speed)*(dt*dt) / (dx*dx);
	m_K1 = (damping*dt - 2.0f) / d;
	m_K2 = (4.0f - 8.0f*e) / d;
	m_K3 = (2.0f*e) / d;

	m_PrevSolution.resize(m*n);
	m_CurrSolution.resize(m*n);
	m_Normals.resize(m*n);
	m_TangentX.resize(m*n);

	float halfWidth = (n - 1)*dx*0.5f;
	float halfDepth = (m - 1)*dx*0.5f;
	for (int i = 0; i < m; ++i)
	{
		float z = halfDepth - i * dx;
		for (int j = 0; j < n; ++j)
		{
			float x = -halfWidth + j * dx;

			m_PrevSolution[i*n + j] = XMFLOAT3(x, 0.0f, z);
			m_CurrSolution[i*n + j] = XMFLOAT3(x, 0.0f, z);
			m_Normals[i*n + j] = XMFLOAT3(0.0f, 1.0f, 0.0f);
			m_TangentX[i*n + j] = XMFLOAT3(1.0f, 0.0f, 0.0f);
		}
	}
	m_nStride = sizeof(CDiffusedVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	//CDiffusedVertex* pVertices = new CDiffusedVertex[m_CurrSolution.size()];
	//for (int i = 0; i < m_CurrSolution.size(); ++i) {
	//	pVertices[i].m_xmf3Position = m_CurrSolution[i];
	//	pVertices[i].m_xmf4Diffuse = XMFLOAT4(0.f, 0.f, 1.f, 1.f);
	//}
	//m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	//m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	//m_d3dVertexBufferView.StrideInBytes = m_nStride;
	//m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	UINT *pnIndices = new UINT[m_nIndices];
	int k = 0;
	for (int i = 0; i < m - 1; ++i)
	{
		for (int j = 0; j < n - 1; ++j)
		{
			pnIndices[k] = i * n + j;
			pnIndices[k + 1] = i * n + j + 1;
			pnIndices[k + 2] = (i + 1)*n + j;

			pnIndices[k + 3] = (i + 1)*n + j;
			pnIndices[k + 4] = i * n + j + 1;
			pnIndices[k + 5] = (i + 1)*n + j + 1;

			k += 6; // next quad
		}
	}
	m_pd3dIndexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pnIndices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);
	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	delete[] pnIndices;

}

///////////////////////////////////////////////////////////////
CLavaMesh::CLavaMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList,
	float fWidth, float fHeight, float fDepth, float fxPosition, float fyPosition, float fzPosition) : CMesh(pd3dDevice, pd3dCommandList) {

	m_nVertices = 24 * 20 * 36;
	m_nStride = sizeof(CLavaVertex);
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	CLavaVertex* pVertices = new CLavaVertex[m_nVertices];
	float fx = (fWidth * 0.5) + fxPosition, fy = (fHeight * 0.5) + fyPosition, fz = (fDepth * 0.5) + fzPosition;
	int index = 0;

	default_random_engine dre;
	uniform_real_distribution<> urd(0, 8);
	for (int i = 0; i < 20; ++i) {
		for (int j = 0; j < 36; ++j) {
			float value = urd(dre);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+0 + i * fWidth, fy, +fz + j * fDepth), XMFLOAT2(0.5f, 0.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+0 + i * fWidth, fy, -0 + j * fDepth), XMFLOAT2(0.5f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-fx + i * fWidth, fy, -0 + j * fDepth), XMFLOAT2(0.f, 0.5), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-fx + i * fWidth, fy, -0 + j * fDepth), XMFLOAT2(0.f, 0.5), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-fx + i * fWidth, fy, +fz + j * fDepth), XMFLOAT2(0.f, 0.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+0 + i * fWidth, fy, +fz + j * fDepth), XMFLOAT2(0.5f, 0.f), value);

			pVertices[index++] = CLavaVertex(XMFLOAT3(+fx + i * fWidth, fy, +fz + j * fDepth), XMFLOAT2(1.f, 0.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+fx + i * fWidth, fy, -0 + j * fDepth), XMFLOAT2(1.f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-0 + i * fWidth, fy, -0 + j * fDepth), XMFLOAT2(0.5f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-0 + i * fWidth, fy, -0 + j * fDepth), XMFLOAT2(0.5f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-0 + i * fWidth, fy, +fz + j * fDepth), XMFLOAT2(0.5f, 0.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+fx + i * fWidth, fy, +fz + j * fDepth), XMFLOAT2(1.f, 0.f), value);

			pVertices[index++] = CLavaVertex(XMFLOAT3(+0 + i * fWidth, fy, +0 + j * fDepth), XMFLOAT2(0.5f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+0 + i * fWidth, fy, -fz + j * fDepth), XMFLOAT2(0.5f, 1.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-fx + i * fWidth, fy, -fz + j * fDepth), XMFLOAT2(0.f, 1.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-fx + i * fWidth, fy, -fz + j * fDepth), XMFLOAT2(0.f, 1.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-fx + i * fWidth, fy, +0 + j * fDepth), XMFLOAT2(0.f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+0 + i * fWidth, fy, +0 + j * fDepth), XMFLOAT2(0.5f, 0.5f), value);

			pVertices[index++] = CLavaVertex(XMFLOAT3(+fx + i * fWidth, fy, +0 + j * fDepth), XMFLOAT2(1.f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+fx + i * fWidth, fy, -fz + j * fDepth), XMFLOAT2(1.f, 1.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-0 + i * fWidth, fy, -fz + j * fDepth), XMFLOAT2(0.5f, 1.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-0 + i * fWidth, fy, -fz + j * fDepth), XMFLOAT2(0.5f, 1.f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(-0 + i * fWidth, fy, +0 + j * fDepth), XMFLOAT2(0.5f, 0.5f), value);
			pVertices[index++] = CLavaVertex(XMFLOAT3(+fx + i * fWidth, fy, +0 + j * fDepth), XMFLOAT2(1.f, 0.5f), value);
		}
	}

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	delete[] pVertices;
}

CUIMesh::CUIMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT2 xmf2Left)
{
	m_nVertices = 6;
	m_nStride = sizeof(UIVertex);
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UIVertex* pVertices = new UIVertex[m_nVertices];

	float size = 0.1f;

	pVertices[0] = { XMFLOAT2(xmf2Left.x, xmf2Left.y + size), XMFLOAT2(0, 0), 1 };
	pVertices[1] = { XMFLOAT2(xmf2Left.x + size, xmf2Left.y + size), XMFLOAT2(1, 0), 0 };
	pVertices[2] = { XMFLOAT2(xmf2Left.x + size, xmf2Left.y - size), XMFLOAT2(1, 1), 0 };
	pVertices[3] = { XMFLOAT2(xmf2Left.x + size, xmf2Left.y - size), XMFLOAT2(1, 1), 0 };
	pVertices[4] = { XMFLOAT2(xmf2Left.x, xmf2Left.y - size), XMFLOAT2(0, 1), 1 };
	pVertices[5] = { XMFLOAT2(xmf2Left.x, xmf2Left.y + size), XMFLOAT2(0, 0), 1 };

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	delete[] pVertices;
}

CWaves::~CWaves()
{
	if (m_pd3dVertexUploadBuffer != nullptr)
		m_pd3dVertexUploadBuffer->Unmap(0, nullptr);

	m_pd3dVertexUploadBuffer = nullptr;
}

void CWaves::CreateShaderVariables(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList)
{
	m_nElementBytes = sizeof(CDiffusedVertex);
	ThrowIfFailed(pd3dDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(m_nElementBytes*m_nVertices),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&m_pd3dVertexUploadBuffer)));

	//m_pd3dVertexUploadBuffer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, m_nElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	ThrowIfFailed(m_pd3dVertexUploadBuffer->Map(0, nullptr, reinterpret_cast<void **>(&m_MappedData)));

}

void CWaves::Animate(float fTimeElapsed)
{
	static float t_base = 0.0f;
	t_base += fTimeElapsed;

	if (t_base >= 0.25f)
	{
		t_base = 0.f;
		int i = Rand(4, m_NumRows - 5);
		int j = Rand(4, m_NumCols - 5);

		float r = RandF(0.2f, 0.5f);

		Disturb(i, j, r);
	}

	static float t = 0;
	// Accumulate time.
	t += fTimeElapsed;
	// Only update the simulation at the specified time step.
	if (t >= m_TimeStep)
	{
		// Only update interior points; we use zero boundary conditions.
		concurrency::parallel_for(1, m_NumRows - 1, [this](int i)
			//for(int i = 1; i < m_NumRows-1; ++i)
		{
			for (int j = 1; j < m_NumCols - 1; ++j)
			{
				// After this update we will be discarding the old previous
				// buffer, so overwrite that buffer with the new update.
				// Note how we can do this inplace (read/write to same element) 
				// because we won't need prev_ij again and the assignment happens last.

				// Note j indexes x and i indexes z: h(x_j, z_i, t_k)
				// Moreover, our +z axis goes "down"; this is just to 
				// keep consistent with our row indices going down.
				m_PrevSolution[i*m_NumCols + j].y =
					m_K1 * m_PrevSolution[i*m_NumCols + j].y +
					m_K2 * m_CurrSolution[i*m_NumCols + j].y +
					m_K3 * (m_CurrSolution[(i + 1)*m_NumCols + j].y +
						m_CurrSolution[(i - 1)*m_NumCols + j].y +
						m_CurrSolution[i*m_NumCols + j + 1].y +
						m_CurrSolution[i*m_NumCols + j - 1].y);
			}
		});
		// We just overwrote the previous buffer with the new data, so
		// this data needs to become the current solution and the old
		// current solution becomes the new previous solution.
		std::swap(m_PrevSolution, m_CurrSolution);
		t = 0.0f; // reset timedd
		//
		// Compute normals using finite difference scheme.
		//
		concurrency::parallel_for(1, m_NumRows - 1, [this](int i)
			//for(int i = 1; i < m_NumRows - 1; ++i)
		{
			for (int j = 1; j < m_NumCols - 1; ++j)
			{
				float l = m_CurrSolution[i*m_NumCols + j - 1].y;
				float r = m_CurrSolution[i*m_NumCols + j + 1].y;
				float t = m_CurrSolution[(i - 1)*m_NumCols + j].y;
				float b = m_CurrSolution[(i + 1)*m_NumCols + j].y;
				m_Normals[i*m_NumCols + j].x = -r + l;
				m_Normals[i*m_NumCols + j].y = 2.0f*m_SpatialStep;
				m_Normals[i*m_NumCols + j].z = b - t;

				XMVECTOR n = XMVector3Normalize(XMLoadFloat3(&m_Normals[i*m_NumCols + j]));
				XMStoreFloat3(&m_Normals[i*m_NumCols + j], n);

				m_TangentX[i*m_NumCols + j] = XMFLOAT3(2.0f*m_SpatialStep, r - l, 0.0f);
				XMVECTOR T = XMVector3Normalize(XMLoadFloat3(&m_TangentX[i*m_NumCols + j]));
				XMStoreFloat3(&m_TangentX[i*m_NumCols + j], T);
			}
		});
	}
	for (int i = 0; i < m_CurrSolution.size(); ++i) {
		CDiffusedVertex v;

		v.m_xmf3Position = m_CurrSolution[i];
		v.m_xmf4Diffuse = XMFLOAT4(0.f, 0.f, 1.f, 1.f);

		memcpy(&m_MappedData[i*m_nElementBytes], &v, sizeof(CDiffusedVertex));
	}

	m_pd3dVertexBuffer = m_pd3dVertexUploadBuffer;
	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

}

void CWaves::Disturb(int i, int j, float magnitude)
{
	// Don't disturb boundaries.
	assert(i > 1 && i < m_NumRows - 2);
	assert(j > 1 && j < m_NumCols - 2);

	float halfMag = 0.5f*magnitude;

	// Disturb the ijth vertex height and its neighbors.
	m_CurrSolution[i*m_NumCols + j].y += magnitude;
	m_CurrSolution[i*m_NumCols + j + 1].y += halfMag;
	m_CurrSolution[i*m_NumCols + j - 1].y += halfMag;
	m_CurrSolution[(i + 1)*m_NumCols + j].y += halfMag;
	m_CurrSolution[(i - 1)*m_NumCols + j].y += halfMag;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CCubeMeshDiffused::CCubeMeshDiffused(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, float fWidth, float fHeight, float fDepth) : CMesh(pd3dDevice, pd3dCommandList)
{
	m_nVertices = 8;
	m_nStride = sizeof(CDiffusedVertex);
	m_nOffset = 0;
	m_nSlot = 0;
	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float fx = fWidth * 0.5f, fy = fHeight * 0.5f, fz = fDepth * 0.5f;

	CDiffusedVertex pVertices[8];

	pVertices[0] = CDiffusedVertex(XMFLOAT3(-fx, +fy, -fz), RANDOM_COLOR);
	pVertices[1] = CDiffusedVertex(XMFLOAT3(+fx, +fy, -fz), RANDOM_COLOR);
	pVertices[2] = CDiffusedVertex(XMFLOAT3(+fx, +fy, +fz), RANDOM_COLOR);
	pVertices[3] = CDiffusedVertex(XMFLOAT3(-fx, +fy, +fz), RANDOM_COLOR);
	pVertices[4] = CDiffusedVertex(XMFLOAT3(-fx, -fy, -fz), RANDOM_COLOR);
	pVertices[5] = CDiffusedVertex(XMFLOAT3(+fx, -fy, -fz), RANDOM_COLOR);
	pVertices[6] = CDiffusedVertex(XMFLOAT3(+fx, -fy, +fz), RANDOM_COLOR);
	pVertices[7] = CDiffusedVertex(XMFLOAT3(-fx, -fy, +fz), RANDOM_COLOR);

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	m_nIndices = 36;
	UINT pnIndices[36];

	pnIndices[0] = 3; pnIndices[1] = 1; pnIndices[2] = 0;
	pnIndices[3] = 2; pnIndices[4] = 1; pnIndices[5] = 3;
	pnIndices[6] = 0; pnIndices[7] = 5; pnIndices[8] = 4;
	pnIndices[9] = 1; pnIndices[10] = 5; pnIndices[11] = 0;
	pnIndices[12] = 3; pnIndices[13] = 4; pnIndices[14] = 7;
	pnIndices[15] = 0; pnIndices[16] = 4; pnIndices[17] = 3;
	pnIndices[18] = 1; pnIndices[19] = 6; pnIndices[20] = 5;
	pnIndices[21] = 2; pnIndices[22] = 6; pnIndices[23] = 1;
	pnIndices[24] = 2; pnIndices[25] = 7; pnIndices[26] = 6;
	pnIndices[27] = 3; pnIndices[28] = 7; pnIndices[29] = 2;
	pnIndices[30] = 6; pnIndices[31] = 4; pnIndices[32] = 5;
	pnIndices[33] = 7; pnIndices[34] = 4; pnIndices[35] = 6;

	m_pd3dIndexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pnIndices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

}