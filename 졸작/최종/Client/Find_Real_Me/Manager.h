#pragma once
#include "stdafx.h"
#include "Network.h"


class CManager {
public:
	static void SendEvent(unsigned short usID, void* pPACKET);
	static void SendUpKeyPacket();
	static void SendLeftKeyPacket();
	static void SendDownKeyPacket();
	static void SendRightKeyPacket();
	static void SendDirPacket();
};