#pragma once
#include "Shader.h"
#include "ModelObject.h"

class CFBXModelShader : public CShader
{
public:
	CFBXModelShader();
	virtual ~CFBXModelShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob** ppd3dShaderBlob);
	virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob** ppd3dShaderBlob);
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader(ID3DBlob** ppd3dShaderBlob);

	virtual void CreateShader(ID3D12Device* pd3dDevice, ID3D12RootSignature* pd3dGraphicsRootSignature, bool is_Shadow = false);
};
class CFBXVillageShader : public CFBXModelShader
{
public:
	CFBXVillageShader();
	virtual ~CFBXVillageShader();

	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);
	virtual void ReleaseObjects();

	virtual void AnimateObjects(float fTimeElapsed);

	virtual void UpdateObjectsCB(FrameResource* pFrameResource, float fTime);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);

	virtual bool CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox) { return false; }
	virtual const aiScene* ReadModelFile(const string& filename);

protected:
	CGameObject** m_ppObjects = nullptr;
	int m_nObjects = 0;
};

class CFBXTopInsideShader : public CFBXVillageShader
{
public:
	CFBXTopInsideShader();
	virtual ~CFBXTopInsideShader();
	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);

	virtual bool CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox);
	virtual void AnimateObjects(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
};

class CFBXDungeon : public CFBXVillageShader
{
public:
	CFBXDungeon();
	virtual ~CFBXDungeon();
	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);

	virtual bool CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox);
	virtual void AnimateObjects(float fTimeElapsed);
};

class CFBXBossDungeon : public CFBXVillageShader
{
public:
	CFBXBossDungeon();
	virtual ~CFBXBossDungeon();
	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);

	virtual bool CollisionCheckFromStaticObject(float fTimeElapsed, DirectX::BoundingOrientedBox& oobbAfterBox);
	virtual void AnimateObjects(float fTimeElapsed);
};

class CSkinnedObjectShader : public CShader
{
public:
	CSkinnedObjectShader();
	virtual ~CSkinnedObjectShader();
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob** ppd3dShaderBlob);
	virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob** ppd3dShaderBlob);
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader(ID3DBlob** ppd3dShaderBlob);
};

//class CNormalDungeonMonsterShader : public CFBXVillageShader
//{
//public:
//	CNormalDungeonMonsterShader();
//	virtual ~CNormalDungeonMonsterShader();
//
//	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
//	virtual D3D12_SHADER_BYTECODE CreateVertexShader(ID3DBlob** ppd3dShaderBlob);
//	virtual D3D12_SHADER_BYTECODE CreatePixelShader(ID3DBlob** ppd3dShaderBlob);
//	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader(ID3DBlob** ppd3dShaderBlob);
//
//	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);
//	virtual void UpdateObjectsCB(FrameResource* pFrameResource, float fTime);
//	virtual void AnimateObjects(float fTimeElapsed);
//	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
//	virtual void ReleaseObjects();
//
//protected:
//	unordered_map<string, ModelData*> m_mModelData;
//};
//
//class CBossDungeonMonsterShader : public CNormalDungeonMonsterShader
//{
//public:
//
//	CBossDungeonMonsterShader();
//	virtual ~CBossDungeonMonsterShader();
//
//	virtual void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pContext = nullptr);
//	virtual void AnimateObjects(float fTimeElapsed);
//	virtual void UpdateObjectsCB(FrameResource* pFrameResource, float fTime);
//	virtual void ReleaseObjects();
//
//	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
//
//private:
//	CBossDragonObject* m_pBossDragon = nullptr;
//};

