#pragma once

#define DIR_FORWARD				0x01
#define DIR_BACKWARD			0x02
#define DIR_LEFT				0x04
#define DIR_RIGHT				0x08
#define DIR_UP					0x10
#define DIR_DOWN				0x20

#include "Object.h"
#include "ModelObject.h"
#include "Camera.h"
#include "SkillEffectObject.h"
#include "CSound.h"
constexpr char g_cObjectLockNum = 13;

class CPlayer : public CSkinAndDivisionObject
{
public:
	static class CSkinnedObjectShader* m_pSkinnedObjectShader;

private:
	concurrency::concurrent_unordered_set<unsigned short>m_ccusViewList;
	std::shared_mutex m_rmPlayerStatusMutex[g_cObjectLockNum];
	std::recursive_mutex m_rmPlayerListMutex;
	const enum e_LOCK_TYPE {
		HP_LOCK, MP_LOCK, POS_LOCK, ATK_LOCK, AMR_LOCK, DIR_LOCK, STATE_LOCK, LOOKVEC_LOCK, ANITIME_LOCK, RIGHTVEC_LOCK, UPVEC_LOCK, POSXZ_LOCK, POSY_LOCK
	};
protected:
	ID3D12Device* m_pd3dDevice = nullptr;
	ID3D12GraphicsCommandList* m_pd3dCommandList = nullptr;
	ID3D12RootSignature* m_pd3dGraphicsRootSignature = nullptr;

	XMFLOAT3 m_xmf3Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 m_xmf3Right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	XMFLOAT3 m_xmf3Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	XMFLOAT3 m_xmf3Look = XMFLOAT3(0.0f, 0.0f, 1.0f);

	XMFLOAT3 m_xmf3Velocity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 m_xmf3Gravity = XMFLOAT3(0.0f, 0.0f, 0.0f);

	float m_fPitch = 0.0f;
	float m_fYaw = 0.0f;
	float m_fRoll = 0.0f;
		  
	float m_fMaxVelocityXZ = 0.0f;
	float m_fMaxVelocityY = 0.0f;
	float m_fFriction = 0.0f;

	DWORD m_dwDirection;

	LPVOID m_pPlayerUpdatedContext = nullptr;
	LPVOID m_pCameraUpdatedContext = nullptr;
	CCamera* m_pCamera = nullptr;

	s_POSSESSION_ITEM m_sPossessionItem;
	wchar_t m_wcID[MAX_STR_LEN];
	bool m_isCrackEffect = false;
	vector<CGameObject*> m_vCrackEffectObject;
	vector<CGameObject*> m_vThunderEffectObject;
	vector<CGameObject*> m_vShieldEffectObject;
	vector<CGameObject*> m_vSword;
	float m_fSwordFrameTime = 0.f;
	bool m_isSwordEffect = false;

	int m_ManSwordSkillObjectCBIndex[60] = { 0 };
	int m_ManSwordSkillObjectChildObjectCBIndex[60][2] = { 0 };
	int m_ManEffectSkillObjectCBIndex[2] = { 0 };

	int m_GirlThunderSkillObjectCBIndex[5] = { 0 };
	int m_GirlShieldSkillObjectCBIndex[16] = { 0 };

	bool m_bCoolTime = false;
	CSound* m_pSound = nullptr;
	bool m_IsAttack = false;
	bool m_IsDamage = false;
public:
	CPlayer(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, void *pContext=nullptr);
	virtual ~CPlayer();

	XMFLOAT3& GetPosition() { std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[POS_LOCK]); return(m_xmf3Position); }
	XMFLOAT3& GetLookVector() { std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[LOOKVEC_LOCK]); return(m_xmf3Look); }
	XMFLOAT3& GetUpVector() { std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[UPVEC_LOCK]); return(m_xmf3Up); }
	XMFLOAT3& GetRightVector() { std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[RIGHTVEC_LOCK]); return(m_xmf3Right); }
	void SetHPPortion(int cnt)
	{
		m_sPossessionItem.m_cHpPortion = cnt;
	}
	void SetMPPortion(int cnt)
	{
		m_sPossessionItem.m_cMpPortion = cnt;
	}
	void SetRepatriation(int cnt)
	{
		m_sPossessionItem.m_cRepatriation = cnt;
	}
	const char& GetHPPortion()
	{
		return m_sPossessionItem.m_cHpPortion;
	}
	const char& GetMPPortion()
	{
		return m_sPossessionItem.m_cMpPortion;
	}
	const char& GetRepatriation()
	{
		return m_sPossessionItem.m_cRepatriation;
	}
	void SetPossessionItem(s_POSSESSION_ITEM pItem)
	{
		m_sPossessionItem = pItem;
	}
	s_POSSESSION_ITEM& GetPossessionItem()
	{
		return m_sPossessionItem;
	}
	void GetPlayerID(wchar_t* ID)
	{ 
		wcsncpy_s(ID, MAX_STR_LEN, m_wcID, (UINT32)wcslen(m_wcID));
	}
	void SetPlayerID(wchar_t* ID)
	{
		wcsncpy_s(m_wcID, MAX_STR_LEN, ID, (UINT32)wcslen(ID));
	}
	void SetPlayerLookVec(const DirectX::XMFLOAT3& xmf3Look) {
		std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[LOOKVEC_LOCK]);
		m_xmf3Look = xmf3Look;
		m_xmf3Look = Vector3::Normalize(m_xmf3Look);
		m_xmf3Right = Vector3::CrossProduct(m_xmf3Up, m_xmf3Look, true);
	}
	void SetPlayeRightVec(const DirectX::XMFLOAT3& xmf3Right) { std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[RIGHTVEC_LOCK]); m_xmf3Right = xmf3Right; }
	void SetPlayeUpVec(const DirectX::XMFLOAT3& xmf3Up) { std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[UPVEC_LOCK]); m_xmf3Up = xmf3Up; }
	void SetPosition(const XMFLOAT3& xmf3Position) { std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[LOOKVEC_LOCK]); Move(XMFLOAT3(xmf3Position.x - m_xmf3Position.x, xmf3Position.y - m_xmf3Position.y, xmf3Position.z - m_xmf3Position.z), false); }
	void SetDirection(DWORD dwDirection) { std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[DIR_LOCK]); m_dwDirection = dwDirection; }
	void SetPosXZ(const float fX, const float fZ) { std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[POS_LOCK]); m_xmf3Position.x = fX; m_xmf3Position.z = fZ;}
	void SetPosY(const float fY) { std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[POS_LOCK]); m_xmf3Position.y = fY; }


	void SetFriction(float fFriction) { m_fFriction = fFriction;  }
	void SetGravity(const XMFLOAT3& xmf3Gravity) { m_xmf3Gravity = xmf3Gravity; }
	void SetMaxVelocityXZ(float fMaxVelocity) { m_fMaxVelocityXZ = fMaxVelocity; }
	void SetMaxVelocityY(float fMaxVelocity) { m_fMaxVelocityY = fMaxVelocity; }
	void SetVelocity(const XMFLOAT3& xmf3Velocity) { m_xmf3Velocity = xmf3Velocity; }


	void SetSword(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size);
	void SetIsSwordEffect(bool flag) { m_isSwordEffect = flag; m_fSwordFrameTime = 0.f; }
	bool GetIsSwordEffect() const { return m_isSwordEffect; }
	const XMFLOAT3& GetVelocity() const { return(m_xmf3Velocity); }
	float GetYaw() const { return(m_fYaw); }
	float GetPitch() const { return(m_fPitch); }
	float GetRoll() const { return(m_fRoll); }
	CCamera* GetCamera() const { return(m_pCamera); }
	DWORD GetDirection() const { return m_dwDirection; }

	void SetCamera(CCamera* pCamera) { m_pCamera = pCamera; }

	void Move(DWORD nDirection, float fDistance, bool bVelocity = false);
	void Move(const XMFLOAT3& xmf3Shift, bool bVelocity = false);
	void Move(float fxOffset = 0.0f, float fyOffset = 0.0f, float fzOffset = 0.0f);
	void Rotate(float x, float y, float z);

	virtual void Animate(float fTimeElapsed);
	virtual void UpdateCB(FrameResource* pFrameResource);
	void Update(float fTimeElapsed);

	virtual void OnPlayerUpdateCallback(float fTimeElapsed) { }
	void SetPlayerUpdatedContext(LPVOID pContext) { m_pPlayerUpdatedContext = pContext; }

	virtual void OnCameraUpdateCallback(float fTimeElapsed) { }
	void SetCameraUpdatedContext(LPVOID pContext) { m_pCameraUpdatedContext = pContext; }


	CCamera* OnChangeCamera(DWORD nNewCameraMode, DWORD nCurrentCameraMode);

	virtual CCamera* ChangeCamera(DWORD nNewCameraMode, float fTimeElapsed) { return nullptr; }
	virtual void OnPrepareRender();
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);

	// test이긴한데 그냥 쓸거같기도..귀찮
	void SetSwordGirlChildsTexIndex() {
		XMUINT2 index = m_xmi2TexIndex;
		for (int i = 0; i < m_vChilds.size(); ++i) {
			index.x += 2; index.y += 2;
			m_vChilds[i]->SetTextureIndex(index);
		}
	}

	void SetSound(CSound* sound) { m_pSound = sound; }

	bool GetIsCool() { return m_bCoolTime; }

	void SetCharacterInfos(CMesh* pParentMesh, vector<CSkinnedModelMesh*> vChildsMeshes, vector<string>& vMeshNames, unordered_map<string, DivisionAnimationClip>& clip, XMFLOAT3 xmf3LocalScale = { 1,1,1 }, bool is_Man = true);
public:
	concurrency::concurrent_unordered_set<unsigned short>& GetPlayerList()
	{
		concurrency::concurrent_unordered_set<unsigned short> list;
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		list = m_ccusViewList;
		return list;
	}
	void CopyBefore(concurrency::concurrent_unordered_set<unsigned short>& usCopyList)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		usCopyList = m_ccusViewList;
	}
	void DeleteListElement(unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		m_ccusViewList.unsafe_erase(bId);
	}
	void InsertListElement(unsigned short bId)
	{
		m_ccusViewList.insert(bId);
	}
	BOOL ExistListElement(unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		if (m_ccusViewList.count(bId) != 0) {
			return true;
		}
		else {
			return false;
		}
	}
};

class CTerrainPlayer : public CPlayer
{
public:
	CTerrainPlayer(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, void* pContext=nullptr);
	virtual ~CTerrainPlayer();

	virtual CCamera* ChangeCamera(DWORD nNewCameraMode, float fTimeElapsed);

	virtual void OnPlayerUpdateCallback(float fTimeElapsed);
	virtual void OnCameraUpdateCallback(float fTimeElapsed);
};


