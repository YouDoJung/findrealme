#pragma once
#include "stdafx.h"


template<class T>
class CTemplateSingleton {
private:
	static T* m_pInstance;

protected:
	CTemplateSingleton() {}
	virtual ~CTemplateSingleton() {}

public:
	CTemplateSingleton(const CTemplateSingleton&) {}
	CTemplateSingleton& operator = (const CTemplateSingleton&) {}

	static T* GetInstance()
	{
		if (!m_pInstance)
			m_pInstance = new T;
		return m_pInstance;
	}

	static T* GetInstance(int iNum)
	{
		if (!m_pInstance)
			m_pInstance = new T(iNum);
		return m_pInstance;
	}

	static void DestoryInstance()
	{
		if (m_pInstance) {
			SAFE_DELETE(m_pInstance);
		}
	}

};

template <class T> T* CTemplateSingleton<T>::m_pInstance = 0;