#pragma once

///////////////////////////////////////
//FOR NETWORK							   
///////////////////////////////////////
constexpr size_t MAX_PACKET_SIZE = 255;
constexpr int SERVER_PORT = 9000;
constexpr unsigned short MAX_PLAYER = 100;
constexpr int NOMALDUNGEON_MONSTER_NUM = 244;
constexpr int BOSSMAP_MONSTER_NUM = 49;
constexpr int BOSS_ID = 48;
constexpr int MAX_MONSTER = NOMALDUNGEON_MONSTER_NUM + BOSSMAP_MONSTER_NUM;
constexpr int MAX_ITEM_NUM = 3; //아이템 종류 수
constexpr size_t BUFSIZE = 12000;
//#define SERVERIP  "127.0.0.1"
constexpr size_t MAX_STR_LEN = 56;

extern char SERVERIP[20];

///////////////////////////////////////
//FOR OVERLAPPED EVENT
///////////////////////////////////////
constexpr char RECV_EVENT = 1;
constexpr char SEND_EVENT = 2;
constexpr char UPDATE_EVENT = 3;
constexpr char PLAYER_UPDATE = 4;
constexpr char DATABASE_EVENT = 5;
constexpr char MONSTER_BY_PLAYER_EVENT = 6;
constexpr char PLAYER_BY_MONSTER_EVENT = 7;


///////////////////////////////////////
//FOR PROTOCOL						   
///////////////////////////////////////
constexpr char CS_LOOK = 0;
constexpr char CS_POS = 1;
constexpr char CS_ATTACK = 2;
constexpr char CS_ATTACK2 = 3;
constexpr char CS_SKILL1 = 4;
constexpr char CS_SKILL2 = 5;
constexpr char CS_SKILL3 = 6;
constexpr char CS_SKILL4 = 7;
constexpr char CS_SKILL5 = 8;
constexpr char CS_IDLE = 9;
constexpr char CS_MAP_SWAP_TO_VILLAGE = 10;
constexpr char CS_MAP_SWAP_TO_LOBY = 11;
constexpr char CS_MAP_SWAP_TO_DUNGEON = 12;
constexpr char CS_MAP_SWAP_TO_BOSS = 13;
constexpr char CS_LOGIN = 14;
constexpr char CS_DEV_MAP_SWAP = 15;
constexpr char CS_DEV_CHARACTER = 16;
constexpr char CS_HP_PORTION_GET = 17;
constexpr char CS_MP_PORTION_GET = 18;
constexpr char CS_RETURN_GET = 19;
constexpr char CS_HP_PORTION_USE = 20;
constexpr char CS_MP_PORTION_USE = 21;
constexpr char CS_RETURN_USE = 22;


constexpr char SC_POS = 1;
constexpr char SC_ATTACK = 2;
constexpr char SC_DIR = 3;
constexpr char SC_CONNECT = 4;
constexpr char SC_DISCONNECT = 5;
constexpr char SC_FIRST_DATA = 6;
constexpr char SC_IDLE = 7;
constexpr char SC_LOGIN = 8;
constexpr char SC_MAP_SWAP = 9;
constexpr char SC_ATTACK2 = 10;
constexpr char SC_SKILL1 = 11;
constexpr char SC_SKILL2 = 12;
constexpr char SC_SKILL3 = 13;
constexpr char SC_SKILL4 = 14;
constexpr char SC_SKILL5 = 15;

constexpr char SC_NPC_LOG_IN = 16;
constexpr char SC_NPC_STATUS = 17;
constexpr char SC_NPC_REMOVE = 18;
constexpr char SC_NPC_POS = 19;
constexpr char SC_NPC_ATTACK = 20;
constexpr char SC_NPC_FOLLOW = 21;
constexpr char SC_NPC_IDLE = 22;
constexpr char SC_NPC_DOWN = 23;
constexpr char SC_NPC_DAMAGED = 24;


constexpr char SC_PLAYER_DAMAGED = 25;
constexpr char SC_PLAYER_DOWN = 26;
constexpr char SC_PLAYER_RESPAWN = 27;


constexpr char SC_LOGIN_SUCCESS = 28;	//아이디 로그인 의미
constexpr char SC_LOGIN_FAIL = 29;

constexpr char SC_CHANGE_STATUS_PLAYER_HP = 30;
constexpr char SC_CHANGE_STATUS_PLAYER_MP = 31;
constexpr char SC_CHANGE_STATUS_PLAYER_EXP = 32;
constexpr char SC_CHANGE_STATUS_PLAYER_LEVEL = 33;
constexpr char SC_CHANGE_STATUS_PLAYER_MONEY = 34;
constexpr char SC_CHANGE_LEVEL_UP = 35;

constexpr char SC_CHANGE_STATUS_NPC_HP = 36;

constexpr char SC_BOSS_LEG_ATTACK = 37;
constexpr char SC_BOSS_TAIL_ATTACK = 38;
constexpr char SC_BOSS_FAST_FLY = 39;
constexpr char SC_BOSS_FLY = 40;
constexpr char SC_BOSS_FLY_DOWN = 41;
constexpr char SC_BOSS_TURN = 42;
constexpr char SC_BOSS_RUN = 43;
constexpr char SC_BOSS_FLY_ATTACK = 44;
constexpr char SC_BOSS_FIRE_BREATH = 45;
constexpr char SC_BOSS_POS = 46;
constexpr char SC_BOSS_STATUS = 47;
constexpr char SC_BOSS_FLY_DEAD = 48;
constexpr char SC_BOSS_RAID = 49;
constexpr char SC_BOSS_LEFT_HIT = 50;
constexpr char SC_BOSS_RIGHT_HIT = 51;
constexpr char SC_BOSS_DOWN_HIT = 52;


constexpr char SC_HP_PORTION_CHANGE = 53;
constexpr char SC_MP_PORTION_CHANGE = 54;
constexpr char SC_REPATRIATION_CHANGE = 55;

///////////////////////////////////////
//FOR MAP									
///////////////////////////////////////
constexpr char MAP_VILLAGE = 0;
constexpr char MAP_LOBY = 1;
constexpr char MAP_DUNGEON = 2;
constexpr char MAP_BOSS = 3;

///////////////////////////////////////
//FOR MAP	IN MONSTER								
///////////////////////////////////////
constexpr char DUNGEON_MONSTER = 0;
constexpr char BOSSMAP_MONSTER = 1;


///////////////////////////////////////
//FOR KEY DIR							   
///////////////////////////////////////
#define DIR_FORWARD				0x01
#define DIR_BACKWARD			0x02
#define DIR_LEFT				0x04
#define DIR_RIGHT				0x08
#define DIR_UP					0x10
#define DIR_DOWN				0x20


/*
For Player
*/
constexpr float PLAYER_MOVE_SPEED = 800.f;
constexpr float PLAYER_DISTANCE = 1000.f;
constexpr float PLAYER_AND_PLAYER_RANGE = 2500.f;
constexpr float PLAYER_ATTACK_RANGE = 200.0f;
constexpr float PLAYER_ATTACK_MINIMUM_CHECK_RANGE = 700.0f;
constexpr float PLAYER_SKILL_RANGE_1 = 100.0f;
constexpr float PLAYER_SKILL_RANGE_2 = 1000.0f;

constexpr int BASIC_PLAYER_ATK = 40;
constexpr int BASIC_PLAYER_AMR = 5;

constexpr int BASIC_PEON_LEVEL = 1;
constexpr int BASIC_LORD_LEVEL = 5;
constexpr int BASIC_GRUNT_LEVEL = 10;
constexpr int BASIC_BOSS_LEVEL = 15;
constexpr int BASIC_MONSTER_ATK = 20;
constexpr int BASIC_MONSTER_AMR = 5;
constexpr int BASIC_MONSTER_HP = 100;
constexpr int BASIC_MONSTER_EXP = 50;

constexpr int HP_PORTION_PRICE = 100;
constexpr int MP_PORTION_PRICE = 100;
constexpr int REPATRIATION_PRICE = 100;



//#define PLAYER_ATTACK_EVENT(type)	

constexpr std::chrono::milliseconds PLAYER_ATTACK1_TIME_LIMIT = std::chrono::milliseconds(1000);
constexpr std::chrono::milliseconds PLAYER_ATTACK2_TIME_LIMIT = std::chrono::milliseconds(1000);
constexpr std::chrono::milliseconds PLAYER_SKILL1_TIME_LIMIT = std::chrono::milliseconds(1000);
constexpr std::chrono::milliseconds PLAYER_SKILL2_TIME_LIMIT = std::chrono::milliseconds(1000);
constexpr std::chrono::milliseconds PLAYER_SKILL3_TIME_LIMIT = std::chrono::milliseconds(1000);
constexpr std::chrono::milliseconds PLAYER_SKILL4_TIME_LIMIT = std::chrono::milliseconds(1000);
constexpr std::chrono::milliseconds PLAYER_SKILL5_TIME_LIMIT = std::chrono::milliseconds(1000);

constexpr std::chrono::microseconds BOSS_LEG_ATTACK_TIME_LIMIT = std::chrono::microseconds(7385);
constexpr std::chrono::microseconds BOSS_TAIL_ATTACK_TIME_LIMIT = std::chrono::microseconds(7738);
constexpr std::chrono::microseconds BOSS_FIRE_BREATH_TIME_LIMIT = std::chrono::microseconds(73);
constexpr std::chrono::microseconds BOSS_FLY_ATTACK_TIME_LIMIT = std::chrono::microseconds(73);

constexpr float CAN_DAMAGED_DURING_BOSS_ATTACK = 1500.0f;

constexpr float BOSS_LEG_ATTACK_TIME = 44.0f;
constexpr float BOSS_TAIL_ATTACK_TIME = 40.0f;
constexpr float BOSS_FIRE_BREATH_TIME = 64.0f;
constexpr float BOSS_FLY_ATTACK_TIME = 60.0f;

constexpr float BOSS_LEG_ATTACK_START_RANGE = 50.0f;
constexpr float BOSS_TAIL_ATTACK_START_RANGE = 800.0f;
constexpr float BOSS_FIRE_BREATH_START_RANGE = 800.0f;
constexpr float BOSS_FLY_ATTACK_START_RANGE = 50.0f;

constexpr float BOSS_LEG_ATTACK_EXTENTS = BOSS_LEG_ATTACK_START_RANGE * 2;
constexpr float BOSS_TAIL_ATTACK_EXTENTS = 800.0f;	//765.0f
constexpr float BOSS_FIRE_BREATH_EXTENTS = 300.0f;
constexpr float BOSS_FLY_ATTACK_EXTENTS = BOSS_FLY_ATTACK_START_RANGE * 2;


/*
For Monster
*/
constexpr float MONSTER_IDLE_MOVE_SPEED = 1.0f;
constexpr float MONSTER_TARGET_MOVE_SPEED = 1.5f;
constexpr float MONSTER_TURN_SPEED = 3.0f;

constexpr float MONSTER_RANGE = 2000.0f;	//View List Check
constexpr float MONSTER_DETECT_PLAYER = 800.0f;	//Monster Detect Player Range -> FOLLOW
constexpr float MONSTER_DETECT_PLAYER_AND_ATTACK = 200.0f;
constexpr float MONSTER_LIMIT_MOVE_RANGE = 1000.0f;	//Monster Detect Player Range -> FOLLOW
constexpr float MONSTER_AND_TARGET_DISTANCE = 800.f;

constexpr float BOSS_TURN_SPEED = 3.0f;
constexpr float BOSS_TARGET_MOVE_SPEED = 5.0f;
constexpr float BOSS_DETECT_PLAYER = 800.0f;
constexpr float BOSS_DETECT_PLAYER_AND_GROUND_ATTACK = 200.0f;
constexpr float BOSS_DETECT_PLAYER_AND_AIR_ATTACK = 200.0f;


constexpr short NO_TARGET_PLAYER = MAX_PLAYER;



#pragma pack(push, 1)

typedef struct _SC_Login_State_Packet		//아이디 비번 입력후 결과 패킷
{
	char m_bSize;
	char m_bType;
	char m_cTempt[6];
}s_SC_LOGIN_STATE_PACKET, *s_pSC_LOGIN_STATE_PACKET;

typedef struct _SC_First_Status_Packet
{
	char m_bSize;
	char m_bType;
	char m_cMap;
	char m_cObjectType;
	char m_cHpPortion;
	char m_cMpPortion;
	char m_cRepatriation;
	char m_cForMax1;

	char m_cForMax2[2];
	unsigned short m_usID;
	float m_fX;

	float m_fY;
	float m_fZ;

	int m_iHP;
	int m_iMP;

	int m_iLevel;
	int m_iExp;

	int m_iMoney;
	int m_iMaxHP;

	int m_iMaxMP;
	int m_iMaxEXP;
}s_SC_FIRST_STATUS_PACKET, *s_pSC_FIRST_STATUS_PACKET;

typedef struct _SC_Login_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	char m_cTempt[4];
}s_SC_LOGIN_PACKET, *s_pSC_LOGIN_PACKET;

typedef struct _SC_Npc_Login_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	char m_cTempt[4];
}s_SC_NPC_LOGIN_PACKET, *s_pSC_NPC_LOGIN_PACKET;

typedef struct _SC_Npc_Status_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	char m_cTempt[4];

	float m_fX;
	float m_fZ;

	int m_iHP;
	int m_iLevel;

	int m_iMaxHP;
	float m_fDirX;

	float m_fDirY;
	float m_fDirZ;
}s_SC_NPC_STATUS_PACKET, *s_pSC_NPC_STATUS_PACKET;

typedef struct _SC_Boss_Status_Packet
{
	char m_bSize;
	char m_bType;
	char m_cMax1;
	char m_cMax2;
	int m_iHP;

	int m_iLevel;
	int m_iMaxHP;

	float m_fPosX;
	float m_fPosY;
	
	float m_fPosZ;
	float m_fDirX;

	float m_fDirY;
	float m_fDirZ;

}s_SC_BOSS_STATUS_PACKET, *s_pSC_BOSS_STATUS_PACKET;

typedef struct _SC_Npc_Pos_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	float m_fX;

	float m_fMax;
	float m_fZ;

	float m_fDirX;
	float m_fDirY;

	float m_fDirZ;
	float m_fTempt;
}s_SC_NPC_POS_PACKET, *s_pSC_NPC_POS_PACKET;

typedef struct _SC_Boss_Fly_Packet
{
	char m_bSize;
	char m_bType;
	char m_cTempt[2];
	float m_fY;
}s_SC_BOSS_FLY_PACKET, *s_pSC_BOSS_FLY_PACKET;

typedef struct _SC_Boss_Pos_Packet
{
	char m_bSize;
	char m_bType;
	char m_cTempt[2];
	float m_fPosX;

	float m_fPosY;
	float m_fPosZ;

	float m_fDirX;
	float m_fDirY;

	float m_fDirZ;
	float m_fTempt;
}s_SC_BOSS_POS_PACKET, *s_pSC_BOSS_POS_PACKET;

typedef struct _SC_Accept_Packet
{
	char m_bSize;
	char m_bType;
	char m_cObjectType;
	char m_cTempt2[3];
	unsigned short m_usID;

	float m_fX;
	float m_fY;
	
	float m_fZ;
	int m_iHP;

	int m_iLevel;
	int m_iMaxHP;

	float m_fDirX;
	float m_fDirY;

	float m_fDirZ;
	float m_fTempt;

	wchar_t m_wcID[MAX_STR_LEN];
}s_SC_ACCEPT_PACKET, *s_pSC_ACCEPT_PACKET;

typedef struct _SC_Dir_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	float m_fDirX;

	float m_fDirY;
	float m_fDirZ;
}s_SC_DIR_PACKET, *s_pSC_DIR_PACKET;

typedef struct _SC_Pos_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	float m_fX;
	
	float m_fY;
	float m_fZ;

	float m_fDirX;
	float m_fDirY;

	float m_fDirZ;
	float m_fTempt;
}s_SC_POS_PACKET, *s_pSC_POS_PACKET;

typedef struct _SC_Disconnect_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	char m_cTempt[4];
}s_SC_DISCONNECT_PACKET, *s_pSC_DISCONNECT_PACKET;

typedef struct _SC_Animation_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	char m_cTempt[4];
}s_SC_ANIMATION_PACKET, *s_pSC_ANIMATION_PACKET;

typedef struct _SC_Player_Respawn_Packet
{
	char m_bSize;
	char m_bType;
	char m_cMax1;
	char m_cMax2;
	int m_iHP;

	int m_iMP;
	int m_iExp;
}s_SC_PLAYER_RESPAWN_PACKET, *s_pSC_PLAYER_RESPAWN_PACKET;

typedef struct _SC_Player_Map_Swap_Packet
{
	char m_bSize;
	char m_bType;
	char m_bMapType;
	char m_cTempt[5];
}s_SC_PLAYER_MAP_SWAP_PACKET, *s_pSC_PLAYER_MAP_SWAP_PACKET;

typedef struct _SC_Change_Status_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	int m_iAny;
}s_SC_CHANGE_STATUS_PACKET, *s_pSC_CHANGE_STATUS_PACKET;

typedef struct _SC_Change_Item_Packet
{
	char m_bSize;
	char m_bType;
	char m_cAny;
	char m_cTempt;
	int m_iTempt;
}s_SC_CHANGE_ITEM_PACKET, *s_pSC_CHANGE_ITEM_PACKET;

typedef struct _SC_Level_Up_Packet
{
	char m_bSize;
	char m_bType;
	unsigned short m_usID;
	int m_iHP;

	int m_iMP, m_iLevel;
	
	int m_iExp;
	int m_iMaxHP;
	
	int m_iMaxMP, m_iMaxEXP;
}s_SC_LEVEL_UP_PACKET, *s_pSC_LEVEL_UP_PACKET;

/*//////////////////////////////////////////////////
클라이언트에서 서버로.
*///////////////////////////////////////////////////
typedef struct _CS_Login_Packet
{
	char m_bSize;
	char m_bType;
	char m_cMax1;
	char m_cMax2;

	wchar_t m_wcID[MAX_STR_LEN];

	wchar_t m_wcPASS[MAX_STR_LEN];
}s_CS_LOGIN_PACKET, *s_pCS_LOGIN_PACKET;

typedef struct _CS_Pos_Packet
{
	char m_bSize;
	char m_bType;
	char m_cMax1;
	char m_cMax2;

	float m_fX, m_fY, m_fZ;

	DirectX::XMFLOAT3 m_Dir;
}s_CS_POS_PACKET, *s_pCS_POS_PACKET;

typedef struct _CS_Player_Dir_Packet
{
	char m_bSize;
	char m_bType;
	char m_cMax1;
	char m_cMax2;

	DirectX::XMFLOAT3 m_Dir;
}s_CS_PLAYER_DIR_PACKET, *s_pCS_PLAYER_DIR_PACKET;

typedef struct _CS_Player_Animation_Packet
{
	char m_bSize;
	char m_bType;
	char m_cTempt[2];
}s_CS_PLAYER_ANIMATION_PACKET, *s_pCS_PLAYER_ANIMATION_PACKET;

typedef struct _CS_Player_Map_Swap_Packet
{
	char m_bSize;
	char m_bType;
	char m_cTempt[2];
}s_CS_PLAYER_MAP_SWAP_PACKET, *s_pCS_PLAYER_MAP_SWAP_PACKET;

typedef struct _CS_Player_Item_Packet
{
	char m_bSize;
	char m_bType;
	char m_cTempt[2];
}s_CS_PLAYER_ITEM_PACKET_PACKET, *s_pCS_PLAYER_ITEM_PACKET_PACKET;
#pragma pack(pop)