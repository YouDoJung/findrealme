#pragma once

typedef enum _Object_State_Type
{
	e_Object_Attack,
	e_Object_Down,
	e_Object_Follow,
	e_Object_Idle,	//기본 상태
	e_Object_MoveToFirstPos,
	e_Object_MoveToNoTarget,
	e_Object_Respawn,
	e_Object_Heal,
	e_Object_Attack2,
	e_Object_Skill1,
	e_Object_Skill2,
	e_Object_Skill3,
	e_Object_Skill4,
	e_Object_Skill5
}e_ObjectState;

typedef enum _Monster_ByPlayer_Event_Type
{
	e_By_Man_Attack1,
	e_By_Man_Attack2,
	e_By_Man_Skill1,
	e_By_Man_Skill2,
	e_By_Man_Skill3,
	e_By_Man_Skill4,
	e_By_Man_Skill5,
	e_By_Woman_Attack1,
	e_By_Woman_Attack2,
	e_By_WoMan_Skill1,
	e_By_WoMan_Skill2,
	e_By_WoMan_Skill3,
	e_By_WoMan_Skill4,
	e_By_WoMan_Skill5
}e_MonsterByPlayerEventType;

typedef enum _Boss_State_Type
{
	e_Boss_Leg_Attack = 8,
	e_Boss_Tail_Attack,
	e_Boss_Fly_Attack,
	e_Boss_FireBreath,
	e_Boss_Down,
	e_Boss_Follow,
	e_Boss_Idle,	//기본 상태
	e_Boss_MoveToFirstPos,
	e_Boss_Respawn,
	e_Boss_Fly,
	e_Boss_Fly_Follow,
	e_Boss_Fly_Down,
	e_Boss_Fly_Dead,
	e_Boss_Raid
}e_BossState;

typedef enum _Boss_Attack_Type
{
	e_Leg_Attack,
	e_Tail_Attack,
	e_Fly_Attack,
	e_Fire_Breath
}e_BossAttackType;

typedef enum _Object_Type
{
	e_Player_Man,
	e_Player_Woman,
	e_Monster_Grunt,
	e_Monster_Lord,
	e_Monster_Peon,
	e_Monster_Boss,
	e_NPC_0
}e_ObjectType;

typedef enum e_Player_Attack_Skill
{
	e_Attack_1 = 0,
	e_Attack_2,
	e_Skill_1,
	e_Skill_2,
	e_Skill_3,
	e_Skill_4,
	e_Skill_5
}e_PlayerAttackSkillType;


typedef enum e_Data_Base_State {
	e_Login,
	e_LogOut,
	e_ItemGet,
	e_ItemGive,
	e_ChangeState
}e_DBState;

typedef enum e_Data_Base_Save_State {
	e_LoginFail,
	e_LoginSuccess,
	e_LogOutFail,
	e_LogOutSuccess,
	e_ItemGetFail,
	e_ItemGetSuccess,
	e_ItemGiveFail,
	e_ItemGiveSuccess,
	e_ChangeStateFail,
	e_ChangeStateSuccess
}e_DBSaveState;

typedef enum e_Object_Dir {
	e_L,
	e_LU,
	e_U,
	e_UR,
	e_R,
	e_RD,
	e_D,
	e_DL
}e_ObjectDir;