#pragma once
#include "stdafx.h"
#include "monster.h"

class CMonsterPool:public CMonster {
private:
	unsigned short m_usNumObject = 0;
public:
	explicit CMonsterPool();
	virtual ~CMonsterPool();

public:
	static concurrency::concurrent_unordered_map<unsigned short, class CMonster*>m_ccumMonsterPool[2];
};