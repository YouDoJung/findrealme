#include "stdafx.h"
#include "process.h"
#include "map_village.h"
#include "map_boss.h"
#include "map_loby.h"
#include "map_dungeon.h"
#include "manager.h"

class CPlayerPool* CProcess::m_pPlayerPool = nullptr;
class CMonsterPool* CProcess::m_pMonsterPool = nullptr;
concurrency::concurrent_unordered_set<unsigned short>CProcess::m_ccusLoginList[4];
concurrency::concurrent_priority_queue<s_ObjectEvent, CEventOperator>CProcess::m_ccpqEventQueue;
std::function<void(char, unsigned short)>CProcess::m_fpAttackEvent[2][7];

CProcess::CProcess()
{
	BindPlayerAttackEvent();
}

CProcess::~CProcess()
{
	InitProcessData();
}

void CProcess::InitProcessData()
{
	for (int i = 0; i < 4; ++i) {
		SAFE_DELETE_CON_SET(CProcess::m_ccusLoginList[i]);
	}
	m_ccpqEventQueue.clear();
}

bool CProcess::CheckPortalByPlayerInVillage(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Village::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckPortalByPlayerInLoby(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Loby::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckPortalByPlayerInDungeon(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Dungeon::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckPortalByPlayerInBoss(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Boss::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckCollisionManPlayerAttack1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;

	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerAttack2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;

	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerSkill1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;

	player.Extents.x = 533.0f;
	player.Extents.z = 584.0f;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerSkill2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	player.Extents.x = 334.0f;
	player.Extents.z = 467.0f;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerSkill3(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionManPlayerSkill4(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionManPlayerSkill5(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionWoManPlayerAttack1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerAttack2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill1(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill2(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	player.Extents.x = 500.0f;
	player.Extents.z = 500.0f;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill3(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;

	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();


	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);

	player.Extents.x = 528.0f;
	player.Extents.z = 300.0f;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionWoManPlayerSkill4(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill5(char& cMap, unsigned short& usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();

	if (!Vector3::ObjectRangeCheck(monster.Center, player.Center, PLAYER_ATTACK_MINIMUM_CHECK_RANGE)) return false;


	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionPlayerByBossLegAttack(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::BoundingOrientedBox m_xmTransOOBB;
	DirectX::BoundingOrientedBox monster = m_xmTransOOBB = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetOOBB();
	DirectX::XMFLOAT3 monster_look = Vector3::Normalize(CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector());

	m_xmTransOOBB.Extents.x = BOSS_LEG_ATTACK_EXTENTS;
	m_xmTransOOBB.Extents.y = 100.0f;
	m_xmTransOOBB.Extents.z = BOSS_LEG_ATTACK_EXTENTS;

	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	xmf4x4World._41 -= monster_look.x * BOSS_LEG_ATTACK_START_RANGE, xmf4x4World._42 = 516.0f, xmf4x4World._43 -= monster_look.z * BOSS_LEG_ATTACK_START_RANGE;

	m_xmTransOOBB.Transform(monster, XMLoadFloat4x4(&xmf4x4World));
	DirectX::XMStoreFloat4(&m_xmTransOOBB.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmTransOOBB.Orientation)));

	m_xmTransOOBB.Center.x = xmf4x4World._41;
	m_xmTransOOBB.Center.y = 516.0f;
	m_xmTransOOBB.Center.z = xmf4x4World._43;


	if (m_xmTransOOBB.Intersects(player)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionPlayerByBossTailAttack(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::BoundingOrientedBox m_xmTransOOBB;
	DirectX::BoundingOrientedBox monster = m_xmTransOOBB = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetOOBB();
	DirectX::XMFLOAT3 monster_look = Vector3::Normalize(CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector());

	m_xmTransOOBB.Extents.x = BOSS_TAIL_ATTACK_EXTENTS;
	m_xmTransOOBB.Extents.y = 100.0f;
	m_xmTransOOBB.Extents.z = BOSS_TAIL_ATTACK_EXTENTS;

	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	xmf4x4World._41 -= monster_look.x * BOSS_TAIL_ATTACK_START_RANGE, xmf4x4World._42 = 516.0f, xmf4x4World._43 -= monster_look.z * BOSS_TAIL_ATTACK_START_RANGE;

	m_xmTransOOBB.Transform(monster, XMLoadFloat4x4(&xmf4x4World));
	DirectX::XMStoreFloat4(&m_xmTransOOBB.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmTransOOBB.Orientation)));

	m_xmTransOOBB.Center.x = xmf4x4World._41;
	m_xmTransOOBB.Center.y = 516.0f;
	m_xmTransOOBB.Center.z = xmf4x4World._43;

	if (m_xmTransOOBB.Intersects(player)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionPlayerByBossFlyAttack(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::BoundingOrientedBox m_xmTransOOBB;
	DirectX::BoundingOrientedBox monster = m_xmTransOOBB = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetOOBB();
	DirectX::XMFLOAT3 monster_look = Vector3::Normalize(CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector());

	m_xmTransOOBB.Extents.x = BOSS_FLY_ATTACK_EXTENTS;
	m_xmTransOOBB.Extents.y = 100.0f;
	m_xmTransOOBB.Extents.z = BOSS_FLY_ATTACK_EXTENTS;

	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	xmf4x4World._41 -= monster_look.x * BOSS_FLY_ATTACK_START_RANGE, xmf4x4World._42 = 516.0f, xmf4x4World._43 -= monster_look.z * BOSS_FLY_ATTACK_START_RANGE;

	m_xmTransOOBB.Transform(monster, XMLoadFloat4x4(&xmf4x4World));
	DirectX::XMStoreFloat4(&m_xmTransOOBB.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmTransOOBB.Orientation)));

	m_xmTransOOBB.Center.x = xmf4x4World._41;
	m_xmTransOOBB.Center.y = 516.0f;
	m_xmTransOOBB.Center.z = xmf4x4World._43;

	if (m_xmTransOOBB.Intersects(player)) {
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionPlayerByBossFireBreath(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::BoundingOrientedBox m_xmTransOOBB;
	DirectX::BoundingOrientedBox monster = m_xmTransOOBB = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetOOBB();
	DirectX::XMFLOAT3 monster_look = Vector3::Normalize(CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector());

	m_xmTransOOBB.Extents.x = BOSS_FIRE_BREATH_EXTENTS;
	m_xmTransOOBB.Extents.y = 100.0f;
	m_xmTransOOBB.Extents.z = BOSS_FIRE_BREATH_EXTENTS;

	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	xmf4x4World._41 -= monster_look.x * BOSS_FIRE_BREATH_START_RANGE, xmf4x4World._42 = 516.0f, xmf4x4World._43 -= monster_look.z * BOSS_FIRE_BREATH_START_RANGE;

	m_xmTransOOBB.Transform(monster, XMLoadFloat4x4(&xmf4x4World));
	DirectX::XMStoreFloat4(&m_xmTransOOBB.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmTransOOBB.Orientation)));

	m_xmTransOOBB.Center.x = xmf4x4World._41;
	m_xmTransOOBB.Center.y = 516.0f;
	m_xmTransOOBB.Center.z = xmf4x4World._43;

	if (m_xmTransOOBB.Intersects(player)) {
		return true;
	}
	else return false;
}

void CProcess::PostEventManPlayerAttack1Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
}
void CProcess::PostEventManPlayerAttack2Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
}
void CProcess::PostEventManPlayerSkill1Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
}
void CProcess::PostEventManPlayerSkill2Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
}
void CProcess::PostEventManPlayerSkill3Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill3, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(700));
}
void CProcess::PostEventManPlayerSkill4Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
}
void CProcess::PostEventManPlayerSkill5Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(300));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1100));
}

void CProcess::PostEventWoManPlayerAttack1Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1200));
}
void CProcess::PostEventWoManPlayerAttack2Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
}
void CProcess::PostEventWoManPlayerSkill1Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
}
void CProcess::PostEventWoManPlayerSkill2Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
}
void CProcess::PostEventWoManPlayerSkill3Monster(char& cMap, unsigned short& usPlayerID)
{
	CProcess::PostEvent(cMap, NO_TARGET_PLAYER, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill3, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
}
