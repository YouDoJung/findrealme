#pragma once
#include "stdafx.h"
#include "object.h"

constexpr char SOCKET_LOCK = 0;
constexpr char CONNECT_LOCK = 1;
constexpr char ID_LOCK = 2;
constexpr char DEV_MAP = 3;
constexpr char DEV_CHARACTER = 4;
constexpr char ITEM_LOCK = 5;
constexpr char g_iPlayerLockNum = 6;

class CPlayer : public CObject {
private:
	volatile bool m_bIsInvincibility = false;
	volatile bool m_bIsDevMapSwap = false;

	char m_cPlayer_Attack_Cnt = 0;
	unsigned short m_usId;
	volatile bool m_bConnect = false;
	SOCKET m_sSock;
	s_Client m_sState;
	s_POSSESSION_ITEM m_sPossessionItem;
	concurrency::concurrent_unordered_set<unsigned short>m_ccusViewList;
	std::shared_mutex m_rmPlayerStatusMutex[g_iPlayerLockNum];
	std::recursive_mutex m_rmPlayerListMutex;
public:
	CPlayer();
	virtual ~CPlayer();

public:
	void SetAttackCnt(char cCnt)
	{
		m_cPlayer_Attack_Cnt = cCnt;
	}
	char GetAttackCnt()
	{
		return m_cPlayer_Attack_Cnt;
	}
	void SetHPPortion(char cCnt)
	{
		std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ITEM_LOCK]);
		m_sPossessionItem.m_cHpPortion = cCnt;
	}
	void SetMPPortion(char cCnt)
	{
		std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ITEM_LOCK]);
		m_sPossessionItem.m_cMpPortion = cCnt;
	}
	void SetRepatriation(char cCnt)
	{
		std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ITEM_LOCK]);
		m_sPossessionItem.m_cRepatriation = cCnt;
	}
	const char& GetHPPortion()
	{
		std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ITEM_LOCK]);
		return m_sPossessionItem.m_cHpPortion;
	}
	const char& GetMPPortion()
	{
		std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ITEM_LOCK]);
		return m_sPossessionItem.m_cMpPortion;
	}
	const char& GetRepatriation()
	{
		std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ITEM_LOCK]);
		return m_sPossessionItem.m_cRepatriation;
	}
	void SetPossessionItem(s_POSSESSION_ITEM pItem)
	{
		m_sPossessionItem = pItem;
	}
	s_POSSESSION_ITEM& GetPossessionItem()
	{
		return m_sPossessionItem;
	}
	void SetInvincibility() { 
		m_bIsInvincibility = !m_bIsInvincibility;
	}
	void SetDevMapSwap() { 
		m_bIsDevMapSwap = !m_bIsDevMapSwap;
	}

	bool GetInvincibility() {
		return m_bIsInvincibility;
	}
	bool GetDevMapSwap() {
		return m_bIsDevMapSwap;
	}

	void SetPlayerSocket(const SOCKET& CLIENT_SOCKET){ std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[SOCKET_LOCK]); m_sSock = CLIENT_SOCKET;}
	void SetPlayerConnect(bool STATE){ std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[CONNECT_LOCK]); m_bConnect = STATE;}
	void SetPlayerID(const unsigned short& ID){ std::unique_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ID_LOCK]); m_usId = ID;}

	const SOCKET GetPlayerSokcet(){ std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[SOCKET_LOCK]); return m_sSock;}
	const BOOL GetPlayerConnection() { std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[CONNECT_LOCK]); return m_bConnect; }
	const unsigned short GetPlayerID(){ std::shared_lock<std::shared_mutex>lock(m_rmPlayerStatusMutex[ID_LOCK]); return m_usId;}
public:
	void InitClientData();
	UCHAR* RecvEvent(int data_size, UCHAR* io_ptr);
	void SetRecvState();
	
public:
	void CopyPlayerList(concurrency::concurrent_unordered_set<unsigned short>& usCopyList) {
		std::unique_lock<std::recursive_mutex>lock(m_rmPlayerListMutex);
		usCopyList = m_ccusViewList;
		lock.unlock();
		
		for (auto au = usCopyList.begin(); au != usCopyList.end();) {
			if (*au >= MAX_PLAYER) {
				au = usCopyList.unsafe_erase(au);
			}
			else {
				++au;
			}
		}
	}
	void CopyMonsterList(concurrency::concurrent_unordered_set<unsigned short>& usCopyList) {
		std::unique_lock<std::recursive_mutex>lock(m_rmPlayerListMutex);
		usCopyList = m_ccusViewList;
		lock.unlock();
		for (auto au = usCopyList.begin(); au != usCopyList.end();) {
			if (*au < MAX_PLAYER) {
				au = usCopyList.unsafe_erase(au);
			}
			else {
				++au;
			}
		}
	}
	void CopyBefore(concurrency::concurrent_unordered_set<unsigned short>& usCopyList)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		usCopyList = m_ccusViewList;
	}
	void DeleteListElement(unsigned short bId)
	{
		concurrency::concurrent_unordered_set<unsigned short>list;
		CopyBefore(list);
		for (auto au = list.begin(); au != list.end();) {
			if (*au == bId) {
				au = list.unsafe_erase(au);
				break;
			}
			else {
				++au;
			}
		}
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		m_ccusViewList = list;
		
	}
	void InsertListElement(unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		m_ccusViewList.insert(bId);
	}
	BOOL ExistListElement(unsigned short bId)
	{
		if (m_ccusViewList.count(bId) != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	void ClearListElement()
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmPlayerListMutex);
		SAFE_DELETE_CON_SET(m_ccusViewList);
	}
	void ReSpawn()
	{
		m_cPlayer_Attack_Cnt = 0;
		SetState(e_ObjectState::e_Object_Idle);
		SetHP(GetLimitHP()/2);
		SetMP(GetLimitMP()/2);
		SetExp(GetExp() / 2);
		SetAniTime();
		DirectX::XMFLOAT3 first_pos = GetFirstPos();
		DirectX::XMFLOAT4X4 xmf4World = Matrix4x4::Identity();
		xmf4World._11 = 1.0f; xmf4World._12 = 0.0f; xmf4World._13 = 0.0f;
		xmf4World._21 = 0.0f; xmf4World._22 *= 1.0f; xmf4World._23 = 0.0f;
		xmf4World._31 = 0.0f; xmf4World._32 = 0.0f; xmf4World._33 = 1.0f;
		xmf4World._41 = first_pos.x; xmf4World._42 = first_pos.y; xmf4World._43 = first_pos.z;

		SetWorld(xmf4World);

	}
};
