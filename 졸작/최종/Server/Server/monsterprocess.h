#pragma once
#include "stdafx.h"
#include "process.h"

class CMonsterProcess: public CProcess {
private:

	std::function<void(char, unsigned short, unsigned short)>m_fpMonsterUpdate[22];
	std::function<void(char, unsigned short)>m_fpMonsterByPlayerUpdate[14];
	std::function<void(unsigned short)>m_fpBossAttackPlayerUpdate[4];

	char m_cBossHitCnt = 0;
	bool m_bIs_Boss_Raid = false;
	char m_cBefore_Boss_State;
	char m_cBossAttackCnt = 0;
	bool m_bIs_Boss_Fly = false;
	bool m_bBoss_Next_Attack = false;
	bool m_bIs_Boss_Attack_Player = false;
	bool m_bIs_Breath = false;

public:
	explicit CMonsterProcess();
	virtual ~CMonsterProcess();

public:
	inline void BindMonsterUpdate()
	{
		m_fpMonsterUpdate[e_ObjectState::e_Object_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->AttackEvent(cMap, usID, usTarget); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Down] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->DownEvent(cMap, usID); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Follow] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->FollowEvent(cMap, usID, usTarget); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Idle] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->IdleEvent(cMap, usID); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_MoveToFirstPos] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->MoveFirstPosEvent(cMap, usID, usTarget); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Respawn] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->RespawnEvent(cMap, usID); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Heal] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->HealEvent(cMap, usID); };
		
		m_fpMonsterUpdate[e_BossState::e_Boss_Leg_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossLegAttackEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Tail_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossTailAttackEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFlyAttackEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_FireBreath] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFireBreathEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Down] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossDownEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Follow] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFollowEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Idle] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossIdleEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_MoveToFirstPos] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossMoveFirstPosEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Respawn] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossRespawnEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFlyEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Follow] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFlyFollowEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Down] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFlyDownEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Dead] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossFlyDeadEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Raid] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->BossRaidEvent(usTarget); };
	}

	inline void BindMonsterDamaged()
	{
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Attack1] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManAttack1(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Attack2] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManAttack2(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill1] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManSkill1(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill2] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManSkill2(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill3] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManSkill3(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill4] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManSkill4(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill5] = [&](char cMap, unsigned short usPlayerId) { this->ByPlayerManSkill5(cMap, usPlayerId); };

		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Woman_Attack1] = [&](char cMap,unsigned short usPlayerId) { this->ByPlayerWoManAttack1(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Woman_Attack2] = [&](char cMap,unsigned short usPlayerId) { this->ByPlayerWoManAttack2(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill1] = [&](char cMap,  unsigned short usPlayerId) { this->ByPlayerWoManSkill1(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill2] = [&](char cMap,unsigned short usPlayerId) { this->ByPlayerWoManSkill2(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill3] = [&](char cMap,  unsigned short usPlayerId) { this->ByPlayerWoManSkill3(cMap, usPlayerId); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill4] = [&](char cMap,  unsigned short usPlayerId) { this->ByPlayerWoManSkill4(cMap, usPlayerId); };
		//m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill5] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManSkill5(cMap, usTarget); };
	}

	inline void BindBossAttackPlayer()
	{
		m_fpBossAttackPlayerUpdate[e_BossAttackType::e_Leg_Attack] = [&](unsigned short usTarget) { 
			this->BossLegAttackPlayerUpdate(usTarget);
		};
		m_fpBossAttackPlayerUpdate[e_BossAttackType::e_Tail_Attack] = [&](unsigned short usTarget) { 
			this->BossTailAttackPlayerUpdate(usTarget);
		};
		m_fpBossAttackPlayerUpdate[e_BossAttackType::e_Fly_Attack] = [&](unsigned short usTarget) { 
			this->BossFlyAttackPlayerUpdate(usTarget);
		};
		m_fpBossAttackPlayerUpdate[e_BossAttackType::e_Fire_Breath] = [&](unsigned short usTarget) {
			this->BossFireBreathPlayerUpdate(usTarget);
		};
	}
public:
	inline void UpdateMonster(char cState, char cMap, unsigned short usID, unsigned short usTarget) {
		m_fpMonsterUpdate[cState](cMap, usID, usTarget); }
	inline void UpdateMonsterByPlayer(char cState, char cMap, unsigned short usPlayerID) {
		m_fpMonsterByPlayerUpdate[cState](cMap, usPlayerID); }
	inline void UpdatePlayerByMonster(char cState, unsigned short usTarget) {
		m_fpBossAttackPlayerUpdate[cState](usTarget);
	}

	void CheckPlayerInRange(char cMap, concurrency::concurrent_unordered_set<unsigned short>& ccusLogin, concurrency::concurrent_unordered_set<unsigned short>& ccusList, unsigned short usID);

	bool CheckPlayerInRangeForBossAttack(unsigned short usID, DirectX::XMFLOAT3& xmf3BossPos,float fDistance);

	void MoveToTargetPlayer(char cMap, unsigned short usTargetID, unsigned short usID);
	void MoveToTargetPlayer(unsigned short usTargetID);
	void MoveToFirstPos(char cMap, unsigned short usID);

	void FollowEvent(char cMap, unsigned short usID, unsigned short usTarget);
	void MoveFirstPosEvent(char cMap, unsigned short usID, unsigned short usTarget);
	void IdleEvent(char cMap, unsigned short usID);
	void AttackEvent(char cMap, unsigned short usID, unsigned short usTarget);
	void DownEvent(char cMap, unsigned short usID);
	void RespawnEvent(char cMap, unsigned short usID);
	void HealEvent(char cMap, unsigned short usID);

	void BossFollowEvent(unsigned short usTarget);
	void BossLegAttackEvent(unsigned short usTarget);
	void BossTailAttackEvent(unsigned short usTarget);
	void BossFlyAttackEvent(unsigned short usTarget);
	void BossFireBreathEvent(unsigned short usTarget);
	void BossIdleEvent();
	void BossMoveFirstPosEvent(unsigned short usTarget);
	void BossDownEvent();
	void BossRespawnEvent();
	void BossFlyEvent(unsigned short usTarget);
	void BossFlyFollowEvent(unsigned short usTarget);
	void BossFlyDownEvent(unsigned short usTarget);
	void BossFlyDeadEvent();
	void BossRaidEvent(unsigned short usTarget);

	void BossLegAttackPlayerUpdate(unsigned short usTarget);
	void BossTailAttackPlayerUpdate(unsigned short usTarget);
	void BossFlyAttackPlayerUpdate(unsigned short usTarget);
	void BossFireBreathPlayerUpdate(unsigned short usTarget);

	void ByPlayerManAttack1(char cMap, unsigned short usPlayerId);
	void ByPlayerManAttack2(char cMap,  unsigned short usPlayerId);
	void ByPlayerManSkill1(char cMap,  unsigned short usPlayerId);
	void ByPlayerManSkill2(char cMap,  unsigned short usPlayerId);
	void ByPlayerManSkill3(char cMap, unsigned short usPlayerId);
	void ByPlayerManSkill4(char cMap, unsigned short usPlayerId);
	void ByPlayerManSkill5(char cMap,unsigned short usPlayerId);

	void ByPlayerWoManAttack1(char cMap,unsigned short usPlayerId);
	void ByPlayerWoManAttack2(char cMap, unsigned short usPlayerId);
	void ByPlayerWoManSkill1(char cMap, unsigned short usPlayerId);
	void ByPlayerWoManSkill2(char cMap, unsigned short usPlayerId);
	void ByPlayerWoManSkill3(char cMap, unsigned short usPlayerId);
	void ByPlayerWoManSkill4(char cMap,  unsigned short usPlayerId);
	void ByPlayerWoManSkill5(char cMap,unsigned short usPlayerId);

	void ByPlayerLevelUpEvent(char cMap, unsigned short usMonsterId, unsigned short usPlayerId, int iAfterHp);
	void BossByPlayerLevelUpEvent(unsigned short usPlayerId, int iAfterHp);
	void ByBossPlayerDamagedEvent(unsigned short usTarget, int iDamage);
public:
	void SetBossNextAttack(bool bState) { m_bBoss_Next_Attack = bState; }
	bool GetBossNextAttack() { return m_bBoss_Next_Attack; }
	void SetBossIsFly(bool bFly) { m_bIs_Boss_Fly = bFly; }
	bool GetBossIsFly() { return m_bIs_Boss_Fly; }
	void SetBossAttackCnt(char cCnt) {
		m_cBossAttackCnt = cCnt;
	}
	char GetBossAttackCnt() { return m_cBossAttackCnt; }
};