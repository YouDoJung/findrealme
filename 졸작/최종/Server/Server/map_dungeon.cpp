#include "stdafx.h"
#include "map_dungeon.h"

CMap_Dungeon::CMap_Dungeon()
{
	std::string sample_id;
	DirectX::XMFLOAT3 sample_extent;
	DirectX::XMFLOAT3 sample_pos;
	DirectX::XMFLOAT3 sample_none; //이건 파일 잘못 저장해둔거 떄문에 넘어가줄 변수
	m_mmObjectBoundingBox.clear();

	std::ifstream in("../MapData/DungeonObject.txt");
	if (!in.is_open()) {
		std::cout << "DungeonObject Open Failed" << std::endl;
		return;
	}

	in >> m_iNumObject;
	for (int i = 0; i < m_iNumObject; ++i) {
		in >> sample_id;
		in >> sample_none.x;
		in >> sample_none.y;
		in >> sample_none.z;
		in >> sample_extent.x;
		in >> sample_extent.y;
		in >> sample_extent.z;
		in >> sample_pos.x;
		in >> sample_pos.y;
		in >> sample_pos.z;
		in >> sample_none.x;
		in >> sample_none.y;
		in >> sample_none.z;
		if (sample_id == std::string("Wall"))
			m_mmObjectBoundingBox.insert(std::make_pair(sample_id, DirectX::BoundingOrientedBox(sample_pos, sample_extent, DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f))));
	}
	m_iNumObject = static_cast<int>(m_mmObjectBoundingBox.size());
	in.close();
}

CMap_Dungeon::~CMap_Dungeon()
{
	m_mmObjectBoundingBox.clear();
}

bool CMap_Dungeon::CheckMapObjectCollision(DirectX::BoundingOrientedBox& ObjectOOBB)
{
	for (auto& au : m_mmObjectBoundingBox) {
		DirectX::BoundingOrientedBox static_object_box = au.second;
		if (!Vector3::ObjectRangeCheck(ObjectOOBB.Center, static_object_box.Center, 1000.0f)) continue;
		if (au.second.Intersects(ObjectOOBB)) return true;
		else continue;
	}
	return false;
}

bool CMap_Dungeon::CheckMapPortal(DirectX::BoundingOrientedBox& ObjectOOBB)
{
	std::multimap<std::string, DirectX::BoundingOrientedBox>::iterator it;

	it = m_mmObjectBoundingBox.find(std::string("Portal"));
	if (it != m_mmObjectBoundingBox.end()) {
		if (it->second.Intersects(ObjectOOBB)) {
			return true;
		}
		else return false;
	}
	else return false;
}