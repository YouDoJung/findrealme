#pragma once
#include "stdafx.h"
#include "object.h"


constexpr char WAKE_LOCK = 0;
constexpr char FOLLOW_LOCK = 1;
constexpr char LAST_CHANGE_DIR_LOCK = 2;
constexpr char RANDOM_POS_LOCK = 3;

constexpr char g_cMonsterLockNum = 4;

class CMonster : public CObject {
private:
	float m_fNpcTime = 0.0f;
	unsigned short m_usTargetId = NO_TARGET_PLAYER;
	bool m_bWakeUp = false;
	std::shared_mutex m_smMonsterStatusMutex[g_cMonsterLockNum];
	float m_fRotate = 0.0f;
	concurrency::concurrent_queue<s_MonsterEvent>m_ccqMonsterEventQueue;
	std::chrono::high_resolution_clock::time_point m_tpLastChangeDir;
	DirectX::XMFLOAT3 m_xmf3RandomPos;
public:
	explicit CMonster();
	virtual ~CMonster();
public:
	void ReSpawn();
	void SetWakeUp(bool bState) { std::unique_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[WAKE_LOCK]); m_bWakeUp = bState; }
	void SetDirTime() { 
		std::unique_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[LAST_CHANGE_DIR_LOCK]); 
		m_tpLastChangeDir = std::chrono::high_resolution_clock::now();
	}
	std::chrono::high_resolution_clock::time_point GetDirTime() {
		std::shared_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[LAST_CHANGE_DIR_LOCK]);
		return m_tpLastChangeDir;
	}
	void SetFollow(unsigned short usID) { std::unique_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[FOLLOW_LOCK]); m_usTargetId = usID; }
	void SetRandomPos(DirectX::XMFLOAT3& xmf3Pos) {
		std::unique_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[RANDOM_POS_LOCK]);
		m_xmf3RandomPos = xmf3Pos;
	}
	DirectX::XMFLOAT3& GetRandomPos() {
		std::shared_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[RANDOM_POS_LOCK]);
		return m_xmf3RandomPos;
	}
	const unsigned short GetFollow() { std::shared_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[FOLLOW_LOCK]); return m_usTargetId; }
	bool GetWakeUp() { std::shared_lock<std::shared_mutex>lock(m_smMonsterStatusMutex[WAKE_LOCK]); return m_bWakeUp; }

	void Move(const float& fTimeDistance);


	void SaveEvent(DirectX::XMFLOAT3& xmf3ToTarget,float& fRotate, e_ObjectState eState)
	{
		m_ccqMonsterEventQueue.push(s_MonsterEvent{xmf3ToTarget,fRotate,eState });
	}
	bool GetEvent(s_MonsterEvent& sEvent)
	{
		if (m_ccqMonsterEventQueue.try_pop(sEvent)) {
			return true;
		}
		else return false;
	}
	void ClearEvent() { m_ccqMonsterEventQueue.clear(); }
};