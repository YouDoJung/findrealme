#pragma once
#include "stdafx.h"

class CData_Base : public CTemplateSingleton<CData_Base> {
private:
	SQLHENV hEnv_;
	SQLHDBC hDbc_;
	SQLHSTMT hStmt_ = 0;

	concurrency::concurrent_queue<s_DataBaseEvent>ccqDataBaseEventQueue_;
	concurrency::concurrent_queue<s_DataBaseEvent>ccqSaveStateQueue;

	std::function<void(s_DataBaseEvent&)>fpDataBaseProcess[5];
	std::vector<_User_Data>UserData_;
public:
	explicit CData_Base();
	~CData_Base();


public:
	void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
	bool ConnectDataBase();
	void DisConnectDataBase();
	void RunDataBase(s_DataBaseEvent& event);

	void LogInProcess(s_DataBaseEvent& event);
	void LogOutProcess(s_DataBaseEvent& event);
	void ItemGetProcess(s_DataBaseEvent& event);
	void ItemGiveProcess(s_DataBaseEvent& event);
	void ChangeStateProcess(s_DataBaseEvent& event);
	
public:
	inline void BindDataBaseFP()
	{
		fpDataBaseProcess[e_Data_Base_State::e_Login] = [&](s_DataBaseEvent& event) { this->LogInProcess(event); };
		fpDataBaseProcess[e_Data_Base_State::e_LogOut] = [&](s_DataBaseEvent& event) { this->LogOutProcess(event); };
		fpDataBaseProcess[e_Data_Base_State::e_ItemGet] = [&](s_DataBaseEvent& event) { this->ItemGetProcess(event); };
		fpDataBaseProcess[e_Data_Base_State::e_ItemGive] = [&](s_DataBaseEvent& event) { this->ItemGiveProcess(event); };
		fpDataBaseProcess[e_Data_Base_State::e_ChangeState] = [&](s_DataBaseEvent& event) {this->ChangeStateProcess(event); };
	}

	inline bool DataBaseQueueIsEmpty()
	{
		return ccqDataBaseEventQueue_.empty();
	}

	inline void InsertToDataBaseQueue(s_DataBaseEvent& event)
	{
		ccqDataBaseEventQueue_.push(event);
	}
	inline bool PopFromDataBaseQueue(s_DataBaseEvent& event)
	{
		return ccqDataBaseEventQueue_.try_pop(event);
	}

	inline void InsertToStateQueue(s_DataBaseEvent& event)
	{
		ccqSaveStateQueue.push(event);
	}
	inline bool PopFromStateQueue(s_DataBaseEvent& event)
	{
		return ccqSaveStateQueue.try_pop(event);
	}

	inline bool GetIsLogin(std::string id)
	{
		for (auto& player : UserData_) {
			if (player.m_sID == id) {
				return true;
			}
		}
		return false;
	}

	inline bool GetIsLogin(unsigned short usID)
	{
		for (auto& player : UserData_) {
			if (player.m_usID == usID) {
				return true;
			}
		}
		return false;
	}

	inline void SetIsLogin(int iPlayerNum, unsigned short usID,std::string id)
	{
		UserData_.emplace_back(_User_Data{ id, usID, iPlayerNum});
	}

	inline void FindIsLogin(unsigned short usID, wchar_t* user_id, bool bDelete = false)
	{
		std::vector<_User_Data>::iterator it;
		for (it = UserData_.begin(); it != UserData_.end();) {
			if (it->m_usID == usID) {
				std::wstring sample_id = std::wstring(it->m_sID.begin(), it->m_sID.end());

				const wchar_t* tempt_id = sample_id.c_str();

				wcsncpy_s(user_id, MAX_STR_LEN, tempt_id, (UINT32)wcslen(tempt_id));

				if(bDelete)
					UserData_.erase(it);
				break;
			}
			else
				++it;
		}
	}

	inline int FindPlayerNumberForItemData(unsigned short usID)
	{
		std::vector<_User_Data>::iterator it;
		for (it = UserData_.begin(); it != UserData_.end();) {
			if (it->m_usID == usID) {
				return it->m_iPlayerNumber;
			}
			else
				++it;
		}
		return static_cast<int>(NO_TARGET_PLAYER);
	}
};