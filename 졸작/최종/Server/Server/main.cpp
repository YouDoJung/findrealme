// main.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"
#include "main.h"

char SERVERIP[20] = {};

#ifdef _DEBUG
#define new new(_CLIENT_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#else
#define new new(_CLIENT_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#endif // _DEBUG

int main()
{
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);

	std::cout << sizeof(_SC_Accept_Packet) << std::endl;
	CMainApp::GetInstance()->InitApp();
	CMainApp::GetInstance()->StartApp();
	CMainApp::GetInstance()->CloseServer();
	CMainApp::GetInstance()->DestoryInstance();

	//_CrtMemDumpAllObjectsSince(0);
	//_CrtSetBreakAlloc();

	_CrtDumpMemoryLeaks();
}

