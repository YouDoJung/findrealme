#pragma once
#include "stdafx.h"
#include "network.h"

class CIocp : public CTemplateSingleton<CIocp>{
private:
	HANDLE m_hCp;

public:
	explicit CIocp() {};
	~CIocp() {};

	/*/////////////////////////////////////////
	 입출력 완료 포트 생성
	FileHandle 값으로
	INVALID_HANDLE_VALUE을 넣으면 포트 생성.
	nMaxNumberOfThreads : 연결될 스레드의 최댓값
	*//////////////////////////////////////////
	inline BOOL InitCP()
	{
		m_hCp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
		if (m_hCp == NULL)
		{
			return false;
		}
		else
			return true;
	}

	/*/////////////////////////////////////////
	소켓과 연결
	소켓이 연결할수 있는지 확인하고 반환하여 다음단계에서 실질적으로 연결.
	*//////////////////////////////////////////
	inline BOOL CreateSockCP(SOCKET& sClient_socket, int id)
	{
		assert(sClient_socket != INVALID_SOCKET);
		if (sClient_socket == INVALID_SOCKET) {
			closesocket(sClient_socket);
			return false;
		}
		return Associate(reinterpret_cast<HANDLE>(sClient_socket), id);
	}

	/*/////////////////////////////////////////
	파일 핸들과 연결
	*//////////////////////////////////////////
	inline BOOL Associate(HANDLE hDevice, int id)
	{
		assert(hDevice != INVALID_HANDLE_VALUE);
		if (hDevice == INVALID_HANDLE_VALUE) {
			return false;
		}
		CreateIoCompletionPort(hDevice, m_hCp, id, 0);
		if (m_hCp == NULL)
		{\
			return false;
		}
		else {
			return true;
		}
	}

	/*//////////////////////////////////////////
	 IO Completion Queue에서 완료 패킷을 얻어온다.
 pOutCompletionKey      : IO Completon 큐에서 빼내온 레코드의 Completion Key
 pdwOutBytesTransferred : IO Completon 큐에서 빼내온 레코드의 바이트수(0 디폴트)
 pOutOverlapped         : IO Completon 큐에서 빼내온 레코드의 오버랩드 구조체(NULL 디폴트)
  pnOutErrCode          : 위와 상동
  dwWaitingTime         : 완료 패킷이 없을 시에 기다릴 시간 (디폴트: 무한대)
	*///////////////////////////////////////////
	inline BOOL GetQCompleteState(LPDWORD pdwNumberOfByte, PULONG_PTR pulCompletionKey, LPOVERLAPPED* pOverlapped, int* error_txt)
	{
		BOOL retval = GetQueuedCompletionStatus(m_hCp, pdwNumberOfByte,
			pulCompletionKey, pOverlapped, INFINITE);
		if (FALSE == retval && NULL != error_txt) {
			*error_txt = WSAGetLastError();
			return FALSE;
		}
		return retval;
	}

	/*//////////////////////////////////////////
	 IO Completion Queue에 한 완료 패킷를 사용자 정의로 추가
 CompletionKey      : 사용자 정의 완료 패킷에 들어갈 Completion Key
 dwBytesTransferred : 사용자 정의 완료 패킷에 들어갈 바이트수(0 디폴트)
 pOverlapped        : 사용자 정의 완료 패킷에 들어가는 오버랩드 구조체(NULL 디폴트)
 pnOutErrCode       : 위와 상동
	*///////////////////////////////////////////
	inline BOOL PostQCompleteState(ULONG pKey, s_pContext context)
	{
		BOOL retval = PostQueuedCompletionStatus(m_hCp, 10, pKey, &context->m_sOverlapped);
		return retval;
	}

	inline void ClosePort()
	{
		CloseHandle(m_hCp);
	}

};