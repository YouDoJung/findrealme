#pragma once
#include "stdafx.h"

class CGameTimer: public CTemplateSingleton<CGameTimer>
{
public:
	CGameTimer();

	float GetTotalTime()const; // in seconds
	float GetDeltaTime()const; // in seconds

	void Reset(); // Call before message loop.
	void Start(); // Call when unpaused.
	void Stop();  // Call when paused.
	void Tick();  // Call every frame.

private:
	double m_SecondsPerCount = 0.f;
	double m_DeltaTime = -1.f;

	__int64 m_BaseTime = 0;
	__int64 m_PausedTime = 0;
	__int64 m_StopTime = 0;
	__int64 m_PrevTime = 0;
	__int64 m_CurrTime = 0;

	bool m_Stopped = false;
};
