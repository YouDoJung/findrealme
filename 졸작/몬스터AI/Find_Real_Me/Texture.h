#pragma once
#define RESOURCE_TEXTURE2D			0x01
#define RESOURCE_TEXTURE2D_ARRAY	0x02	//[]
#define RESOURCE_TEXTURE2DARRAY		0x03
#define RESOURCE_TEXTURE_CUBE		0x04
#define RESOURCE_BUFFER				0x05

struct SRVROOTARGUMENTINFO
{
	UINT							m_nRootParameterIndex = 0;
	D3D12_GPU_DESCRIPTOR_HANDLE		m_d3dSrvGpuDescriptorHandle;
};

class CTexture
{
public:
	CTexture(int nTextureResources = 1, UINT nResourceType = RESOURCE_TEXTURE2D, int nSamplers = 0, bool is_upload = true);
	virtual ~CTexture();

private:

	UINT m_nTextureType = RESOURCE_TEXTURE2D;
	int m_nTextures = 0;
	ID3D12Resource** m_ppd3dTextures = nullptr;
	ID3D12Resource** m_ppd3dTextureUploadBuffers = nullptr;

	SRVROOTARGUMENTINFO* m_pRootArgumentInfos = nullptr;

	int m_nSamplers = 0;
	D3D12_GPU_DESCRIPTOR_HANDLE* m_pd3dSamplerGpuDescriptorHandles = nullptr;

public:

	ID3D12Resource *CreateTexture(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, UINT nWidth, UINT nHeight, DXGI_FORMAT dxgiFormat, D3D12_RESOURCE_FLAGS d3dResourceFlags, D3D12_RESOURCE_STATES d3dResourceStates, D3D12_CLEAR_VALUE *pd3dClearValue, UINT nIndex);

	void SetRootArgument(int nIndex, UINT nRootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dsrvGpuDescriptorHandle);
	void SetSampler(int nIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSamplerGpuDescriptorHandle);
	void SetTexture(int nIndex, ID3D12Resource* Resource) { m_ppd3dTextures[nIndex] = Resource; }
	void UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList);
	void UpdateShaderVariable(ID3D12GraphicsCommandList* pd3dCommandList, int nIndex);

	void LoadTextureFromFile(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, wchar_t* pszFileName, UINT nIndex);

	int GetTextures() { return(m_nTextures); }
	ID3D12Resource* GetTexture(int nIndex) { return(m_ppd3dTextures[nIndex]); }
	UINT GetTextureType() { return(m_nTextureType); }

	void ReleaseUploadBuffers();
};

class TextureManager
{
public:
	TextureManager(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int nTextureResources, UINT nResourceType);
	~TextureManager();
public:
	void ShadowResourceSetting(D3D12_CPU_DESCRIPTOR_HANDLE hCpuDsv);

	void CreateSrvAndUavDescriptorHeaps(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int nShaderResourceViews, int nUnorderedAccessViews = 0);
	void CreateShaderResourceViews(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, CTexture* pTexture, UINT nRootParameterStartIndex, bool bAutoIncrement);
	D3D12_SHADER_RESOURCE_VIEW_DESC GetShaderResourceViewDesc(D3D12_RESOURCE_DESC d3dResourceDesc, UINT nTextureType);

	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapStart() const { return(m_pd3dSrvUavDescriptorHeap->GetCPUDescriptorHandleForHeapStart()); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandleForHeapStart() const { return(m_pd3dSrvUavDescriptorHeap->GetGPUDescriptorHandleForHeapStart()); }

	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUSrvDescriptorStartHandle() const { return(m_d3dSrvCPUDescriptorStartHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUSrvDescriptorStartHandle() const { return(m_d3dSrvGPUDescriptorStartHandle); }
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUUavDescriptorStartHandle() const { return(m_d3dUavCPUDescriptorStartHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUUavDescriptorStartHandle() const { return(m_d3dUavGPUDescriptorStartHandle); }

	void ReleaseUploadBuffers();

	void SetDescriptorHeapsAndTextureUpdate(ID3D12GraphicsCommandList* pd3dCommandList);

	void SetDescriptorHeaps(ID3D12GraphicsCommandList* pd3dCommandList);
	void TexturesUpdate(ID3D12GraphicsCommandList* pd3dCommandList);
	
	void ShadowSrvUpdate(ID3D12GraphicsCommandList* pd3dCommandList);

private:
	CTexture* m_pTexture = nullptr;
	CTexture* m_pSkyBoxTexture = nullptr;
	ComPtr<ID3D12DescriptorHeap> m_pd3dSrvUavDescriptorHeap = nullptr;
	D3D12_CPU_DESCRIPTOR_HANDLE	m_d3dSrvCPUDescriptorStartHandle = {};
	D3D12_GPU_DESCRIPTOR_HANDLE	m_d3dSrvGPUDescriptorStartHandle = {};
	D3D12_CPU_DESCRIPTOR_HANDLE m_d3dUavCPUDescriptorStartHandle = {};
	D3D12_GPU_DESCRIPTOR_HANDLE	m_d3dUavGPUDescriptorStartHandle = {};

	D3D12_GPU_DESCRIPTOR_HANDLE m_d3dShaderSrvGPUDescriptorHandle = {};
};
