#include "stdafx.h"
#include "ModelObject.h"
#include "Material.h"
#include "Shader.h"
#include "ModelShader.h"

CStaticModelObject::CStaticModelObject() : CGameObject(1)
{
}

CStaticModelObject::~CStaticModelObject()
{
}

CSkinnedObject::CSkinnedObject() : CGameObject(1)
{

}

void CSkinnedObject::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
	//cout << "플레이어 : " << m_xmOOBB.Center.x << ", " << m_xmOOBB.Center.y << ", " << m_xmOOBB.Center.z << endl;


	m_fElapsedTime = fTimeElapsed;
}

void CSkinnedObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	UINT objCBByteSize = (sizeof(ObjectConstants) + 255) & ~255;
	OnPrepareRender();

	// 재질을 오브젝트가 갖고있으면, 오브젝트 내부에서 상수버퍼를 수정한다고 생각하자.

	for (int i = 0; i < m_nMaterials; i++)
	{
		if (m_ppMaterials[i])
		{
			if (m_ppMaterials[i]->m_pShader)
			{
				m_ppMaterials[i]->m_pShader->Render(pd3dCommandList, pCamera, is_Shadow);
			}
		}
	}

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pCurrResource->ObjectCB->Resource()->GetGPUVirtualAddress() + m_ObjectCBIndex * objCBByteSize;
	pd3dCommandList->SetGraphicsRootConstantBufferView(1, d3dGpuVirtualAddress);
}

//////
CSkinAndCombinedObject::CSkinAndCombinedObject()
{

}

CSkinAndCombinedObject::~CSkinAndCombinedObject()
{
	m_mCombinedAnimationClip.clear();
}

void CSkinAndCombinedObject::LoadModelAndInitCombinedAnimationClip(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) {
	m_Importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	m_pModel = m_Importer.ReadFile(filename.c_str(), (aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded) &
		~aiProcess_FindInvalidData |
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_GenUVCoords |
		aiProcess_TransformUVCoords |
		aiProcess_LimitBoneWeights |
		aiProcess_OptimizeMeshes |
		aiProcess_GenSmoothNormals |
		aiProcess_SplitLargeMeshes |
		aiProcess_Triangulate |
		aiProcess_SortByPType);

	m_ppMeshes[0] = new CSkinnedModelMesh(pd3dDevice, pd3dCommandList, m_pModel->mMeshes[0]);

	float start_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[0].mTime;
	float end_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[m_pModel->mAnimations[0]->mChannels[0]->mNumPositionKeys - 1].mTime - 1.0f;
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle", CombinedAnimationClip(start_time, end_time, 10.f)));

	m_strNowClip = "idle";
}

void CSkinAndCombinedObject::SetNowAnimation(string& s) {
	if (m_strNowClip != s)
	{
		m_strNowClip = s;
		m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;
	}
}

void CSkinAndCombinedObject::DragonAI(float fTimeElapsed)
{
	XMFLOAT3 xmf3TargetPosition = XMFLOAT3(m_pTarget->GetPosition().x, 800.f, m_pTarget->GetPosition().z);	// 플레이어의 포지션

	XMFLOAT3 xmf3Look = Vector3::ScalarProduct(GetUp(), -1.f); // 드레곤 룩벡터

	XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, GetPosition(), true);

	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);

	XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);

	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.f);

	float moveSpeed = 400.f;

	if (Vector3::Distance(xmf3TargetPosition, GetPosition()) < 1000.0f) {
		if (m_strNowClip == "idle2" || m_strNowClip == "run") {
			m_Routine = true;
			Rotate(0, 0, fAngle * xmf3CrossProduct.y);
			SetNowAnimation(string("leg_attack"));
		}
		if (m_strNowClip == "leg_attack" && m_mCombinedAnimationClip[m_strNowClip].m_fEndTime <= m_fNowTime) {
			Rotate(0, 0, fAngle * xmf3CrossProduct.y);
			cout << fAngle << endl;
			SetNowAnimation(string("fire_breath"));
		}
		if (m_strNowClip == "fire_breath" && m_mCombinedAnimationClip[m_strNowClip].m_fEndTime <= m_fNowTime) {
			Rotate(0, 0, fAngle * xmf3CrossProduct.y);
			Rotate(0, 0, 180.f);
			SetNowAnimation(string("tail_attack"));
		}
		if (m_strNowClip == "tail_attack" && m_mCombinedAnimationClip[m_strNowClip].m_fEndTime <= m_fNowTime) {
			Rotate(0, 0, 180.f);
			SetNowAnimation(string("idle2"));
		}
	}
	else if (Vector3::Distance(xmf3TargetPosition, GetPosition()) < 2000.0f && m_Routine) {
		if (m_strNowClip == "run" || m_mCombinedAnimationClip[m_strNowClip].m_fEndTime <= m_fNowTime) {
			SetNowAnimation(string("run"));
			if (fAngle > 10.f) {
				Rotate(0, 0, xmf3CrossProduct.y * fTimeElapsed * moveSpeed);
			}
			SetPosition(m_xmf4x4World._41 + xmf3ToTarget.x * moveSpeed * fTimeElapsed, m_xmf4x4World._42 + xmf3ToTarget.y * moveSpeed * fTimeElapsed, m_xmf4x4World._43 + xmf3ToTarget.z * moveSpeed * fTimeElapsed);
		}
	}
	else {
		SetNowAnimation(string("idle2"));
	}
	/*if (Vector3::Distance(xmf3TargetPosition, GetPosition()) < 800.0f) {
		SetNowAnimation(string("tail_attack"));
		if (GetNowAnimation() != string("tail_attack"))
			Rotate(0, 0, 180.f);
	}*/
	/*else if (Vector3::Distance(xmf3TargetPosition, GetPosition()) < 1200.0f ) {
		if (GetNowAnimation() != "tail_attack") {
			SetNowAnimation(string("run"));
			moveSpeed = 400.f;
			if (fAngle > 10.f) {
				Rotate(0, 0, xmf3CrossProduct.y * fTimeElapsed * moveSpeed);
			}
			SetPosition(m_xmf4x4World._41 + xmf3ToTarget.x * moveSpeed * fTimeElapsed, m_xmf4x4World._42 + xmf3ToTarget.y * moveSpeed * fTimeElapsed, m_xmf4x4World._43 + xmf3ToTarget.z * moveSpeed * fTimeElapsed);
		}
	}*/
	/*else {
		SetNowAnimation(string("idle2"));
		Rotate(0, 0, 1);
		SetPosition(m_xmf4x4World._41 + xmf3Look.x * moveSpeed * fTimeElapsed, m_xmf4x4World._42 + xmf3Look.y  * moveSpeed * fTimeElapsed, m_xmf4x4World._43 + xmf3Look.z * moveSpeed * fTimeElapsed);
	}*/
}

void CSkinAndCombinedObject::UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime)
{
	m_pCurrResource = pFrameResource;
	for (int i = 0; i < m_nMeshes; ++i) {
		if (m_ppMeshes[i]) {
			if (m_fNowTime < m_mCombinedAnimationClip[m_strNowClip].m_fStartTime)
				m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;
			
			m_ppMeshes[i]->BoneTransform(m_pCurrResource, m_pModel, m_fNowTime, m_SkinCBIndex);
			m_fNowTime += m_fElapsedTime * m_mCombinedAnimationClip[m_strNowClip].m_fSpeed;
			/*if (m_fNowTime > m_mCombinedAnimationClip[m_strNowClip].m_fEndTime) {
				m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;
			}*/
		}
	}
}

void CSkinAndCombinedObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CSkinnedObject::Render(pd3dCommandList, pCamera, is_Shadow);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				UINT objCBByteSize = (sizeof(SkinnedConstants) + 255) & ~255;
				D3D12_GPU_VIRTUAL_ADDRESS d3dcbTransformsGpuVirtualAddress = m_pCurrResource->SkinnedCB->Resource()->GetGPUVirtualAddress() + m_SkinCBIndex * objCBByteSize;
				pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dcbTransformsGpuVirtualAddress);

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}
}
//////////////////////////////////////
CBossDragonObject::CBossDragonObject() {}
CBossDragonObject::~CBossDragonObject() {}

void CBossDragonObject::LoadModelAndInitCombinedAnimationClip(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) {
	m_Importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	m_pModel = aiImportFile(filename.c_str(), (aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded) &
		~aiProcess_FindInvalidData |
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_GenUVCoords |
		aiProcess_TransformUVCoords |
		aiProcess_LimitBoneWeights |
		aiProcess_OptimizeMeshes |
		aiProcess_GenSmoothNormals |
		aiProcess_SplitLargeMeshes |
		aiProcess_Triangulate |
		aiProcess_SortByPType);

	m_ppMeshes[0] = new CSkinnedModelMesh(pd3dDevice, pd3dCommandList, m_pModel->mMeshes[0]);

	float start_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[0].mTime;
	float end_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[m_pModel->mAnimations[0]->mChannels[0]->mNumPositionKeys - 1].mTime - 1.0f;
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle", CombinedAnimationClip(start_time, 339.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("jump", CombinedAnimationClip(340.f, 365.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("leg_attack", CombinedAnimationClip(366.f, 410.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fire_breath", CombinedAnimationClip(411.f, 475.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("tail_attack", CombinedAnimationClip(475.f, 515.f, 10.f))); 
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("left_hit", CombinedAnimationClip(516.f, 533.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("right_hit", CombinedAnimationClip(534.f, 551.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("die", CombinedAnimationClip(552.f, 611.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("walk", CombinedAnimationClip(612.f, 644.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("run", CombinedAnimationClip(645.f, 660.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fly", CombinedAnimationClip(661.f, 707.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fly_attack", CombinedAnimationClip(708.f, 768, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fast_fly", CombinedAnimationClip(862.f, 884.f, 15.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("down_hit", CombinedAnimationClip(885.f, 905.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("turn_die", CombinedAnimationClip(906.f, 970.f, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle2", CombinedAnimationClip(1005.f, 1100, 10.f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("turn_walk", CombinedAnimationClip(1101.f, end_time, 10.f)));

	m_strNowClip = "idle2";
}

void CBossDragonObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CSkinnedObject::Render(pd3dCommandList, pCamera, is_Shadow);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				UINT objCBByteSize = (sizeof(SkinnedConstants) + 255) & ~255;
				D3D12_GPU_VIRTUAL_ADDRESS d3dcbTransformsGpuVirtualAddress = m_pCurrResource->SkinnedCB->Resource()->GetGPUVirtualAddress() + m_SkinCBIndex * objCBByteSize;
				pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dcbTransformsGpuVirtualAddress);

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}
}

//////////////////
CSkinAndDivisionObject::CSkinAndDivisionObject()
{

}

CSkinAndDivisionObject::~CSkinAndDivisionObject()
{
	m_mDivisionAnimationClip.clear();
}

void CSkinAndDivisionObject::SetNowAnimation(string& s)
{
	if (m_strNowClip != s)
	{
		m_strNowClip = s;
		m_fNowTime = 0.f;
	}
}

void CSkinAndDivisionObject::SetAnimClip(unordered_map<string, DivisionAnimationClip>& clip) {
	m_mDivisionAnimationClip = clip;
}

void CSkinAndDivisionObject::UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime)
{
	m_pCurrResource = pFrameResource;
	for (int i = 0; i < m_nMeshes; ++i) {
		if (m_ppMeshes[i]) {
			m_ppMeshes[i]->BoneTransform(m_pCurrResource, m_mDivisionAnimationClip[m_strNowClip].m_pAnimationModel, m_fNowTime, m_SkinCBIndex);
			m_fNowTime += m_fElapsedTime * m_mDivisionAnimationClip[m_strNowClip].m_fSpeed;

			if (m_mDivisionAnimationClip[m_strNowClip].m_isRoof) {
				if (m_fNowTime > m_mDivisionAnimationClip[m_strNowClip].m_fEndTime) {
					m_fNowTime = m_mDivisionAnimationClip[m_strNowClip].m_fStartTime;
				}
			}
			else {
				if (m_fNowTime > m_mDivisionAnimationClip[m_strNowClip].m_fEndTime) {
					m_fNowTime = 0;
					m_strNowClip = "idle";
				}
			}
		}
	}

	// 자식을 갖고 있다면.
	if (m_vChilds.size() > 0) {
		for (int i = 0; i < m_vChilds.size(); ++i) {
			// 부모의 메시의 애니메이션에서 자식의 변환행렬을 가져옴.
			XMFLOAT4X4 world = m_ppMeshes[0]->GetChildWorlds(m_vChilds[i]->m_strObjectName);
			// 부모의 월드변환행렬과 곱
			world = Matrix4x4::Multiply(world, m_xmf4x4World);
			// 월드 적용 후 상수버퍼 업데이트
			m_vChilds[i]->SetWorld(world);
			// 상수버퍼 업데이트를 왜 여기서 하냐면, 위에 부모객체의 메시에서 자식의 변환행렬을 가져오기 때문에 위에 코드가 수행된 후에 업데이트를 해야함.
			m_vChilds[i]->UpdateCB(pFrameResource);
			// 매프레임 새로운 정보가 상수버퍼에 들어가야 하기 때문에 dirty플래그 초기화
			m_vChilds[i]->SetFrameDirty(gNumFrameResources);
		}
	}
}

void CSkinAndDivisionObject::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));

	// 내가 수정했다 이말이다 이개쌔이야~
	for (int i = 0; i < m_vChilds.size(); ++i) {
		m_vChilds[i]->m_xmOOBBTransformed.Transform(m_vChilds[i]->m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
		XMStoreFloat4(&m_vChilds[i]->m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_vChilds[i]->m_xmOOBBTransformed.Orientation)));
	}
	m_fElapsedTime = fTimeElapsed;
}

void CSkinAndDivisionObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CSkinnedObject::Render(pd3dCommandList, pCamera, is_Shadow);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{

				UINT objCBByteSize = (sizeof(SkinnedConstants) + 255) & ~255;
				D3D12_GPU_VIRTUAL_ADDRESS d3dcbTransformsGpuVirtualAddress = m_pCurrResource->SkinnedCB->Resource()->GetGPUVirtualAddress() + m_SkinCBIndex * objCBByteSize;
				pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dcbTransformsGpuVirtualAddress);

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}

	// 자식을 갖고 있다면.
	if (m_vChilds.size() > 0) {
		for (int i = 0; i < m_vChilds.size(); ++i) {

			// 렌더링 ㄱ
			m_vChilds[i]->Render(pd3dCommandList, pCamera, is_Shadow);
		}
	}
}