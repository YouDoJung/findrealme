#pragma once
#include "Texture.h"
class CShader;

class CMaterial
{
public:
	CMaterial();
	virtual ~CMaterial();

private:
	int m_nReferences = 0;

	XMFLOAT4 m_xmf4DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
	XMFLOAT3 m_xmf3FresnelR0 = { 0.1f, 0.1f, 0.1f };
	float m_fRoughness = 0.1f;
	XMFLOAT4X4 m_xmf4x4MatTransform = Matrix4x4::Identity();

public:
	void AddRef() { m_nReferences++; }
	void Release() { if (--m_nReferences <= 0) delete this; }

	CShader* m_pShader = nullptr;

	void SetAlbedo(XMFLOAT4 xmf4Albedo) { m_xmf4DiffuseAlbedo = xmf4Albedo; }
	void SetFresneIRO(XMFLOAT3 xmf3FresneIRO) { m_xmf3FresnelR0 = xmf3FresneIRO; }
	void SetRoughness(float fRoughness) { m_fRoughness = fRoughness; }
	XMFLOAT4 GetDiffuseAlbedo() const { return m_xmf4DiffuseAlbedo; }
	XMFLOAT3 GetFresneIRO() const { return m_xmf3FresnelR0; }
	float GetRoughness() const { return m_fRoughness; }

	void SetShader(CShader* pShader);

	void LoadMaterial(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, void* pMaterial);
};

