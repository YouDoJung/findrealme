#pragma once
#include "stdafx.h"
#include "ModelMesh.h"

inline void CalculateTangentArray(UINT vertexCount, CModelVertex* vertices, long triangleCount, UINT* indices)
{
	XMFLOAT3 *tan1 = new XMFLOAT3[vertexCount * 2];
	::ZeroMemory(tan1, vertexCount * sizeof(XMFLOAT3) * 2);

	for (long a = 0; a < triangleCount; a++)
	{
		UINT i1 = indices[a * 3 + 0];
		UINT i2 = indices[a * 3 + 1];
		UINT i3 = indices[a * 3 + 2];

		const XMFLOAT3& v1 = vertices[i1].m_xmf3Position;
		const XMFLOAT3& v2 = vertices[i2].m_xmf3Position;
		const XMFLOAT3& v3 = vertices[i3].m_xmf3Position;

		const XMFLOAT2& w1 = vertices[i1].m_xmf2TexCoord;
		const XMFLOAT2& w2 = vertices[i2].m_xmf2TexCoord;
		const XMFLOAT2& w3 = vertices[i3].m_xmf2TexCoord;

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;

		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;

		float r = 1.0F / (s1 * t2 - s2 * t1);
		XMFLOAT3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
			(t2 * z1 - t1 * z2) * r);
		XMFLOAT3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
			(s1 * z2 - s2 * z1) * r);

		tan1[i1] = Vector3::Add(tan1[i1], sdir);
		tan1[i2] = Vector3::Add(tan1[i2], sdir);
		tan1[i3] = Vector3::Add(tan1[i3], sdir);
	}
	for (UINT a = 0; a < vertexCount; a++)
	{
		XMFLOAT3& n = vertices[a].m_xmf3Normal;
		XMFLOAT3& t = tan1[a];

		// Gram-Schmidt orthogonalize
		vertices[a].m_xmf3Tangent = Vector3::Normalize(Vector3::Subtract(t, Vector3::ScalarProduct(n, Vector3::DotProduct(n, t), false), false));
	}

	delete[] tan1;
}

CModelMesh::CModelMesh()
{
}

CModelMesh::CModelMesh(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const aiMesh* pMesh)
{
	m_nVertices = pMesh->mNumVertices;
	m_nStride = sizeof(CModelVertex);
	m_nOffset = 0;
	m_nSlot = 0;

	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	CModelVertex* pVertices = new CModelVertex[m_nVertices];

	for (UINT i = 0; i < m_nVertices; ++i) {

		pVertices[i].m_xmf3Position = XMFLOAT3(&pMesh->mVertices[i].x);
		if (m_xmf3Max.x < pVertices[i].m_xmf3Position.x)
			m_xmf3Max.x = pVertices[i].m_xmf3Position.x;
		if (m_xmf3Max.y < pVertices[i].m_xmf3Position.y)
			m_xmf3Max.y = pVertices[i].m_xmf3Position.y;
		if (m_xmf3Max.z < pVertices[i].m_xmf3Position.z)
			m_xmf3Max.z = pVertices[i].m_xmf3Position.z;

		if (m_xmf3Min.x > pVertices[i].m_xmf3Position.x)
			m_xmf3Min.x = pVertices[i].m_xmf3Position.x;
		if (m_xmf3Min.y > pVertices[i].m_xmf3Position.y)
			m_xmf3Min.y = pVertices[i].m_xmf3Position.y;
		if (m_xmf3Min.z > pVertices[i].m_xmf3Position.z)
			m_xmf3Min.z = pVertices[i].m_xmf3Position.z;
		if (pMesh->HasNormals())
			pVertices[i].m_xmf3Normal = XMFLOAT3(&pMesh->mNormals[i].x);
		else
			pVertices[i].m_xmf3Normal = { 0.f, 0.f, 1.f };
		if (pMesh->HasTextureCoords(0)) {
			pVertices[i].m_xmf2TexCoord = XMFLOAT2(&pMesh->mTextureCoords[0][i].x);
		}
	}

	m_nIndices = pMesh->mNumFaces * 3;
	UINT* pIndices = new UINT[m_nIndices];
	for (UINT i = 0; i < pMesh->mNumFaces; ++i)
	{
		const aiFace& face = pMesh->mFaces[i];
		pIndices[i * 3] = face.mIndices[0];
		pIndices[i * 3 + 1] = face.mIndices[1];
		pIndices[i * 3 + 2] = face.mIndices[2];
	}

	CalculateTangentArray(m_nVertices, pVertices, pMesh->mNumFaces, pIndices);

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);
	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	m_pd3dIndexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pIndices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);
	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	delete[] pVertices;
	delete[] pIndices;
}

CModelMesh::~CModelMesh()
{
}

CSkinnedModelMesh::CSkinnedModelMesh()
{
}

CSkinnedModelMesh::CSkinnedModelMesh(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, const aiMesh* pMesh)
{
	m_nVertices = pMesh->mNumVertices;
	m_nStride = sizeof(CSkinnedVertex);
	m_nOffset = 0;
	m_nSlot = 0;

	m_d3dPrimitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	CSkinnedVertex* pVertices = new CSkinnedVertex[m_nVertices];
	XMFLOAT3 Max = { 0.f, 0.f, 0.f };
	XMFLOAT3 Min = { 10000.f, 10000.f, 10000.f };
	for (UINT i = 0; i < m_nVertices; ++i) {
		pVertices[i].m_xmf3Position = XMFLOAT3(&pMesh->mVertices[i].x);
		if (m_xmf3Max.x < pVertices[i].m_xmf3Position.x)
			m_xmf3Max.x = pVertices[i].m_xmf3Position.x;
		if (m_xmf3Max.y < pVertices[i].m_xmf3Position.y)
			m_xmf3Max.y = pVertices[i].m_xmf3Position.y;
		if (m_xmf3Max.z < pVertices[i].m_xmf3Position.z)
			m_xmf3Max.z = pVertices[i].m_xmf3Position.z;

		if (m_xmf3Min.x > pVertices[i].m_xmf3Position.x)
			m_xmf3Min.x = pVertices[i].m_xmf3Position.x;
		if (m_xmf3Min.y > pVertices[i].m_xmf3Position.y)
			m_xmf3Min.y = pVertices[i].m_xmf3Position.y;
		if (m_xmf3Min.z > pVertices[i].m_xmf3Position.z)
			m_xmf3Min.z = pVertices[i].m_xmf3Position.z;

		if (pMesh->HasNormals())
			pVertices[i].m_xmf3Normal = XMFLOAT3(&pMesh->mNormals[i].x);
		else
			pVertices[i].m_xmf3Normal = { 0.f, 0.f, 1.f };
		if (pMesh->HasTextureCoords(0)) {
			pVertices[i].m_xmf2TexCoord = XMFLOAT2(&pMesh->mTextureCoords[0][i].x);
		}
	}

	m_nBones = pMesh->mNumBones;
	for (UINT i = 0; i < m_nBones; ++i) {
		int BoneIndex = -1;
		const aiBone* pBone = pMesh->mBones[i];
		int tmpIndex = 0;
		//cout << pBone->mName.C_Str() << endl;
		for (const auto& p : m_vBones) { //이미 존재하는 뼈인지 검색
			if (p.first == pBone->mName.data) {
				BoneIndex = tmpIndex;
				//현재 뼈가 이미 벡터에 저장된 뼈일 경우
				//인덱스를 해당 뼈의 인덱스로 저장
				break;
			}
			tmpIndex++;
		}
		if (BoneIndex < 0) { //없으면 새로 추가
			BoneIndex = (int)m_vBones.size();
			//새로 저장하는 뼈일 경우 
			//인덱스는 현재 뼈의 개수 (0개일 경우 0부터 시작)
			Bone bone;
			bone.BoneOffset = XMMATRIX(&pBone->mOffsetMatrix.a1);
			string boneName = pBone->mName.data;
			if (boneName == "Bip001 Muffler2")
				boneName = "Bip001 Muffler02";
			if (boneName == "Bip001 Muffler3")
				boneName = "Bip001 Muffler03";
			if (boneName == "Bip001 Muffler4")
				boneName = "Bip001 Muffler04";
			if (boneName == "Bip001 Muffler202")
				boneName = "Bip001 Muffler";
			if (boneName == "Bip001 Muffler203")
				boneName = "Bip001 MufflerNub";

			m_vBones.emplace_back(make_pair(boneName, bone));
		}
		for (UINT b = 0; b < pBone->mNumWeights; ++b) {
			UINT vertexID = pBone->mWeights[b].mVertexId;
			float weight = pBone->mWeights[b].mWeight;
			pVertices[vertexID].AddBoneData(BoneIndex, weight);
		}
	}

	m_nIndices = pMesh->mNumFaces * 3;
	UINT* pIndices = new UINT[m_nIndices];
	for (UINT i = 0; i < pMesh->mNumFaces; ++i)
	{
		const aiFace& face = pMesh->mFaces[i];
		pIndices[i * 3] = face.mIndices[0];
		pIndices[i * 3 + 1] = face.mIndices[1];
		pIndices[i * 3 + 2] = face.mIndices[2];
	}

	//CalculateTangentArray(m_nVertices, pVertices, pMesh->mNumFaces, pIndices);

	m_pd3dVertexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pVertices, m_nStride * m_nVertices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_pd3dVertexUploadBuffer);

	m_d3dVertexBufferView.BufferLocation = m_pd3dVertexBuffer->GetGPUVirtualAddress();
	m_d3dVertexBufferView.StrideInBytes = m_nStride;
	m_d3dVertexBufferView.SizeInBytes = m_nStride * m_nVertices;

	m_pd3dIndexBuffer = CreateBufferResource(pd3dDevice, pd3dCommandList, pIndices, sizeof(UINT) * m_nIndices, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_pd3dIndexUploadBuffer);

	m_d3dIndexBufferView.BufferLocation = m_pd3dIndexBuffer->GetGPUVirtualAddress();
	m_d3dIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_d3dIndexBufferView.SizeInBytes = sizeof(UINT) * m_nIndices;

	delete[] pVertices;
	delete[] pIndices;
}

CSkinnedModelMesh::~CSkinnedModelMesh()
{
	m_vBones.clear();
	m_xmf4x4ChildWorld.clear();
}

void CSkinnedModelMesh::BoneTransform(FrameResource* pFrameResource, const aiScene* pModel, float fTime, UINT nSkinCBindex)
{
	m_pScene = pModel;
	auto currSkinnedCB = pFrameResource->SkinnedCB.get();

	XMMATRIX Identity = XMMatrixIdentity();
	//루트노드부터 계층구조를 훝어가며 변환 수행 및 뼈에 최종변환 계산
	ReadNodeHeirarchy(fTime, pModel->mRootNode, Identity);

	SkinnedConstants skinnedConstants;

	for (UINT i = 0; i < m_nBones; ++i) {
		//XMFLOAT4X4 mat = Matrix4x4::Identity();
		//XMStoreFloat4x4(&mat, m_vBones[i].second.FinalTransformation);
		//if (mat._11 == 1 && mat._22 == 1 && mat._33 == 1 && mat._44 == 1) {
		//	cout << m_vBones[i].first << endl;
		//}
		//뼈의 최종변환을 반환
		XMStoreFloat4x4(&skinnedConstants.m_xmf4x4BoneTransforms[i], m_vBones[i].second.FinalTransformation);
	}
	currSkinnedCB->CopyData(nSkinCBindex, skinnedConstants);
}

void CSkinnedModelMesh::ReadNodeHeirarchy(float AnimationTime, const aiNode * pNode, const XMMATRIX& ParentTransform)
{
	const aiAnimation* pAnim = m_pScene->mAnimations[0];

	XMMATRIX NodeTransformation = XMMatrixIdentity();// XMMATRIX(&pNode->mTransformation.a1);

	const aiNodeAnim* pNodeAnim = FindNodeAnim(pAnim, pNode->mName.data);
	//현재 노드가 animation channel 에 속한지 확인
	if (pNodeAnim) {

		aiVector3D t;
		CalcInterpolatedPosition(t, AnimationTime, pNodeAnim);
		XMMATRIX TranslationM = XMMatrixTranslation(t.x, t.y, t.z);

		aiQuaternion q;
		CalcInterpolatedRotation(q, AnimationTime, pNodeAnim);
		XMMATRIX RotationM = XMMatrixRotationQuaternion(XMVectorSet(q.x, q.y, q.z, q.w));

		aiVector3D s;
		CalcInterpolatedScaling(s, AnimationTime, pNodeAnim);
		XMMATRIX ScalingM = XMMatrixScaling(s.x, s.y, s.z);

		NodeTransformation = ScalingM * RotationM * TranslationM; //스케일 * 회전 * 이동 변환
		NodeTransformation = XMMatrixTranspose(NodeTransformation);
		//transpos 안시켜주면 모델 깨짐
	}

	//부모노드에 변환값 중첩해서 곱하기
	XMMATRIX GlobalTransformation = ParentTransform * NodeTransformation;


	bool is_ok = false;
	//현재노드가 뼈 노드이면 변환정보를 뼈에 적용
	for (auto& p : m_vBones) {
		if (p.first == pNode->mName.data) {
			p.second.FinalTransformation = GlobalTransformation * p.second.BoneOffset;
			is_ok = true;
			break;
		}
	}
	// swordR이거나 swordL이면
	for (int i = 0; i < vNames.size(); ++i) {

		if (pNode->mName.data == vNames[i]) {
			//cout << "pNode: " << pNode->mName.C_Str() << endl;
			//cout << "vNames: " << vNames[i] << endl;
			XMFLOAT4X4 g = Matrix4x4::Identity();
			XMStoreFloat4x4(&g, XMMatrixTranspose(GlobalTransformation));
			m_xmf4x4ChildWorld[pNode->mName.data] = g;
		}
	}

	for (UINT i = 0; i < pNode->mNumChildren; ++i) {
		//계층구조를 이룸. 자식노드 탐색 및 변환
		ReadNodeHeirarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	}
}

const aiNodeAnim* CSkinnedModelMesh::FindNodeAnim(const aiAnimation* pAnimation, const string& NodeName)
{
	for (UINT i = 0; i < pAnimation->mNumChannels; ++i) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];

		if (pNodeAnim->mNodeName.data == NodeName) {
			return pNodeAnim;
		}
	}
	return nullptr;
}

void CSkinnedModelMesh::CalcInterpolatedScaling(aiVector3D & Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	UINT ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	UINT NextScalingIndex = ScalingIndex + 1;

	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);

	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;

	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void CSkinnedModelMesh::CalcInterpolatedRotation(aiQuaternion & Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	UINT RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	UINT NextRotationIndex = (RotationIndex + 1);

	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);

	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;

	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}

void CSkinnedModelMesh::CalcInterpolatedPosition(aiVector3D & Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	UINT PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	UINT NextPositionIndex = (PositionIndex + 1);

	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);

	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;

	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

UINT CSkinnedModelMesh::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (UINT i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}
	return 0;
}

UINT CSkinnedModelMesh::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (UINT i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}
	return 0;
}

UINT CSkinnedModelMesh::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (UINT i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}
	return 0;
}
