#include "ModelShader.hlsl"

struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VS_TEXTURED_OUTPUT VSPostProcessing(uint nVertexID : SV_VertexID)
{
	VS_TEXTURED_OUTPUT output;
	if (nVertexID == 0) { output.position = float4(-1.0f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 0.f); }
	if (nVertexID == 1) { output.position = float4(-0.5f, +1.0f, 0.0f, 1.0f); output.uv = float2(1.f, 0.f); }
	if (nVertexID == 2) { output.position = float4(-0.5f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }
	if (nVertexID == 3) { output.position = float4(-1.0f, +1.0f, 0.0f, 1.0f); output.uv = float2(0.f, 0.f); }
	if (nVertexID == 4) { output.position = float4(-0.5f, +0.5f, 0.0f, 1.0f); output.uv = float2(1.f, 1.f); }
	if (nVertexID == 5) { output.position = float4(-1.0f, +0.5f, 0.0f, 1.0f); output.uv = float2(0.f, 1.f); }

	return output;
}

float4 PSPostProcessing(VS_TEXTURED_OUTPUT input) : SV_Target
{
	float4 cColor = float4(gtxtShadow.Sample(gSamplerState, input.uv).rrr, 1.0f);

	//return float4(gtxtShadow.Sample(gSamplerState, input.uv).rgb, 1.0f);
	return cColor;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////