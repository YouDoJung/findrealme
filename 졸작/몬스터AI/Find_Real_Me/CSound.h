#pragma once
#include "fmod_lowlevel/inc/fmod.hpp"
#pragma comment(lib, "fmod64_vc.lib")

class CSound
{
public:
	CSound();
	~CSound();

	static CSound* instance;
	//static CSound* sharedManager();
	void init();
	void loading();
	void play(int type);
	void stop(int chNum);
	void ErrorCheck(FMOD_RESULT _r);

private:
	FMOD::System* system = nullptr;
	FMOD::Sound* sound[4];
	FMOD::Channel* channel = nullptr;
	FMOD_RESULT result;
};

