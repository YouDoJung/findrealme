#pragma once
#include "FrameResource.h"

struct Bone
{
	XMMATRIX BoneOffset;
	XMMATRIX FinalTransformation;

	Bone() {
		BoneOffset = XMMatrixIdentity();
		FinalTransformation = XMMatrixIdentity();
	}
};

class AnimationLoarder
{
public:
	AnimationLoarder(string filename);
	~AnimationLoarder();

	//void BoneTransform(float fTime);

	//void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const XMMATRIX& ParentTransform);
	//const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const string& NodeName);

	//void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	//void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	//void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);

	//unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	//unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	//unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);

	//virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* pd3dCommandList, FrameResource* pFrameResource, float fAnimationtime);


private:
	const aiScene* m_pScene = nullptr;
	vector<pair<string, Bone>> m_vBones;		//�� ����
	UINT m_nBones = 0;
	FrameResource* m_pCurrResouce = nullptr;

	UINT m_SkinCBindex = -1;
};

