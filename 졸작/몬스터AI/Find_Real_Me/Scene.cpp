//-----------------------------------------------------------------------------
// File: CScene.cpp
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "Scene.h"

//ofstream out("lorAlt.txt");

CScene::CScene()
{

}

CScene::~CScene()
{
}

void CScene::BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList)
{
	CreateGraphicsRootSignature(pd3dDevice);

	m_pSkyBox = new CSkyBox(pd3dDevice, pd3dCommandList, m_pd3dGraphicsRootSignature.Get());

	XMFLOAT3 xmf3Scale(100.f, 2.0f, 100.f);
	XMFLOAT4 xmf4Color(0.0f, 0.5f, 0.0f, 0.0f);

	m_pTerrain = new CHeightMapTerrain(pd3dDevice, pd3dCommandList, m_pd3dGraphicsRootSignature.Get(), _T("../Image/Terrain/height_map.raw"), 100, 100, 100, 100, xmf3Scale, xmf4Color);
	
	m_pWaterShader = new CFlowingAnimationShader();
	m_pWaterShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get(), false);
	m_pWaterShader->BuildObjects(pd3dDevice, pd3dCommandList, m_pTerrain);

	m_pLavaShader = new CLavaShader();
	m_pLavaShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get(), false);
	m_pLavaShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);

	m_pSpurtShader = new CGeometryParticleShader();
	m_pSpurtShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get(), false);
	m_pSpurtShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);

	m_pHpShader = new CHpShader();
	m_pHpShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get(), false);
	m_pHpShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);
	
	m_pHpBackgroundShader = new CHpBackgroundShader();
	m_pHpBackgroundShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get(), false);
	m_pHpBackgroundShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);

	m_nShaders = 4;
	m_ppShaders = new CShader*[m_nShaders];

	// 마을
	CFBXVillageShader* pObjectShader = new CFBXVillageShader();
	pObjectShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get(), true);
	pObjectShader->BuildObjects(pd3dDevice, pd3dCommandList, m_pTerrain);
	m_ppShaders[0] = pObjectShader;

	// 탑 내부
	CFBXTopInsideShader* pTopShader = new CFBXTopInsideShader();
	pTopShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get());
	pTopShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);
	m_ppShaders[1] = pTopShader;

	// 일반던전
	CFBXDungeon* pDungeonShader = new CFBXDungeon();
	pDungeonShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get());
	pDungeonShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);
	m_ppShaders[2] = pDungeonShader;

	// 보스던전
	CFBXBossDungeon* pBossDungeonShader = new CFBXBossDungeon();
	pBossDungeonShader->CreateShader(pd3dDevice, m_pd3dGraphicsRootSignature.Get());
	pBossDungeonShader->BuildObjects(pd3dDevice, pd3dCommandList, nullptr);
	m_ppShaders[3] = pBossDungeonShader;
}

void CScene::ReleaseObjects()
{
	if (m_ppShaders)
	{
		for (int i = 0; i < m_nShaders; i++)
		{
			m_ppShaders[i]->ReleaseShaderVariables();
			m_ppShaders[i]->ReleaseObjects();
			m_ppShaders[i]->Release();
			m_ppShaders[i] = nullptr;
		}
		delete[] m_ppShaders;
		m_ppShaders = nullptr;
	}

	if (m_pSkyBox) {
		delete m_pSkyBox;
		m_pSkyBox = nullptr;
	}

	if (m_pTerrain) {
		delete m_pTerrain;
		m_pTerrain = nullptr;
	}

	if (m_pWaterShader) {
		m_pWaterShader->ReleaseObjects();
		m_pWaterShader->Release();
		m_pWaterShader = nullptr;
	}
	if (m_pLavaShader) {
		m_pLavaShader->ReleaseObjects();
		m_pLavaShader->Release();
		m_pLavaShader = nullptr;
	}
	if (m_pSpurtShader) {
		m_pSpurtShader->ReleaseObjects();
		m_pSpurtShader->Release();
		m_pSpurtShader = nullptr;
	}
	if (m_pHpShader) {
		m_pHpShader->ReleaseObjects();
		m_pHpShader->Release();
		m_pHpShader = nullptr;
	}
	if (m_pHpBackgroundShader) {
		m_pHpBackgroundShader->ReleaseObjects();
		m_pHpBackgroundShader->Release();
		m_pHpBackgroundShader = nullptr;
	}
}

void CScene::CreateGraphicsRootSignature(ID3D12Device* pd3dDevice)
{
	D3D12_DESCRIPTOR_RANGE pd3dDescriptorRanges[3];

	//pd3dDescriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	//pd3dDescriptorRanges[0].NumDescriptors = 1;
	//pd3dDescriptorRanges[0].BaseShaderRegister = 2; //All Objects
	//pd3dDescriptorRanges[0].RegisterSpace = 0;
	//pd3dDescriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	//pd3dDescriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	//pd3dDescriptorRanges[0].NumDescriptors = 2;
	//pd3dDescriptorRanges[0].BaseShaderRegister = 0; //t0~t1: gtxtCharactorTexture[2]
	//pd3dDescriptorRanges[0].RegisterSpace = 0;
	//pd3dDescriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[0].NumDescriptors = 67;
	pd3dDescriptorRanges[0].BaseShaderRegister = 0; //t0~t66: gtxtAllTextures[64]
	pd3dDescriptorRanges[0].RegisterSpace = 0;
	pd3dDescriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[1].NumDescriptors = 1;
	pd3dDescriptorRanges[1].BaseShaderRegister = 67; //t67: gtxtShadow;
	pd3dDescriptorRanges[1].RegisterSpace = 0;
	pd3dDescriptorRanges[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	pd3dDescriptorRanges[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	pd3dDescriptorRanges[2].NumDescriptors = 1;
	pd3dDescriptorRanges[2].BaseShaderRegister = 68; //t68: gtxtSkyBox;
	pd3dDescriptorRanges[2].RegisterSpace = 0;
	pd3dDescriptorRanges[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;


	D3D12_ROOT_PARAMETER pd3dRootParameters[6];
	//b0
	pd3dRootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[0].Descriptor.ShaderRegister = 0; //Camera
	pd3dRootParameters[0].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	//b1
	pd3dRootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[1].Descriptor.ShaderRegister = 1; //allObjects
	pd3dRootParameters[1].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	//b2
	pd3dRootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	pd3dRootParameters[2].Descriptor.ShaderRegister = 2; // finaltransforms
	pd3dRootParameters[2].Descriptor.RegisterSpace = 0;
	pd3dRootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	pd3dRootParameters[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[3].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[3].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[0];
	pd3dRootParameters[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	pd3dRootParameters[4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[4].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[4].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[1];
	pd3dRootParameters[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	
	pd3dRootParameters[5].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	pd3dRootParameters[5].DescriptorTable.NumDescriptorRanges = 1;
	pd3dRootParameters[5].DescriptorTable.pDescriptorRanges = &pd3dDescriptorRanges[2];
	pd3dRootParameters[5].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	D3D12_STATIC_SAMPLER_DESC d3dSamplerDesc[3];
	::ZeroMemory(d3dSamplerDesc, sizeof(D3D12_STATIC_SAMPLER_DESC));
	d3dSamplerDesc[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	d3dSamplerDesc[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	d3dSamplerDesc[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	d3dSamplerDesc[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	d3dSamplerDesc[0].MipLODBias = 0;
	d3dSamplerDesc[0].MaxAnisotropy = 1;
	d3dSamplerDesc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	d3dSamplerDesc[0].MinLOD = 0;
	d3dSamplerDesc[0].MaxLOD = D3D12_FLOAT32_MAX;
	d3dSamplerDesc[0].ShaderRegister = 0;
	d3dSamplerDesc[0].RegisterSpace = 0;
	d3dSamplerDesc[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	d3dSamplerDesc[1] = CD3DX12_STATIC_SAMPLER_DESC(1, // shaderRegister
		D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT, // filter
		D3D12_TEXTURE_ADDRESS_MODE_BORDER,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_BORDER,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_BORDER,  // addressW
		0.0f,                               // mipLODBias
		16,                                 // maxAnisotropy
		D3D12_COMPARISON_FUNC_LESS,
		D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK);

	d3dSamplerDesc[2] = CD3DX12_STATIC_SAMPLER_DESC(2, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP);  // addressW

	D3D12_ROOT_SIGNATURE_FLAGS d3dRootSignatureFlags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT | D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;
	D3D12_ROOT_SIGNATURE_DESC d3dRootSignatureDesc;
	::ZeroMemory(&d3dRootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
	d3dRootSignatureDesc.NumParameters = _countof(pd3dRootParameters);
	d3dRootSignatureDesc.pParameters = pd3dRootParameters;
	d3dRootSignatureDesc.NumStaticSamplers = 3;
	d3dRootSignatureDesc.pStaticSamplers = d3dSamplerDesc;
	d3dRootSignatureDesc.Flags = d3dRootSignatureFlags;

	ID3DBlob* pd3dSignatureBlob = nullptr;
	ID3DBlob* pd3dErrorBlob = nullptr;
	D3D12SerializeRootSignature(&d3dRootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &pd3dSignatureBlob, &pd3dErrorBlob);
	pd3dDevice->CreateRootSignature(0, pd3dSignatureBlob->GetBufferPointer(), pd3dSignatureBlob->GetBufferSize(), IID_PPV_ARGS(&m_pd3dGraphicsRootSignature));
	if (pd3dSignatureBlob) pd3dSignatureBlob->Release();
	if (pd3dErrorBlob) pd3dErrorBlob->Release();
}

bool CScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	return(false);
}

bool CScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_KEYUP:
		switch (wParam)
		{
		// 여기 있던거 프레임워크 cpp로 옮김
		case VK_CONTROL:
			//out << m_pPlayer->GetPosition().x << " " << m_pPlayer->GetPosition().y << " " << m_pPlayer->GetPosition().z << endl;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return(false);
	return(false);
}

bool CScene::ProcessInput(UCHAR* pKeysBuffer)
{
	return(false);
}

void CScene::AnimateObjects(float fTimeElapsed)
{
	if (m_eMaplist < m_nShaders && m_ppShaders[m_eMaplist]) {
		m_ppShaders[m_eMaplist]->AnimateObjects(fTimeElapsed);
	}

}

void CScene::UpdateObjects(FrameResource* pFrameResource, float fTime)
{
	if (m_pSkyBox && m_eMaplist == MAP_LIST::VILLAGE)
		m_pSkyBox->UpdateCB(pFrameResource);

	m_pTerrain->UpdateCB(pFrameResource);
	if (m_pWaterShader)
		m_pWaterShader->UpdateObjectsCB(pFrameResource, fTime);

	if (m_pLavaShader)
		m_pLavaShader->UpdateObjectsCB(pFrameResource, fTime);
	
	if (m_pSpurtShader)
		m_pSpurtShader->UpdateObjectsCB(pFrameResource, fTime);

	if (m_pHpShader)
		m_pHpShader->UpdateObjectsCB(pFrameResource, fTime);

	if (m_pHpBackgroundShader)
		m_pHpBackgroundShader->UpdateObjectsCB(pFrameResource, fTime);

	if (m_eMaplist < m_nShaders && m_ppShaders[m_eMaplist])
	{
		m_ppShaders[m_eMaplist]->UpdateObjectsCB(pFrameResource, fTime);
	}
}

void CScene::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	if (m_pSkyBox && m_eMaplist == MAP_LIST::VILLAGE) m_pSkyBox->Render(pd3dCommandList, pCamera, false);

	if (m_pTerrain && !m_eMaplist) m_pTerrain->Render(pd3dCommandList, pCamera, is_Shadow);
	
	if (!is_Shadow) {
		if (m_pWaterShader && MAP_LIST::VILLAGE == m_eMaplist)
			m_pWaterShader->Render(pd3dCommandList, pCamera, false);
		if (m_pLavaShader&& MAP_LIST::BOSS_DUNGEON == m_eMaplist)
			m_pLavaShader->Render(pd3dCommandList, pCamera, false);
		if (m_pSpurtShader&& MAP_LIST::BOSS_DUNGEON == m_eMaplist)
			m_pSpurtShader->Render(pd3dCommandList, pCamera, false);
		if (m_pHpShader&& MAP_LIST::그냥DUNGEON == m_eMaplist)
			m_pHpShader->Render(pd3dCommandList, pCamera, false);
		if (m_pHpBackgroundShader&& MAP_LIST::그냥DUNGEON == m_eMaplist)
			m_pHpBackgroundShader->Render(pd3dCommandList, pCamera, false);
	}

	if (m_eMaplist < m_nShaders && m_ppShaders[m_eMaplist])
	{
		m_ppShaders[m_eMaplist]->Render(pd3dCommandList, pCamera, is_Shadow);
	}
}