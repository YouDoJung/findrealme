#pragma once
#include "Object.h"

class CStaticModelObject : public CGameObject
{
public:
	CStaticModelObject();
	CStaticModelObject(int num) : CGameObject(num) {}
	virtual ~CStaticModelObject();
};

struct CombinedAnimationClip
{
	float m_fStartTime;
	float m_fEndTime;
	float m_fSpeed;

	CombinedAnimationClip() : m_fStartTime{ 0.f }, m_fEndTime{ 0.f }, m_fSpeed{ 0.f } {}
	CombinedAnimationClip(float start, float end, float speed) : m_fStartTime{ start }, m_fEndTime{ end }, m_fSpeed{ speed } {}
};

struct DivisionAnimationClip
{
	const aiScene* m_pAnimationModel;
	float m_fStartTime;
	float m_fEndTime;
	float m_fSpeed;
	bool m_isRoof;

	DivisionAnimationClip() : m_pAnimationModel{ nullptr }, m_fStartTime{ 0.f }, m_fEndTime{ 0.f }, m_fSpeed{ 0.f }, m_isRoof{ false } {}
	DivisionAnimationClip(const aiScene* pAnimationModel, float start, float end, float speed, bool is_roof)
		: m_pAnimationModel{ pAnimationModel }, m_fStartTime{ start }, m_fEndTime{ end }, m_fSpeed{ speed }, m_isRoof{ is_roof } {}

};

struct ModelData
{
	CSkinnedModelMesh* m_pParentMesh;
	vector<CSkinnedModelMesh*> m_vChildMesh;
	vector<string> m_vNames;
	unordered_map<string, DivisionAnimationClip> m_mDivisionAnimationClip;
	Assimp::Importer m_Importer;

	ModelData() : m_pParentMesh{ nullptr } {}
	void ModelLoadAndMeshLoad(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) {

		m_Importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
		const aiScene* pModel = m_Importer.ReadFile(filename.c_str(), (aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded) &
			~aiProcess_FindInvalidData |
			aiProcess_JoinIdenticalVertices |
			aiProcess_ValidateDataStructure |
			aiProcess_ImproveCacheLocality |
			aiProcess_RemoveRedundantMaterials |
			aiProcess_GenUVCoords |
			aiProcess_TransformUVCoords |
			aiProcess_LimitBoneWeights |
			aiProcess_OptimizeMeshes |
			aiProcess_GenSmoothNormals |
			aiProcess_SplitLargeMeshes |
			aiProcess_Triangulate |
			aiProcess_SortByPType);

		for (UINT i = 0; i < pModel->mNumMeshes; ++i) {
			if (pModel->mMeshes[i]->mNumBones > 20) {

				m_pParentMesh = new CSkinnedModelMesh(pd3dDevice, pd3dCommandList, pModel->mMeshes[i]);
			}
			else {
				CSkinnedModelMesh* pSkinnedModelMesh = new CSkinnedModelMesh(pd3dDevice, pd3dCommandList, pModel->mMeshes[i]);
				m_vChildMesh.emplace_back(pSkinnedModelMesh);
				m_vNames.emplace_back(pModel->mMeshes[i]->mName.data);
			}
		}
	}

	void AnimationLoad(const string& filename, const string& clipname, float speedweight, bool is_roof) {
		const aiScene* pModel = aiImportFile(filename.c_str(), (aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded) & ~aiProcess_FindInvalidData);

		if (!pModel)
			printf("Error parsing '%s': '%s'\n", filename.c_str(), m_Importer.GetErrorString());
		else {

			float start_time = (float)pModel->mAnimations[0]->mChannels[0]->mPositionKeys[0].mTime;
			float end_time = (float)pModel->mAnimations[0]->mChannels[0]->mPositionKeys[pModel->mAnimations[0]->mChannels[0]->mNumPositionKeys - 1].mTime - 0.001f;
			float speed = (float)pModel->mAnimations[0]->mTicksPerSecond * speedweight;

			m_mDivisionAnimationClip.insert(pair<string, DivisionAnimationClip>(clipname, DivisionAnimationClip(pModel, start_time, end_time, speed, is_roof)));
		}
	}
	~ModelData() {

		if (m_vChildMesh.size() > 0)
			m_vChildMesh.clear();
		if (m_vNames.size() > 0)
			m_vNames.clear();
	}
};
class CSkinnedObject : public CGameObject
{
public:
	CSkinnedObject();
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
	virtual ~CSkinnedObject() {}

protected:
	float m_fElapsedTime = 0.f;
};

class CSkinAndCombinedObject : public CSkinnedObject
{
public:
	CSkinAndCombinedObject();
	virtual ~CSkinAndCombinedObject();
	// 애니메이션들이 하나에 뭉쳐있는 경우.
	virtual void LoadModelAndInitCombinedAnimationClip(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void SetNowAnimation(string& s);
	void DragonAI(float fTimeElapsed);
	virtual void UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
protected:
	unordered_map<string, CombinedAnimationClip> m_mCombinedAnimationClip;
	const aiScene* m_pModel = nullptr;
	Assimp::Importer m_Importer;
	bool m_Routine = false;
};

class CBossDragonObject : public CSkinAndCombinedObject
{
public:
	CBossDragonObject();
	virtual ~CBossDragonObject();
	virtual void LoadModelAndInitCombinedAnimationClip(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
};

class CSkinAndDivisionObject : public CSkinnedObject
{
public:
	CSkinAndDivisionObject();
	virtual ~CSkinAndDivisionObject();

	virtual void UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow = false);
	virtual void SetNowAnimation(string& s);
	void SetAnimClip(unordered_map<string, DivisionAnimationClip>& clip);

	virtual void Animate(float fTimeElapsed);

protected:
	unordered_map<string, DivisionAnimationClip> m_mDivisionAnimationClip;
};