#pragma once
#include "GameTimer.h"
#include "Scene.h"
#include "Player.h"
#include "Camera.h"
#include "FrameResource.h"
#include "ShadowMap.h"
#include "CSound.h"

class GameFramework
{
public:
	GameFramework();
	~GameFramework();

	bool OnCreate(HINSTANCE hInstance, HWND hMainWnd);
	void OnDestroy();

	void CreateDirect3DDevice();
	void CreateCommandQueueAndList();
	void CreateSwapChain();

	void CreateRtvAndDsvDescriptorHeaps();
	void CreateRenderTargetViews();
	void CreateDepthStencilView();

	void OnResizeBackBuffers();

	void ModelLoad();
	void BuildMonsters();
	void BuildFrameResources();
	void BuildObjects();

	void MoveToTargetPlayer(unsigned short usID, float fUpdateTime);
	void Update();
	void ProcessInput();
	void AnimateObjects();
	
	void DrawSceneToShadowMap();
	void FrameAdvance();

	void FlushCommandQueue();
	void CalculateFrameStats();

	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

public:
	ID3D12Resource* GetCurrentBackBuffer() const;
	D3D12_CPU_DESCRIPTOR_HANDLE GetCurrentBackBufferView() const;
	D3D12_CPU_DESCRIPTOR_HANDLE GetDepthStencilView() const;

	float     AspectRatio() const;

private:
	HINSTANCE m_hAppInst = nullptr; // application instance handle
	HWND      m_hMainWnd = nullptr; // main window handle

	// Set true to use 4X MSAA (?.1.8).  The default is false.
	bool      m_is4xMsaaState = false;    // 4X MSAA enabled
	UINT      m_4xMsaaQuality = 0;      // quality level of 4X MSAA

	// Used to keep track of the �delta-time?and game time (?.4).
	GameTimer m_Timer;

	ComPtr<IDXGIFactory4> m_pDxgiFactory = nullptr;
	ComPtr<IDXGISwapChain> m_pSwapChain = nullptr;
	ComPtr<ID3D12Device> m_pD3dDevice = nullptr;

	ComPtr<ID3D12Fence> m_pFence = nullptr;
	UINT64 m_CurrentFence = 0;

	ComPtr<ID3D12CommandQueue> m_pCommandQueue = nullptr;
	ComPtr<ID3D12CommandAllocator> m_pDirectCmdListAlloc = nullptr;
	ComPtr<ID3D12GraphicsCommandList> m_pCommandList;

	static const int m_SwapChainBufferCount = 2;
	int m_CurrBackBuffer = 0;
	ComPtr<ID3D12Resource> m_pSwapChainBuffer[m_SwapChainBufferCount] = {};
	ComPtr<ID3D12Resource> m_pDepthStencilBuffer = nullptr;

	ComPtr<ID3D12DescriptorHeap> m_pRtvHeap = nullptr;
	ComPtr<ID3D12DescriptorHeap> m_pDsvHeap = nullptr;

	D3D12_VIEWPORT m_ScreenViewport = {};
	D3D12_RECT m_ScissorRect = {};

	UINT m_RtvDescriptorSize = 0;
	UINT m_DsvDescriptorSize = 0;
	UINT m_CbvSrvUavDescriptorSize = 0;

	// Derived class should set these in derived constructor to customize starting values.
	std::wstring m_MainWndCaption = L"FindRealMe";
	D3D_DRIVER_TYPE m_d3dDriverType = D3D_DRIVER_TYPE_HARDWARE;
	DXGI_FORMAT m_BackBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	DXGI_FORMAT m_DepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

private:
	int							m_WndClientWidth = 0;
	int							m_WndClientHeight = 0;
	POINT						m_OldCursorPos = {};

	CScene* m_pScene = nullptr;
	CPlayer* m_pPlayer = nullptr;
	CCamera* m_pCamera = nullptr;

	vector<unique_ptr<FrameResource>> mFrameResources;
	FrameResource* mCurrFrameResource = nullptr;
	int mCurrFrameResourceIndex = 0;

	UINT m_nTextures = 0;
	TextureManager* m_TextureMgr = nullptr;
	unordered_map<string, ModelData*> m_mModelData;

	unique_ptr<ShadowMap> mShadowMap;
	CPostProcessingShader* m_pTestRectShader = nullptr;

	int m_eMaplist = MAP_LIST::VILLAGE;

	int m_nNormalDungeonObjects = 0;
	int m_nBossDungeonObjects = 0;
	CShader* m_pSkinnedObjectShader = nullptr;
	CGameObject** m_ppMonsterObjects = nullptr;

	CBossDragonObject* m_pBoss = nullptr;

	// sound
	CSound* m_pSound = nullptr;
};

