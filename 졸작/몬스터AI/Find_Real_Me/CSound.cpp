#include "stdafx.h"
#include "CSound.h"
#include "fmod_lowlevel/inc/fmod_errors.h"

CSound::CSound()
{

}


CSound::~CSound()
{
}

//CSound* CSound::sharedManager()
//{
//	if (instance == nullptr) instance = new CSound;
//	return instance;
//}

void CSound::init()
{
	result = System_Create(&system);
	ErrorCheck(result);
	result = system->init(100, FMOD_INIT_NORMAL, nullptr);
	ErrorCheck(result);
}

void CSound::loading()
{
	result = system->createSound("../Sound/village.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::VILLAGE]);
	ErrorCheck(result);
	result = system->createSound("../Sound/top_inside.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::TOP_INSIDE]);
	ErrorCheck(result);
	result = system->createSound("../Sound/dungeon.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::�׳�DUNGEON]);
	ErrorCheck(result);
	result = system->createSound("../Sound/boss_dungeon.mp3", FMOD_LOOP_NORMAL, nullptr, &sound[MAP_LIST::BOSS_DUNGEON]);
	ErrorCheck(result);
}

void CSound::play(int type)
{
	system->update();
	result = system->playSound(sound[type], 0, false, &channel);
	ErrorCheck(result);
}

void CSound::stop(int chNum)
{
	channel->stop();
}

void CSound::ErrorCheck(FMOD_RESULT r)
{
	if (r != FMOD_OK) {
		TCHAR str[256] = { 0 };
		MultiByteToWideChar(CP_ACP, NULL, FMOD_ErrorString(result), -1, str, 256);
		MessageBox(NULL, str, L"Sound Error", MB_OK);
	}
}