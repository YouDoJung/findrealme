#include "Shaders.hlsl"

struct VS_TERRAIN_INPUT
{
	float3 position : POSITION;
	float4 color : COLOR;
	float2 uv0 : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
	float3 normal : NORMAL;
};

struct VS_TERRAIN_OUTPUT
{
	float3 positionW : POSITION0;
	float4 shadowPosition : POSITION1;
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 uv0 : TEXCOORD0;
	float2 uv1 : TEXCOORD1;
	float3 normal : NORMAL;
	
};

VS_TERRAIN_OUTPUT VSTerrain(VS_TERRAIN_INPUT input)
{
	VS_TERRAIN_OUTPUT output;
	output.positionW = mul(input.position, (float3x3)gmtxObjectWorld);
	output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
	output.color = input.color;
	output.uv0 = input.uv0;
	output.uv1 = input.uv1;
	output.normal = (float3)mul(float4(input.normal, 1.f), gmtxObjectWorld);
	output.shadowPosition = mul(float4(output.positionW, 1.f), gmtxShadowTransform);
	return(output);
}

float4 PSTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
	float4 cBaseTexColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].Sample(gSamplerState, input.uv0);
	float4 cDetailStoneTexColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.y)].Sample(gSamplerState, input.uv1);
	float4 diffuseAlbedo = saturate(cBaseTexColor * 0.5f + cDetailStoneTexColor * 0.5f);

	float3 shadowFactor = float3(1.0f, 1.0f, 1.0f);
	shadowFactor[0] = CalcShadowFactor(input.shadowPosition) * 2;

	input.normal = normalize(input.normal);
	float3 toEyeW = normalize(gvCameraPosition - input.positionW);
	float4 directLight = ComputeLighting(gLights, gMaterial, input.positionW, input.normal, toEyeW, shadowFactor);

	float4 ambient = gAmbientLight * diffuseAlbedo;
	float4 litColor = input.color * ambient + directLight;
	litColor.a = diffuseAlbedo.a;
	return litColor;
	//return lerp(cColor, directLight, 0.5f);
}