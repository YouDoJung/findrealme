#include "Shaders.hlsl"

struct VS_INPUT
{
	float3 posL			: POSITION;
	float2 sizeW		: SIZE;
};

struct VS_OUTPUT
{
	float3 centerW		: POSITION;
	float2 sizeW		: SIZE;
};

struct GS_OUTPUT
{
	float4 positionH	: SV_POSITION;
	float4 shadowPosition : POSITION0;
	float3 positionW	: POSITION1;
	float3 normalW		: NORMAL;
	float2 uv			: TEXCOORD;
	uint primID			: SV_PrimitiveID;
};

VS_OUTPUT VS_Hp(VS_INPUT input)
{
	VS_OUTPUT output;

	float3 newPos = input.posL;

	output.centerW = mul(float4(newPos, 1.0f), gmtxObjectWorld).xyz;
	output.sizeW = input.sizeW;

	return output;
}

[maxvertexcount(4)]
void GS_Hp(point VS_OUTPUT input[1], uint primID : SV_PrimitiveID, inout TriangleStream<GS_OUTPUT> outStream)
{
	float3 vUp = float3(0.0f, 1.0f, 0.0f);
	float3 vLook = gvCameraPosition.xyz - input[0].centerW;
	vLook = normalize(vLook);
	float3 vRight = cross(vUp, vLook);

	float fHalfW = input[0].sizeW.x * 0.5f;
	float fHalfH = input[0].sizeW.y * 0.5f;

	// 사각형 정점들을 월드변환행렬로 변환하고, 그것들을 하나의 삼각형으로 출력
	float4 pVertices[4];
	input[0].centerW += (gMaxHp / 2.f) * vRight;
	pVertices[0] = float4(input[0].centerW + 0 * vRight - fHalfH * vUp, 1.0f);
	pVertices[1] = float4(input[0].centerW + 0 * vRight + fHalfH * vUp, 1.0f);
	pVertices[2] = float4(input[0].centerW - gHp * vRight - fHalfH * vUp, 1.0f);
	pVertices[3] = float4(input[0].centerW - gHp * vRight + fHalfH * vUp, 1.0f);

	float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };

	GS_OUTPUT output;
	[unroll]
	for (int i = 0; i < 4; ++i)
	{
		output.positionW = pVertices[i].xyz;
		output.shadowPosition = mul(float4(output.positionW, 1.f), gmtxShadowTransform);
		output.positionH = mul(pVertices[i], gmtxViewProjection);
		output.normalW = vLook;
		output.uv = pUVs[i];
		output.primID = primID;
		outStream.Append(output);
	}
}

float4 PS_Hp(GS_OUTPUT input) : SV_Target
{
	/*
	float4 diffuseAlbedo = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);

	float4 ambient = gAmbientLight * diffuseAlbedo;
	float4 cColor = diffuseAlbedo;//lerp(diffuseAlbedo, Color2, 0.5f);//ambient + directLight;

	//cColor.a = diffuseAlbedo.a;
	float newTime = gfTotalTime * 200.f - gStartTime;
	newTime = fmod(newTime, 400.f);

	cColor.a = diffuseAlbedo.a - newTime / 400.f;
	//cColor.a -= gfTotalTime / 5;
	*/

	float4 cColor = float4(1.f, 0.f, 0.f, 1.f);

	return cColor;
}

[maxvertexcount(4)]
void GS_HpBackground(point VS_OUTPUT input[1], uint primID : SV_PrimitiveID, inout TriangleStream<GS_OUTPUT> outStream)
{
	float3 vUp = float3(0.0f, 1.0f, 0.0f);
	float3 vLook = gvCameraPosition.xyz - input[0].centerW;
	vLook = normalize(vLook);
	float3 vRight = cross(vUp, vLook);

	float fHalfH = input[0].sizeW.y * 0.5f;

	// 사각형 정점들을 월드변환행렬로 변환하고, 그것들을 하나의 삼각형으로 출력
	float4 pVertices[4];
	input[0].centerW += (gMaxHp / 2.f) * vRight - 1*vLook;
	pVertices[0] = float4(input[0].centerW + 0 * vRight - fHalfH * vUp, 1.0f);
	pVertices[1] = float4(input[0].centerW + 0 * vRight + fHalfH * vUp, 1.0f);
	pVertices[2] = float4(input[0].centerW - gMaxHp * vRight - fHalfH * vUp, 1.0f);
	pVertices[3] = float4(input[0].centerW - gMaxHp * vRight + fHalfH * vUp, 1.0f);

	float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };

	GS_OUTPUT output;
	[unroll]
	for (int i = 0; i < 4; ++i)
	{
		output.positionW = pVertices[i].xyz;
		output.shadowPosition = mul(float4(output.positionW, 1.f), gmtxShadowTransform);
		output.positionH = mul(pVertices[i], gmtxViewProjection);
		output.normalW = vLook;
		output.uv = pUVs[i];
		output.primID = primID;
		outStream.Append(output);
	}
}

float4 PS_HpBackground(GS_OUTPUT input) : SV_Target
{
	float4 cColor = float4(0.f, 0.f, 0.f, 1.f);

	return cColor;
}