Image, Model, Mapdata같은 경우 각 폴더마다 복사하면 너무 크므로
Common이라는 공유폴더에 넣어놨으니 거기서 빼내서 사용할 것.

Assimp폴더도 빼놨는데 클라이언트 stdafx.h에서 Assimp관련 헤더참조
경로를 ../Assimp...로 바꿔서 하고 코드있는 폴더에서 지워줄것.
Image, Model, Mapdata있는 곳에다가 Assimp폴더도 놓고 공유하면서 쓰도록 하자
