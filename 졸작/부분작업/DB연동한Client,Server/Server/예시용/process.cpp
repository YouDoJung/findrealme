#include "stdafx.h"
#include "process.h"
#include "map_village.h"
#include "map_boss.h"
#include "map_loby.h"
#include "map_dungeon.h"
#include "manager.h"

class CPlayerPool* CProcess::m_pPlayerPool = nullptr;
class CMonsterPool* CProcess::m_pMonsterPool = nullptr;
concurrency::concurrent_unordered_set<unsigned short>CProcess::m_ccusLoginList[4];
concurrency::concurrent_priority_queue<s_ObjectEvent, CEventOperator>CProcess::m_ccpqEventQueue;
std::function<bool(char&, unsigned short&, unsigned short&)>CProcess::m_fpAttackEvent[2][7];

CProcess::CProcess()
{
	BindPlayerAttackEvent();
}

CProcess::~CProcess()
{
	InitProcessData();
}

void CProcess::InitProcessData()
{
	for (int i = 0; i < 4; ++i) {
		SAFE_DELETE_CON_SET(CProcess::m_ccusLoginList[i]);
	}
	m_ccpqEventQueue.clear();
}

bool CProcess::CheckPortalByPlayerInVillage(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Village::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckPortalByPlayerInLoby(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Loby::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckPortalByPlayerInDungeon(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Dungeon::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckPortalByPlayerInBoss(unsigned short& usPlayerID)
{
	DirectX::BoundingOrientedBox player_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	return CMap_Boss::GetInstance()->CheckMapPortal(player_box);
}

bool CProcess::CheckCollisionManPlayerAttack1(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
		return true;
	}
	else return false;

	//광선 충돌 어케 해야하냐
	/*
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetPos();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetPos();
	DirectX::XMFLOAT3 monster_look = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetLookVector();

	DirectX::XMVECTOR origin = DirectX::XMLoadFloat3(&monster_pos);
	DirectX::XMVECTOR dir = DirectX::XMLoadFloat3(&monster_look);
	float range = PLAYER_ATTACK_RANGE;

	if (player.Intersects(origin, dir, range)) {
		std::cout << "충돌\n";
		return true;
	}
	else return false;*/
}

bool CProcess::CheckCollisionManPlayerAttack2(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerSkill1(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	//범위 설정
	player.Extents.x = 533.0f;
	player.Extents.z = 584.0f;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerSkill2(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	//범위 설정
	player.Extents.x = 334.0f;
	player.Extents.z = 467.0f;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionManPlayerSkill3(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill3, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(700));
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionManPlayerSkill4(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionManPlayerSkill5(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(300));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Man_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1100));
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionWoManPlayerAttack1(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1200));
		return true;
	}
	else return false;


	//광선 충돌 어케 해야하냐
	/*
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetPos();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetPos();
	DirectX::XMFLOAT3 monster_look = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetLookVector();

	DirectX::XMVECTOR origin = DirectX::XMLoadFloat3(&monster_pos);
	DirectX::XMVECTOR dir = DirectX::XMLoadFloat3(&monster_look);
	float range = PLAYER_ATTACK_RANGE;

	if (player.Intersects(origin, dir, range)) {
		std::cout << "충돌\n";
		return true;
	}
	else return false;*/
}

bool CProcess::CheckCollisionWoManPlayerAttack2(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(500));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_Woman_Attack2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill1(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill1, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill2(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	//범위 설정
	player.Extents.x = 500.0f;
	player.Extents.z = 500.0f;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill2, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill3(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리

	//범위 설정
	player.Extents.x = 528.0f;
	player.Extents.z = 300.0f;//100.0f

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill3, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
		return true;
	}
	else return false;
}


bool CProcess::CheckCollisionWoManPlayerSkill4(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill4, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1500));
		return true;
	}
	else return false;
}

bool CProcess::CheckCollisionWoManPlayerSkill5(char& cMap, unsigned short& usPlayerID, unsigned short& usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	monster.Center.y = 516.0f;
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	player.Extents.x = PLAYER_ATTACK_RANGE;
	player.Extents.z = PLAYER_ATTACK_RANGE;

	if (player.Intersects(monster)) {
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(300));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(600));
		CProcess::PostEvent(cMap, usMonsterID, usPlayerID, MONSTER_BY_PLAYER_EVENT, e_MonsterByPlayerEventType::e_By_WoMan_Skill5, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1100));
		return true;
	}
	else return false;
}