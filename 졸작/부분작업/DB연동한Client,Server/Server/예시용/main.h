#pragma once
#include "stdafx.h"
#include "network.h"
#include "init_map.h"
#include "data_base.h"





class CMainApp: public CTemplateSingleton<CMainApp>{
private:
	int m_iNumworkerthread;
	std::vector<std::shared_ptr<std::thread> > m_vWorkerThread;
	std::shared_ptr<std::thread> m_tAcceptthread;
	std::shared_ptr<std::thread> m_tUpdatethread;
	/*/////////////////////////////
		DataBase
		////////////////////////////*/
	std::shared_ptr<std::thread> m_tDataBasethread;
	
public:
	explicit CMainApp()
	{
		m_iNumworkerthread = 0;
		m_vWorkerThread.clear();
	};
	~CMainApp()
	{
		CInit_Map::DestoryInstance();
		CNetwork::DestoryInstance();
		/*/////////////////////////////
		DataBase
		////////////////////////////*/
		CData_Base::DestoryInstance();
	};

public:
	void InitApp()
	{
		CInit_Map::GetInstance();
		//std::cout << "WorkerThread Num : ";
		//std::cin >> m_iNumworkerthread;
		//잘못입력하면 그냥 평균 값으로.
		//if (m_iNumworkerthread <= 18) {

		//	// CPU 개수 확인용으로
		//	SYSTEM_INFO si;

		//	//시스템의 정보를 받아온다.
		//	GetSystemInfo(&si);

		//	m_iNumworkerthread = si.dwNumberOfProcessors * 2 + 2;
		//}
		SYSTEM_INFO si;
		GetSystemInfo(&si);

		m_iNumworkerthread = si.dwNumberOfProcessors - 3;
		//코어 수 * 2 - 3(AcceptThread, UpdateThread, DataBaseThread)

		std::cout <<"WorkerThread Num: "<< m_iNumworkerthread << std::endl;
		CNetwork::GetInstance();

		/*/////////////////////////////
		DataBase
		////////////////////////////*/
		CData_Base::GetInstance();
	}

	void StartApp()
	{
		/*/////////////////////////////
		DataBase
		////////////////////////////*/
		m_tDataBasethread = std::shared_ptr<std::thread>(new std::thread{ [&]() {CNetwork::GetInstance()->DataBaseThread(); } });
		std::cout << "DBThreadCreate" << std::endl;

		m_tAcceptthread = std::shared_ptr<std::thread>(new std::thread{ [&]() {CNetwork::GetInstance()->AcceptThread(); } });
		std::cout << "AcceptThreadCreate" << std::endl;
		
		m_tUpdatethread = std::shared_ptr<std::thread>(new std::thread{ [&]() {CNetwork::GetInstance()->UpdateThread(); } });
		std::cout << "UpdateThreadCreate" << std::endl;

		for (int i = 0; i < this->m_iNumworkerthread; ++i) {
			m_vWorkerThread.push_back(std::shared_ptr<std::thread>(new std::thread{ [&]() {CNetwork::GetInstance()->WorkerThread(); } }));
		}
		std::cout << "WorkerThreadCreate" << std::endl;
		std::cout << "Start!" << std::endl;
		
	}

	void CloseServer()
	{
		/*/////////////////////////////
		DataBase
		////////////////////////////*/
		m_tDataBasethread->join();
		m_tAcceptthread->join();
		m_tUpdatethread->join();
		for (auto& au : m_vWorkerThread) {
			au->join();
		}
		CNetwork::GetInstance()->EndServer();
		CNetwork::GetInstance()->Disconnect();
		/*/////////////////////////////
		DataBase
		////////////////////////////*/
		CData_Base::GetInstance()->DisConnectDataBase();
	}
};