#include "stdafx.h"
#include "network.h"


CNetwork::CNetwork()
{
	m_bRunningServer = true;
	this->Initialize();
}

CNetwork::~CNetwork()
{

}

void CNetwork::Initialize()
{
	CProcess::InitializeBeforeStart();

	m_pPlayerProcess = new CPlayerProcess();
	m_pMonsterProcess = new CMonsterProcess();

	CGameTimer::GetInstance()->Reset();
	CRandomCalculate::GetInstance();

	if (!InitWinSock()) {
		Err_display("InitWinSock() err");
	}

	if (!InitCompletionPort()) {
		Err_display("InitCompletionPort() err");
	}

	if (!InitSock()) {
		Err_display("InitSock() err");
	}
}

BOOL CNetwork::InitWinSock()
{
	// 윈속 초기화
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) return false;
	else return true;
}

BOOL CNetwork::InitCompletionPort()
{
	BOOL retval = CIocp::GetInstance()->InitCP();
	return retval;
}

BOOL CNetwork::InitSock()
{
	int retval;

	m_Listensock = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (m_Listensock == INVALID_SOCKET) {
		Err_quit("socket()");
		return false;
	}


	//////////////////////////////////////
	//TODO: 네이글
	//////////////////////////////////////
	int opt_val = true;
	retval = setsockopt(m_Listensock, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<const char*>(&opt_val), sizeof(opt_val));
	if (retval != 0) {
		Err_display("Err SetSockOpt()");
	}


	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(SERVER_PORT);
	retval = bind(m_Listensock, reinterpret_cast<SOCKADDR *>(&serveraddr), sizeof(serveraddr));
	if (retval == SOCKET_ERROR) {
		Err_quit("bind()");
		return false;
	}

	retval = listen(m_Listensock, SOMAXCONN);
	if (retval == SOCKET_ERROR) {
		Err_quit("listen()");
		return false;
	}

	return true;
}

void CNetwork::Disconnect()
{
	SAFE_DELETE(m_pPlayerProcess);
	closesocket(m_Listensock);
	CIocp::GetInstance()->ClosePort();
	CIocp::DestoryInstance();
	WSACleanup();
	m_Listensock = INVALID_SOCKET;
}

void CNetwork::WorkerThread()
{
	//x86이면 ULONGLONG id
	DWORD size;
	DWORD flag = 0;
	ULONGLONG key;
	int err_code;
	while (m_bRunningServer) {
		err_code = 0;
		//std::shared_ptr<s_Context>scontext;
		s_pContext scontext;
		BOOL retval = CIocp::GetInstance()->GetQCompleteState(&size, reinterpret_cast<ULONG_PTR*>(&key), reinterpret_cast<LPWSAOVERLAPPED*>(&scontext), &err_code);

		unsigned short id = static_cast<unsigned short>(key);

		//비정상 종료: FALSE, 수신 바이트 크기 = 0
		//정상 종료: TRUE, 수신 바이트 크기 = 0
		if (retval == false || size == 0) {
			err_code = WSAGetLastError();
			/*////////////////////////////////////////
			 여기로 오면 에러가 64일 가능성이 높다.
			즉 지정된 네트워크 이름을 사용할 수 없습니다. 이다
			*/////////////////////////////////////////
			if (err_code == 64) {
				Err_display("err_code 64, ERR GetQCompleteState()! Socket Close");
			}
			else {
				Err_display("ERR GetQCompleteState()! Socket Close");
			}
			m_pPlayerProcess->PlayerDisconnect(id);
			continue;
		}

		switch (scontext->m_bType)
		{
		case RECV_EVENT:
		{
			m_pPlayerProcess->Recv(id, size, scontext->m_cBuf);
			break;
		}
		case SEND_EVENT:
		{
			//if (size != scontext->m_sWsabuf.len) {
			//	Err_display("[WorkerThread]Send Error! Socket Close");
			//	//오류가 발생하면 왠만하면 소켓 종료를 하는게 좋다.
			//	m_pPlayerProcess->PlayerDisconnect(id);
			//}
			delete scontext;
			break;
		}
		case UPDATE_EVENT:
		{
			CGameTimer::GetInstance()->Tick();
			m_pMonsterProcess->UpdateMonster(scontext->m_cState, scontext->m_cMap, id, scontext->m_usOtherID, CGameTimer::GetInstance()->GetDeltaTime());
			delete scontext;
			break;
		}
		case PLAYER_UPDATE:
		{
			m_pPlayerProcess->PlayerUpdate(scontext->m_cState, id);
			delete scontext;
			break;
		}
		case MONSTER_BY_PLAYER_EVENT:
		{
			m_pMonsterProcess->UpdateMonsterByPlayer(scontext->m_cState, scontext->m_cMap, id, scontext->m_usOtherID);
			delete scontext;
			break;
		}
		case DATABASE_EVENT:
		{
			s_DataBaseEvent sample_event;
			if (CData_Base::GetInstance()->PopFromStateQueue(sample_event)) {
				switch (sample_event.state_)
				{
				case e_Data_Base_Save_State::e_LoginFail:
				{
					m_pPlayerProcess->PlayerLogInFail(sample_event.client_num_);
					break;
				}
				case e_Data_Base_Save_State::e_LoginSuccess:
				{
					m_pPlayerProcess->PlayerLogIn(sample_event);
					break;
				}
				default:
					break;
				}
			}
			delete scontext;
			break;
		}
		}
	}
	return;
}

void CNetwork::AcceptThread()
{
	while (m_bRunningServer){
		SOCKADDR_IN addr;
		ZeroMemory(&addr, sizeof(SOCKADDR_IN));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(SERVER_PORT);
		addr.sin_addr.s_addr = INADDR_ANY;
		int addrsize = sizeof(SOCKADDR_IN);

		auto client_sock = WSAAccept(m_Listensock, reinterpret_cast<sockaddr*>(&addr), &addrsize, NULL, NULL);

		if (client_sock == INVALID_SOCKET) {
			Err_display("ACCEPT INVALID_SOCKET!");
			break;
		}
		else {
			if (!CIocp::GetInstance()->CreateSockCP(client_sock, this->m_usUserID)) {
				//안에서 소켓 오류시 설정 해둠
				continue;
			}
			else {
				std::cout << "접속 : " << inet_ntoa(addr.sin_addr) << '\t' << ntohs(addr.sin_port) << std::endl;
				m_pPlayerProcess->AcceptClient(client_sock, m_usUserID);
				std::cout << "Usernum : " << this->m_usUserID << std::endl;
				++m_usUserID;
				continue;
			}
		}
	}
	return;
}

void CNetwork::DataBaseThread()
{
	while (m_bRunningServer) {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		while (!CData_Base::GetInstance()->DataBaseQueueIsEmpty()) {
			s_DataBaseEvent sample_event;
			if (CData_Base::GetInstance()->PopFromDataBaseQueue(sample_event)) {
				CData_Base::GetInstance()->RunDataBase(sample_event);
			}
			else break;
		}
	}
	return;
}

void CNetwork::UpdateThread()
{
	//일정 시간이 지날때마다 업데이트를 해주라는 함수 부분.
	if (CGameTimer::GetInstance()) {	//스타트.
		CGameTimer::GetInstance()->Start();
	}
	while (m_bRunningServer) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		while (!CProcess::StateEventQueue()) {
			s_ObjectEvent sample_event;
			if (CProcess::CheckEventStart(sample_event))
			{
				s_pContext sample = new s_Context;
				sample->m_bType = sample_event.m_cOverType;
				sample->m_cState = sample_event.m_cEventType;
				sample->m_cMap = sample_event.m_cMap;
				sample->m_usOtherID = sample_event.m_usOtherID;
				CIocp::GetInstance()->PostQCompleteState(sample_event.m_usID, sample);
			}
			else break;
		}
	}
	return;
}

// 소켓 함수 오류 출력 후 종료
void CNetwork::Err_quit(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBoxA(NULL, (LPCSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}

// 소켓 함수 오류 출력
void CNetwork::Err_display(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("[%s] %s", msg, (char *)lpMsgBuf);
	LocalFree(lpMsgBuf);
}