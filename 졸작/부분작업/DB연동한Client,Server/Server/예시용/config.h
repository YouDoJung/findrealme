#pragma once
#include "stdafx.h"

extern BOOL LoadConfig(TiXmlDocument* config)
{
	if (!config->LoadFile("./config.xml")) {
		printf("! not exist config file.\n");
		return false;
	}
	return true;
}