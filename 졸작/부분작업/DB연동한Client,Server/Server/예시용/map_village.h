#pragma once
#include "stdafx.h"
#include "staticobject.h"


class CMap_Village : public CTemplateSingleton<CMap_Village> {
private:
	int m_iNumObject = 0;
	std::multimap<std::string, DirectX::BoundingOrientedBox>m_mmObjectBoundingBox;
public:
	explicit CMap_Village();
	~CMap_Village();

	
public:
	bool CheckMapObjectCollision(DirectX::BoundingOrientedBox& ObjectOOBB);
	bool CheckMapPortal(DirectX::BoundingOrientedBox& ObjectOOBB);
};