#pragma once
#include "stdafx.h"
#include "process.h"
#include "map_village.h"
#include "map_boss.h"
#include "map_dungeon.h"
#include "map_loby.h"

class CPlayerProcess: public CProcess{
private:
	std::recursive_mutex m_rmPlayerProcessMutex;
	std::function<void(unsigned short)>m_fpUpdateViewList[4];
	std::function<void(unsigned short, UCHAR*)>m_fpPacketProcess[17];
public:
	explicit CPlayerProcess();
	virtual ~CPlayerProcess();
	
public:
	inline void BindPacketProcess()
	{
		m_fpPacketProcess[CS_LOOK] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return;  this->PlayerLook(usID, pPacket); };
		m_fpPacketProcess[CS_POS] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerPos(usID, pPacket); };
		m_fpPacketProcess[CS_ATTACK] = [&](unsigned short usID, UCHAR* pPacket) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerAttack(usID); };
		m_fpPacketProcess[CS_ATTACK2] = [&](unsigned short usID, UCHAR* pPacket) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerAttack2(usID); };
		m_fpPacketProcess[CS_SKILL1] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerSkill1(usID); };
		m_fpPacketProcess[CS_SKILL2] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerSkill2(usID); };
		m_fpPacketProcess[CS_SKILL3] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerSkill3(usID); };
		m_fpPacketProcess[CS_SKILL4] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerSkill4(usID); };
		m_fpPacketProcess[CS_SKILL5] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerSkill5(usID); };
		m_fpPacketProcess[CS_IDLE] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; this->PlayerIdle(usID); };

		m_fpPacketProcess[CS_MAP_SWAP_TO_VILLAGE] = [&](unsigned short usID, UCHAR* pPacket) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return;
			if (!CProcess::CheckPortalByPlayerInLoby(usID) && !CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetDevMapSwap())return;
			this->PlayerMapSwap(usID); this->PlayerAcceptMap(MAP_VILLAGE,usID);
		};

		m_fpPacketProcess[CS_MAP_SWAP_TO_LOBY] = [&](unsigned short usID, UCHAR* pPacket) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down)
				return; 
			if (!CProcess::CheckPortalByPlayerInVillage(usID) && !CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetDevMapSwap())return;
			this->PlayerMapSwap(usID); this->PlayerAcceptMap(MAP_LOBY, usID); 
		};

		m_fpPacketProcess[CS_MAP_SWAP_TO_DUNGEON] = [&](unsigned short usID, UCHAR* pPacket) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return;
			if (!CProcess::CheckPortalByPlayerInLoby(usID) && !CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetDevMapSwap())return;
			this->PlayerMapSwap(usID); this->PlayerAcceptMap(MAP_DUNGEON, usID);
		};
		m_fpPacketProcess[CS_MAP_SWAP_TO_BOSS] = [&](unsigned short usID, UCHAR* pPacket) { 
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return;
			if (!CProcess::CheckPortalByPlayerInDungeon(usID) && !CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetDevMapSwap())return;
			this->PlayerMapSwap(usID); this->PlayerAcceptMap(MAP_BOSS, usID);
		};
		m_fpPacketProcess[CS_LOGIN] = [&](unsigned short usID, UCHAR* pPacket) {  
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) return; 
			this->ClientLoginInput(usID, pPacket);
		};
		m_fpPacketProcess[CS_DEV_MAP_SWAP] = [&](unsigned short usID, UCHAR* pPacket) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetDevMapSwap();
		};
		m_fpPacketProcess[CS_DEV_CHARACTER] = [&](unsigned short usID, UCHAR* pPacket) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetInvincibility();
		};
	}
	inline void BindUpdateViewListFuntion()
	{
		m_fpUpdateViewList[MAP_VILLAGE] = [&](unsigned short usID) {this->UpdateViewListInVillage(usID); };
		m_fpUpdateViewList[MAP_LOBY] = [&](unsigned short usID) {this->UpdateViewListInLoby(usID); };
		m_fpUpdateViewList[MAP_DUNGEON] = [&](unsigned short usID) {this->UpdateViewListInDungeon(usID); };
		m_fpUpdateViewList[MAP_BOSS] = [&](unsigned short usID) {this->UpdateViewListInBoss(usID); };
	}
	void AcceptClient(const SOCKET& sSOCKET, unsigned short iID);
	void PacketProcess(unsigned short usID, UCHAR* ucPacket) { m_fpPacketProcess[ucPacket[1]](usID, ucPacket); }
	void Recv(unsigned short usID, DWORD dwSize, UCHAR* ucPacket);
public:
	void ClientLoginInput(unsigned short usID, UCHAR* ucPacket);

	void UpdateViewListInVillage(unsigned short usID);
	void UpdateViewListInLoby(unsigned short usID);
	void UpdateViewListInDungeon(unsigned short usID);
	void UpdateViewListInBoss(unsigned short usID);

	void PlayerDisconnect(unsigned short usID);
	void PlayerLogIn(s_DataBaseEvent& event);
	void PlayerLogInFail(unsigned short usID);

	void PlayerPos(unsigned short usID, UCHAR* ucPacket);

	void PlayerMapSwap(unsigned short usID);
	void PlayerAcceptMap(char cMap, unsigned short usID);

	void PlayerAttack(unsigned short usID);
	void PlayerAttack2(unsigned short usID);
	void PlayerSkill1(unsigned short usID);
	void PlayerSkill2(unsigned short usID);
	void PlayerSkill3(unsigned short usID);
	void PlayerSkill4(unsigned short usID);
	void PlayerSkill5(unsigned short usID);
	void PlayerIdle(unsigned short usID);

	void PlayerLook(unsigned short usID, UCHAR* ucPacket);

	void PlayerUpdate(char cState, unsigned short usID);
	void PlayerReSpawn(unsigned short usID);

	bool CalculateInteraction(unsigned short usID, char cType);

};