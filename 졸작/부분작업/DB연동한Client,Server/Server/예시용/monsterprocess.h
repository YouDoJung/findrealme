#pragma once
#include "stdafx.h"
#include "process.h"

class CMonsterProcess: public CProcess {
private:

	std::function<void(char, unsigned short, unsigned short, float)>m_fpMonsterUpdate[20];
	std::function<void(char, unsigned short, unsigned short)>m_fpMonsterByPlayerUpdate[14];
public:
	explicit CMonsterProcess();
	virtual ~CMonsterProcess();

public:
	//Inline은 알아서 판단해서 해준다고는 하지만 뭔가 이건 Inline을 명시적으로 해놓고서 쓰자.
	inline void BindMonsterUpdate()
	{
		m_fpMonsterUpdate[e_ObjectState::e_Object_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->AttackEvent(cMap, usID, usTarget); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Down] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->DownEvent(cMap, usID); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Follow] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->FollowEvent(cMap, usID, usTarget, fUpdateTime); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Idle] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->IdleEvent(cMap, usID); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_MoveToFirstPos] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->MoveFirstPosEvent(cMap, usID, usTarget, fUpdateTime); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Respawn] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->RespawnEvent(cMap, usID); };
	
		
		m_fpMonsterUpdate[e_BossState::e_Boss_Leg_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossLegAttackEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Tail_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossTailAttackEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Attack] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFlyAttackEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_FireBreath] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFireBreathEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Down] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossDownEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Follow] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFollowEvent(usTarget, fUpdateTime); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Idle] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossIdleEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_MoveToFirstPos] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossMoveFirstPosEvent(usTarget, fUpdateTime); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Respawn] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossRespawnEvent(); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFlyEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Follow] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFlyFollowEvent(usTarget, fUpdateTime); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Down] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFlyDownEvent(usTarget); };
		m_fpMonsterUpdate[e_BossState::e_Boss_Fly_Dead] = [&](char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) { this->BossFlyDeadEvent(); };
	}
	inline void BindMonsterDamaged()
	{
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Attack1] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManAttack1(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Attack2] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManAttack2(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill1] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManSkill1(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill2] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManSkill2(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill3] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManSkill3(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill4] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManSkill4(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Man_Skill5] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerManSkill5(cMap, usID, usTarget); };

		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Woman_Attack1] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManAttack1(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_Woman_Attack2] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManAttack2(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill1] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManSkill1(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill2] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManSkill2(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill3] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManSkill3(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill4] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManSkill4(cMap, usID, usTarget); };
		m_fpMonsterByPlayerUpdate[e_MonsterByPlayerEventType::e_By_WoMan_Skill5] = [&](char cMap, unsigned short usID, unsigned short usTarget) { this->ByPlayerWoManSkill5(cMap, usID, usTarget); };
	}
public:
	inline void UpdateMonster(char cState, char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime) {
		m_fpMonsterUpdate[cState](cMap, usID, usTarget, fUpdateTime); }
	inline void UpdateMonsterByPlayer(char cState, char cMap, unsigned short usID, unsigned short usTarget) {
		m_fpMonsterByPlayerUpdate[cState](cMap, usID, usTarget); }

	void CheckPlayerInRange(char cMap, concurrency::concurrent_unordered_set<unsigned short>& ccusLogin, concurrency::concurrent_unordered_set<unsigned short>& ccusList, unsigned short usID);

	void MoveToTargetPlayer(char cMap, unsigned short usTargetID, unsigned short usID, float fUpdateTime);
	void MoveToTargetPlayer(unsigned short usTargetID, float fUpdateTime);
	void MoveToFirstPos(char cMap, unsigned short usID, float fUpdateTime);

	void FollowEvent(char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime);
	void MoveFirstPosEvent(char cMap, unsigned short usID, unsigned short usTarget, float fUpdateTime);
	void IdleEvent(char cMap, unsigned short usID);
	void AttackEvent(char cMap, unsigned short usID, unsigned short usTarget);
	void DownEvent(char cMap, unsigned short usID);
	void RespawnEvent(char cMap, unsigned short usID);

	void BossFollowEvent(unsigned short usTarget, float fUpdateTime);
	void BossLegAttackEvent(unsigned short usTarget);
	void BossTailAttackEvent(unsigned short usTarget);
	void BossFlyAttackEvent(unsigned short usTarget);
	void BossFireBreathEvent(unsigned short usTarget);
	void BossIdleEvent();
	void BossMoveFirstPosEvent(unsigned short usTarget, float fUpdateTime);
	void BossDownEvent();
	void BossRespawnEvent();
	void BossFlyEvent(unsigned short usTarget);
	void BossFlyFollowEvent(unsigned short usTarget, float fUpdateTime);
	void BossFlyDownEvent(unsigned short usTarget);
	void BossFlyDeadEvent();

	void ByPlayerManAttack1(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerManAttack2(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerManSkill1(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerManSkill2(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerManSkill3(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerManSkill4(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerManSkill5(char cMap, unsigned short usID, unsigned short usTarget);

	void ByPlayerWoManAttack1(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerWoManAttack2(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerWoManSkill1(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerWoManSkill2(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerWoManSkill3(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerWoManSkill4(char cMap, unsigned short usID, unsigned short usTarget);
	void ByPlayerWoManSkill5(char cMap, unsigned short usID, unsigned short usTarget);

	void ByPlayerLevelUpEvent(char& cMap, unsigned short& usMonsterId, unsigned short& usPlayerId, int& iAfterHp);
	void BossByPlayerLevelUpEvent(unsigned short& usPlayerId, int& iAfterHp);

};