#include "stdafx.h"
#include "data_base.h"
#include "network.h"


CData_Base::CData_Base()
{
	if (!ConnectDataBase()) {
		printf("DataBaseConnect Fail.\n");
	}
	else {
		BindDataBaseFP();
	}
}

CData_Base::~CData_Base()
{
	DisConnectDataBase();
}

void CData_Base::HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];


	setlocale(LC_ALL, "korean");
	if (RetCode == SQL_INVALID_HANDLE) {
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}
	while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT *)NULL) == SQL_SUCCESS) {
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5)) {
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
}

void CData_Base::DisConnectDataBase()
{
	SQLCancel(hStmt_);
	SQLFreeHandle(SQL_HANDLE_STMT, hStmt_);
	SQLDisconnect(hDbc_);
}

bool CData_Base::ConnectDataBase()
{
	SQLRETURN ret;
	ret = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &hEnv_);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLSetEnvAttr(hEnv_, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);
		if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
			ret = SQLAllocHandle(SQL_HANDLE_DBC, hEnv_, &hDbc_);
			if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
				ret = SQLSetConnectAttr(hDbc_, SQL_LOGIN_TIMEOUT, (SQLPOINTER)(5), 0);
				ret = SQLConnect(hDbc_, (SQLWCHAR*)L"2019_Dev_SenierProject", SQL_NTS, (SQLWCHAR*)L"2019_Senier_User", SQL_NTS, (SQLWCHAR*)L"findrealme1234", SQL_NTS);
				if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
					ret = SQLAllocHandle(SQL_HANDLE_STMT, hDbc_, &hStmt_);
					return true;
				}
				else {
					HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
					return false;
				}
			}
			else {
				HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
				return false;
			}
		}
		else {
			HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
			return false;
		}
	}
	else {
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
		return false;
	}
	return false;
}

void CData_Base::RunDataBase(s_DataBaseEvent& event)
{
	fpDataBaseProcess[event.state_](event);
}

void CData_Base::LogInProcess(s_DataBaseEvent& event)
{
	char* sample_id;
	int for_str_len = WideCharToMultiByte(CP_ACP, 0, event.id_, -1, NULL, 0, NULL, NULL);
	sample_id = new char[for_str_len];
	WideCharToMultiByte(CP_ACP, 0, event.id_, -1, sample_id, for_str_len, 0, 0);

	if (GetIsLogin(sample_id)) {		//동일 아이디가 로그인 경우도 그냥 Fail로 -> 나중에 차등을 두던지 하고
		s_pContext sample = new s_Context;
		sample->m_bType = DATABASE_EVENT;
		sample->m_cState = e_Data_Base_Save_State::e_LoginFail;
		CIocp::GetInstance()->PostQCompleteState(event.client_num_, sample);
		return;
	}

	wchar_t querry_state[512];

	swprintf_s(querry_state, L"SELECT [PLAYER_NUMBER], [LEVEL], [EXP], [MONEY], [HP], [MP], [POSX], [POSY], [POSZ], [MAP], [CHARACTER] FROM [2019_DEV_SenierProject].[dbo].[USER_DATA] WHERE ID = '%s' AND PASSWORD = '%s'", event.id_, event.password_);

	//아이디 비밀번호는 쿼리해서 가져올 필요 없어짐.
	//SQLWCHAR sample_name[MAX_STR_LEN], sample_password[MAX_STR_LEN];
	SQLDOUBLE sample_pos_x = 5000.0f, sample_pos_y = 516.0f, sample_pos_z = 5000.0f;
	SQLINTEGER sample_player_number = 0,sample_exp = 0, sample_hp = 100, sample_mp = 100, sample_level = 1, sample_money = 0, sample_map = 0, sample_character = 0;
	SQLLEN /*b_name = 0, b_pass = 0,*/b_player_number = 0, b_x = 0, b_y = 0, b_z = 0, b_level = 0, b_exp = 0, b_hp = 0, b_mp = 0, b_money = 0, b_map = 0, b_character = 0;

	SQLRETURN ret;
	s_pContext sample = new s_Context;
	sample->m_bType = DATABASE_EVENT;

	ret = SQLExecDirect(hStmt_,(SQLWCHAR *)querry_state,SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		//ret = SQLBindCol(hStmt_, 1, SQL_C_CHAR, sample_name, MAX_STR_LEN, &b_name);
		//ret = SQLBindCol(hStmt_, 2, SQL_C_CHAR, sample_password, MAX_STR_LEN, &b_pass);
		ret = SQLBindCol(hStmt_, 1, SQL_C_LONG, &sample_player_number, 4, &b_player_number);
		ret = SQLBindCol(hStmt_, 2, SQL_C_LONG, &sample_level, 4, &b_level);
		ret = SQLBindCol(hStmt_, 3, SQL_C_LONG, &sample_exp, 4, &b_exp);
		ret = SQLBindCol(hStmt_, 4, SQL_C_LONG, &sample_money, 4, &b_money);
		ret = SQLBindCol(hStmt_, 5, SQL_C_LONG, &sample_hp, 4, &b_hp);
		ret = SQLBindCol(hStmt_, 6, SQL_C_LONG, &sample_mp, 4, &b_mp);
		ret = SQLBindCol(hStmt_, 7, SQL_C_DOUBLE, &sample_pos_x, 8, &b_x);
		ret = SQLBindCol(hStmt_, 8, SQL_C_DOUBLE, &sample_pos_y, 8, &b_y);
		ret = SQLBindCol(hStmt_, 9, SQL_C_DOUBLE, &sample_pos_z, 8, &b_z);
		ret = SQLBindCol(hStmt_, 10, SQL_C_LONG, &sample_map, 4, &b_map);
		ret = SQLBindCol(hStmt_, 11, SQL_C_LONG, &sample_character, 4, &b_character);
		ret = SQLFetch(hStmt_);
		if (ret == SQL_ERROR)
			printf("SQLFetch In UserLoginData error\n");
		if (ret == SQL_NO_DATA_FOUND || ret == SQL_NO_DATA) {
			s_DataBaseEvent sample_event{ e_Data_Base_Save_State::e_LoginFail,event.client_num_ };
			InsertToStateQueue(sample_event);
			CIocp::GetInstance()->PostQCompleteState(event.client_num_, sample);
			SQLCancel(hStmt_);
			return;
		}
		if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
			SetIsLogin(sample_player_number, event.client_num_, sample_id);
			//아이템 추가전 부분.
			/*s_DataBaseEvent sample_event{ e_Data_Base_Save_State::e_LoginSuccess,event.client_num_,sample_level,sample_exp,sample_hp, sample_mp, (float)sample_pos_x,(float)sample_pos_y,(float)sample_pos_z, (char)sample_map,(char)sample_character};
			InsertToStateQueue(sample_event);
			CIocp::GetInstance()->PostQCompleteState(event.client_num_, sample);*/
			SQLCancel(hStmt_);
		}
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
		return;
	}

	SQLINTEGER sample_item_number[3]{ 0,0,0 };/* = 0, sample_item_number1 = 0, sample_item_number2 = 0;*/
	SQLINTEGER sample_item_number_cnt[3]{ 0,0,0 }; /*= 0, sample_item_number1_cnt = 0, sample_item_number2_cnt = 0;*/
	SQLLEN b_item_number[3]{ 0,0,0 };/* = 0, b_item_number1 = 0, b_item_number2 = 0;*/
	SQLLEN b_item_number_cnt[3]{ 0,0,0 };/* = 0, b_item_number1_cnt = 0, b_item_number2_cnt = 0;*/

	swprintf_s(querry_state, L"SELECT [ITEM_NUMBER], [ITEM_CNT] FROM [2019_DEV_SenierProject].[dbo].[USER_ITEM_DATA] WHERE PLAYER_NUMBER = '%d'", sample_player_number);

	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		for (int i = 0; i < MAX_ITEM_NUM; i++) {
			ret = SQLBindCol(hStmt_, 1, SQL_C_LONG, &sample_item_number[i], 4, &b_item_number[i]);
			ret = SQLBindCol(hStmt_, 2, SQL_C_LONG, &sample_item_number_cnt[i], 4, &b_item_number_cnt[i]);
			ret = SQLFetch(hStmt_);
			if (ret == SQL_ERROR)
				printf("SQLFetch In UserItemData error\n");
		}
		SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}
	


	//플레이어 정보 - 소유 - 아이템 정보
	//아이템 정보 -> 아이템 번호, 아이템 이름
	SQLWCHAR sample_item0_name[MAX_STR_LEN], sample_item1_name[MAX_STR_LEN], sample_item2_name[MAX_STR_LEN];
	SQLLEN b_item0_name = 0, b_item1_name = 0, b_item2_name = 0;

	swprintf_s(querry_state, L"SELECT [ITEM_NAME] FROM [2019_DEV_SenierProject].[dbo].[ITEM_DATA] WHERE ITEM_NUMBER = '%d'", sample_item_number[0]);

	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLBindCol(hStmt_, 1, SQL_C_CHAR, sample_item0_name, MAX_STR_LEN, &b_item0_name);
		ret = SQLFetch(hStmt_);
		if (ret == SQL_ERROR) SQLCancel(hStmt_);
		if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}

	swprintf_s(querry_state, L"SELECT [ITEM_NAME] FROM [2019_DEV_SenierProject].[dbo].[ITEM_DATA] WHERE ITEM_NUMBER = '%d'", sample_item_number[1]);

	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLBindCol(hStmt_, 1, SQL_C_CHAR, sample_item1_name, MAX_STR_LEN, &b_item1_name);
		ret = SQLFetch(hStmt_);
		if (ret == SQL_ERROR) SQLCancel(hStmt_);
		if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}

	swprintf_s(querry_state, L"SELECT [ITEM_NAME] FROM [2019_DEV_SenierProject].[dbo].[ITEM_DATA] WHERE ITEM_NUMBER = '%d'", sample_item_number[2]);

	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLBindCol(hStmt_, 1, SQL_C_CHAR, sample_item2_name, MAX_STR_LEN, &b_item2_name);
		ret = SQLFetch(hStmt_);
		if (ret == SQL_ERROR) SQLCancel(hStmt_);
		if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}

	s_DataBaseEvent sample_event{ e_Data_Base_Save_State::e_LoginSuccess,event.client_num_,sample_level,sample_exp,sample_hp, sample_mp, (float)sample_pos_x,(float)sample_pos_y,(float)sample_pos_z, (char)sample_map,sample_money,(char)sample_character,(char)sample_item_number_cnt[0], (char)sample_item_number_cnt[1], (char)sample_item_number_cnt[2] };
	InsertToStateQueue(sample_event);
	CIocp::GetInstance()->PostQCompleteState(event.client_num_, sample);

}

void CData_Base::LogOutProcess(s_DataBaseEvent& event)
{
	wchar_t user_id[MAX_STR_LEN];
	wchar_t querry_state[512];

	int player_number = FindPlayerNumberForItemData(event.client_num_);

	FindIsLogin(event.client_num_, user_id, true);

	swprintf_s(querry_state, L"UPDATE [2019_DEV_SenierProject].[dbo].[USER_DATA] SET [LEVEL] = '%d', [EXP] = '%d', [MONEY] = '%d', [HP] = '%d', [MP] = '%d', [POSX] = '%f', [POSY] = '%f', [POSZ] = '%f', [MAP] = '%d', [CHARACTER] = '%d' WHERE ID =  '%s'", event.level_, event.exp_, event.money_, event.hp_, event.mp_, event.posx_, event.posy_, event.posz_, (int)event.map_, (int)event.charcter_ ,user_id);

	SQLRETURN ret;

	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		printf("Update LogOutState.\n");
		SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}

	if (player_number == NO_TARGET_PLAYER) return;

	std::string item_name{ "HPPORTION" };

	SQLINTEGER sample_item_number0 = 0, sample_item_number1 = 1, sample_item_number2 = 2;
	SQLLEN b_item_number0 = 0, b_item_number1 = 0, b_item_number2 = 0;

	/*swprintf_s(querry_state, L"SELECT [ITEM_NUMBER] FROM [2019_DEV_SenierProject].[dbo].[ITEM_DATA] WHERE ITEM_NAME = '%s'", item_name);
	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLBindCol(hStmt_, 1, SQL_C_LONG, &sample_item_number0, 4, &b_item_number0);
		for (int i = 0; ; i++) {
			ret = SQLFetch(hStmt_);
			if (ret == SQL_ERROR)
				printf("SQLFetch in ItemData error\n");
			if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
				SQLCancel(hStmt_);
			}
		}
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}*/

	swprintf_s(querry_state, L"UPDATE [2019_DEV_SenierProject].[dbo].[USER_ITEM_DATA] SET [ITEM_CNT] = '%d' WHERE PLAYER_NUMBER = '%d' AND ITEM_NUMBER = '%d'", (int)event.hp_portion_, player_number, sample_item_number0);
	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		printf("Update ItemData.\n");
		SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}

	/*item_name = std::string("MPPORTION");
	swprintf_s(querry_state, L"SELECT [ITEM_NUMBER] FROM [2019_DEV_SenierProject].[dbo].[ITEM_DATA] WHERE ITEM_NAME = '%s'", item_name);
	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLBindCol(hStmt_, 1, SQL_C_LONG, &sample_item_number1, 4, &b_item_number1);
		for (int i = 0; ; i++) {
			ret = SQLFetch(hStmt_);
			if (ret == SQL_ERROR)
				printf("SQLFetch in ItemData error\n");
			if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
				SQLCancel(hStmt_);
			}
		}
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}*/

	swprintf_s(querry_state, L"UPDATE [2019_DEV_SenierProject].[dbo].[USER_ITEM_DATA] SET [ITEM_CNT] = '%d' WHERE PLAYER_NUMBER = '%d' AND ITEM_NUMBER = '%d'", (int)event.mp_portion_, player_number, sample_item_number1);
	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		printf("Update ItemData.\n");
		SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}

	/*item_name = std::string("RETURN");
	swprintf_s(querry_state, L"SELECT [ITEM_NUMBER] FROM [2019_DEV_SenierProject].[dbo].[ITEM_DATA] WHERE ITEM_NAME = '%s'", item_name);
	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		ret = SQLBindCol(hStmt_, 1, SQL_C_LONG, &sample_item_number2, 4, &b_item_number2);
		for (int i = 0; ; i++) {
			ret = SQLFetch(hStmt_);
			if (ret == SQL_ERROR)
				printf("SQLFetch in ItemData error\n");
			if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
				SQLCancel(hStmt_);
			}
		}
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}*/

	swprintf_s(querry_state, L"UPDATE [2019_DEV_SenierProject].[dbo].[USER_ITEM_DATA] SET [ITEM_CNT] = '%d' WHERE PLAYER_NUMBER = '%d' AND ITEM_NUMBER = '%d'", (int)event.repatriation_, player_number, sample_item_number2);
	ret = SQLExecDirect(hStmt_, (SQLWCHAR *)querry_state, SQL_NTS);
	if (ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO) {
		printf("Update ItemData.\n");
		SQLCancel(hStmt_);
	}
	else {
		SQLCancel(hStmt_);
		HandleDiagnosticRecord(hStmt_, SQL_HANDLE_STMT, ret);
	}
}

void CData_Base::ItemGetProcess(s_DataBaseEvent& event)
{

}

void CData_Base::ItemGiveProcess(s_DataBaseEvent& event)
{

}

void CData_Base::ChangeStateProcess(s_DataBaseEvent& event)
{

}