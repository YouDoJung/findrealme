#pragma once
#include "stdafx.h"

class CStaticObject {
private:
	XMFLOAT4X4 m_xmf4x4World;
	BoundingOrientedBox m_xmOOBB;
	BoundingOrientedBox m_xmOOBBTransformed;
	XMFLOAT3 m_xmf3Rotkey = { 0.f, 0.f, 0.f };
public:
	explicit CStaticObject() {
		m_xmf4x4World = Matrix4x4::Identity();
	}
	~CStaticObject() {}

public:
	void Reset(){ m_xmf4x4World = Matrix4x4::Identity(); }
	void SetPos(XMFLOAT3& xmfPos)
	{
		m_xmf4x4World._41 = xmfPos.x;
		m_xmf4x4World._42 = xmfPos.y;
		m_xmf4x4World._43 = xmfPos.z;
	}
	void SetPos(float x, float y, float z) {
		m_xmf4x4World._41 = x;
		m_xmf4x4World._42 = y;
		m_xmf4x4World._43 = z;
	}
	DirectX::XMFLOAT3& GetPos() const{
		return DirectX::XMFLOAT3(m_xmf4x4World._41,
			m_xmf4x4World._42,
			m_xmf4x4World._43);
	}

	void SetOOBB(XMFLOAT3& xmCenter, XMFLOAT3& xmExtents, XMFLOAT4& xmOrientation) {
		m_xmOOBBTransformed = m_xmOOBB = BoundingOrientedBox(xmCenter, xmExtents, xmOrientation);
	}
	const DirectX::BoundingOrientedBox& GetOOBB() { return m_xmOOBB; }

	void SetWorld(XMFLOAT4X4 world) { m_xmf4x4World = world; }
	void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f) 
	{
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(fPitch), XMConvertToRadians(fYaw), XMConvertToRadians(fRoll));
		m_xmf4x4World = Matrix4x4::Multiply(mtxRotate, m_xmf4x4World);
		m_xmf3Rotkey = XMFLOAT3(fPitch, fYaw, fRoll);
	}

	void Animate()
	{
		m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
		XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
	}
};