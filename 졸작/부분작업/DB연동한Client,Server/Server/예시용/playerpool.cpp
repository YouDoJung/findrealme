#include "stdafx.h"
#include "playerpool.h"

concurrency::concurrent_unordered_map<unsigned short, class CPlayer*>CPlayerPool::m_ccumPlayerPool;

CPlayerPool::CPlayerPool()
{
	for (unsigned short i = 0; i < MAX_PLAYER; ++i) {
		CPlayerPool::m_ccumPlayerPool.insert(std::make_pair(i, new class CPlayer()));
	}
}

CPlayerPool::~CPlayerPool()
{ 
	SAFE_DELETE_MAP(CPlayerPool::m_ccumPlayerPool); 
}
