#pragma once
#include "stdafx.h"

//
//class CLogBase {
//public:
//	CLogBase() {}
//	virtual ~CLogBase() {}
//	virtual void Initialize() {}
//	virtual void UnInitialize() {}
//	virtual void log(WCHAR* wcLogStr) = 0;
//};
//
//class CLogPrintf : public CLogBase {
//public:
//	CLogPrintf();
//	void Log(WCHAR* wcLogStr);
//};
//
//class CLogFile :public CLogBase {
//private:
//	std::wfstream m_wfsFs;
//	std::wstring m_wsFileName;
//
//public:
//	CLogFile(TiXmlDocument* config);
//	virtual ~CLogFile();
//
//	void Initialize() {}
//	void Initialize(WCHAR* wcLogFileName);
//	void Log(WCHAR* wcLogStr);
//};
//
//class CLogWriter {
//private:
//	CLogBase* m_pBase;
//	std::wstring m_wsPrefix;
//
//public:
//	CLogWriter();
//	virtual ~CLogWriter();
//
//	void SetLogger(CLogBase* pBase, const WCHAR* wcLogPrefix);
//	CLogBase* m_pLogger();
//	void Log(WCHAR* pFmt, ...);
//	void Log(WCHAR* pFmt, va_list args);
//};
//typedef CLogWriter* pLogWriterPtr;
//
//class CSystemLog : public CSingleton<CSystemLog>
//{
//private:
//	CLogWriter m_cLogWrite;
//public:
//	CSystemLog();
//	virtual ~CSystemLog();
//
//	void Initialize(TiXmlDocument* config);
//	void Log (WCHAR* wcFmt);
//};