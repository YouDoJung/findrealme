#include "stdafx.h"
#include "monsterprocess.h"
#include "manager.h"
#include "map_dungeon.h"
#include "map_boss.h"

CMonsterProcess::CMonsterProcess()
{
	if (!CProcess::m_pMonsterPool) {
		CProcess::m_pMonsterPool = new class CMonsterPool();
		BindMonsterUpdate();
		BindMonsterDamaged();
	}
}

CMonsterProcess::~CMonsterProcess()
{
	if (m_pMonsterPool)
		SAFE_DELETE(m_pMonsterPool);
}

void CMonsterProcess::CheckPlayerInRange(char cMap, concurrency::concurrent_unordered_set<unsigned short>& ccusLogin, concurrency::concurrent_unordered_set<unsigned short>& ccusList, unsigned short usID)
{
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	for (auto& au : ccusLogin) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPlayerConnection();
		if (!is_connect) continue;
		//if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetState() == e_ObjectState::e_Object_Down)continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_RANGE)) {
			ccusList.insert(au);
		}
	}
}

void CMonsterProcess::MoveToTargetPlayer(unsigned short usTargetID, float fUpdateTime)
{
	s_MonsterEvent sample_event;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	float boss_y = xmf3Position.y;
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	float attack_detect_range = BOSS_DETECT_PLAYER_AND_GROUND_ATTACK;
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossIsFly()) {
		attack_detect_range = BOSS_DETECT_PLAYER_AND_AIR_ATTACK;
	}
	if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, attack_detect_range)) {
		DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
		float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
		float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
		DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
		float fNextRotate = 0.0f;
		if (fDotProduct > -0.95) {
			fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
			//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
			XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
			//몬스터를 회전시킨다.
			xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		}
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * BOSS_TARGET_MOVE_SPEED * fUpdateTime, boss_y, xmf4x4World._43 + xmf3ToTarget.z * BOSS_TARGET_MOVE_SPEED * fUpdateTime);

		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
	}

	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetEvent(sample_event)) {
		float monster_rotate;
		DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
		monster_rotate = sample_event.m_fRotate;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * BOSS_TARGET_MOVE_SPEED * fUpdateTime, boss_y, xmf4x4World._43 + xmf3ToTarget.z * BOSS_TARGET_MOVE_SPEED * fUpdateTime);
		xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetWorld(xmf4x4World);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->TransFormedOOBB();
		if (Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, attack_detect_range))
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
	}
}

void CMonsterProcess::MoveToTargetPlayer(char cMap, unsigned short usTargetID, unsigned short usID, float fUpdateTime)
{
	s_MonsterEvent sample_event;
	
	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
		DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
		float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
		float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
		DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
		float fNextRotate = 0.0f;
		if (fDotProduct > -0.95) {
			fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
			//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
			XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
			//몬스터를 회전시킨다.
			xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		}
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);

		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
		
	}

	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
		float monster_rotate;
		DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
		monster_rotate = sample_event.m_fRotate;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);
		xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->TransFormedOOBB();
	}
}

void CMonsterProcess::MoveToFirstPos(char cMap, unsigned short usID, float fUpdateTime)
{
	s_MonsterEvent sample_event;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();   // 플레이어 좌표

	if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, 10)) {
		DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
		float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
		float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
		DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
		float fNextRotate = 0.0f;
		if (fDotProduct > -0.95) {
			fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
			//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
			XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
			//몬스터를 회전시킨다.
			xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		}
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);

		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
	}

	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
		float monster_rotate;
		DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
		monster_rotate = sample_event.m_fRotate;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);
		xmf4x4World._41 = xmf3Position.x, xmf4x4World._42 = xmf3Position.y, xmf4x4World._43 = xmf3Position.z;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->TransFormedOOBB();
	}
}

void CMonsterProcess::FollowEvent(char cMap, unsigned short usID, unsigned short usTargetID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>before_login;
	concurrency::concurrent_unordered_set<unsigned short>after_login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	char this_map = cMap + 2;
	unsigned short this_monster_id = usID + MAX_PLAYER;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, before_login);
	CheckPlayerInRange(cMap, before_login, before, usID);

	s_SC_NPC_LOGIN_PACKET login_packet;
	s_SC_NPC_STATUS_PACKET status_packet;
	s_SC_NPC_POS_PACKET npc_pos_packet;
	s_SC_DISCONNECT_PACKET remove_packet;
	s_SC_ANIMATION_PACKET state_packet;

	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;

	npc_pos_packet.m_bSize = sizeof(s_SC_NPC_POS_PACKET);
	npc_pos_packet.m_bType = SC_NPC_POS;
	npc_pos_packet.m_usId = usID;

	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = usID;
	state_packet.m_bType = SC_NPC_FOLLOW;

	//쫓는 와중에 플레이어가 죽으면 어택과 동시에 일어나면 어택부분에서 NO_TARGET으로 바꿔준 상태라 이상한 곳을 참조하게 된다.
	//여기서 체크해주기.
	//그니깐 뭔가 동시성 떄문에? shared_mutex를 쓰니 이렇게 되는 거임 동시에 읽을 수는 있어서.

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();

	if (usTargetID == NO_TARGET_PLAYER || (usTargetID != NO_TARGET_PLAYER && CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	MoveToTargetPlayer(cMap, usTargetID, usID, fUpdateTime);

	CopyBefore(this_map, after_login);
	CheckPlayerInRange(cMap, after_login, after, usID);

	sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	status_packet.m_fX = npc_pos_packet.m_fX = sample_pos.x;
	status_packet.m_fZ = npc_pos_packet.m_fZ = sample_pos.z;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = npc_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLookVector();
	status_packet.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	for (auto& pu : before) {
		if (after.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->DeleteListElement(this_monster_id);
				CManager::SendEvent(pu, &remove_packet);
			}
		}
	}

	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		for (auto& pu : after) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (before.count(pu) == 0) {
				if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->InsertListElement(this_monster_id);
					CManager::SendEvent(pu, &login_packet);
					CManager::SendEvent(pu, &status_packet);
				}
			}
			else {
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
					CManager::SendEvent(pu, &npc_pos_packet);
				}
			}
			CManager::SendEvent(pu, &state_packet);
		}
		if (after.empty()) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
		}
		else {
			if (!Vector3::ObjectRangeCheck(sample_pos, first_pos, MONSTER_LIMIT_MOVE_RANGE)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else if (Vector3::ObjectRangeCheck(sample_pos, target_pos, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Attack);
				CProcess::PostEvent(cMap, usID, usTargetID, UPDATE_EVENT, e_ObjectState::e_Object_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else if (!Vector3::ObjectRangeCheck(sample_pos, target_pos, MONSTER_DETECT_PLAYER)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::PostEvent(cMap, usID, usTargetID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
		}
	}
}

void CMonsterProcess::IdleEvent(char cMap, unsigned short usID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	if (range_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
		return;
	}

	s_SC_ANIMATION_PACKET state_packet;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = usID;
	state_packet.m_bType = SC_NPC_IDLE;

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();

	unsigned short target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow();
	for (auto& pu : range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &state_packet);	//IDLE패킷을 먼저 보내야 죽었을때 동기화가 된다.
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, sample_pos, MONSTER_DETECT_PLAYER) && CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow() == NO_TARGET_PLAYER) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(pu);
			target_id = pu;
		}
	}

	if (target_id != NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Follow);
		CProcess::PostEvent(cMap, usID, target_id, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
	}
}

void CMonsterProcess::AttackEvent(char cMap, unsigned short usID, unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetHP();
	int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetAmr();
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = usID;

	s_SC_ANIMATION_PACKET player_state_packet;
	player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	s_SC_CHANGE_STATUS_PACKET player_status_packet;
	player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
	player_status_packet.m_usId = usTargetID;

	if (usTargetID == NO_TARGET_PLAYER || range_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
	}
	else {
		int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAtk();
		
		///////////////////////////////
		//수정할거
		int after_player_hp = player_hp - (monster_atk - player_amr);
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetInvincibility()) after_player_hp = 100;

		if (after_player_hp <= 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetState(e_ObjectState::e_Object_Down);

			player_state_packet.m_bType = SC_PLAYER_DOWN;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = 0;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(0);

			state_packet.m_bType = SC_NPC_IDLE;

			for (auto& pu : range_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_status_packet);
				CManager::SendEvent(pu, &player_state_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);

			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			//여기서 부활 큐 ㄱㄱ
			CProcess::PostEvent(cMap, usTargetID, NO_TARGET_PLAYER, PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));

		}
		else {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(after_player_hp);
			player_state_packet.m_bType = SC_PLAYER_DAMAGED;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = after_player_hp;

			state_packet.m_bType = SC_NPC_ATTACK;

			for (auto& pu : range_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_state_packet);
				CManager::SendEvent(pu, &player_status_packet);
			}

			if (Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Attack);
				CProcess::PostEvent(cMap, usID, usTargetID,UPDATE_EVENT, e_ObjectState::e_Object_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(800));
			}
			else if (!Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_DETECT_PLAYER)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				CProcess::PostEvent(cMap, usID, usTargetID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(30));
			}
		}
	}
}

void CMonsterProcess::DownEvent(char cMap, unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	s_SC_DISCONNECT_PACKET remove_packet;
	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	//시체가 사라지도록
	for (auto& player : range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(player, &remove_packet);
	}

	CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Respawn, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
}

void CMonsterProcess::RespawnEvent(char cMap, unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	if (range_list.empty())return;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(true);
	unsigned short this_monster_id = usID + MAX_PLAYER;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));

	s_SC_NPC_LOGIN_PACKET login_packet;
	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	s_SC_NPC_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;
	status_packet.m_fX = monster_pos.x;
	status_packet.m_fZ = monster_pos.z;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();
	status_packet.m_xmf3Dir = xmf3Look;

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;
	state_packet.m_usId = usID;

	for (auto& player: range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->ExistListElement(this_monster_id)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->InsertListElement(this_monster_id);
			CManager::SendEvent(player, &login_packet);
		}
		CManager::SendEvent(player, &status_packet);
		CManager::SendEvent(player, &state_packet);
	}

	CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
}

void CMonsterProcess::MoveFirstPosEvent(char cMap, unsigned short usID, unsigned short usTargetID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>before_login;
	concurrency::concurrent_unordered_set<unsigned short>after_login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	s_SC_NPC_LOGIN_PACKET login_packet;
	s_SC_NPC_STATUS_PACKET status_packet;
	s_SC_NPC_POS_PACKET npc_pos_packet;
	s_SC_DISCONNECT_PACKET remove_packet;
	s_SC_ANIMATION_PACKET state_packet;
	s_SC_CHANGE_STATUS_PACKET npc_hp_packet;

	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	npc_hp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	npc_hp_packet.m_bType = SC_CHANGE_STATUS_NPC_HP;
	npc_hp_packet.m_usId = usID;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;

	npc_pos_packet.m_bSize = sizeof(s_SC_NPC_POS_PACKET);
	npc_pos_packet.m_bType = SC_NPC_POS;
	npc_pos_packet.m_usId = usID;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_FOLLOW;
	state_packet.m_usId = usID;

	char this_map = cMap + 2;
	unsigned short this_monster_id = usID + MAX_PLAYER;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, before_login);
	CheckPlayerInRange(cMap, before_login, before, usID);

	MoveToFirstPos(cMap, usID, fUpdateTime);

	CopyBefore(this_map, after_login);
	CheckPlayerInRange(cMap, after_login, after, usID);

	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	int limit_monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();

	//돌아가는 중에 hp 리셋 시키기
	if (monster_hp < limit_monster_hp) {
		double percent_hp = (double)limit_monster_hp * 0.1;
		monster_hp += (int)percent_hp;
		if (monster_hp >= limit_monster_hp) {
			monster_hp = limit_monster_hp;
		}
	}

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();
	status_packet.m_fX = npc_pos_packet.m_fX = sample_pos.x;
	status_packet.m_fZ = npc_pos_packet.m_fZ = sample_pos.z;
	status_packet.m_iHP = npc_hp_packet.m_iAny = monster_hp;
	status_packet.m_iMaxHP = limit_monster_hp;
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = npc_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLookVector();

	for (auto& pu : after) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		if (before.count(pu) == 0) {
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->InsertListElement(this_monster_id);
				CManager::SendEvent(pu, &login_packet);
				CManager::SendEvent(pu, &status_packet);
			}
		}
		else {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CManager::SendEvent(pu, &npc_pos_packet);
				CManager::SendEvent(pu, &npc_hp_packet);
			}
		}
		CManager::SendEvent(pu, &state_packet);
	}

	for (auto& pu : before) {
		if (after.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(this_monster_id)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->DeleteListElement(this_monster_id);
				CManager::SendEvent(pu, &remove_packet);
			}
		}
	}

	if (after.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
	}
	else {
		if (Vector3::ObjectRangeCheck(sample_pos, first_pos, 10.0f)) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(limit_monster_hp);
			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(monster_hp);
			CProcess::PostEvent(cMap, usID, NO_TARGET_PLAYER,UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
}


void CMonsterProcess::BossFollowEvent(unsigned short usTargetID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	s_SC_BOSS_POS_PACKET boss_pos_packet;
	s_SC_ANIMATION_PACKET state_packet;

	boss_pos_packet.m_bSize = sizeof(s_SC_BOSS_POS_PACKET);
	boss_pos_packet.m_bType = SC_BOSS_POS;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;
	state_packet.m_bType = SC_BOSS_RUN;

	
	if (usTargetID == NO_TARGET_PLAYER || (usTargetID != NO_TARGET_PLAYER && CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	MoveToTargetPlayer(usTargetID,fUpdateTime);

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	boss_pos_packet.m_xmf3Pos = sample_pos;
	boss_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &boss_pos_packet);
		CManager::SendEvent(pu, &state_packet);
	}

	
	sample_pos.y = target_pos.y;
	if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_DETECT_PLAYER_AND_GROUND_ATTACK)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Leg_Attack);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Follow);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
}

void CMonsterProcess::BossIdleEvent()
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	s_SC_ANIMATION_PACKET state_packet;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;
	state_packet.m_bType = SC_NPC_IDLE;

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();

	unsigned short target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFollow();
	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &state_packet);	//IDLE패킷을 먼저 보내야 죽었을때 동기화가 된다.
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, sample_pos, MONSTER_DETECT_PLAYER) && CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFollow() == NO_TARGET_PLAYER) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(pu);
			target_id = pu;
		}
	}

	if (target_id != NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Follow);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, target_id,UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER,UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
	}
}

void CMonsterProcess::BossLegAttackEvent(unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;
	
	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER || login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetHP();
		int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetAmr();
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();


		DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
		DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

		s_SC_ANIMATION_PACKET state_packet;
		state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
		state_packet.m_usId = BOSS_ID;

		s_SC_ANIMATION_PACKET player_state_packet;
		player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

		s_SC_CHANGE_STATUS_PACKET player_status_packet;
		player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
		player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
		player_status_packet.m_usId = usTargetID;

		int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetAtk();

		/////////////////////////////////
		//수정할거
		int after_player_hp = player_hp - (monster_atk - player_amr);
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetInvincibility()) after_player_hp = 100;
		if (after_player_hp <= 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetState(e_ObjectState::e_Object_Down);

			player_state_packet.m_bType = SC_PLAYER_DOWN;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = 0;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(0);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &player_status_packet);
				CManager::SendEvent(pu, &player_state_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);

			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER,UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			//여기서 부활 큐 ㄱㄱ
			CProcess::PostEvent(MAP_BOSS, usTargetID, NO_TARGET_PLAYER,PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
		}
		else {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(after_player_hp);
			player_state_packet.m_bType = SC_PLAYER_DAMAGED;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = after_player_hp;

			state_packet.m_bType = SC_BOSS_LEG_ATTACK;

			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_state_packet);
				CManager::SendEvent(pu, &player_status_packet);
			}
			char boss_attack_cnt = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossAttackCnt();
			++boss_attack_cnt;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossAttackCnt(boss_attack_cnt);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Tail_Attack);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Tail_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
		}
	}
}

void CMonsterProcess::BossMoveFirstPosEvent(unsigned short usTargetID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	s_SC_BOSS_STATUS_PACKET status_packet;
	s_SC_ANIMATION_PACKET state_packet;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_BOSS_STATUS;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;

	unsigned short this_monster_id = BOSS_ID + MAX_PLAYER;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	MoveToFirstPos(BOSSMAP_MONSTER, BOSS_ID, fUpdateTime);

	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	int limit_monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP();
	bool is_fly = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossIsFly();

	if (is_fly) {
		state_packet.m_bType = SC_BOSS_FAST_FLY;
	}
	else {
		state_packet.m_bType = SC_BOSS_RUN;
	}
	//돌아가는 중에 hp 리셋 시키기
	if (monster_hp < limit_monster_hp) {
		double percent_hp = limit_monster_hp * 0.5;
		monster_hp += (int)percent_hp;
		if (monster_hp >= limit_monster_hp) {
			monster_hp = limit_monster_hp;
		}
	}

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFirstPos();
	status_packet.m_xmf3Pos = sample_pos;
	status_packet.m_iHP = monster_hp;
	status_packet.m_iMaxHP = limit_monster_hp;
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLevel();
	status_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();

	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &status_packet);
		CManager::SendEvent(pu, &state_packet);
	}

	if (Vector3::ObjectRangeCheck(sample_pos, first_pos, 10.0f)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(limit_monster_hp);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(monster_hp);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
}

void CMonsterProcess::BossTailAttackEvent(unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER || login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetHP();
		int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetAmr();
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();


		DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
		DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

		s_SC_ANIMATION_PACKET state_packet;
		state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
		state_packet.m_usId = BOSS_ID;

		s_SC_ANIMATION_PACKET player_state_packet;
		player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

		s_SC_CHANGE_STATUS_PACKET player_status_packet;
		player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
		player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
		player_status_packet.m_usId = usTargetID;

		int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetAtk();

		///////////////////////////////
		//수정할거
		int after_player_hp = player_hp - (monster_atk - player_amr);
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetInvincibility()) after_player_hp = 100;
		if (after_player_hp <= 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetState(e_ObjectState::e_Object_Down);

			player_state_packet.m_bType = SC_PLAYER_DOWN;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = 0;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(0);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &player_status_packet);
				CManager::SendEvent(pu, &player_state_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);

			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER,UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			//여기서 부활 큐 ㄱㄱ
			CProcess::PostEvent(MAP_BOSS, usTargetID, NO_TARGET_PLAYER,PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
		}
		else {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(after_player_hp);
			player_state_packet.m_bType = SC_PLAYER_DAMAGED;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = after_player_hp;

			state_packet.m_bType = SC_BOSS_TAIL_ATTACK;

			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_state_packet);
				CManager::SendEvent(pu, &player_status_packet);
			}
			char boss_attack_cnt = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossAttackCnt();
			if (boss_attack_cnt >= 3) {
				boss_attack_cnt = 0;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossAttackCnt(boss_attack_cnt);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
			}
			else {
				monster_pos.y = 516.0f;
				if (Vector3::ObjectRangeCheck(monster_pos, player_pos, BOSS_DETECT_PLAYER_AND_GROUND_ATTACK)) {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Leg_Attack);
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Leg_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
				}
				else {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Follow);
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
				}
			}
		}
	}
}

void CMonsterProcess::BossFlyEvent(unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossIsFly(true);
		DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
		float y_pos = boss_pos.y;

		s_SC_BOSS_FLY_PACKET boss_y_packet;
		boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
		boss_y_packet.m_bType = SC_BOSS_FLY;

		y_pos += 1.0f;
		boss_y_packet.m_fY = y_pos;


		for (auto& pu : login_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &boss_y_packet);
		}

		boss_pos.y = y_pos;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

		if (y_pos >= 1200.0f) {
			boss_pos.y = 1200.0f;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &boss_y_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Follow);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID,UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		}
		else {
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
}


void CMonsterProcess::BossFlyFollowEvent(unsigned short usTargetID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
		return;
	}

	s_SC_BOSS_POS_PACKET boss_pos_packet;
	s_SC_ANIMATION_PACKET state_packet;

	boss_pos_packet.m_bSize = sizeof(s_SC_BOSS_POS_PACKET);
	boss_pos_packet.m_bType = SC_BOSS_POS;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_usId = BOSS_ID;
	state_packet.m_bType = SC_BOSS_FAST_FLY;

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetFirstPos();

	if (usTargetID == NO_TARGET_PLAYER || (usTargetID != NO_TARGET_PLAYER && CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetState() == e_ObjectState::e_Object_Down)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER,UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	MoveToTargetPlayer(usTargetID, fUpdateTime);

	sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
	boss_pos_packet.m_xmf3Pos = sample_pos;
	boss_pos_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLookVector();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	for (auto& pu : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(pu, &boss_pos_packet);
		CManager::SendEvent(pu, &state_packet);
	}

	sample_pos.y = target_pos.y;
	if (Vector3::ObjectRangeCheck(sample_pos, target_pos, BOSS_DETECT_PLAYER_AND_AIR_ATTACK)) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Attack);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
}

void CMonsterProcess::BossFlyAttackEvent(unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER || login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetHP();
		int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetAmr();
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();


		DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
		DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

		s_SC_ANIMATION_PACKET state_packet;
		state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
		state_packet.m_usId = BOSS_ID;

		s_SC_ANIMATION_PACKET player_state_packet;
		player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

		s_SC_CHANGE_STATUS_PACKET player_status_packet;
		player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
		player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
		player_status_packet.m_usId = usTargetID;

		int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetAtk();
		///////////////////////////////
		//수정할거
		int after_player_hp = player_hp - (monster_atk - player_amr);
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetInvincibility()) after_player_hp = 100;
		if (after_player_hp <= 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetState(e_ObjectState::e_Object_Down);

			player_state_packet.m_bType = SC_PLAYER_DOWN;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = 0;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(0);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &player_status_packet);
				CManager::SendEvent(pu, &player_state_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);

			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			//여기서 부활 큐 ㄱㄱ
			CProcess::PostEvent(MAP_BOSS, usTargetID, NO_TARGET_PLAYER, PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
		}
		else {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(after_player_hp);
			player_state_packet.m_bType = SC_PLAYER_DAMAGED;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = after_player_hp;

			state_packet.m_bType = SC_BOSS_FLY_ATTACK;

			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_state_packet);
				CManager::SendEvent(pu, &player_status_packet);
			}
			char boss_attack_cnt = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossAttackCnt();
			++boss_attack_cnt;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_FireBreath);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossAttackCnt(boss_attack_cnt);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID,UPDATE_EVENT, e_BossState::e_Boss_FireBreath, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
		}
	}
}

void CMonsterProcess::BossFireBreathEvent(unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (usTargetID == NO_TARGET_PLAYER || login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetHP();
		int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetAmr();
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();

		DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
		DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

		s_SC_ANIMATION_PACKET state_packet;
		state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
		state_packet.m_usId = BOSS_ID;

		s_SC_ANIMATION_PACKET player_state_packet;
		player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

		s_SC_CHANGE_STATUS_PACKET player_status_packet;
		player_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
		player_status_packet.m_bType = SC_CHANGE_STATUS_PLAYER_HP;
		player_status_packet.m_usId = usTargetID;

		int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetAtk();

		///////////////////////////////
		//수정할거
		int after_player_hp = player_hp - (monster_atk - player_amr);
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetInvincibility()) after_player_hp = 100;
		if (after_player_hp <= 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetState(e_ObjectState::e_Object_Down);

			player_state_packet.m_bType = SC_PLAYER_DOWN;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = 0;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(0);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &player_status_packet);
				CManager::SendEvent(pu, &player_state_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetFollow(NO_TARGET_PLAYER);

			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			//여기서 부활 큐 ㄱㄱ
			CProcess::PostEvent(MAP_BOSS, usTargetID, NO_TARGET_PLAYER, PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
		}
		else {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->SetHP(after_player_hp);
			player_state_packet.m_bType = SC_PLAYER_DAMAGED;
			player_state_packet.m_usId = usTargetID;

			player_status_packet.m_iAny = after_player_hp;

			state_packet.m_bType = SC_BOSS_FIRE_BREATH;

			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &state_packet);
				CManager::SendEvent(pu, &player_state_packet);
				CManager::SendEvent(pu, &player_status_packet);
			}
			
			char boss_attack_cnt = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossAttackCnt();
			if (boss_attack_cnt >= 3) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Down);
				CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
				boss_attack_cnt = 0;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossAttackCnt(boss_attack_cnt);
			}
			else {
				monster_pos.y = 516.0f;
				if (Vector3::ObjectRangeCheck(monster_pos, player_pos, BOSS_DETECT_PLAYER_AND_AIR_ATTACK)) {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Attack);
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
				}
				else {
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Fly_Follow);
					CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
				}
			}
		}
	}
}

void CMonsterProcess::BossRespawnEvent()
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty())return;
	CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetWakeUp(true);
	unsigned short this_monster_id = BOSS_ID + MAX_PLAYER;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));

	s_SC_NPC_LOGIN_PACKET login_packet;
	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = BOSS_ID;

	s_SC_BOSS_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_BOSS_STATUS_PACKET);
	status_packet.m_bType = SC_BOSS_STATUS;
	status_packet.m_xmf3Pos = monster_pos;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLevel();
	status_packet.m_iMaxHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetLimitHP();
	status_packet.m_xmf3Dir = xmf3Look;

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;
	state_packet.m_usId = BOSS_ID;

	for (auto& player : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->ExistListElement(this_monster_id)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->InsertListElement(this_monster_id);
		}
		CManager::SendEvent(player, &login_packet);
		CManager::SendEvent(player, &status_packet);
		CManager::SendEvent(player, &state_packet);
	}

	CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
}

void CMonsterProcess::BossFlyDownEvent(unsigned short usTargetID)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossIsFly(false);
		DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
		float y_pos = boss_pos.y;

		s_SC_BOSS_FLY_PACKET boss_y_packet;
		boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
		boss_y_packet.m_bType = SC_BOSS_FLY_DOWN;

		y_pos -= 1.0f;
		boss_y_packet.m_fY = y_pos;

		for (auto& pu : login_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &boss_y_packet);
		}

		boss_pos.y = y_pos;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

		if (y_pos <= 900.0f) {
			boss_pos.y = 900.0f;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &boss_y_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Follow);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10));
		}
		else {
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, usTargetID, UPDATE_EVENT, e_BossState::e_Boss_Fly_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		
	}
}

void CMonsterProcess::BossDownEvent()
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;

	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(MAP_BOSS, login_list);

	s_SC_DISCONNECT_PACKET remove_packet;
	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = BOSS_ID;

	//시체가 사라지도록
	for (auto& player : login_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(player, &remove_packet);
	}

	CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Respawn, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));
}

void CMonsterProcess::BossFlyDeadEvent()
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetState() == e_BossState::e_Boss_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>login_list;

	CopyBefore(MAP_BOSS, login_list);

	if (login_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ReSpawn();
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetBossIsFly(false);
		DirectX::XMFLOAT3 boss_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetPos();
		float y_pos = boss_pos.y;

		s_SC_BOSS_FLY_PACKET boss_y_packet;
		boss_y_packet.m_bSize = sizeof(s_SC_BOSS_FLY_PACKET);
		boss_y_packet.m_bType = SC_BOSS_FLY_DOWN;

		y_pos -= 1.0f;
		boss_y_packet.m_fY = y_pos;

		for (auto& pu : login_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &boss_y_packet);
		}

		boss_pos.y = y_pos;
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);

		if (y_pos <= 750.0f) {
			boss_pos.y = 750.0f;
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetPos(boss_pos);
			for (auto& pu : login_list) {
				bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
				if (!is_connect) continue;
				CManager::SendEvent(pu, &boss_y_packet);
			}
			CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(e_BossState::e_Boss_Down);
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, e_BossState::e_Boss_Fly_Dead, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}

	}
}

void CMonsterProcess::ByPlayerManAttack1(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerManAttack2(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerManSkill1(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerManSkill2(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerManSkill3(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
		//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerManSkill4(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerManSkill5(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}


void CMonsterProcess::ByPlayerWoManAttack1(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerWoManAttack2(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerWoManSkill1(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerWoManSkill2(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerWoManSkill3(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetAtk();
	int after_monster_hp = monster_hp - (player_atk - monster_amr);
	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usTarget]->GetInvincibility()) after_monster_hp = 0;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerWoManSkill4(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	//after_monster_hp = monster_hp - (player_atk - monster_amr);
	int after_monster_hp = monster_hp - 20;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerWoManSkill5(char cMap, unsigned short usID, unsigned short usTarget)
{
	int monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAmr();
	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();

	////////////////////////////////
	//수정할거
	//after_monster_hp = monster_hp - (player_atk - monster_amr);
	int after_monster_hp = monster_hp - 20;

	if (usID == BOSS_ID) {
		BossByPlayerLevelUpEvent(usTarget, after_monster_hp);
	}
	else ByPlayerLevelUpEvent(cMap, usID, usTarget, after_monster_hp);
}

void CMonsterProcess::ByPlayerLevelUpEvent(char& cMap, unsigned short& usMonsterId, unsigned short& usPlayerId, int& iAfterHp)
{
	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->CopyPlayerList(player_list);

	s_SC_ANIMATION_PACKET monster_state_packet;
	monster_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	s_SC_CHANGE_STATUS_PACKET npc_status_packet;
	npc_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	npc_status_packet.m_bType = SC_CHANGE_STATUS_NPC_HP;

	//NPC HP변화등
	monster_state_packet.m_usId = npc_status_packet.m_usId = usMonsterId;
	npc_status_packet.m_iAny = iAfterHp;

	if (iAfterHp <= 0) {
		int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetLevel();
		int monster_money = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->GetMoney();
		int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->GetExp();
		int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetMoney();
		int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetExp();

		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->SetState(e_ObjectState::e_Object_Down);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->SetHP(0);

		monster_state_packet.m_bType = SC_NPC_DOWN;

		npc_status_packet.m_iAny = 0;

		int after_exp = player_exp + monster_exp;

		if (after_exp >= player_level * 1000) {
			player_money += monster_money;

			s_SC_LEVEL_UP_PACKET level_up_packet;
			level_up_packet.m_bSize = sizeof(s_SC_LEVEL_UP_PACKET);
			level_up_packet.m_bType = SC_CHANGE_LEVEL_UP;

			s_SC_CHANGE_STATUS_PACKET player_money_packet;
			player_money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
			player_money_packet.m_usId = usPlayerId;
			player_money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
			player_money_packet.m_iAny = player_money;

			after_exp -= (player_level * 1000);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			player_level += 1;
			int limit_stat = player_level * 1000;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitMP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitEXP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLevel(player_level);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetMP(limit_stat);

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAtk(player_level * 10);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAmr(player_level * 10);

			level_up_packet.m_usId = usPlayerId;
			level_up_packet.m_iExp = after_exp;
			level_up_packet.m_iHP = limit_stat;
			level_up_packet.m_iMP = limit_stat;
			level_up_packet.m_iLevel = player_level;
			level_up_packet.m_iMaxEXP = limit_stat;
			level_up_packet.m_iMaxHP = limit_stat;
			level_up_packet.m_iMaxMP = limit_stat;

			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &level_up_packet);
				CManager::SendEvent(player, &player_money_packet);
			}
		}
		else {
			s_SC_CHANGE_STATUS_PACKET player_exp_packet;
			player_exp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
			player_exp_packet.m_usId = usPlayerId;
			player_exp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_EXP;
			player_exp_packet.m_iAny = after_exp;

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &player_exp_packet);
			}
		}
		CProcess::PostEvent(cMap, usMonsterId, NO_TARGET_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1300));
	}
	else {
		monster_state_packet.m_bType = SC_NPC_DAMAGED;
		for (auto& player : player_list) {
			CManager::SendEvent(player, &monster_state_packet);
			CManager::SendEvent(player, &npc_status_packet);
		}
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterId]->SetHP(iAfterHp);
	}
}


void CMonsterProcess::BossByPlayerLevelUpEvent(unsigned short& usPlayerId, int& iAfterHp)
{
	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->CopyPlayerList(player_list);

	s_SC_ANIMATION_PACKET monster_state_packet;
	monster_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	s_SC_CHANGE_STATUS_PACKET npc_status_packet;
	npc_status_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
	npc_status_packet.m_bType = SC_CHANGE_STATUS_NPC_HP;

	//NPC HP변화등
	monster_state_packet.m_usId = npc_status_packet.m_usId = BOSS_ID;
	npc_status_packet.m_iAny = iAfterHp;

	if (iAfterHp <= 0) {
		int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetLevel();
		int monster_money = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetMoney();
		int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetExp();
		int player_money = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetMoney();
		int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->GetExp();

		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetHP(0);

		int next_event_time = 1300;
		char boss_state = e_BossState::e_Boss_Down;
		monster_state_packet.m_bType = SC_NPC_DOWN;
		if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->GetBossIsFly()) {
			monster_state_packet.m_bType = SC_BOSS_FLY_DEAD;
			boss_state = e_BossState::e_Boss_Fly_Dead;
			next_event_time = 800;
		}
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][BOSS_ID]->SetState(boss_state);

		npc_status_packet.m_iAny = 0;

		int after_exp = player_exp + monster_exp;

		if (after_exp >= player_level * 1000) {
			player_money += monster_money;

			s_SC_LEVEL_UP_PACKET level_up_packet;
			level_up_packet.m_bSize = sizeof(s_SC_LEVEL_UP_PACKET);
			level_up_packet.m_bType = SC_CHANGE_LEVEL_UP;

			s_SC_CHANGE_STATUS_PACKET player_money_packet;
			player_money_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
			player_money_packet.m_usId = usPlayerId;
			player_money_packet.m_bType = SC_CHANGE_STATUS_PLAYER_MONEY;
			player_money_packet.m_iAny = player_money;

			after_exp -= (player_level * 1000);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			player_level += 1;
			int limit_stat = player_level * 1000;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitMP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLimitEXP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetLevel(player_level);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetHP(limit_stat);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetMP(limit_stat);

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAtk(player_level * 10);
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetAmr(player_level * 10);

			level_up_packet.m_usId = usPlayerId;
			level_up_packet.m_iExp = after_exp;
			level_up_packet.m_iHP = limit_stat;
			level_up_packet.m_iMP = limit_stat;
			level_up_packet.m_iLevel = player_level;
			level_up_packet.m_iMaxEXP = limit_stat;
			level_up_packet.m_iMaxHP = limit_stat;
			level_up_packet.m_iMaxMP = limit_stat;

			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &level_up_packet);
				CManager::SendEvent(player, &player_money_packet);
			}
		}
		else {
			s_SC_CHANGE_STATUS_PACKET player_exp_packet;
			player_exp_packet.m_bSize = sizeof(s_SC_CHANGE_STATUS_PACKET);
			player_exp_packet.m_usId = usPlayerId;
			player_exp_packet.m_bType = SC_CHANGE_STATUS_PLAYER_EXP;
			player_exp_packet.m_iAny = after_exp;

			CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerId]->SetExp(after_exp);
			for (auto& player : player_list) {
				CManager::SendEvent(player, &monster_state_packet);
				CManager::SendEvent(player, &npc_status_packet);
				CManager::SendEvent(player, &player_exp_packet);
			}
		}
		CProcess::PostEvent(MAP_BOSS, BOSS_ID, NO_TARGET_PLAYER, UPDATE_EVENT, boss_state, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(next_event_time));
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSS_ID][MAP_BOSS]->SetHP(iAfterHp);
	}
}
