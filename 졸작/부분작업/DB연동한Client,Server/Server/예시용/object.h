#pragma once
#include "stdafx.h"

constexpr char g_cObjectLockNum = 17;


class CObject {
private:
	const enum e_LOCK_TYPE {
		HP_LOCK, MP_LOCK, ATK_LOCK, AMR_LOCK, STATE_LOCK, ANITIME_LOCK, MAP_LOCK, OOBB_LOCK, TRANS_OOBB_LOCK, WORLD_LOCK, LIMIT_HP_LOCK, LIMIT_MP_LOCK, FIRST_POS_LOCK, LEVEL_LOCK, EXP_LOCK, MONEY_LOCK, LIMIT_EXP_LOCK
	};

	s_Object m_sObjectdata;

	std::shared_mutex m_mObjectmutex[g_cObjectLockNum];
	
	DirectX::BoundingOrientedBox m_xmOOBB;
	DirectX::BoundingOrientedBox m_xmTransOOBB;
public:
	explicit CObject()
	{
		this->InitializeData();
	};
	virtual ~CObject()
	{
	}

public:
	void SetWorld(const DirectX::XMFLOAT4X4& xmf4World) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]); m_sObjectdata.m_xmf4World = xmf4World; }
	void SetFirstPos(const DirectX::XMFLOAT3& xmf3Pos) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[FIRST_POS_LOCK]); m_sObjectdata.m_xmf3FirstPos = xmf3Pos; }
	void SetOOBBExtern(const DirectX::XMFLOAT3& xmfExternPos) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[OOBB_LOCK]); m_xmOOBB.Extents = xmfExternPos; }
	void SetTransOOBBExtern(const DirectX::XMFLOAT3& xmfExternPos) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[TRANS_OOBB_LOCK]); m_xmTransOOBB.Extents = xmfExternPos; }
	void SetOOBB(const DirectX::XMFLOAT3& xmf3CenterPos) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[OOBB_LOCK]); m_xmOOBB.Center = xmf3CenterPos; }
	void SetTransOOBB(const DirectX::XMFLOAT3& xmf3CenterPos) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[TRANS_OOBB_LOCK]); m_xmTransOOBB.Center = xmf3CenterPos; }

	void TransFormedOOBB() 
	{ 
		std::unique_lock<std::shared_mutex>oobb_lock(m_mObjectmutex[OOBB_LOCK]);
		std::unique_lock<std::shared_mutex>trans_oobb_lock(m_mObjectmutex[TRANS_OOBB_LOCK]);
		m_xmTransOOBB.Transform(m_xmOOBB, XMLoadFloat4x4(&GetWorld()));
		DirectX::XMStoreFloat4(&m_xmTransOOBB.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmTransOOBB.Orientation)));
	}
	void SetIsPlayer(char WHAT)
	{
		m_sObjectdata.m_eObjectType = WHAT;
	}
	void SetHP(int HP)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[HP_LOCK]);
		m_sObjectdata.m_iHP = HP;
	}
	void SetMP(int MP)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[MP_LOCK]);
		m_sObjectdata.m_iMP = MP;
	}
	void SetLimitHP(int HP)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[LIMIT_HP_LOCK]);
		m_sObjectdata.m_iLimitHP = HP;
	}
	void SetLimitMP(int MP)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[LIMIT_MP_LOCK]);
		m_sObjectdata.m_iLimitMP = MP;
	}
	void SetLimitEXP(int exp)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[LIMIT_EXP_LOCK]);
		m_sObjectdata.m_iLimitExp = exp;
	}
	void SetLevel(int level)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[LEVEL_LOCK]);
		m_sObjectdata.m_iLevel = level;
	}
	void SetMoney(int money)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[MONEY_LOCK]);
		m_sObjectdata.m_iMoney = money;
	}
	void SetExp(int exp)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[EXP_LOCK]);
		m_sObjectdata.m_iExp = exp;
	}
	void SetPos(const DirectX::XMFLOAT3& POS)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]);
		m_sObjectdata.m_xmf4World._41 = POS.x;
		m_sObjectdata.m_xmf4World._42 = POS.y;
		m_sObjectdata.m_xmf4World._43 = POS.z;
	}
	void SetAtk(int ATK)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[ATK_LOCK]);
		m_sObjectdata.m_iATK = ATK;
	}
	void SetAmr(int AMR)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[AMR_LOCK]);
		m_sObjectdata.m_iAMR = AMR;
	}
	void SetState(char STATE)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[STATE_LOCK]);
		m_sObjectdata.m_cState = STATE;
	}
	void SetLookVec(const DirectX::XMFLOAT3& xmm3Look)
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]);
		m_sObjectdata.m_xmf4World._31 = xmm3Look.x;
		m_sObjectdata.m_xmf4World._32 = xmm3Look.y;
		m_sObjectdata.m_xmf4World._33 = xmm3Look.z;
	}

	void SetAniTime()
	{
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[ANITIME_LOCK]);
		m_sObjectdata.m_tStartTime = std::chrono::high_resolution_clock::now();
	}
	
	void SetRightVec(const DirectX::XMFLOAT3& xmf3RightVec) { 
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]);
		m_sObjectdata.m_xmf4World._11 = xmf3RightVec.x;
		m_sObjectdata.m_xmf4World._12 = xmf3RightVec.y;
		m_sObjectdata.m_xmf4World._13 = xmf3RightVec.z;
	};
	void SetUpVec(const DirectX::XMFLOAT3& xmf3UpVec) {
		std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]);
		m_sObjectdata.m_xmf4World._21 = xmf3UpVec.x;
		m_sObjectdata.m_xmf4World._22 = xmf3UpVec.y;
		m_sObjectdata.m_xmf4World._23 = xmf3UpVec.z;
	};
	void SetMap(char bMap) { std::unique_lock<std::shared_mutex>lock(m_mObjectmutex[MAP_LOCK]); m_sObjectdata.m_bMap = bMap; }
	char GetMap() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[MAP_LOCK]); return m_sObjectdata.m_bMap; }

	const DirectX::BoundingOrientedBox& GetOOBB() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[OOBB_LOCK]); return m_xmOOBB; }
	const DirectX::BoundingOrientedBox& GetTransOOBB() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[TRANS_OOBB_LOCK]); return m_xmTransOOBB; }

	DirectX::XMFLOAT3& GetLookVector() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]); return(DirectX::XMFLOAT3(m_sObjectdata.m_xmf4World._31, m_sObjectdata.m_xmf4World._32, m_sObjectdata.m_xmf4World._33)); }
	DirectX::XMFLOAT3& GetUpVector() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]); return(DirectX::XMFLOAT3(m_sObjectdata.m_xmf4World._21, m_sObjectdata.m_xmf4World._22, m_sObjectdata.m_xmf4World._23)); }
	DirectX::XMFLOAT3& GetRightVector() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]); return(DirectX::XMFLOAT3(m_sObjectdata.m_xmf4World._11, m_sObjectdata.m_xmf4World._12, m_sObjectdata.m_xmf4World._13));
	}
	DirectX::XMFLOAT3& GetFirstPos() { std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[FIRST_POS_LOCK]); return m_sObjectdata.m_xmf3FirstPos; }
	const char GetIsPlayer()
	{
		return m_sObjectdata.m_eObjectType;
	}
	const int GetHP()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[HP_LOCK]);
		return m_sObjectdata.m_iHP;
	}
	const int GetMP()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[MP_LOCK]);
		return m_sObjectdata.m_iMP;
	}
	const int GetMoney()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[MONEY_LOCK]);
		return m_sObjectdata.m_iMoney;
	}
	const int GetExp()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[EXP_LOCK]);
		return m_sObjectdata.m_iExp;
	}
	int GetLimitHP()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[LIMIT_HP_LOCK]);
		return m_sObjectdata.m_iLimitHP;
	}
	int GetLimitMP()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[LIMIT_MP_LOCK]);
		return m_sObjectdata.m_iLimitMP;
	}
	int GetLimitEXP()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[LIMIT_EXP_LOCK]);
		return m_sObjectdata.m_iLimitExp;
	}
	DirectX::XMFLOAT3& GetPos()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]);
		return (DirectX::XMFLOAT3(m_sObjectdata.m_xmf4World._41, m_sObjectdata.m_xmf4World._42, m_sObjectdata.m_xmf4World._43));
	}
	DirectX::XMFLOAT4X4& GetWorld()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[WORLD_LOCK]);
		return m_sObjectdata.m_xmf4World;
	}
	const int GetLevel()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[LEVEL_LOCK]);
		return m_sObjectdata.m_iLevel;
	}
	const int GetAtk()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[ATK_LOCK]);
		return m_sObjectdata.m_iATK;
	}
	const int GetAmr()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[AMR_LOCK]);
		return m_sObjectdata.m_iAMR;
	}
	const char GetState()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[STATE_LOCK]);
		return m_sObjectdata.m_cState;
	}
	const std::chrono::high_resolution_clock::time_point GetAniTime()
	{
		std::shared_lock<std::shared_mutex>lock(m_mObjectmutex[ANITIME_LOCK]);
		return m_sObjectdata.m_tStartTime;
	}

public:
	void StatResetForSwapMap()
	{
		std::unique_lock<std::shared_mutex>statelock(m_mObjectmutex[STATE_LOCK]);
		this->m_sObjectdata.m_cState = e_ObjectState::e_Object_Idle;
		statelock.unlock();
		SetAniTime();
		std::unique_lock<std::shared_mutex>poslock(m_mObjectmutex[WORLD_LOCK]);
		this->m_sObjectdata.m_xmf4World._41 = 5000.0f, this->m_sObjectdata.m_xmf4World._42 = 516.0f, this->m_sObjectdata.m_xmf4World._43 = 5000.0f;
	}
protected:
	void InitializeData() 
	{
		m_sObjectdata.m_eObjectType = e_ObjectType::e_Player_Man;
		m_sObjectdata.m_cState = e_ObjectState::e_Object_Idle;
		m_sObjectdata.m_iAMR = 5;
		m_sObjectdata.m_iATK = 10;
		m_sObjectdata.m_iHP = 100;
		m_sObjectdata.m_iMP = 100;
		m_sObjectdata.m_iLimitHP = 100;
		m_sObjectdata.m_iLimitMP = 100;
		m_sObjectdata.m_iLevel = 1;
		m_sObjectdata.m_iMoney = 0;
		m_sObjectdata.m_iExp = 0;
		m_sObjectdata.m_iLimitExp = 100;
		m_sObjectdata.m_xmf4World = Matrix4x4::Identity();

		m_sObjectdata.m_xmf4World._11 = 1.0f; m_sObjectdata.m_xmf4World._12 = 0.0f; m_sObjectdata.m_xmf4World._13 = 0.0f;
		m_sObjectdata.m_xmf4World._21 = 0.0f;  m_sObjectdata.m_xmf4World._22 *= 1.0f; m_sObjectdata.m_xmf4World._23 = 0.0f;
		m_sObjectdata.m_xmf4World._31 = 0.0f; m_sObjectdata.m_xmf4World._32 = 0.0f; m_sObjectdata.m_xmf4World._33 = 1.0f;
		m_sObjectdata.m_xmf4World._41 = 5000.0f; m_sObjectdata.m_xmf4World._42 = 516.0f; m_sObjectdata.m_xmf4World._43 = 5000.0f;

		m_sObjectdata.m_xmf3FirstPos = DirectX::XMFLOAT3(5000.0f, 516.0f, 5000.0f);
		m_sObjectdata.m_iLimitHP = 100;
		m_sObjectdata.m_iLimitMP = 100;

		m_sObjectdata.m_tStartTime = std::chrono::high_resolution_clock::now();

		//플레이어 남캐
		m_xmTransOOBB = m_xmOOBB = BoundingOrientedBox(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.455595f, 0.360972f, 0.870532f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	}
};