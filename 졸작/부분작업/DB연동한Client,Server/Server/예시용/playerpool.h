#pragma once
#include "stdafx.h"
#include "Player.h"

class CPlayerPool{
private:

public:
	CPlayerPool();
	virtual ~CPlayerPool();

public:
	static concurrency::concurrent_unordered_map<unsigned short, class CPlayer*>m_ccumPlayerPool;
};