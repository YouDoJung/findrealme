#include "Shaders.hlsl"
struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};
//////////////////////////////////////////////////////////////////////////////////////////////
VS_TEXTURED_OUTPUT VS_UI(uint nVertexID : SV_VertexID)
{
	VS_TEXTURED_OUTPUT output;
	if (nVertexID == 0) { 
		output.position = float4(gCenterPosition + float2(-gXYScale.x * 0.1f, +gXYScale.y * 0.1f), 0.0f, 1.0f);
		output.uv = float2(0.f, 0.f); 
	}
	if (nVertexID == 1) { 
		output.position = float4(gCenterPosition + float2(+gXYScale.x * 0.1f, +gXYScale.y * 0.1f), 0.0f, 1.0f);
		output.uv = float2(1.f, 0.f); 
	}
	if (nVertexID == 2) { 
		output.position = float4(gCenterPosition + float2(+gXYScale.x * 0.1f, -gXYScale.y * 0.1f), 0.0f, 1.0f);
		output.uv = float2(1.f, 1.f);
	}
	if (nVertexID == 3) { 
		output.position = float4(gCenterPosition + float2(+gXYScale.x * 0.1f, -gXYScale.y * 0.1f), 0.0f, 1.0f);
		output.uv = float2(1.f, 1.f);
	}
	if (nVertexID == 4) { 
		output.position = float4(gCenterPosition + float2(-gXYScale.x * 0.1f, -gXYScale.y * 0.1f), 0.0f, 1.0f);
		output.uv = float2(0.f, 1.f); 
	}
	if (nVertexID == 5) { 
		output.position = float4(gCenterPosition + float2(-gXYScale.x * 0.1f, +gXYScale.y * 0.1f), 0.0f, 1.0f);
		output.uv = float2(0.f, 0.f); 
	}
	return output;
}

float4 PS_UI(VS_TEXTURED_OUTPUT input) : SV_Target
{
	float4 cColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].Sample(gSamplerState, input.uv);
	if (gTextureIndex.x == 74) {
		cColor = float4(1.f, 0.f, 0.f, 1.f);
	}
	else if (gTextureIndex.x == 75) {
		cColor = float4(0.f, 0.f, 1.f, 1.f);
	}
	else if (gTextureIndex.x == 76) {
		cColor = float4(0.7f, 1.f, 0.f, 1.f);
	}
	return cColor;
}

