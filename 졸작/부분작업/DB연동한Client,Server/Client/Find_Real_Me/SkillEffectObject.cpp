#include "stdafx.h"
#include "SkillEffectObject.h"

CCrackEffectShader* CCrackObject::m_pCrackEffectShader = nullptr;
CThunderShader* CThunderObject::m_pThunderShader = nullptr;
CShieldShader* CShieldObject::m_pShieldShader = nullptr;

default_random_engine dre;
uniform_real_distribution<> uid(-0.3, 0.3);
uniform_real_distribution<> uid2(0, 1);

CCrackObject::CCrackObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int CBindex) : CGameObject(1)
{
	if (CCrackObject::m_pCrackEffectShader == nullptr) {
		CCrackObject::m_pCrackEffectShader = new CCrackEffectShader();
		CCrackObject::m_pCrackEffectShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);
	}

	CMaterial* pCrackMaterial = new CMaterial();
	pCrackMaterial->SetAlbedo(XMFLOAT4(0.f, 0.f, 0.f, 1.f));

	CTexturedRectMesh* pMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, xmf2Size.x, 0.f, xmf2Size.y, 0.f, 0.f, 0.f);
	SetMesh(0, pMesh);
	m_strObjectName = "Crack";
	SetMaterial(0, pCrackMaterial);
	SetTextureIndex(XMUINT2(62, 63));
	SetObjectCBIndex(CBindex);
}

CCrackObject::~CCrackObject()
{
	if (CCrackObject::m_pCrackEffectShader) {
		delete CCrackObject::m_pCrackEffectShader;
		CCrackObject::m_pCrackEffectShader = nullptr;
	}
}

void CCrackObject::UpdateCB(FrameResource* pFrameResource)
{
	if (m_isEffect) {
		CGameObject::UpdateCB(pFrameResource);
		SetFrameDirty(::gNumFrameResources);
	}
}

void CCrackObject::Animate(float fTimeElapsed)
{
	if (m_isEffect) {
		if (m_fFrameTime < 1.f) {
			m_fFrameTime += fTimeElapsed;
		}
		else {
			m_fFrameTime = 0.f;
			m_isEffect = false;
		}
	}
}

void CCrackObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	if (m_isEffect) {
		CCrackObject::m_pCrackEffectShader->Render(pd3dCommandList, pCamera, false);
		CGameObject::Render(pd3dCommandList, pCamera, false);
	}
}

//////////////////////////////////////////////////////////////////////
CEffectObject::CEffectObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int CBindex)
{
	if (CCrackObject::m_pCrackEffectShader == nullptr) {
		CCrackObject::m_pCrackEffectShader = new CCrackEffectShader();
		CCrackObject::m_pCrackEffectShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);
	}

	CTexturedRectMesh* pMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, xmf2Size.x, 0.f, xmf2Size.y, 0.f, 0.f, 0.f);
	CMaterial* pEffectMaterial = new CMaterial();

	SetMesh(0, pMesh);
	m_strObjectName = "Effect";
	SetMaterial(0, pEffectMaterial);
	SetTextureIndex(XMUINT2(64, 65));
	SetObjectCBIndex(CBindex);
}

CEffectObject::~CEffectObject()
{
}

CSwordSkillEffectObject::CSwordSkillEffectObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size,
	CMesh* pMesh, int Sword_cnt, CPlayer* pPlayer, XMUINT2 xmi2TexIndex, int CBindex, int* ChildCBindex) : CStaticModelObject(1)
{
	SetMesh(0, pMesh);
	m_strObjectName = "Sword";
	CMaterial* pMaterial = new CMaterial();
	SetMaterial(0, pMaterial);
	SetObjectCBIndex(CBindex);
	XMUINT2 index = xmi2TexIndex;
	index.x += 2;
	index.y += 2;
	SetTextureIndex(index);
	SetScale(100, 100, 100);
	float dir = (float)uid(dre);
	Rotate(dir * 90.f, 90, dir * 90.f);
	m_xmf3Direction = { -dir, -1, -dir };
	SetStartTime(uid2(dre));
	m_iSwordCnt = Sword_cnt;
	m_pTarget = pPlayer;
	m_vCrackEffectObject.resize(2);
	m_vCrackEffectObject[0] = new CCrackObject(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, XMFLOAT2(150.f, 200.f), ChildCBindex[0]);
	m_vCrackEffectObject[1] = new CEffectObject(pd3dDevice, pd3dCommandList, pd3dGraphicsRootSignature, XMFLOAT2(150.f, 200.f), ChildCBindex[1]);
}

CSwordSkillEffectObject::~CSwordSkillEffectObject()
{
	if (m_vCrackEffectObject.size() > 0) {
		for (auto& obj : m_vCrackEffectObject) {
			if (obj) {
				delete obj;
				obj = nullptr;
			}
		}
		m_vCrackEffectObject.clear();
	}
}

void CSwordSkillEffectObject::UpdateCB(FrameResource* pFrameResource)
{
	CGameObject::UpdateCB(pFrameResource);
	SetFrameDirty(::gNumFrameResources);

	for (auto& obj : m_vCrackEffectObject) {
		obj->UpdateCB(pFrameResource);
	}
}

void CSwordSkillEffectObject::Animate(float fTimeElapsed)
{
	m_fSwordFrameTime += fTimeElapsed;
	XMFLOAT3 pos = m_pTarget->GetPosition();		// 플레이어 포지션.
	// 자리잡는 부분.
	if (m_fSwordFrameTime < 1.f) {
		if (m_iSwordCnt < 30) {
			SetPosition(XMFLOAT3(pos.x + 300 * m_fSwordFrameTime * sin(m_iSwordCnt / ((float)m_iMaxSwordCnt / 2.f) * 3.14f * 2.f), 1000,
				pos.z + 300 * m_fSwordFrameTime * cos(m_iSwordCnt / ((float)m_iMaxSwordCnt / 2.f) * 3.14f * 2.f)));
		}
		else {
			SetPosition(XMFLOAT3(pos.x - 400 * m_fSwordFrameTime * sin(m_iSwordCnt / ((float)m_iMaxSwordCnt / 2.f) * 3.14f * 2.f), 1000,
				pos.z - 400 * m_fSwordFrameTime * cos(m_iSwordCnt / ((float)m_iMaxSwordCnt / 2.f) * 3.14f * 2.f)));
		}

	}
	// 밑으로 내리찍는 부분
	if (m_fSwordFrameTime >= 1.f) {

		// GetStartTime()이 0~1사이임. m_fSwordFrameTime은 계속 증가중. 1초때부터 이쪽루프로 들어오기 때문에 1초 빼고 계속 더해서 starttime에따라 밑으로 애니메이트.
		if (GetStartTime() < m_fSwordFrameTime - 1.f) {
			if (Vector3::Add(Vector3::Multiply(fTimeElapsed * 1200.f, m_xmf3Direction), GetPosition()).y > 516.f + 80.f) {
				SetPosition(Vector3::Add(Vector3::Multiply(fTimeElapsed * 1200.f, m_xmf3Direction), GetPosition()));
			}
			else {
				// 요기가 한번씩만 들어가야되는데. 시간이 안맞아서 두번 들어가는 놈들이 생겨버려~
				// 만만한 bool변수로 해결하자.
				if (!m_isCrackEffect) {
					m_isCrackEffect = true;
					for (auto& obj : m_vCrackEffectObject) {
						XMFLOAT3 pos = GetPosition();
						pos.y += 5.f;
						obj->SetEffectStart(pos, true);
					}
				}
			}
		}
		if (m_fSwordFrameTime >= 3.f) {
			m_isCrackEffect = false;
			m_fSwordFrameTime = 0.f;
			SetPosition(XMFLOAT3(0, 0, 0));
			for (auto& obj : m_vCrackEffectObject) {
				obj->SetEffectStart(GetPosition(), false);
			}
		}
	}

	for (auto& obj : m_vCrackEffectObject) {
		obj->Animate(fTimeElapsed);
	}
}

void CSwordSkillEffectObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CGameObject::Render(pd3dCommandList, pCamera, false);

	for (auto& obj : m_vCrackEffectObject) {
		obj->Render(pd3dCommandList, pCamera, false);
	}
}

////////////////////////////////////////////
CThunderObject::CThunderObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int iTexIndex, int CBIndex) : CGameObject(1)
{
	if (CThunderObject::m_pThunderShader == nullptr) {
		CThunderObject::m_pThunderShader = new CThunderShader();
		CThunderObject::m_pThunderShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);
	}

	CMaterial* pCrackMaterial = new CMaterial();
	pCrackMaterial->SetAlbedo(XMFLOAT4(0.f, 0.f, 0.f, 1.f));

	CTexturedRectMesh* pMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, xmf2Size.x, xmf2Size.y, 0.f, 0.f, 0.f, 0.f);
	SetMesh(0, pMesh);
	m_strObjectName = "Thunder";
	SetMaterial(0, pCrackMaterial);
	SetTextureIndex(XMUINT2(iTexIndex, iTexIndex));
	SetObjectCBIndex(CBIndex);
}

CThunderObject::~CThunderObject()
{
	if (CThunderObject::m_pThunderShader) {
		delete CThunderObject::m_pThunderShader;
		CThunderObject::m_pThunderShader = nullptr;
	}
}

void CThunderObject::UpdateCB(FrameResource* pFrameResource)
{
	if (m_isEffect) {
		CGameObject::UpdateCB(pFrameResource);
		SetFrameDirty(::gNumFrameResources);
	}
}

void CThunderObject::Animate(float fTimeElapsed)
{
	if (m_isEffect) {
		if (m_fFrameTime < 2.f) {
			m_fFrameTime += fTimeElapsed;
		}
		else {
			m_fFrameTime = 0.f;
			m_isEffect = false;
		}
	}
}

void CThunderObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	if (m_isEffect) {

		CThunderObject::m_pThunderShader->Render(pd3dCommandList, pCamera, false);
		CGameObject::Render(pd3dCommandList, pCamera, false);
	}
}

////////////////////////////////////////////
CShieldObject::CShieldObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int iTexIndex, int CBIndex) : CGameObject(1)
{
	if (CShieldObject::m_pShieldShader == nullptr) {
		CShieldObject::m_pShieldShader = new CShieldShader();
		CShieldObject::m_pShieldShader->CreateShader(pd3dDevice, pd3dGraphicsRootSignature, false);
	}

	CMaterial* pCrackMaterial = new CMaterial();
	pCrackMaterial->SetAlbedo(XMFLOAT4(0.f, 0.f, 0.f, 1.f));

	CTexturedRectMesh* pMesh = new CTexturedRectMesh(pd3dDevice, pd3dCommandList, xmf2Size.x, xmf2Size.y, 0.f, 0.f, 0.f, 0.f);
	SetMesh(0, pMesh);
	m_strObjectName = "Shield";
	SetMaterial(0, pCrackMaterial);
	SetTextureIndex(XMUINT2(iTexIndex, iTexIndex));
	SetObjectCBIndex(CBIndex);
}

CShieldObject::~CShieldObject()
{
	if (CShieldObject::m_pShieldShader) {
		delete CShieldObject::m_pShieldShader;
		CShieldObject::m_pShieldShader = nullptr;
	}
}

void CShieldObject::UpdateCB(FrameResource* pFrameResource)
{
	if (m_isEffect) {
		CGameObject::UpdateCB(pFrameResource);
		SetFrameDirty(::gNumFrameResources);
	}
}

void CShieldObject::Animate(float fTimeElapsed)
{
	if (m_isEffect) {
		if (m_fFrameTime < 2.f) {
			m_fFrameTime += fTimeElapsed;
		}
		else {
			m_fFrameTime = 0.f;
			m_isEffect = false;
		}
	}
}

void CShieldObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	if (m_isEffect) {
		CShieldObject::m_pShieldShader->Render(pd3dCommandList, pCamera, false);
		CGameObject::Render(pd3dCommandList, pCamera, false);
	}
}