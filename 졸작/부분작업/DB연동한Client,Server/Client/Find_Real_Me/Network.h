#pragma once
#include "stdafx.h"
#include "GameFramework.h"
#include "Player.h"
#include "Scene.h"

constexpr char NETWORK_LOCK_NUM = 2;

constexpr char PLAYER_LIST = 0;
constexpr char MONSTER_LIST = 1;

class CNetwork : public CTemplateSingleton<CNetwork> {
private:
	SOCKET m_sGamesock;

	WSAEVENT m_hEvent;
	WSANETWORKEVENTS m_weEvent;

	WSABUF m_wbSendwsabuf;
	WSABUF m_wbRecvwsabuf;

	char m_cSendbuf[BUFSIZE];
	char m_cRecvbuf[BUFSIZE];
	char m_cPacketbuf[MAX_PACKET_SIZE];

	DWORD m_dwPacketsize = 0;
	DWORD m_dwSavepacketsize = 0;

	int m_iIndex;

	std::recursive_mutex m_rmListMutex[NETWORK_LOCK_NUM];

	std::atomic<bool> m_abLoginSuccess;
	bool m_bLoopStart = false;
	bool m_bPushKey = false;
	bool m_bClientClose = false;
	char m_cMap = MAP_VILLAGE;
	bool m_bCollision = false;
	wchar_t m_wcID[MAX_STR_LEN];
public:
	explicit CNetwork();
	~CNetwork();

public:
	static unsigned short m_usId;

	BOOL Initalize();
	BOOL ConnectServer();
	BOOL CreateEventSelect();


	void DisConnect();
	void Recvpacket();
	void ProcessPacket(char* PACKET);

	void SendPosPacket();
	void SendDirPacket();
	void SendAttackPacket();
	void SendAttack2Packet();
	void SendSkill1Packet();
	void SendSkill2Packet();
	void SendSkill3Packet();
	void SendSkill4Packet();
	void SendSkill5Packet();
	void SendIdlePacket();

	void SendLoginPacket();

	void SendMapSwapPacket();

	void SendForDevMapSwapPacket();
	void SendForDevCharacterInvincibility();

	void SetPushKey(bool bPush) { m_bPushKey = bPush; }
	bool GetPushKey() { return m_bPushKey; }

	bool GetLoopStart() { return m_bLoopStart; }
	void SetClient(bool bStart) { m_bClientClose = bStart; }
	char GetCurrentMap() { return m_cMap; }
	void SetCurrentMap() { ++m_cMap %= 4; }
	void SetCollision(bool bCollision) { m_bCollision = bCollision; }
	bool GetCollision() { return m_bCollision; }

	void SetIsLogin(bool bLogin) { m_abLoginSuccess = bLogin; }
	bool GetIsLogin() { return m_abLoginSuccess; }
public:
	static concurrency::concurrent_unordered_set<unsigned short>m_ccusViewList[NETWORK_LOCK_NUM];

	void CopyBefore(char cWhat, concurrency::concurrent_unordered_set<unsigned short>& usCopyList)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		usCopyList = m_ccusViewList[cWhat];
	}
	void DeleteListElement(char cWhat, unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		m_ccusViewList[cWhat].unsafe_erase(bId);
	}
	void InsertListElement(char cWhat, unsigned short bId)
	{
		m_ccusViewList[cWhat].insert(bId);
	}
	BOOL ExistListElement(char cWhat, unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		if (m_ccusViewList[cWhat].count(bId) != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	void ClearListElement(char cWhat)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmListMutex[cWhat]);
		m_ccusViewList[cWhat].clear();
	}
};