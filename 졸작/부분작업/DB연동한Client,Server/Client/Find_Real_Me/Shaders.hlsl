#ifndef NUM_DIR_LIGHTS
#define NUM_DIR_LIGHTS 3
#endif

#ifndef NUM_POINT_LIGHTS
#define NUM_POINT_LIGHTS 0
#endif

#ifndef NUM_SPOT_LIGHTS
#define NUM_SPOT_LIGHTS 0
#endif
#include "LightingUtil.hlsl"

#define SKINNED_ANIMATION_BONES		96


cbuffer cbCameraInfo : register(b0)
{
	matrix		gmtxView;
	matrix		gmtxInverseView;
	matrix		gmtxProjection;
	matrix		gmtxInverseProjection;
	matrix		gmtxViewProjection;
	matrix		gmtxShadowTransform;

	float3		gvCameraPosition;
	float		gfDeltaTime;

	float4		gAmbientLight;
	Light		gLights[MaxLights];

	float		gfTotalTime;
	int		gMapIndex;
	float		gPlayerZ;
};

cbuffer cbObjectInfo : register(b1)
{
	matrix	gmtxObjectWorld;
	Material gMaterial;
	uint2 gTextureIndex;
	float gFrameTime;
	float gStartTime;

	// ui��
	float2 gXYScale;
	float2 gCenterPosition;

	int gMaxHp;
	int gHp;
	int gHpSize;
};

cbuffer cbSkinnedInfo : register(b2)
{
	matrix		gmtxBoneTransforms[SKINNED_ANIMATION_BONES];
}
#define NUM_OF_TEXTURES	120
Texture2D gtxtAllTextures[NUM_OF_TEXTURES] : register(t0);
Texture2D gtxtShadow : register(t120);
TextureCube gtxtSkyCubeTexture : register(t121);

SamplerState gSamplerState : register(s0);
SamplerComparisonState gShadowSamplerState : register(s1);
SamplerState gSamplerClamp : register(s2);

float3 NormalSampleToWorldSpace(float3 normalMapSample, float3 unitNormalW, float3 tangentW)
{
	// Uncompress each component from [0,1] to [-1,1].
	float3 normalT = 2.0f*normalMapSample - 1.0f;

	// Build orthonormal basis.
	float3 N = unitNormalW;
	float3 T = normalize(tangentW - dot(tangentW, N) * N);
	float3 B = cross(N, T);

	float3x3 TBN = float3x3(T, B, N);

	// Transform from tangent space to world space.
	float3 bumpedNormalW = mul(normalT, TBN);

	return bumpedNormalW;
}

//---------------------------------------------------------------------------------------
// PCF for shadow mapping.
//---------------------------------------------------------------------------------------
//#define SMAP_SIZE = (2048.0f)
//#define SMAP_DX = (1.0f / SMAP_SIZE)
float CalcShadowFactor(float4 shadowPosH)
{
	// Complete projection by doing division by w.
	shadowPosH.xyz /= shadowPosH.w;

	// Depth in NDC space.
	float depth = shadowPosH.z;

	uint width, height, numMips;
	gtxtShadow.GetDimensions(0, width, height, numMips);

	// Texel size.
	float dx = 1.0f / (float)width;

	float percentLit = 0.0f;
	const float2 offsets[9] =
	{
		float2(-dx,  -dx), float2(0.0f,  -dx), float2(dx,  -dx),
		float2(-dx, 0.0f), float2(0.0f, 0.0f), float2(dx, 0.0f),
		float2(-dx,  +dx), float2(0.0f,  +dx), float2(dx,  +dx)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
		percentLit += gtxtShadow.SampleCmpLevelZero(gShadowSamplerState,
			shadowPosH.xy + offsets[i], depth).r;
	}

	return percentLit / 9.0f;
}