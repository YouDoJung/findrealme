#include "stdafx.h"
#include "ModelObject.h"
#include "Material.h"
#include "Shader.h"
#include "ModelShader.h"

CStaticModelObject::CStaticModelObject() : CGameObject(1)
{

}

CStaticModelObject::~CStaticModelObject()
{
}

CSkinnedObject::CSkinnedObject() : CGameObject(1)
{
}

void CSkinnedObject::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
	//cout << "플레이어 : " << m_xmOOBB.Center.x << ", " << m_xmOOBB.Center.y << ", " << m_xmOOBB.Center.z << endl;
	m_fElapsedTime = fTimeElapsed;
}

void CSkinnedObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	UINT objCBByteSize = (sizeof(ObjectConstants) + 255) & ~255;
	OnPrepareRender();

	// 재질을 오브젝트가 갖고있으면, 오브젝트 내부에서 상수버퍼를 수정한다고 생각하자.

	for (int i = 0; i < m_nMaterials; i++)
	{
		if (m_ppMaterials[i])
		{
			if (m_ppMaterials[i]->m_pShader)
			{
				m_ppMaterials[i]->m_pShader->Render(pd3dCommandList, pCamera, is_Shadow);
			}
		}
	}

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_pCurrResource->ObjectCB->Resource()->GetGPUVirtualAddress() + m_ObjectCBIndex * objCBByteSize;
	pd3dCommandList->SetGraphicsRootConstantBufferView(1, d3dGpuVirtualAddress);
}

//////
CSkinAndCombinedObject::CSkinAndCombinedObject()
{

}

CSkinAndCombinedObject::~CSkinAndCombinedObject()
{
	m_mCombinedAnimationClip.clear();
}

void CSkinAndCombinedObject::LoadModelAndInitCombinedAnimationClip(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) {
	m_Importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	m_pModel = m_Importer.ReadFile(filename.c_str(), (aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded) &
		~aiProcess_FindInvalidData |
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_GenUVCoords |
		aiProcess_TransformUVCoords |
		aiProcess_LimitBoneWeights |
		aiProcess_OptimizeMeshes |
		aiProcess_GenSmoothNormals |
		aiProcess_SplitLargeMeshes |
		aiProcess_Triangulate |
		aiProcess_SortByPType);

	m_ppMeshes[0] = new CSkinnedModelMesh(pd3dDevice, pd3dCommandList, m_pModel->mMeshes[0]);

	float start_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[0].mTime;
	float end_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[m_pModel->mAnimations[0]->mChannels[0]->mNumPositionKeys - 1].mTime - 1.0f;
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle", CombinedAnimationClip(start_time, end_time, 10.f)));

	m_strNowClip = "idle";
}

void CSkinAndCombinedObject::SetNowAnimation(string& s)
{
	if (m_strNowClip != s && (m_strNowClip != "die" && m_strNowClip != "turn_die")|| (m_strNowClip != s && (s == "idle"|| s == "idle2")))
	{
		m_strNowClip = s;
		m_fNowTime = 0.f;
	}
	//if (m_strNowClip != s /*&& (m_strNowClip == std::string("idle") || m_strNowClip == std::string("idle2"))*/)
	//{
	//	m_strNowClip = s;
	//	m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;
	//}

	/*
	m_strNowClip = s;
	m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;*/
}

void CSkinAndCombinedObject::UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime)
{
	m_pCurrResource = pFrameResource;
	for (int i = 0; i < m_nMeshes; ++i) {
		if (m_ppMeshes[i]) {
			if (m_fNowTime < m_mCombinedAnimationClip[m_strNowClip].m_fStartTime)
				m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;
			std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
			m_ppMeshes[i]->BoneTransform(m_pCurrResource, m_pModel, m_fNowTime, m_SkinCBIndex);
			m_fNowTime += m_fElapsedTime * m_mCombinedAnimationClip[m_strNowClip].m_fSpeed;
			if (m_fNowTime > m_mCombinedAnimationClip[m_strNowClip].m_fEndTime) {
				/*if (m_strNowClip == "leg_attack" || m_strNowClip == "tail_attack") {
					std::chrono::duration<double> sec = std::chrono::high_resolution_clock::now() - start;
					std::cout << m_strNowClip << ": " << sec.count() << '\n';
				}*/
				if (m_strNowClip == std::string("die") || m_strNowClip == std::string("turn_die")) {
					m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fEndTime;
				}
				else {
					//m_strNowClip = std::string("idle");
					if (m_strNowClip == std::string("idle")) {
						m_strNowClip = std::string("idle2");
					}
					else if (m_strNowClip == std::string("idle2")) {
						m_strNowClip = std::string("idle");
					}
					m_fNowTime = m_mCombinedAnimationClip[m_strNowClip].m_fStartTime;
				}
			}

		}
	}
}

void CSkinAndCombinedObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CSkinnedObject::Render(pd3dCommandList, pCamera, is_Shadow);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				UINT objCBByteSize = (sizeof(SkinnedConstants) + 255) & ~255;
				D3D12_GPU_VIRTUAL_ADDRESS d3dcbTransformsGpuVirtualAddress = m_pCurrResource->SkinnedCB->Resource()->GetGPUVirtualAddress() + m_SkinCBIndex * objCBByteSize;
				pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dcbTransformsGpuVirtualAddress);

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}
	if (!is_Shadow)
		CollisionBoxRender(pd3dCommandList, pCamera);
}
//////////////////////////////////////
CBossDragonObject::CBossDragonObject() {}
CBossDragonObject::~CBossDragonObject() {}

void CBossDragonObject::LoadModelAndInitCombinedAnimationClip(const string& filename, ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) {
	m_Importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	m_pModel = aiImportFile(filename.c_str(), (aiProcessPreset_TargetRealtime_Quality | aiProcess_ConvertToLeftHanded) &
		~aiProcess_FindInvalidData |
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure |
		aiProcess_ImproveCacheLocality |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_GenUVCoords |
		aiProcess_TransformUVCoords |
		aiProcess_LimitBoneWeights |
		aiProcess_OptimizeMeshes |
		aiProcess_GenSmoothNormals |
		aiProcess_SplitLargeMeshes |
		aiProcess_Triangulate |
		aiProcess_SortByPType);

	m_ppMeshes[0] = new CSkinnedModelMesh(pd3dDevice, pd3dCommandList, m_pModel->mMeshes[0]);

	float start_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[0].mTime;
	float end_time = (float)m_pModel->mAnimations[0]->mChannels[0]->mPositionKeys[m_pModel->mAnimations[0]->mChannels[0]->mNumPositionKeys - 1].mTime - 1.0f;
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle", CombinedAnimationClip(start_time, 339.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("jump", CombinedAnimationClip(340.f, 365.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("leg_attack", CombinedAnimationClip(366.f, 410.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fire_breath", CombinedAnimationClip(411.f, 475.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("tail_attack", CombinedAnimationClip(475.f, 515.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("left_hit", CombinedAnimationClip(516.f, 533.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("right_hit", CombinedAnimationClip(534.f, 551.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("die", CombinedAnimationClip(552.f, 611.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("walk", CombinedAnimationClip(612.f, 644.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("run", CombinedAnimationClip(645.f, 660.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fly", CombinedAnimationClip(661.f, 707.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fly_attack", CombinedAnimationClip(708.f, 768, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("fast_fly", CombinedAnimationClip(862.f, 884.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("down_hit", CombinedAnimationClip(885.f, 905.f, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("turn_die", CombinedAnimationClip(906.f, 969.0f/*970.f*/, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("idle2", CombinedAnimationClip(1005.f, 1100, 30.0f)));
	m_mCombinedAnimationClip.insert(pair<string, CombinedAnimationClip>("turn_walk", CombinedAnimationClip(1101.f, end_time, 30.0f)));

	//m_strNowClip = "idle";
}

void CBossDragonObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CSkinnedObject::Render(pd3dCommandList, pCamera, is_Shadow);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				UINT objCBByteSize = (sizeof(SkinnedConstants) + 255) & ~255;
				D3D12_GPU_VIRTUAL_ADDRESS d3dcbTransformsGpuVirtualAddress = m_pCurrResource->SkinnedCB->Resource()->GetGPUVirtualAddress() + m_SkinCBIndex * objCBByteSize;
				pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dcbTransformsGpuVirtualAddress);

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}
	if (!is_Shadow)
		CollisionBoxRender(pd3dCommandList, pCamera);
}

//////////////////
CSkinAndDivisionObject::CSkinAndDivisionObject()
{

}

CSkinAndDivisionObject::~CSkinAndDivisionObject()
{
	m_mDivisionAnimationClip.clear();
}

void CSkinAndDivisionObject::SetNowAnimation(string& s)
{
	if (m_strNowClip != s && (m_strNowClip == "idle" || m_strNowClip == "run" || m_strNowClip == "walk" || m_strNowClip == "stun" || m_strNowClip == "die")) {
		m_strNowClip = s;
		m_fNowTime = 0.f;
	}
}

void CSkinAndDivisionObject::SetNowAnimationForMonster(string& s)
{
	if (m_strNowClip != s && m_strNowClip != "die" ||( m_strNowClip != s && s == "idle") || (m_strNowClip != s && s == "hit" &&  m_strNowClip != "die"))
	{
		m_strNowClip = s;
		m_fNowTime = 0.f;
	}
}

void CSkinAndDivisionObject::SetAnimClip(unordered_map<string, DivisionAnimationClip>& clip) {
	m_mDivisionAnimationClip = clip;
}

void CSkinAndDivisionObject::UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime)
{
	m_pCurrResource = pFrameResource;
	for (int i = 0; i < m_nMeshes; ++i) {
		if (m_ppMeshes[i]) {

			m_ppMeshes[i]->BoneTransform(m_pCurrResource, m_mDivisionAnimationClip[m_strNowClip].m_pAnimationModel, m_fNowTime, m_SkinCBIndex);
			m_fNowTime += m_fElapsedTime * m_mDivisionAnimationClip[m_strNowClip].m_fSpeed;

			if (m_mDivisionAnimationClip[m_strNowClip].m_isRoof) {
				if (m_fNowTime > m_mDivisionAnimationClip[m_strNowClip].m_fEndTime) {
					m_fNowTime = m_mDivisionAnimationClip[m_strNowClip].m_fStartTime;
				}
			}
			else {
				if (m_fNowTime > m_mDivisionAnimationClip[m_strNowClip].m_fEndTime) {
					if (m_strNowClip == std::string("die")) {
						m_fNowTime = m_mDivisionAnimationClip[m_strNowClip].m_fEndTime;
					}
					else {
						m_fNowTime = 0;
						m_strNowClip = "idle";
					}
				}
			}
		}
	}

	// 자식을 갖고 있다면.
	if (m_vChilds.size() > 0) {
		for (int i = 0; i < m_vChilds.size(); ++i) {
			// 부모의 메시의 애니메이션에서 자식의 변환행렬을 가져옴.
			XMFLOAT4X4 world = m_ppMeshes[0]->GetChildWorlds(m_vChilds[i]->m_strObjectName);

			// 부모의 월드변환행렬과 곱
			world = Matrix4x4::Multiply(world, m_xmf4x4World);

			XMFLOAT3 xmf3LocalScale = m_vChilds[i]->GetLocalScale();
			XMMATRIX mtxScale = XMMatrixScaling(xmf3LocalScale.x, xmf3LocalScale.y, xmf3LocalScale.z);
			world = Matrix4x4::Multiply(mtxScale, world);

			// 월드 적용 후 상수버퍼 업데이트
			m_vChilds[i]->SetWorld(world);
			// 상수버퍼 업데이트를 왜 여기서 하냐면, 위에 부모객체의 메시에서 자식의 변환행렬을 가져오기 때문에 위에 코드가 수행된 후에 업데이트를 해야함.
			m_vChilds[i]->UpdateCB(pFrameResource);
			// 매프레임 새로운 정보가 상수버퍼에 들어가야 하기 때문에 dirty플래그 초기화
			m_vChilds[i]->SetFrameDirty(gNumFrameResources);

		}
	}
}

void CSkinAndDivisionObject::Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera, bool is_Shadow)
{
	CSkinnedObject::Render(pd3dCommandList, pCamera, is_Shadow);

	if (m_ppMeshes)
	{
		for (int i = 0; i < m_nMeshes; i++)
		{
			if (m_ppMeshes[i])
			{
				UINT objCBByteSize = (sizeof(SkinnedConstants) + 255) & ~255;
				D3D12_GPU_VIRTUAL_ADDRESS d3dcbTransformsGpuVirtualAddress = m_pCurrResource->SkinnedCB->Resource()->GetGPUVirtualAddress() + m_SkinCBIndex * objCBByteSize;
				pd3dCommandList->SetGraphicsRootConstantBufferView(2, d3dcbTransformsGpuVirtualAddress);

				m_ppMeshes[i]->Render(pd3dCommandList);
			}
		}
	}

	// 자식을 갖고 있다면.
	if (m_vChilds.size() > 0) {
		for (int i = 0; i < m_vChilds.size(); ++i) {
			
			// 렌더링 ㄱ
			m_vChilds[i]->Render(pd3dCommandList, pCamera, is_Shadow);
		}
	}
	if (!is_Shadow) {
		CollisionBoxRender(pd3dCommandList, pCamera);
		HpBarRender(pd3dCommandList, pCamera);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
CNPCObject::~CNPCObject()
{
}

void CNPCObject::Animate(float fTimeElapsed)
{
	m_xmOOBBTransformed.Transform(m_xmOOBB, XMLoadFloat4x4(&m_xmf4x4World));
	XMStoreFloat4(&m_xmOOBBTransformed.Orientation, XMQuaternionNormalize(XMLoadFloat4(&m_xmOOBBTransformed.Orientation)));
	//cout << "플레이어 : " << m_xmOOBB.Center.x << ", " << m_xmOOBB.Center.y << ", " << m_xmOOBB.Center.z << endl;
	m_fElapsedTime = fTimeElapsed;

	for (auto& obj : m_vChilds) {
		obj->Animate(fTimeElapsed);
	}
}

void CNPCObject::UpdateCB(FrameResource* pFrameResource)
{
	m_pCurrResource = pFrameResource;

	if (m_nNumFrameDirty > 0) {
		auto currObjectCB = m_pCurrResource->ObjectCB.get();
		ObjectConstants objConstants;

		XMStoreFloat4x4(&objConstants.m_xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_xmf4x4World)));
		objConstants.m_xmf4DiffuseAlbedo = m_ppMaterials[0]->GetDiffuseAlbedo();
		objConstants.m_xmf3FresnelR0 = m_ppMaterials[0]->GetFresneIRO();
		objConstants.Shininess = 1.f - m_ppMaterials[0]->GetRoughness();
		// 어떤 텍스처를 사용할지 알려주는 텍스처인덱스변수도 넘겨주고,.
		objConstants.m_xmi2TexIndex = m_xmi2TexIndex;
		objConstants.m_fFrameTime = m_fFrameTime;
		objConstants.m_fStartTime = m_fStartTime;
		currObjectCB->CopyData(m_ObjectCBIndex, objConstants);
		// 다음 프레임으로 넘어간다.
		m_nNumFrameDirty--;
	}
	if (m_vChilds.size() > 0) {
		for (auto& obj : m_vChilds) {
			obj->UpdateCB(pFrameResource);
			obj->SetFrameDirty(::gNumFrameResources);
		}
	}

	if (m_pCollisionBox) {
		m_pCollisionBox->SetWorld(m_xmf4x4World);
		m_pCollisionBox->UpdateCB(pFrameResource);
		m_pCollisionBox->SetFrameDirty(::gNumFrameResources);
	}
}

void CNPCObject::UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime)
{
	m_pCurrResource = pFrameResource;
	for (int i = 0; i < 1; ++i) {
		if (m_ppMeshes[i]) {

			m_ppMeshes[i]->BoneTransform(m_pCurrResource, m_mDivisionAnimationClip[m_strNowClip].m_pAnimationModel, m_fNowTime, m_SkinCBIndex);
			m_fNowTime += m_fElapsedTime * m_mDivisionAnimationClip[m_strNowClip].m_fSpeed;

			if (m_mDivisionAnimationClip[m_strNowClip].m_isRoof) {
				if (m_fNowTime > m_mDivisionAnimationClip[m_strNowClip].m_fEndTime) {
					m_fNowTime = m_mDivisionAnimationClip[m_strNowClip].m_fStartTime;
				}
			}
			else {
				if (m_fNowTime > m_mDivisionAnimationClip[m_strNowClip].m_fEndTime) {
					m_fNowTime = 0;
					m_strNowClip = "idle";
				}
			}
		}
	}

	// 자식을 갖고 있다면.
	if (m_vChilds.size() > 0) {
		for (int i = 0; i < m_vChilds.size(); ++i) {
			m_vChilds[i]->UpdateSkinCB(pFrameResource, fElapsedTime);
			// 매프레임 새로운 정보가 상수버퍼에 들어가야 하기 때문에 dirty플래그 초기화
			m_vChilds[i]->SetFrameDirty(gNumFrameResources);

		}
	}
}

void CNPCObject::SetChilds(ID3D12Device* pd3dDevice, vector<CSkinnedModelMesh*> pMesh, vector<string>& meshName, XMFLOAT3 xmf3LocalScale)
{
	for (int i = 0; i < m_nMeshes; ++i) {
		m_ppMeshes[i]->SetChildNames(meshName);
	}
	m_vChilds.resize(pMesh.size());
	XMUINT2 index = m_xmi2TexIndex;
	for (int i = 0; i < pMesh.size(); ++i) {

		m_vChilds[i] = new CNPCObject(1);
		m_vChilds[i]->SetMesh(0, pMesh[i]);
		m_vChilds[i]->m_strObjectName = meshName[i];
		m_vChilds[i]->Rotate(90, 0, 0);
		// 각 자식들에 Material 셋, 상수버퍼인덱스, 텍스처인덱스 셋
		CMaterial* pMaterial = new CMaterial();
		m_vChilds[i]->SetMaterial(0, pMaterial);
		m_vChilds[i]->SetObjectCBIndex(::gnObjCBindex++);
		m_vChilds[i]->SetSkinCBIndex(::gnSkinCBindex++);
		index.x += 2;
		index.y += 2;
		m_vChilds[i]->SetTextureIndex(index);
	}
}