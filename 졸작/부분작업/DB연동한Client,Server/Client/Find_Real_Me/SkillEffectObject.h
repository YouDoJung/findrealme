#pragma once
#include "Shader.h"

class CCrackObject : public CGameObject
{
public:
	static class CCrackEffectShader* m_pCrackEffectShader;
public:
	CCrackObject() : CGameObject(1) {}
	CCrackObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int CBindex);
	virtual ~CCrackObject();
	virtual void UpdateCB(FrameResource* pFrameResource);
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);

};

class CEffectObject : public CCrackObject
{
public:
	CEffectObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int CBindex);
	virtual ~CEffectObject();

};

class CSwordSkillEffectObject : public CStaticModelObject
{
public:
	CSwordSkillEffectObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size,
		CMesh* pMesh, int Sword_cnt, CPlayer* pPlayer, XMUINT2 xmi2TexIndex, int CBindex, int* ChildCBindex);
	virtual ~CSwordSkillEffectObject();
	virtual void UpdateCB(FrameResource* pFrameResource);
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);

private:
	float m_fSwordFrameTime = 0.f;
	int m_iSwordCnt = 0;
	int m_iMaxSwordCnt = 60;


	bool m_isCrackEffect = false;
	vector<CGameObject*> m_vCrackEffectObject;
};

class CThunderObject : public CGameObject
{
public:
	static class CThunderShader* m_pThunderShader;
public:
	CThunderObject() : CGameObject(1) {}
	CThunderObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int iTexIndex, int CBIndex);
	virtual ~CThunderObject();
	virtual void UpdateCB(FrameResource* pFrameResource);
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);

};

class CShieldObject : public CGameObject
{
public:
	static class CShieldShader* m_pShieldShader;
public:
	CShieldObject() : CGameObject(1) {}
	CShieldObject(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, ID3D12RootSignature* pd3dGraphicsRootSignature, XMFLOAT2 xmf2Size, int iTexIndex, int CBIndex);
	virtual ~CShieldObject();
	virtual void UpdateCB(FrameResource* pFrameResource);
	virtual void Animate(float fTimeElapsed);
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);

};