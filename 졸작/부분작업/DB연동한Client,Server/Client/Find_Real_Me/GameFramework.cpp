#include "stdafx.h"
#include "GameFramework.h"
#include "Network.h"
#include <chrono>
using namespace chrono;

concurrency::concurrent_unordered_map<unsigned short, CPlayer*>GameFramework::m_ccumPlayer;
concurrency::concurrent_unordered_map<unsigned short, CGameObject*>GameFramework::m_ccumMonster;
concurrency::concurrent_unordered_set<unsigned short>GameFramework::m_ccusLoginList;
CBossDragonObject* GameFramework::m_pBoss = nullptr;
std::recursive_mutex GameFramework::m_mMutex;

bool GameFramework::GetStartLoop()
{
	return CNetwork::GetInstance()->GetLoopStart();

	//return true;
}
void GameFramework::InitNetWork()
{
	CNetwork::GetInstance();
}

void GameFramework::StopBGM()
{
	m_pSound->stop(0);
}

void GameFramework::SetNewBGM()
{
	m_pSound->play(CNetwork::GetInstance()->GetCurrentMap());
}

bool GameFramework::GetPlayerSet()
{
	if (CNetwork::GetInstance()->GetCurrentMap()) {
		for (auto& au : GameFramework::m_ccumPlayer) {
			au.second->SetPlayerUpdatedContext(nullptr);
		}
	}
	else {
		for (auto& au : GameFramework::m_ccumPlayer) {
			au.second->SetPlayerUpdatedContext(m_pScene->GetTerrain());
		}
	}
	return m_bSetPlayer;
}

void GameFramework::SetCharacterType(char cType, unsigned short usID)
{
	if (GameFramework::m_ccumPlayer[usID]->GetCharacterSet()) return;

	if (cType == e_ObjectType::e_Player_Man) {

		FlushCommandQueue();
		ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));
		//m_pCamera->SetOffset(XMFLOAT3(0.0f, 1500.0f, -1000.0f));
		GameFramework::m_ccumPlayer[usID]->SetCharacterInfos(m_mModelData["SwordMan"]->m_pParentMesh,
			m_mModelData["SwordMan"]->m_vChildMesh, m_mModelData["SwordMan"]->m_vNames,
			m_mModelData["SwordMan"]->m_mDivisionAnimationClip);
		
		ThrowIfFailed(m_pCommandList->Close());
		ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
		m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);
		FlushCommandQueue();

	}
	else if(cType == e_ObjectType::e_Player_Woman){

		FlushCommandQueue();
		ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));
		//m_pCamera->SetOffset(XMFLOAT3(0.0f, 300.0f, -500.0f));
		GameFramework::m_ccumPlayer[usID]->SetCharacterInfos(m_mModelData["SwordGirl"]->m_pParentMesh,
			m_mModelData["SwordGirl"]->m_vChildMesh, m_mModelData["SwordGirl"]->m_vNames,
			m_mModelData["SwordGirl"]->m_mDivisionAnimationClip, XMFLOAT3(3.f, 3.f, 3.f), false);
		
		ThrowIfFailed(m_pCommandList->Close());
		ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
		m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);
		FlushCommandQueue();

	}

	GameFramework::m_ccumPlayer[usID]->SetCharacterSet(true);
}

void GameFramework::RunNetWork()
{
	m_tNetworkThread = std::make_shared<std::thread>([]() {CNetwork::GetInstance()->Recvpacket(); });
}

GameFramework::GameFramework()
{
	m_WndClientWidth = FRAME_BUFFER_WIDTH;
	m_WndClientHeight = FRAME_BUFFER_HEIGHT;
}

float GameFramework::AspectRatio()const
{
	return static_cast<float>(FRAME_BUFFER_WIDTH) / FRAME_BUFFER_HEIGHT;
}

bool GameFramework::OnCreate(HINSTANCE hInstance, HWND hMainWnd)
{
	m_hAppInst = hInstance;
	m_hMainWnd = hMainWnd;

	CreateDirect3DDevice();
	CreateCommandQueueAndList();
#ifdef _WITH_DIRECT2D
	CreateDirect2DDevice();
#endif
	CreateRtvAndDsvDescriptorHeaps();
	CreateSwapChain();

	CreateDepthStencilView();

	BuildObjects();
	return(true);
}

void GameFramework::OnDestroy()
{
	FlushCommandQueue();

	CNetwork::GetInstance()->SetClient(true);
	/////////////////////////////
	//TODO: FOR NETWORK
	////////////////////////////
	m_tNetworkThread->join();

	CNetwork::GetInstance()->DisConnect();
	CNetwork::DestoryInstance();

	if (m_pSound) {
		delete m_pSound;
		m_pSound = nullptr;
	}

	if (m_pScene) {
		m_pScene->ReleaseObjects();
		delete m_pScene;
	}

	for (auto& au : GameFramework::m_ccumMonster) {
		if (au.second) {
			delete au.second;
			au.second = nullptr;
		}
	}

	if (m_pBoss) {
		delete m_pBoss;
		m_pBoss = nullptr;
	}
	if (m_pSkinnedObjectShader) {
		m_pSkinnedObjectShader->Release();
		m_pSkinnedObjectShader = nullptr;
	}

	// 메모리릭 때문에 추가.
	if (m_pPlayer) {
		if (m_pPlayer->GetMesh(0) == m_mModelData["SwordMan"]->m_pParentMesh) {
			m_mModelData["SwordGirl"]->DeleteMeshes();
		}
		if (m_pPlayer->GetMesh(0) == m_mModelData["SwordGirl"]->m_pParentMesh) {
			m_mModelData["SwordMan"]->DeleteMeshes();
		}
	}

	for (auto& a : m_ccumPlayer) {
		if (a.second) {
			delete a.second;
			a.second = nullptr;
		}
	}
	m_ccumPlayer.clear();

	if (CPlayer::m_pSkinnedObjectShader) {
		delete CPlayer::m_pSkinnedObjectShader;
		CPlayer::m_pSkinnedObjectShader = nullptr;
	}

	if (m_ppNpcObjects) {
		for (int i = 0; i < m_nNpcObjects; ++i) {
			if (m_ppNpcObjects[i]) {
				delete m_ppNpcObjects[i];
				m_ppNpcObjects[i] = nullptr;
			}
		}
		delete[] m_ppNpcObjects;
		m_ppNpcObjects = nullptr;
	}

	for (auto& data : m_mModelData) {
		if (data.second) {
			delete data.second;
			data.second = nullptr;
		}
	}
	m_mModelData.clear();

	if (m_TextureMgr) delete m_TextureMgr;

	if (m_pUIShader) {

		delete m_pUIShader;
		m_pUIShader = nullptr;
	}

	if (m_pMinimapRectShader) {
		delete m_pMinimapRectShader;
		m_pMinimapRectShader = nullptr;
	}
	if (m_pMinimapObjectShader) {
		delete m_pMinimapObjectShader;
		m_pMinimapObjectShader = nullptr;
	}
	if (m_ppMinimapPlayers) {
		for (int i = 0; i < m_nPlayers; ++i) {
			if (m_ppMinimapPlayers[i]) {
				delete m_ppMinimapPlayers[i];
				m_ppMinimapPlayers[i] = nullptr;
			}
		}
		delete[] m_ppMinimapPlayers;
		m_ppMinimapPlayers = nullptr;
	}
	if (m_ppMinimapMonsters) {
		for (int i = 0; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (m_ppMinimapMonsters[i]) {
				delete m_ppMinimapMonsters[i];
				m_ppMinimapMonsters[i] = nullptr;
			}
		}
		delete[] m_ppMinimapMonsters;
		m_ppMinimapMonsters = nullptr;
	}
	if (m_pMinimapBoss) {
		delete m_pMinimapBoss;
		m_pMinimapBoss = nullptr;
	}

	if (m_pTmp) {
		m_pTmp->ReleaseObjects();
		m_pTmp->Release();
		m_pTmp = nullptr;
	}
#ifdef _WITH_DIRECT2D
	if (m_pd2dbrBackground) m_pd2dbrBackground->Release();
	if (m_pd2dbrBorder) m_pd2dbrBorder->Release();
	if (m_pdwFont) m_pdwFont->Release();
	if (m_pdwTextLayout) m_pdwTextLayout->Release();
	if (m_pd2dbrText) m_pd2dbrText->Release();

	if (m_pd2dDeviceContext) m_pd2dDeviceContext->Release();
	if (m_pd2dDevice) m_pd2dDevice->Release();
	if (m_pdWriteFactory) m_pdWriteFactory->Release();
	if (m_pd3d11On12Device) m_pd3d11On12Device->Release();
	if (m_pd3d11DeviceContext) m_pd3d11DeviceContext->Release();
	if (m_pd2dFactory) m_pd2dFactory->Release();

	for (int i = 0; i < m_SwapChainBufferCount; i++)
	{
		if (m_ppd3d11WrappedBackBuffers[i]) m_ppd3d11WrappedBackBuffers[i]->Release();
		if (m_ppd2dRenderTargets[i]) m_ppd2dRenderTargets[i]->Release();
	}
#endif
}

void GameFramework::CreateDirect3DDevice()
{
#if defined(DEBUG) || defined(_DEBUG) 
	// 디버그 층 활성화
	{
		ComPtr<ID3D12Debug> debugController;

		ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)));
		debugController->EnableDebugLayer();
	}
#endif

	ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&m_pDxgiFactory)));

	// 하드웨어 어댑터를 나타내는 장치를 생성해 본다.
	HRESULT hardwareResult = D3D12CreateDevice(
		nullptr,             // 기본.
		D3D_FEATURE_LEVEL_11_0,
		IID_PPV_ARGS(&m_pD3dDevice));

	// 실패했으면 WARP device생성
	if (FAILED(hardwareResult))
	{
		ComPtr<IDXGIAdapter> pWarpAdapter;
		ThrowIfFailed(m_pDxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&pWarpAdapter)));

		ThrowIfFailed(D3D12CreateDevice(
			pWarpAdapter.Get(),
			D3D_FEATURE_LEVEL_11_0,
			IID_PPV_ARGS(&m_pD3dDevice)));
	}

	ThrowIfFailed(m_pD3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE,
		IID_PPV_ARGS(&m_pFence)));

	m_RtvDescriptorSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_DsvDescriptorSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	gnCbvSrvUavDescriptorIncrementSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	// Check 4X MSAA quality support for our back buffer format.
	// All Direct3D 11 capable devices support 4X MSAA for all render 
	// target formats, so we only need to check quality support.
	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msQualityLevels;
	msQualityLevels.Format = m_BackBufferFormat;
	msQualityLevels.SampleCount = 4;
	msQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	msQualityLevels.NumQualityLevels = 0;
	ThrowIfFailed(m_pD3dDevice->CheckFeatureSupport(
		D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS,
		&msQualityLevels,
		sizeof(msQualityLevels)));

	m_4xMsaaQuality = msQualityLevels.NumQualityLevels;
	assert(m_4xMsaaQuality > 0 && "Unexpected MSAA quality level.");

	//m_pD3dDevice->QueryInterface(IID_PPV_ARGS(&device));
	//device->ReportLiveDeviceObjects(D3D12_RLDO_DETAIL);
}

void GameFramework::CreateSwapChain()
{
	// 새 스왑체인을 만들기 전에 먼저 기존 스왑체인을 해제.
	m_pSwapChain.Reset();

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = m_WndClientWidth;
	sd.BufferDesc.Height = m_WndClientHeight;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = m_BackBufferFormat;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.SampleDesc.Count = m_is4xMsaaState ? 4 : 1;
	sd.SampleDesc.Quality = m_is4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = m_SwapChainBufferCount;
	sd.OutputWindow = m_hMainWnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	// Note: 스왑체인은  명령큐를 이용해 방출(flush)를 수행한다.
	ThrowIfFailed(m_pDxgiFactory->CreateSwapChain(
		m_pCommandQueue.Get(),
		&sd,	
		m_pSwapChain.GetAddressOf()));
}

void GameFramework::CreateCommandQueueAndList()
{
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	ThrowIfFailed(m_pD3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue)));

	ThrowIfFailed(m_pD3dDevice->CreateCommandAllocator(
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		IID_PPV_ARGS(m_pDirectCmdListAlloc.GetAddressOf())));

	ThrowIfFailed(m_pD3dDevice->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		m_pDirectCmdListAlloc.Get(), // 연관된 명령 할당자.
		nullptr,                   // 초기 파이프라인 상태 객체
		IID_PPV_ARGS(m_pCommandList.GetAddressOf())));

	// 닫힌 상태로 시작한다. 이후 명령 리스트를 처음 참조할 때 Reset을 호출하는데
	// Reset을 호출하려면 명령 리스트가 닫혀 있어야 한다.
	ThrowIfFailed(m_pCommandList->Close());
}

void GameFramework::CreateRtvAndDsvDescriptorHeaps()
{
	// 다중렌더타겟할라면 NumDescriptors 수정하셈.
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
	rtvHeapDesc.NumDescriptors = m_SwapChainBufferCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	rtvHeapDesc.NodeMask = 0;
	ThrowIfFailed(m_pD3dDevice->CreateDescriptorHeap(
		&rtvHeapDesc, IID_PPV_ARGS(m_pRtvHeap.GetAddressOf())));

	// 그림자 때문에.
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
	dsvHeapDesc.NumDescriptors = 2;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dsvHeapDesc.NodeMask = 0;
	ThrowIfFailed(m_pD3dDevice->CreateDescriptorHeap(
		&dsvHeapDesc, IID_PPV_ARGS(m_pDsvHeap.GetAddressOf())));
}

void GameFramework::CreateRenderTargetViews()
{
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_pRtvHeap->GetCPUDescriptorHandleForHeapStart());
	for (UINT i = 0; i < m_SwapChainBufferCount; ++i)
	{
		ThrowIfFailed(m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(&m_pSwapChainBuffer[i])));
		m_pD3dDevice->CreateRenderTargetView(m_pSwapChainBuffer[i].Get(), nullptr, rtvHeapHandle);
		rtvHeapHandle.Offset(1, m_RtvDescriptorSize);
	}
#ifdef _WITH_DIRECT2D
	float fxDPI, fyDPI;
	m_pd2dFactory->GetDesktopDpi(&fxDPI, &fyDPI); //UINT GetDpiForWindow(HWND hwnd);

	D2D1_BITMAP_PROPERTIES1 d2dBitmapProperties = D2D1::BitmapProperties1(D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), fxDPI, fyDPI);

	for (UINT i = 0; i < m_SwapChainBufferCount; i++)
	{
		D3D11_RESOURCE_FLAGS d3d11Flags = { D3D11_BIND_RENDER_TARGET };
		m_pd3d11On12Device->CreateWrappedResource(m_pSwapChainBuffer[i].Get(), &d3d11Flags, D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT, __uuidof(ID3D11Resource), (void **)&m_ppd3d11WrappedBackBuffers[i]);

		IDXGISurface *pdxgiSurface = NULL;
		m_ppd3d11WrappedBackBuffers[i]->QueryInterface(__uuidof(IDXGISurface), (void **)&pdxgiSurface);
		m_pd2dDeviceContext->CreateBitmapFromDxgiSurface(pdxgiSurface, &d2dBitmapProperties, &m_ppd2dRenderTargets[i]);
		if (pdxgiSurface) pdxgiSurface->Release();
	}
#endif
}

void GameFramework::CreateDepthStencilView()
{
	// Create the depth/stencil buffer and view.
	D3D12_RESOURCE_DESC depthStencilDesc;
	depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	depthStencilDesc.Alignment = 0;
	depthStencilDesc.Width = m_WndClientWidth;
	depthStencilDesc.Height = m_WndClientHeight;
	depthStencilDesc.DepthOrArraySize = 1;
	depthStencilDesc.MipLevels = 1;

	depthStencilDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;

	depthStencilDesc.SampleDesc.Count = m_is4xMsaaState ? 4 : 1;
	depthStencilDesc.SampleDesc.Quality = m_is4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE optClear;
	optClear.Format = m_DepthStencilFormat;
	optClear.DepthStencil.Depth = 1.0f;
	optClear.DepthStencil.Stencil = 0;
	ThrowIfFailed(m_pD3dDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthStencilDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&optClear,
		IID_PPV_ARGS(m_pDepthStencilBuffer.GetAddressOf())));

	// Create descriptor to mip level 0 of entire resource using the format of the resource.
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = m_DepthStencilFormat;
	dsvDesc.Texture2D.MipSlice = 0;
	m_pD3dDevice->CreateDepthStencilView(m_pDepthStencilBuffer.Get(), &dsvDesc, GetDepthStencilView());
}

void GameFramework::OnResizeBackBuffers()
{
	assert(m_pD3dDevice);
	assert(m_pSwapChain);
	assert(m_pDirectCmdListAlloc);

	// Flush before changing any resources.
	FlushCommandQueue();

	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

	// Release the previous resources we will be recreating.
	for (int i = 0; i < m_SwapChainBufferCount; ++i)
		m_pSwapChainBuffer[i].Reset();
	m_pDepthStencilBuffer.Reset();


#ifndef _WITH_DIRECT2D
	// Resize the swap chain.
	ThrowIfFailed(m_pSwapChain->ResizeBuffers(
		m_SwapChainBufferCount,
		m_WndClientWidth, m_WndClientHeight,
		m_BackBufferFormat,
		DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));
#endif


	m_CurrBackBuffer = 0;

	CreateRenderTargetViews();
	CreateDepthStencilView();
	// Execute the resize commands.
	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	// Wait until resize is complete.
	FlushCommandQueue();

}

void GameFramework::FlushCommandQueue()
{
	// 현재 펜스 지점까지의 명령들을 표시하도록 펜스 값을 증가시킨다.
	m_CurrentFence++;

	// 새 펜스 지점을 설정하는 명령(Signal)을 명령 대기열에 추가한다.
	// 지금 우리는 gpu 시간선에 있으므로, 새 펜스 지점은 gpu가 Signal 명령까지의
	// 모든 명령을 처리하기 전까지는 설정되지 않는다.
	ThrowIfFailed(m_pCommandQueue->Signal(m_pFence.Get(), m_CurrentFence));

	// GPU가 이 펜스 지점까지의 명령들을 완료할 때까지 기다린다.
	if (m_pFence->GetCompletedValue() < m_CurrentFence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);

		// 도달했으면 이벤트를 발생시킨다. 
		ThrowIfFailed(m_pFence->SetEventOnCompletion(m_CurrentFence, eventHandle));

		// GPU가 현재 펜스 지점에 도달했음을 뜻하는 이벤트를 기다린다.
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}
}

ID3D12Resource* GameFramework::GetCurrentBackBuffer()const
{
	return m_pSwapChainBuffer[m_CurrBackBuffer].Get();
}

D3D12_CPU_DESCRIPTOR_HANDLE GameFramework::GetCurrentBackBufferView()const
{
	return CD3DX12_CPU_DESCRIPTOR_HANDLE(
		m_pRtvHeap->GetCPUDescriptorHandleForHeapStart(),
		m_CurrBackBuffer,
		m_RtvDescriptorSize);
}

D3D12_CPU_DESCRIPTOR_HANDLE GameFramework::GetDepthStencilView()const
{
	return m_pDsvHeap->GetCPUDescriptorHandleForHeapStart();
}

void GameFramework::CalculateFrameStats()
{
	// Code computes the average frames per second, and also the 
	// average time it takes to render one frame.  These stats 
	// are appended to the window caption bar.

	static int frameCnt = 0;
	static float timeElapsed = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if ((m_Timer.GetTotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;

		wstring fpsStr = to_wstring(fps);
		wstring mspfStr = to_wstring(mspf);

		wstring windowText = m_MainWndCaption +
			L"    fps: " + fpsStr +
			L"   mspf: " + mspfStr + 
			L"   " + to_wstring(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetPosition().x) +
			L"," + to_wstring(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetPosition().y) +
			L"," + to_wstring(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetPosition().z) + L"FrameTime : " + to_wstring(m_Timer.GetDeltaTime());

		SetWindowText(m_hMainWnd, windowText.c_str());

		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}

void GameFramework::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		::SetCapture(hWnd);
		::GetCursorPos(&m_OldCursorPos);
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		::ReleaseCapture();
		break;
	case WM_MOUSEMOVE:
		break;
	default:
		break;
	}
}

void GameFramework::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_KEYUP:
		switch (wParam)
		{
		case VK_ESCAPE:

			::PostQuitMessage(0);
			break;
		case VK_RETURN:
			break;
		case VK_CONTROL: {
			//static int i = 0;
			//if (i != 0) break;
			//cout << "여캐 1 , 남캐 2" << endl;
			//cin >> i;
			//FlushCommandQueue();
			//ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

			////i % 2 ? m_pCamera->SetOffset(XMFLOAT3(0.0f, 300.0f, -500.0f)) : m_pCamera->SetOffset(XMFLOAT3(0.0f, 1500.0f, -1000.0f));
			//i % 2 ? m_pPlayer->SetCharacterInfos(m_mModelData["SwordGirl"]->m_pParentMesh,
			//	m_mModelData["SwordGirl"]->m_vChildMesh, m_mModelData["SwordGirl"]->m_vNames,
			//	m_mModelData["SwordGirl"]->m_mDivisionAnimationClip, XMFLOAT3(3.f, 3.f, 3.f), false)
			//	: m_pPlayer->SetCharacterInfos(m_mModelData["SwordMan"]->m_pParentMesh,
			//		m_mModelData["SwordMan"]->m_vChildMesh, m_mModelData["SwordMan"]->m_vNames,
			//		m_mModelData["SwordMan"]->m_mDivisionAnimationClip);

			//ThrowIfFailed(m_pCommandList->Close());
			//ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
			//m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);
			//FlushCommandQueue();



			/*FlushCommandQueue();
			ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

			m_pCamera->SetOffset(XMFLOAT3(0.0f, 300.0f, -500.0f));
			m_pPlayer->SetCharacterInfos(m_mModelData["SwordGirl"]->m_pParentMesh,
				m_mModelData["SwordGirl"]->m_vChildMesh, m_mModelData["SwordGirl"]->m_vNames,
				m_mModelData["SwordGirl"]->m_mDivisionAnimationClip, XMFLOAT3(3.f, 3.f, 3.f), false);

			ThrowIfFailed(m_pCommandList->Close());
			ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
			m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);
			FlushCommandQueue();*/
			break;
		}
		case VK_F1:
		case VK_F2:
		case VK_F3:
			m_pCamera = GameFramework::m_ccumPlayer[CNetwork::m_usId]->ChangeCamera((DWORD)(wParam - VK_F1 + 1), m_Timer.GetDeltaTime());
			break;
		case VK_F9:
		{
			BOOL bFullScreenState = FALSE;
			m_pSwapChain->GetFullscreenState(&bFullScreenState, NULL);
			m_pSwapChain->SetFullscreenState(!bFullScreenState, NULL);

			OnResizeBackBuffers();
			break;
		}
		case 0x5A:	//z
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("attack1"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			CNetwork::GetInstance()->SendAttackPacket();
			break;
		case 0x58:	//x
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("attack2"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			CNetwork::GetInstance()->SendAttack2Packet();
			break;
		case 0x31:	//1
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("skill1"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			CNetwork::GetInstance()->SendSkill1Packet();
			break;
		case 0x32:	//2
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("skill2"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			CNetwork::GetInstance()->SendSkill2Packet();
			break;
		case 0x33:	//3
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("skill3"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			CNetwork::GetInstance()->SendSkill3Packet();
			break;
		case 0x34:	//4
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("skill4"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			if (GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->GetObjectType() == e_ObjectType::e_Player_Woman) break;
			CNetwork::GetInstance()->SendSkill4Packet();
			break;
		case 0x35:	//5
			/////////////////////////////
			//TODO: DELETE FOR NETWORK
			////////////////////////////
			//m_pPlayer->SetNowAnimation(string("skill5"));

			/////////////////////////////
			//TODO: FOR NETWORK
			////////////////////////////
			//CNetwork::GetInstance()->SendDirPacket();
			if (GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->GetObjectType() == e_ObjectType::e_Player_Woman) break;
			CNetwork::GetInstance()->SendSkill5Packet();
			break;
		case 0x36:	//6
			/*cout << GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetHP() << endl;
			GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetHP(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetHP() - 10);
			cout << GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetHP() << endl;*/
			CNetwork::GetInstance()->SendForDevMapSwapPacket();
			break;
		case 0x37:	//7
			/*cout << GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetMP() << endl;
			GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetMP(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetMP() - 10);
			cout << GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetMP() << endl;*/
			CNetwork::GetInstance()->SendForDevCharacterInvincibility();
			break;
		case 0x38:	//8
			/*cout << GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetEXP() << endl;
			GameFramework::m_ccumPlayer[CNetwork::m_usId]->SetEXP(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetEXP() + 10);
			cout << GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetEXP() << endl;*/
			break;
		case 0x43:	//c
			m_pPlayer->SetNowAnimation(string("stun"));
			break;
		case 0x56:	//v
			m_pPlayer->SetNowAnimation(string("damage"));
			break;
		case 0x42:	//b
			m_pPlayer->SetNowAnimation(string("die"));
			break;
		case VK_SHIFT:
			/*CNetwork::GetInstance()->SetCurrentMap();
			if (CNetwork::GetInstance()->GetCurrentMap()) {
				for (auto& au : GameFramework::m_ccumPlayer) {
					au.second->SetPlayerUpdatedContext(nullptr);
				}
			}
			else {
				for (auto& au : GameFramework::m_ccumPlayer) {
					au.second->SetPlayerUpdatedContext(m_pScene->GetTerrain());
				}
			}*/
			CNetwork::GetInstance()->SendMapSwapPacket();
			//++gnCurrentMapIndex %= 4;
			//m_pSound->stop(0);
			//m_pSound->play(gnCurrentMapIndex);
			break;
		default:
			m_pScene->OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
			break;
		}
		break;
	default:
		break;
	}
}

LRESULT CALLBACK GameFramework::OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_ACTIVATE:
	{
		if (LOWORD(wParam) == WA_INACTIVE)
			m_Timer.Stop();
		else
			m_Timer.Start();
		break;
	}
	case WM_SIZE:
	{
		m_WndClientWidth = LOWORD(lParam);
		m_WndClientHeight = HIWORD(lParam);

		if (m_WndClientWidth != 0 && m_WndClientHeight != 0)
			OnResizeBackBuffers();
		break;
	}
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam);
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
		break;
	}
	return(0);
}

void GameFramework::ModelLoad()
{
	ModelData* pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Character/SwordMan/SwordMan3.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Run_.fbx", "walk", 0.8f, true);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Idle2.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Attack1.fbx", "attack1", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Attack2.fbx", "attack2", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill1.fbx", "skill1", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill2.fbx", "skill2", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill3.fbx", "skill3", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill4.fbx", "skill4", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill5.fbx", "skill5", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Stun_.fbx", "stun", 0.8f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Damage.fbx", "damage", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Die_.fbx", "die", 0.5f, false);
	m_mModelData["SwordMan"] = pData;

	pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Character/SwordGirl/SwordGirl01.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Attack01.fbx", "attack1", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Attack02.fbx", "attack2", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Damage.fbx", "damage", 0.5f, false);
	//pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Dash.fbx", "dash", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Die.fbx", "die", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Idle02.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Run.fbx", "walk", 0.8f, true);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Skill01.fbx", "skill1", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Skill02.fbx", "skill2", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Skill03.fbx", "skill3", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordGirl/Animation/SwordGirl@Stun.fbx", "stun", 0.8f, false);
	m_mModelData["SwordGirl"] = pData;

	// npc
	pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Npc/Scarecrow/Scarecrow.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Npc/Scarecrow/Animation/Scarecrow@Dance.fbx", "dance", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Scarecrow/Animation/Scarecrow@Idle.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Scarecrow/Animation/Scarecrow@Jump.fbx", "jump", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Scarecrow/Animation/Scarecrow@Sad.fbx", "sad", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Scarecrow/Animation/Scarecrow@Spin.fbx", "spin", 0.5f, true);
	m_mModelData["Scarecrow"] = pData;

	pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Npc/Archer_Female/Archer_Female01.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Npc/Archer_Female/Animation/Archer_Female@Idle.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Female/Animation/Archer_Female@Pick.fbx", "pick", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Female/Animation/Archer_Female@RideIdle.fbx", "rideIdle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Female/Animation/Archer_Female@Run.fbx", "run", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Female/Animation/Archer_Female@Sit.fbx", "sit", 0.5f, true);
	m_mModelData["Archer_Female"] = pData;

	pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Npc/Archer_Male/Archer_Male.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Npc/Archer_Male/Animation/Archer_Male@Idle.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Male/Animation/Archer_Male@Pick.fbx", "pick", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Male/Animation/Archer_Male@RideIdle.fbx", "rideIdle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Male/Animation/Archer_Male@Run.fbx", "run", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Archer_Male/Animation/Archer_Male@Sit.fbx", "sit", 0.5f, true);
	m_mModelData["Archer_Male"] = pData;

	pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Npc/Mage_Female/Mage_Female.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Npc/Mage_Female/Animation/Mage_Female@Idle.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Female/Animation/Mage_Female@Pick.fbx", "pick", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Female/Animation/Mage_Female@RideIdle.fbx", "rideIdle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Female/Animation/Mage_Female@Run.fbx", "run", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Female/Animation/Mage_Female@Sit.fbx", "sit", 0.5f, true);
	m_mModelData["Mage_Female"] = pData;

	pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Npc/Mage_Male/Mage_Male.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Npc/Mage_Male/Animation/Mage_Male@Idle.fbx", "idle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Male/Animation/Mage_Male@Pick.fbx", "pick", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Male/Animation/Mage_Male@RideIdle.fbx", "rideIdle", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Male/Animation/Mage_Male@Run.fbx", "run", 0.5f, true);
	pData->AnimationLoad("../Model/Npc/Mage_Male/Animation/Mage_Male@Sit.fbx", "sit", 0.5f, true);
	m_mModelData["Mage_Male"] = pData;
	// 몬스터들
	string strModelFilenames[] = {
"../Model/Monster/Peon/peon.fbx", "../Model/Monster/Grunt/Grunt.fbx", "../Model/Monster/Lord/Lord.fbx"
	};

	unordered_map<string, string> PeonAnimaionFiles;
	PeonAnimaionFiles["attack1"] = "../Model/Monster/Peon/peon@attack01.fbx";
	PeonAnimaionFiles["attack2"] = "../Model/Monster/Peon/peon@attack02.fbx";
	PeonAnimaionFiles["die"] = "../Model/Monster/Peon/peon@die.fbx";
	PeonAnimaionFiles["hit"] = "../Model/Monster/Peon/peon@hit.fbx";
	PeonAnimaionFiles["idle"] = "../Model/Monster/Peon/peon@idle.fbx";
	PeonAnimaionFiles["run"] = "../Model/Monster/Peon/peon@run.fbx";
	PeonAnimaionFiles["taunt"] = "../Model/Monster/Peon/peon@taunt.fbx";
	PeonAnimaionFiles["walk"] = "../Model/Monster/Peon/peon@walk.fbx";

	unordered_map<string, string> GruntAnimaionFiles;
	GruntAnimaionFiles["attack1"] = "../Model/Monster/Grunt/Grunt@attack01.fbx";
	GruntAnimaionFiles["die"] = "../Model/Monster/Grunt/Grunt@die.fbx";
	GruntAnimaionFiles["hit"] = "../Model/Monster/Grunt/Grunt@hit.fbx";
	GruntAnimaionFiles["idle"] = "../Model/Monster/Grunt/Grunt@idle.fbx";
	GruntAnimaionFiles["run"] = "../Model/Monster/Grunt/Grunt@run.fbx";
	GruntAnimaionFiles["taunt"] = "../Model/Monster/Grunt/Grunt@taunt.fbx";
	GruntAnimaionFiles["walk"] = "../Model/Monster/Grunt/Grunt@walk.fbx";

	unordered_map<string, string> LordAnimaionFiles;
	LordAnimaionFiles["attack1"] = "../Model/Monster/Lord/Lord@attack01.fbx";
	LordAnimaionFiles["attack2"] = "../Model/Monster/Lord/Lord@attack02.fbx";
	LordAnimaionFiles["die"] = "../Model/Monster/Lord/Lord@die.fbx";
	LordAnimaionFiles["hit"] = "../Model/Monster/Lord/Lord@hit.fbx";
	LordAnimaionFiles["idle"] = "../Model/Monster/Lord/Lord@idle.fbx";
	LordAnimaionFiles["run"] = "../Model/Monster/Lord/Lord@run.fbx";
	LordAnimaionFiles["taunt"] = "../Model/Monster/Lord/Lord@taunt.fbx";
	LordAnimaionFiles["walk"] = "../Model/Monster/Lord/Lord@walk.fbx";

	string strMonsternames[] = { "Peon", "Grunt", "Lord" };
	// peon
	pData = new ModelData();
	pData->ModelLoadAndMeshLoad(strModelFilenames[0], m_pD3dDevice.Get(), m_pCommandList.Get());
	for (auto& obj : PeonAnimaionFiles) {
		pData->AnimationLoad(obj.second, obj.first, 0.8f, true);
	}
	m_mModelData.insert(pair<string, ModelData*>(strMonsternames[0], pData));
	// grunt
	pData = new ModelData();
	pData->ModelLoadAndMeshLoad(strModelFilenames[1], m_pD3dDevice.Get(), m_pCommandList.Get());
	for (auto& obj : GruntAnimaionFiles) {
		pData->AnimationLoad(obj.second, obj.first, 0.8f, true);
	}
	m_mModelData.insert(pair<string, ModelData*>(strMonsternames[1], pData));
	// lord
	pData = new ModelData();
	pData->ModelLoadAndMeshLoad(strModelFilenames[2], m_pD3dDevice.Get(), m_pCommandList.Get());
	for (auto& obj : LordAnimaionFiles) {
		pData->AnimationLoad(obj.second, obj.first, 0.8f, true);
	}
	m_mModelData.insert(pair<string, ModelData*>(strMonsternames[2], pData));
}

void GameFramework::BuildMonsters()
{
	// 몬스터
	CMaterial* pMaterial = new CMaterial();
	m_nNormalDungeonObjects = 244;
	m_nBossDungeonObjects = 48;
	//m_ppMonsterObjects = new CGameObject*[m_nNormalDungeonObjects + m_nBossDungeonObjects];
	m_pBoss = new CBossDragonObject();

	// 일반던전 몬스터.
	ifstream in("test/peon.txt");
	XMFLOAT3 pos;
	unsigned short index = 0;
	for (int i = 0; i < 96; ++i) {
		CSkinAndDivisionObject* pPeon = new CSkinAndDivisionObject();
		pPeon->SetTextureIndex(XMUINT2(38, 39));
		pPeon->SetMesh(0, m_mModelData["Peon"]->m_pParentMesh);
		pPeon->SetChilds(m_pD3dDevice.Get(), m_mModelData["Peon"]->m_vChildMesh, m_mModelData["Peon"]->m_vNames);
		pPeon->SetAnimClip(m_mModelData["Peon"]->m_mDivisionAnimationClip);
		pPeon->SetScale(8.f, 8.f, 8.f);
		//in >> pos.x >> pos.y >> pos.z;
		//pPeon->SetPosition(pos);
		pPeon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pPeon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pPeon->SetMaterial(0, pMaterial);
		pPeon->m_strObjectName = "Peon";
		pPeon->SetMaxHP(100);
		pPeon->SetHP(100);
		pPeon->SetHpSize(100);
		pPeon->SetObjectCBIndex(::gnObjCBindex++);
		pPeon->SetSkinCBIndex(::gnSkinCBindex++);

		pPeon->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		pPeon->CreateHpBar(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		m_ccumMonster.insert(std::make_pair(index++, pPeon));
		//m_ppMonsterObjects[index++] = pPeon;
	}
	in.close();
	in.open("test/grunt.txt");
	for (int i = 0; i < 85; ++i) {
		CSkinAndDivisionObject* pGrunt = new CSkinAndDivisionObject();
		pGrunt->SetTextureIndex(XMUINT2(34, 35));
		pGrunt->SetMesh(0, m_mModelData["Grunt"]->m_pParentMesh);
		pGrunt->SetChilds(m_pD3dDevice.Get(), m_mModelData["Grunt"]->m_vChildMesh, m_mModelData["Grunt"]->m_vNames);
		pGrunt->SetAnimClip(m_mModelData["Grunt"]->m_mDivisionAnimationClip);
		pGrunt->SetScale(10.f, 10.f, 10.f);
		//in >> pos.x >> pos.y >> pos.z;
		//pGrunt->SetPosition(pos);
		pGrunt->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pGrunt->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pGrunt->SetMaterial(0, pMaterial);
		pGrunt->m_strObjectName = "Grunt";
		pGrunt->SetMaxHP(200);
		pGrunt->SetHP(200);
		pGrunt->SetHpSize(200);
		pGrunt->SetObjectCBIndex(::gnObjCBindex++);
		pGrunt->SetSkinCBIndex(::gnSkinCBindex++);

		pGrunt->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		pGrunt->CreateHpBar(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		m_ccumMonster.insert(std::make_pair(index++, pGrunt));
		//m_ppMonsterObjects[index++] = pGrunt;
	}
	in.close();
	in.open("test/lord.txt");
	for (int i = 0; i < 63; ++i) {
		CSkinAndDivisionObject* pLord = new CSkinAndDivisionObject();
		pLord->SetTextureIndex(XMUINT2(42, 43));
		pLord->SetMesh(0, m_mModelData["Lord"]->m_pParentMesh);
		pLord->SetChilds(m_pD3dDevice.Get(), m_mModelData["Lord"]->m_vChildMesh, m_mModelData["Lord"]->m_vNames);
		pLord->SetAnimClip(m_mModelData["Lord"]->m_mDivisionAnimationClip);
		pLord->SetScale(12.f, 12.f, 12.f);
		//in >> pos.x >> pos.y >> pos.z;
		//pLord->SetPosition(pos);
		pLord->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pLord->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pLord->SetMaterial(0, pMaterial);
		pLord->m_strObjectName = "Lord";
		pLord->SetMaxHP(300);
		pLord->SetHP(300);
		pLord->SetHpSize(300);
		pLord->SetObjectCBIndex(::gnObjCBindex++);
		pLord->SetSkinCBIndex(::gnSkinCBindex++);

		pLord->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		pLord->CreateHpBar(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		m_ccumMonster.insert(std::make_pair(index++, pLord));
		//m_ppMonsterObjects[index++] = pLord;
	}
	in.close();


	// 보스던전 몬스터
	in.open("test/peonAlt.txt");
	for (int i = 0; i < 24; ++i) {
		CSkinAndDivisionObject* pPeon = new CSkinAndDivisionObject();
		pPeon->SetTextureIndex(XMUINT2(50, 51));
		pPeon->SetMesh(0, m_mModelData["Peon"]->m_pParentMesh);
		pPeon->SetChilds(m_pD3dDevice.Get(), m_mModelData["Peon"]->m_vChildMesh, m_mModelData["Peon"]->m_vNames);
		pPeon->SetAnimClip(m_mModelData["Peon"]->m_mDivisionAnimationClip);
		pPeon->SetScale(8.f, 8.f, 8.f);
		in >> pos.x >> pos.y >> pos.z;
		pPeon->SetPosition(pos);
		pPeon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pPeon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pPeon->SetMaterial(0, pMaterial);
		pPeon->m_strObjectName = "Peon";
		pPeon->SetMaxHP(150);
		pPeon->SetHP(150);
		pPeon->SetHpSize(150);
		pPeon->SetObjectCBIndex(::gnObjCBindex++);
		pPeon->SetSkinCBIndex(::gnSkinCBindex++);

		pPeon->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		pPeon->CreateHpBar(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		m_ccumMonster.insert(std::make_pair(index++, pPeon));
		//m_ppMonsterObjects[index++] = pPeon;
	}
	in.close();

	in.open("test/gruntAlt.txt");
	for (int i = 0; i < 14; ++i) {
		CSkinAndDivisionObject* pGrunt = new CSkinAndDivisionObject();
		pGrunt->SetTextureIndex(XMUINT2(46, 47));
		pGrunt->SetMesh(0, m_mModelData["Grunt"]->m_pParentMesh);
		pGrunt->SetChilds(m_pD3dDevice.Get(), m_mModelData["Grunt"]->m_vChildMesh, m_mModelData["Grunt"]->m_vNames);
		pGrunt->SetAnimClip(m_mModelData["Grunt"]->m_mDivisionAnimationClip);
		pGrunt->SetScale(10.f, 10.f, 10.f);
		in >> pos.x >> pos.y >> pos.z;
		pGrunt->SetPosition(pos);
		pGrunt->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pGrunt->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pGrunt->SetMaterial(0, pMaterial);
		pGrunt->m_strObjectName = "Grunt";
		pGrunt->SetMaxHP(250);
		pGrunt->SetHP(250);
		pGrunt->SetHpSize(250);
		pGrunt->SetObjectCBIndex(::gnObjCBindex++);
		pGrunt->SetSkinCBIndex(::gnSkinCBindex++);

		pGrunt->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		pGrunt->CreateHpBar(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		m_ccumMonster.insert(std::make_pair(index++, pGrunt));
		//m_ppMonsterObjects[index++] = pGrunt;
	}
	in.close();

	in.open("test/lordAlt.txt");
	for (int i = 0; i < 10; ++i) {
		CSkinAndDivisionObject* pLord = new CSkinAndDivisionObject();
		pLord->SetTextureIndex(XMUINT2(54, 55));
		pLord->SetMesh(0, m_mModelData["Lord"]->m_pParentMesh);
		pLord->SetChilds(m_pD3dDevice.Get(), m_mModelData["Lord"]->m_vChildMesh, m_mModelData["Lord"]->m_vNames);
		pLord->SetAnimClip(m_mModelData["Lord"]->m_mDivisionAnimationClip);
		pLord->SetScale(12.f, 12.f, 12.f);
		in >> pos.x >> pos.y >> pos.z;
		pLord->SetPosition(pos);
		pLord->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pLord->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pLord->SetMaterial(0, pMaterial);
		pLord->m_strObjectName = "Lord";
		pLord->SetMaxHP(350);
		pLord->SetHP(350);
		pLord->SetHpSize(350);
		pLord->SetObjectCBIndex(::gnObjCBindex++);
		pLord->SetSkinCBIndex(::gnSkinCBindex++);

		pLord->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		pLord->CreateHpBar(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
		m_ccumMonster.insert(std::make_pair(index++, pLord));
		//m_ppMonsterObjects[index++] = pLord;
	}
	in.close();

	m_pBoss->LoadModelAndInitCombinedAnimationClip("../Model/Monster/Dragon/DragonBoss.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	m_pBoss->SetTextureIndex(XMUINT2(58, 59));
	m_pBoss->SetPosition(XMFLOAT3(5000.f, 1000.f, 26500.f));
	m_pBoss->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.1f, m_pBoss->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	m_pBoss->SetMaterial(0, pMaterial);
	m_pBoss->m_strObjectName = "Boss";
	m_pBoss->SetScale(5.f, 5.f, 5.f);
	m_pBoss->Rotate(90.f, 0, 0);
	m_pBoss->SetObjectCBIndex(::gnObjCBindex++);
	m_pBoss->SetSkinCBIndex(::gnSkinCBindex++);

	m_pBoss->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
}

void GameFramework::BuildNpcs()
{
	CMaterial* pMaterial = new CMaterial();
	m_nNpcObjects = 5;

	m_ppNpcObjects = new CGameObject*[m_nNpcObjects];

	CNPCObject* pArcher = new CNPCObject(1);
	pArcher->SetTextureIndex(XMUINT2(80, 81));
	pArcher->SetMesh(0, m_mModelData["Archer_Female"]->m_pParentMesh);
	pArcher->SetChilds(m_pD3dDevice.Get(), m_mModelData["Archer_Female"]->m_vChildMesh, m_mModelData["Archer_Female"]->m_vNames);
	pArcher->SetAnimClip(m_mModelData["Archer_Female"]->m_mDivisionAnimationClip);
	pArcher->SetScale(1.f, 1.f, 1.f);
	pArcher->Rotate(90, 0, 0);
	pArcher->SetPosition(XMFLOAT3(4940, 510, 7800));
	pArcher->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pArcher->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	pArcher->SetMaterial(0, pMaterial);
	pArcher->m_strObjectName = "Archer_Female";
	pArcher->SetObjectCBIndex(::gnObjCBindex++);
	pArcher->SetSkinCBIndex(::gnSkinCBindex++);

	pArcher->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
	m_ppNpcObjects[0] = pArcher;


	pArcher = new CNPCObject(1);
	pArcher->SetTextureIndex(XMUINT2(88, 89));
	pArcher->SetMesh(0, m_mModelData["Archer_Male"]->m_pParentMesh);
	pArcher->SetChilds(m_pD3dDevice.Get(), m_mModelData["Archer_Male"]->m_vChildMesh, m_mModelData["Archer_Male"]->m_vNames);
	pArcher->SetAnimClip(m_mModelData["Archer_Male"]->m_mDivisionAnimationClip);
	pArcher->SetScale(1.f, 1.f, 1.f);
	pArcher->Rotate(90, 0, 0);
	pArcher->SetPosition(XMFLOAT3(7300, 510, 5700));
	pArcher->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pArcher->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	pArcher->SetMaterial(0, pMaterial);
	pArcher->m_strObjectName = "Archer_Male";
	pArcher->SetObjectCBIndex(::gnObjCBindex++);
	pArcher->SetSkinCBIndex(::gnSkinCBindex++);

	pArcher->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
	m_ppNpcObjects[1] = pArcher;

	pArcher = new CNPCObject(1);
	pArcher->SetTextureIndex(XMUINT2(96, 97));
	pArcher->SetMesh(0, m_mModelData["Mage_Female"]->m_pParentMesh);
	pArcher->SetChilds(m_pD3dDevice.Get(), m_mModelData["Mage_Female"]->m_vChildMesh, m_mModelData["Mage_Female"]->m_vNames);
	pArcher->SetAnimClip(m_mModelData["Mage_Female"]->m_mDivisionAnimationClip);
	pArcher->SetScale(1.f, 1.f, 1.f);
	pArcher->Rotate(90, 0, 0);
	pArcher->SetPosition(XMFLOAT3(7250, 510, 3540));
	pArcher->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pArcher->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	pArcher->SetMaterial(0, pMaterial);
	pArcher->m_strObjectName = "Mage_Female";
	pArcher->SetObjectCBIndex(::gnObjCBindex++);
	pArcher->SetSkinCBIndex(::gnSkinCBindex++);

	pArcher->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
	m_ppNpcObjects[2] = pArcher;

	pArcher = new CNPCObject(1);
	pArcher->SetTextureIndex(XMUINT2(102, 103));
	pArcher->SetMesh(0, m_mModelData["Mage_Male"]->m_pParentMesh);
	pArcher->SetChilds(m_pD3dDevice.Get(), m_mModelData["Mage_Male"]->m_vChildMesh, m_mModelData["Mage_Male"]->m_vNames);
	pArcher->SetAnimClip(m_mModelData["Mage_Male"]->m_mDivisionAnimationClip);
	pArcher->SetScale(1.f, 1.f, 1.f);
	pArcher->Rotate(90, 0, 0);
	pArcher->SetPosition(XMFLOAT3(3500, 510, 6800));
	pArcher->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pArcher->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	pArcher->SetMaterial(0, pMaterial);
	pArcher->m_strObjectName = "Mage_Male";
	pArcher->SetObjectCBIndex(::gnObjCBindex++);
	pArcher->SetSkinCBIndex(::gnSkinCBindex++);

	pArcher->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
	m_ppNpcObjects[3] = pArcher;

	CSkinAndDivisionObject* Scarecrow = new CSkinAndDivisionObject();
	Scarecrow->SetTextureIndex(XMUINT2(108, 109));
	Scarecrow->SetMesh(0, m_mModelData["Scarecrow"]->m_pParentMesh);
	Scarecrow->SetChilds(m_pD3dDevice.Get(), m_mModelData["Scarecrow"]->m_vChildMesh, m_mModelData["Scarecrow"]->m_vNames);
	Scarecrow->SetAnimClip(m_mModelData["Scarecrow"]->m_mDivisionAnimationClip);
	Scarecrow->SetScale(1.f, 1.f, 1.f);
	Scarecrow->Rotate(90, 0, 0);
	Scarecrow->SetPosition(XMFLOAT3(9200, 550, 8750));
	Scarecrow->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pArcher->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	Scarecrow->SetMaterial(0, pMaterial);
	Scarecrow->m_strObjectName = "Scarecrow";
	Scarecrow->SetObjectCBIndex(::gnObjCBindex++);
	Scarecrow->SetSkinCBIndex(::gnSkinCBindex++);

	Scarecrow->CreateCollisionBox(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature());
	m_ppNpcObjects[4] = Scarecrow;
}

void GameFramework::BuildFrameResources()
{
	cout << "상수버퍼 개수 : "<<gnObjCBindex << endl;
	cout << "스킨상수버퍼 개수 : " << gnSkinCBindex << endl;
	for (int i = 0; i < gNumFrameResources; ++i)
	{
		mFrameResources.push_back(std::make_unique<FrameResource>(m_pD3dDevice.Get(),
			2, ::gnObjCBindex, ::gnSkinCBindex));
	}
}

void GameFramework::SetPlayer()
{
	/////////////////////////////
	//TODO: DLETE FOR NETWORK
	////////////////////////////
	/*m_ccusLoginList.insert(CNetwork::m_usId);
	m_pPlayer = m_ccumPlayer[CNetwork::m_usId];
	CNetwork::m_usId = 0;*/

	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]) {

		m_pScene->m_pPlayer = m_pPlayer = GameFramework::m_ccumPlayer[CNetwork::m_usId];
		std::cout << CNetwork::m_usId << std::endl;
		m_pCamera = GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetCamera();
		m_pScene->SetPlayer();
		SetCharacterType(GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetObjectType(), CNetwork::m_usId);
		m_bSetPlayer = true;
		m_pUIShader->SetPlayer(m_ccumPlayer[CNetwork::m_usId]);
	}
	else
		return;
}

void GameFramework::BuildObjects()
{
	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

	m_pScene = new CScene();
	m_pScene->BuildObjects(m_pD3dDevice.Get(), m_pCommandList.Get());
	
	// 모델 정보 로드.
	ModelLoad();
	BuildMonsters();
	BuildNpcs();
	// 스키닝 모델들을 그려줄 파이프라인상태객체.
	m_pSkinnedObjectShader = new CSkinnedObjectShader();
	m_pSkinnedObjectShader->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), true);


	auto t = high_resolution_clock::now();

	m_nPlayers = 5;

	//5개만 나중엔 MAX_PLAYER
	for (unsigned short i = 0; i < m_nPlayers; ++i) {
		GameFramework::m_ccumPlayer.insert(std::make_pair(i, new CTerrainPlayer(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature(), m_pScene->GetTerrain())));
		//GameFramework::m_ccumPlayer[i]->SetMesh(0, m_mModelData["SwordMan"]->m_pParentMesh);
		//GameFramework::m_ccumPlayer[i]->SetChilds(m_pD3dDevice.Get(), m_mModelData["SwordMan"]->m_vChildMesh, m_mModelData["SwordMan"]->m_vNames);
		//GameFramework::m_ccumPlayer[i]->SetAnimClip(m_mModelData["SwordMan"]->m_mDivisionAnimationClip);
		GameFramework::m_ccumPlayer[i]->SetSkinCBIndex(::gnSkinCBindex++);
		/*GameFramework::m_ccumPlayer[i]->SetCharacterInfos(m_mModelData["SwordGirl"]->m_pParentMesh, 
			m_mModelData["SwordGirl"]->m_vChildMesh, m_mModelData["SwordGirl"]->m_vNames,
			m_mModelData["SwordGirl"]->m_mDivisionAnimationClip, XMFLOAT3(3.f, 3.f, 3.f), false);*/
		//GameFramework::m_ccumPlayer[i]->SetCharacterInfos(m_mModelData["SwordMan"]->m_pParentMesh,
		//	m_mModelData["SwordMan"]->m_vChildMesh, m_mModelData["SwordMan"]->m_vNames,
		//	m_mModelData["SwordMan"]->m_mDivisionAnimationClip);

		//GameFramework::m_ccumPlayer[i]->SetMaxHP(100);
		// 남캐 스킬전용.
		//GameFramework::m_ccumPlayer[i]->SetSword(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature(), XMFLOAT2(250.f, 300.f));
	}

	m_pSound = new CSound();
	m_pSound->init();
	m_pSound->loading();
	m_pSound->play(0);

	auto d = high_resolution_clock::now() - t;
	t = high_resolution_clock::now();
	cout << "cpu : " << duration_cast<milliseconds>(d).count() << endl;
	
	//GameFramework::m_sPlayerDir.m_Dir = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
	m_nTextures = 120;
	m_TextureMgr = new TextureManager(m_pD3dDevice.Get(), m_pCommandList.Get(), m_nTextures, RESOURCE_TEXTURE2D_ARRAY);

	mShadowMap = make_unique<ShadowMap>(
		m_pD3dDevice.Get(), FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
	auto srvCpuStart = m_TextureMgr->GetCPUDescriptorHandleForHeapStart();
	auto srvGpuStart = m_TextureMgr->GetGPUDescriptorHandleForHeapStart();
	auto dsvCpuStart = GetDepthStencilView();

	mShadowMap->BuildDescriptors(
		CD3DX12_CPU_DESCRIPTOR_HANDLE(srvCpuStart, m_nTextures, ::gnCbvSrvUavDescriptorIncrementSize),
		CD3DX12_GPU_DESCRIPTOR_HANDLE(srvGpuStart, m_nTextures, ::gnCbvSrvUavDescriptorIncrementSize),
		CD3DX12_CPU_DESCRIPTOR_HANDLE(dsvCpuStart, 1, m_DsvDescriptorSize));

	
	m_pUIShader = new CUIShader();
	m_pUIShader->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), false);
	m_pUIShader->BuildObjects(m_pD3dDevice.Get(), m_pCommandList.Get(), nullptr);

	//
	m_pMinimapRectShader = new CMiniMapShader();
	m_pMinimapRectShader->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), false);

	m_ppMinimapPlayers = new CGameObject*[m_nPlayers];
	CMesh* pMesh = new CColorRectMesh(m_pD3dDevice.Get(), m_pCommandList.Get(), 2, 0, 2, 0, 0, 0, XMFLOAT4(0, 1, 0, 1.f));
	CMaterial* pMat = new CMaterial();
	for (int i = 0; i < m_nPlayers; ++i) {
		m_ppMinimapPlayers[i] = new CGameObject(1);
		m_ppMinimapPlayers[i]->SetMesh(0, pMesh);
		m_ppMinimapPlayers[i]->SetObjectCBIndex(::gnObjCBindex++);
		m_ppMinimapPlayers[i]->SetMaterial(0, pMat);
	}

	pMesh = new CColorRectMesh(m_pD3dDevice.Get(), m_pCommandList.Get(), 20, 0, 20, 0, 0, 0, XMFLOAT4(1, 0, 0, 1));
	m_ppMinimapMonsters = new CGameObject*[m_nNormalDungeonObjects + m_nBossDungeonObjects];
	for (int i = 0; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
		m_ppMinimapMonsters[i] = new CGameObject(1);
		m_ppMinimapMonsters[i]->SetMesh(0, pMesh);
		m_ppMinimapMonsters[i]->SetObjectCBIndex(::gnObjCBindex++);
		m_ppMinimapMonsters[i]->SetMaterial(0, pMat);
	}

	pMesh = new CColorRectMesh(m_pD3dDevice.Get(), m_pCommandList.Get(), 400, 0, 400, 0, 0, 0, XMFLOAT4(0, 0, 1, 1));
	m_pMinimapBoss = new CGameObject(1);
	m_pMinimapBoss->SetMesh(0, pMesh);
	m_pMinimapBoss->SetObjectCBIndex(::gnObjCBindex++);
	m_pMinimapBoss->SetMaterial(0, pMat);

	m_pMinimapObjectShader = new CMiniMapObjectShader();
	m_pMinimapObjectShader->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), false);
	//
	m_pTmp = new CGeometryPortalShader();
	m_pTmp->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), false);
	m_pTmp->BuildObjects(m_pD3dDevice.Get(), m_pCommandList.Get(), nullptr);

	BuildFrameResources();

	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	FlushCommandQueue();

	if (m_TextureMgr) m_TextureMgr->ReleaseUploadBuffers();

	m_Timer.Reset();
}

void GameFramework::AnimateObjects()
{
	if (m_pScene) {
		m_pScene->AnimateObjects(m_Timer.GetDeltaTime());
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		// 일반던전 애니메이트
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			GameFramework::m_ccumMonster[i]->Animate(m_Timer.GetDeltaTime());
		}
	}
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			GameFramework::m_ccumMonster[i]->Animate(m_Timer.GetDeltaTime());
		}
		m_pBoss->Animate(m_Timer.GetDeltaTime());
	}
	for (auto& au : m_ccusLoginList) {
		GameFramework::m_ccumPlayer[au]->Animate(m_Timer.GetDeltaTime());
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::VILLAGE) {
		for (int i = 0; i < m_nNpcObjects; ++i) {
			m_ppNpcObjects[i]->Animate(m_Timer.GetDeltaTime());
		}
	}
}

void GameFramework::Update()
{
	ProcessInput();
	AnimateObjects();
	for (auto& au : m_ccusLoginList) {
		GameFramework::m_ccumPlayer[au]->Update(m_Timer.GetDeltaTime());
	}
	// 순환적으로 자원 프레임 배열의 다음 원소에 접근한다.
	mCurrFrameResourceIndex = (mCurrFrameResourceIndex + 1) % gNumFrameResources;
	mCurrFrameResource = mFrameResources[mCurrFrameResourceIndex].get();
	// GPU가 현재 프레임 자원의 명령들을 다 처리했는지 확인한다.
	// 아직 다 처리하지 않았으면 GPU가 이 울타리 지점까지의 명령들을 처리할 때 까지 기다린다.
	if (mCurrFrameResource->Fence != 0 && m_pFence->GetCompletedValue() < mCurrFrameResource->Fence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);
		ThrowIfFailed(m_pFence->SetEventOnCompletion(mCurrFrameResource->Fence, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	m_pCamera->UpdateShadowTransform(m_Timer.GetDeltaTime());
	m_pCamera->UpdateCB(mCurrFrameResource, m_Timer.GetDeltaTime());
	m_pCamera->UpdateShadowPassCB(mCurrFrameResource, m_Timer.GetDeltaTime());
	//shader객체들, 터레인 상수버퍼 업데이트, 	//shader객체들 스킨정보 업데이트
	m_pScene->UpdateObjects(mCurrFrameResource, m_Timer.GetDeltaTime());

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				GameFramework::m_ccumMonster[i]->SetFrameDirty(::gNumFrameResources);
				GameFramework::m_ccumMonster[i]->UpdateCB(mCurrFrameResource);
				GameFramework::m_ccumMonster[i]->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
			}
		}
	}
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				GameFramework::m_ccumMonster[i]->SetFrameDirty(::gNumFrameResources);
				GameFramework::m_ccumMonster[i]->UpdateCB(mCurrFrameResource);
				GameFramework::m_ccumMonster[i]->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
			}
		}
		if (m_pBoss && m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			m_pBoss->SetFrameDirty(::gNumFrameResources);
			m_pBoss->UpdateCB(mCurrFrameResource);
			m_pBoss->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
		}
	}

	// 플레이어 상수버퍼업데이트
	for (auto& au : m_ccusLoginList) {
		m_ccumPlayer[au]->UpdateCB(mCurrFrameResource);
		m_ccumPlayer[au]->SetFrameDirty(::gNumFrameResources);
		// 플레이어 스킨정보 업데이트
		m_ccumPlayer[au]->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
	}

	if (m_pUIShader) {
		m_pUIShader->SetMapindex(CNetwork::GetInstance()->GetCurrentMap());
		m_pUIShader->UpdateObjectsCB(mCurrFrameResource, m_Timer.GetDeltaTime());
	}
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::VILLAGE) {
		for (int i = 0; i < m_nNpcObjects; ++i) {
			if (m_ppNpcObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppNpcObjects[i]->SetFrameDirty(::gNumFrameResources);
				m_ppNpcObjects[i]->UpdateCB(mCurrFrameResource);
				m_ppNpcObjects[i]->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
			}
		}
	}

	for (int i = 0; i < m_nPlayers; ++i) {
		m_ppMinimapPlayers[i]->SetWorld(m_ccumPlayer[i]->GetWorld());
		m_ppMinimapPlayers[i]->UpdateCB(mCurrFrameResource);
		m_ppMinimapPlayers[i]->SetFrameDirty(::gNumFrameResources);
	}
	
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMinimapMonsters[i]->SetWorld(GameFramework::m_ccumMonster[i]->GetWorld());
				m_ppMinimapMonsters[i]->UpdateCB(mCurrFrameResource);
				m_ppMinimapMonsters[i]->SetFrameDirty(::gNumFrameResources);
			}
		}
	}
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMinimapMonsters[i]->SetWorld(GameFramework::m_ccumMonster[i]->GetWorld());
				m_ppMinimapMonsters[i]->UpdateCB(mCurrFrameResource);
				m_ppMinimapMonsters[i]->SetFrameDirty(::gNumFrameResources);
			}
		}

		if (m_pBoss && m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			//m_pMinimapBoss->SetWorld(m_pBoss->GetWorld());
			m_pMinimapBoss->SetPosition(m_pBoss->GetPosition());
			m_pMinimapBoss->UpdateCB(mCurrFrameResource);
			m_pMinimapBoss->SetFrameDirty(::gNumFrameResources);
		}
	}

	//
	m_pTmp->UpdateObjectsCB(mCurrFrameResource, m_Timer.GetDeltaTime());
}

void GameFramework::DrawSceneToShadowMap()
{
	m_pCommandList->RSSetViewports(1, &mShadowMap->Viewport());
	m_pCommandList->RSSetScissorRects(1, &mShadowMap->ScissorRect());

	// Change to DEPTH_WRITE.
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mShadowMap->Resource(),
		D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_DEPTH_WRITE));

	UINT passCBByteSize = (sizeof(PassConstants) + 255) & ~255;

	// Clear the back buffer and depth buffer.
	m_pCommandList->ClearDepthStencilView(mShadowMap->Dsv(),
		D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	m_pCommandList->OMSetRenderTargets(0, nullptr, false, &mShadowMap->Dsv());

	// Bind the pass constant buffer for the shadow map pass.
	auto passCB = mCurrFrameResource->PassCB->Resource();
	D3D12_GPU_VIRTUAL_ADDRESS passCBAddress = passCB->GetGPUVirtualAddress() + 1 * passCBByteSize;
	m_pCommandList->SetGraphicsRootConstantBufferView(0, passCBAddress);

	m_pScene->Render(m_pCommandList.Get(), m_pCamera, true);
	
	// 스키닝 객체 그려줄때 파이프라인 상태객체를 셋해주는 역할.
	m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, true);
	// 몬스터들 렌더. 해당맵이 활성화 되있을 경우 && 카메라에 보일경우만.
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				GameFramework::m_ccumMonster[i]->Render(m_pCommandList.Get(), m_pCamera, true);
			}
		}
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				GameFramework::m_ccumMonster[i]->Render(m_pCommandList.Get(), m_pCamera, true);
			}
		}
		if (GameFramework::m_pBoss->GetMonsterWakeUp()) {
			if (GameFramework::m_pBoss && GameFramework::m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				GameFramework::m_pBoss->Render(m_pCommandList.Get(), m_pCamera, true);
			}
		}
	}

	for (auto& au : CNetwork::m_ccusViewList[PLAYER_LIST]) {
		m_ccumPlayer[au]->Render(m_pCommandList.Get(), m_pCamera, true);
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::VILLAGE) {
		for (int i = 0; i < m_nNpcObjects; ++i) {
			if (m_ppNpcObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppNpcObjects[i]->Render(m_pCommandList.Get(), m_pCamera, true);
			}
		}
	}
	// Change back to GENERIC_READ so we can read the texture in a shader.
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mShadowMap->Resource(),
		D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_GENERIC_READ));

}

void GameFramework::FrameAdvance()
{
	//auto t = high_resolution_clock::now();

	m_Timer.Tick(60.0f);

	/////////////////////////////
	//TODO: FOR NETWORK
	////////////////////////////
	CNetwork::GetInstance()->CopyBefore(PLAYER_LIST ,m_ccusLoginList);

	for (auto& au : m_ccusLoginList) {
		if (au == CNetwork::m_usId) continue;
		SetCharacterType(GameFramework::m_ccumPlayer[au]->GetObjectType(), au);
	}

	Update();
	auto cmdListAlloc = mCurrFrameResource->CmdListAlloc;
	
	ThrowIfFailed(cmdListAlloc->Reset());
	ThrowIfFailed(m_pCommandList->Reset(cmdListAlloc.Get(), nullptr));

	m_pScene->SetGraphicsRootSignature(m_pCommandList.Get());
	m_TextureMgr->SetDescriptorHeaps(m_pCommandList.Get());
	m_TextureMgr->TexturesUpdate(m_pCommandList.Get());

	DrawSceneToShadowMap();

	m_pCamera->SetViewportsAndScissorRects(m_pCommandList.Get());

	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	m_pCommandList->ClearRenderTargetView(GetCurrentBackBufferView(), Colors::AliceBlue, 0, nullptr);
	m_pCommandList->ClearDepthStencilView(GetDepthStencilView(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	m_pCommandList->OMSetRenderTargets(1, &GetCurrentBackBufferView(), true, &GetDepthStencilView());


	// 그림자패스에서 얻은 텍스처정보를 gpu에 올림.
	m_pCommandList->SetGraphicsRootDescriptorTable(4, mShadowMap->Srv());

	// 카메라 정보 gpu전달.
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = mCurrFrameResource->PassCB->Resource()->GetGPUVirtualAddress();
	m_pCommandList->SetGraphicsRootConstantBufferView(0, d3dGpuVirtualAddress);

	m_pScene->Render(m_pCommandList.Get(), m_pCamera, false);

	// 스키닝 객체 그려줄때 파이프라인 상태객체를 셋해주는 역할. ( 충돌박스 그릴때는 한번만 셋해주면 안되기 때문에 얘는 주석해놓고 충돌박스 없애면 주석풀자 )
	//m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);
	// 몬스터들 렌더. 해당맵이 활성화 되있을 경우 && 카메라에 보일경우만.
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				// 충돌박스 그리는 쉐이더랑 달라서 매번 이렇게 셋해줘야댐. 나주에 충돌박스 없애면 빼자.
				m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);
				//
				GameFramework::m_ccumMonster[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i] && GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				// 충돌박스 그리는 쉐이더랑 달라서 매번 이렇게 셋해줘야댐. 나주에 충돌박스 없애면 빼자.
				m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);
				//
				GameFramework::m_ccumMonster[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
		if (m_pBoss && m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			// 충돌박스 그리는 쉐이더랑 달라서 매번 이렇게 셋해줘야댐. 나주에 충돌박스 없애면 빼자.
			m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);
			//
			m_pBoss->Render(m_pCommandList.Get(), m_pCamera, false);
		}
	}

	for (auto& au : m_ccusLoginList) {
		m_ccumPlayer[au]->Render(m_pCommandList.Get(), m_pCamera, false);
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::VILLAGE) {
		for (int i = 0; i < m_nNpcObjects; ++i) {
			if (m_ppNpcObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				// 충돌박스 그리는 쉐이더랑 달라서 매번 이렇게 셋해줘야댐. 나주에 충돌박스 없애면 빼자.
				m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);
				//
				m_ppNpcObjects[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
	}

	m_pMinimapRectShader->Render(m_pCommandList.Get(), m_pCamera, false);
	m_pMinimapObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);

	for (auto& au : m_ccusLoginList) {
		m_ppMinimapPlayers[au]->Render(m_pCommandList.Get(), m_pCamera, false);
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMinimapMonsters[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
	}
	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			if (GameFramework::m_ccumMonster[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMinimapMonsters[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
		if (m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			m_pMinimapBoss->Render(m_pCommandList.Get(), m_pCamera, false);
		}
	}
	if (m_pUIShader)
		m_pUIShader->Render(m_pCommandList.Get(), m_pCamera, false);

	m_pTmp->Render(m_pCommandList.Get(), m_pCamera, false);

#ifndef _WITH_DIRECT2D
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
#endif
	
	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

#ifdef _WITH_DIRECT2D
	//Direct2D Drawing
	m_pd3d11On12Device->AcquireWrappedResources(&m_ppd3d11WrappedBackBuffers[m_CurrBackBuffer], 1);
	m_pd2dDeviceContext->SetTarget(m_ppd2dRenderTargets[m_CurrBackBuffer]);

	m_pd2dDeviceContext->BeginDraw();

	m_pd2dDeviceContext->SetTransform(D2D1::Matrix3x2F::Identity());

	D2D1_SIZE_F szRenderTarget = m_ppd2dRenderTargets[m_CurrBackBuffer]->GetSize();
	//m_Timer.GetDeltaTime(m_pszFrameRate + 12, 37);
	D2D1_RECT_F rcUpperText = D2D1::RectF(0, 0, szRenderTarget.width, szRenderTarget.height * 0.5f);
	//m_pd2dDeviceContext->DrawTextW(m_pszFrameRate, (UINT32)wcslen(m_pszFrameRate), m_pdwFont, &rcUpperText, m_pd2dbrText);

	DirectX::XMFLOAT2 name_pos;

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			m_pScene->GetObjectNamePos(false, i, name_pos);
			D2D1_RECT_F rect_pos;
			rect_pos = D2D1::RectF(name_pos.x - 100.0f, name_pos.y, name_pos.x + 100.0f, name_pos.y);
			//D2D1_RECT_F rcLowerText = D2D1::RectF(0, szRenderTarget.height * 0.5f, szRenderTarget.width, szRenderTarget.height);
			m_pd2dDeviceContext->DrawTextW(L"김도엽", (UINT32)wcslen(L"김도엽"), m_pdwFont, &rect_pos, m_pd2dbrText);
		}
	}

	if (CNetwork::GetInstance()->GetCurrentMap() == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (!GameFramework::m_ccumMonster[i]->GetMonsterWakeUp()) continue;
			m_pScene->GetObjectNamePos(false, i, name_pos);
			D2D1_RECT_F rect_pos;
			rect_pos = D2D1::RectF(name_pos.x - 100.0f, name_pos.y, name_pos.x + 100.0f, name_pos.y);
			//D2D1_RECT_F rcLowerText = D2D1::RectF(0, szRenderTarget.height * 0.5f, szRenderTarget.width, szRenderTarget.height);
			m_pd2dDeviceContext->DrawTextW(L"김도엽", (UINT32)wcslen(L"김도엽"), m_pdwFont, &rect_pos, m_pd2dbrText);
		}
	}

	for (auto& player : m_ccusLoginList) {
		m_pScene->GetObjectNamePos(true, player, name_pos);
		D2D1_RECT_F rect_pos;
		rect_pos = D2D1::RectF(name_pos.x - 100.0f, name_pos.y, name_pos.x + 100.0f, name_pos.y);
		wchar_t player_id[MAX_STR_LEN];
		GameFramework::m_ccumPlayer[player]->GetPlayerID(player_id);
		//D2D1_RECT_F rcLowerText = D2D1::RectF(0, szRenderTarget.height * 0.5f, szRenderTarget.width, szRenderTarget.height);
		//m_pd2dDeviceContext->DrawTextW(L"김도엽", (UINT32)wcslen(L"김도엽"), m_pdwFont, &rect_pos, m_pd2dbrText);
		m_pd2dDeviceContext->DrawTextW(player_id, (UINT32)wcslen(player_id), m_pdwFont, &rect_pos, m_pd2dbrText);
	}

	m_pd2dDeviceContext->EndDraw();

	m_pd3d11On12Device->ReleaseWrappedResources(&m_ppd3d11WrappedBackBuffers[m_CurrBackBuffer], 1);

	m_pd3d11DeviceContext->Flush();
	
#endif
	ThrowIfFailed(m_pSwapChain->Present(0, 0));
	m_CurrBackBuffer = (m_CurrBackBuffer + 1) % m_SwapChainBufferCount;

	CalculateFrameStats();
	//auto d = high_resolution_clock::now() - t;
	//t = high_resolution_clock::now();
	//FlushCommandQueue();
	mCurrFrameResource->Fence = ++m_CurrentFence;
	m_pCommandQueue->Signal(m_pFence.Get(), m_CurrentFence);
	//auto s = high_resolution_clock::now() - t;

	//cout << "cpu : " << duration_cast<milliseconds>(d).count() << endl;
	//cout << "gpu : " << duration_cast<milliseconds>(s).count() << endl;
}

void GameFramework::ProcessInput()
{
	static UCHAR pKeysBuffer[256];
	bool bProcessedByScene = false;
	if (GetKeyboardState(pKeysBuffer) && m_pScene)
		bProcessedByScene = m_pScene->ProcessInput(pKeysBuffer);

	if (GameFramework::m_ccumPlayer[CNetwork::m_usId]->GetIsDie()) return;
	if (!bProcessedByScene)
	{
		DWORD dwDirection = 0;
		if (pKeysBuffer[VK_UP] & 0xF0 || pKeysBuffer[0x57] & 0xF0) {
			dwDirection |= DIR_FORWARD;
		}
		if (pKeysBuffer[VK_DOWN] & 0xF0 || pKeysBuffer[0x53] & 0xF0) {
			dwDirection |= DIR_BACKWARD;
		}
		if (pKeysBuffer[VK_LEFT] & 0xF0 || pKeysBuffer[0x41] & 0xF0) {
			dwDirection |= DIR_LEFT;
		}
		if (pKeysBuffer[VK_RIGHT] & 0xF0 || pKeysBuffer[0x44] & 0xF0) {
			dwDirection |= DIR_RIGHT;
		}
		if (pKeysBuffer[VK_PRIOR] & 0xF0 || pKeysBuffer[0x45] & 0xF0) dwDirection |= DIR_UP;
		if (pKeysBuffer[VK_NEXT] & 0xF0 || pKeysBuffer[0x51] & 0xF0) dwDirection |= DIR_DOWN;

		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos;
		if (GetCapture() == m_hMainWnd)
		{
			SetCursor(NULL);
			GetCursorPos(&ptCursorPos);
			cxDelta = (float)(ptCursorPos.x - m_OldCursorPos.x) / 3.0f;
			cyDelta = (float)(ptCursorPos.y - m_OldCursorPos.y) / 3.0f;
			SetCursorPos(m_OldCursorPos.x, m_OldCursorPos.y);
		}


		if ((dwDirection != 0) || (cxDelta != 0.0f) || (cyDelta != 0.0f))
		{
			if (cxDelta || cyDelta)
			{
				if (pKeysBuffer[VK_RBUTTON] & 0xF0)
					GameFramework::m_ccumPlayer[CNetwork::m_usId]->Rotate(cyDelta, cxDelta, 0.0f);
				else
					GameFramework::m_ccumPlayer[CNetwork::m_usId]->Rotate(cyDelta, cxDelta, 0.0f);
				CNetwork::GetInstance()->SendDirPacket();

			}
			if (dwDirection) {
				// skill2는 날아서 공격하는 거니깐 움직이면서도 공격할 수 있게.
				if (m_pPlayer->GetNowAnimation() == "idle" || m_pPlayer->GetNowAnimation() == "walk" || m_pPlayer->GetNowAnimation() == "skill2" || m_pPlayer->GetNowAnimation() == "stun") {

					if (m_pPlayer->GetNowAnimation() != "skill2") {
						/////////////////////////////
						//TODO: FOR NETWORK
						////////////////////////////
						if (!CNetwork::GetInstance()->GetCollision())
							m_pPlayer->SetNowAnimation(string("walk"));

						/////////////////////////////
						//TODO: DELETE FOR NETWORK
						////////////////////////////
						//m_pPlayer->SetNowAnimation(string("walk"));
					}
					CheckMove(dwDirection);
				}
			}
		}
		if (!dwDirection) {
			if (m_pPlayer->GetNowAnimation() == "walk") {
				m_pPlayer->SetNowAnimation(string("idle"));
				/////////////////////////////
				//TODO: FOR NETWORK
				////////////////////////////
				CNetwork::GetInstance()->SendIdlePacket();
			}

		}
		//	cout << "안무브중" << endl;
	}
}

GameFramework::~GameFramework()
{
	if (m_pD3dDevice != nullptr)
		FlushCommandQueue();
}


//2D추가
#ifdef _WITH_DIRECT2D
void GameFramework::CreateDirect2DDevice()
{
	UINT nD3D11DeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if defined(_DEBUG)
	nD3D11DeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	ID3D11Device *pd3d11Device = NULL;

	HRESULT hResult = ::D3D11On12CreateDevice(m_pD3dDevice.Get(), nD3D11DeviceFlags, NULL, 0, (IUnknown **)m_pCommandQueue.GetAddressOf(), 1, 0, &pd3d11Device, &m_pd3d11DeviceContext, NULL);

	hResult = pd3d11Device->QueryInterface(__uuidof(ID3D11On12Device), (void **)&m_pd3d11On12Device);

	if (pd3d11Device) pd3d11Device->Release();

	D2D1_FACTORY_OPTIONS nD2DFactoryOptions = { D2D1_DEBUG_LEVEL_NONE };
#if defined(_DEBUG)
	nD2DFactoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif
	hResult = ::D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory3), &nD2DFactoryOptions, (void **)&m_pd2dFactory);

	IDXGIDevice *pdxgiDevice = NULL;
	hResult = m_pd3d11On12Device->QueryInterface(__uuidof(IDXGIDevice), (void **)&pdxgiDevice);
	hResult = m_pd2dFactory->CreateDevice(pdxgiDevice, &m_pd2dDevice);
	hResult = m_pd2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &m_pd2dDeviceContext);
	hResult = ::DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), (IUnknown **)&m_pdWriteFactory);

	if (pdxgiDevice) pdxgiDevice->Release();

	m_pd2dDeviceContext->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_GRAYSCALE);

	m_pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(0.3f, 0.0f, 0.0f, 0.5f), &m_pd2dbrBackground);
	m_pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF(0x9ACD32, 1.0f)), &m_pd2dbrBorder);

	hResult = m_pdWriteFactory->CreateTextFormat(L"궁서체", NULL, DWRITE_FONT_WEIGHT_DEMI_BOLD, DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_STRETCH_NORMAL, 12.0f, L"en-US", &m_pdwFont);
	hResult = m_pdwFont->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	hResult = m_pdwFont->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	m_pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Purple, 1.0f), &m_pd2dbrText);
	hResult = m_pdWriteFactory->CreateTextLayout(L"텍스트 레이아웃", 8, m_pdwFont, 4096.0f, 4096.0f, &m_pdwTextLayout);
}
#endif

void GameFramework::CheckMove(DWORD dwDir)
{
	DirectX::BoundingOrientedBox temp_box = GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->m_xmOOBB;
	DirectX::BoundingOrientedBox temp_trans_box = GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->m_xmOOBBTransformed;
	DirectX::XMFLOAT4X4 xm4x4Player = GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->GetWorld();

	DirectX::XMFLOAT3 temp_pos = GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->GetPosition();

	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);
	XMFLOAT3 xmf3Look = { 0, 0, 0 };
	if (dwDir & DIR_FORWARD) {
		if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(0, 0, 1))))
			xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(0, 0, 1));
	}
	if (dwDir & DIR_BACKWARD) {
		if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(0, 0, -1))))
			xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(0, 0, -1));
	}
	if (dwDir & DIR_RIGHT) {
		if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(1, 0, 0))))
			xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(1, 0, 0));
	}
	if (dwDir & DIR_LEFT) {
		if (!Vector3::IsZero(Vector3::Add(xmf3Look, XMFLOAT3(-1, 0, 0))))
			xmf3Look = Vector3::Add(xmf3Look, XMFLOAT3(-1, 0, 0));
	}

	xmf3Shift = Vector3::Add(xmf3Shift, Vector3::Normalize(xmf3Look), PLAYER_DISTANCE * m_Timer.GetDeltaTime());
	temp_pos = Vector3::Add(temp_pos, xmf3Shift);

	xm4x4Player._41 = temp_pos.x, xm4x4Player._42 = temp_pos.y, xm4x4Player._43 = temp_pos.z;

	temp_trans_box.Transform(temp_box, XMLoadFloat4x4(&xm4x4Player));

	if (!m_pScene->CheckCollisionStaticObject(m_Timer.GetDeltaTime(), temp_box)) {
		GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->Move(dwDir, PLAYER_DISTANCE * m_Timer.GetDeltaTime(), false);
		m_pPlayer->SetDirection(dwDir);
		CNetwork::GetInstance()->SendPosPacket();
	}
	else {
		/*if (CNetwork::GetInstance()->GetCurrentMap() == MAP_VILLAGE) {
			GameFramework::m_ccumPlayer[CNetwork::GetInstance()->m_usId]->Move(dwDir, PLAYER_DISTANCE * m_Timer.GetDeltaTime(), false);
			m_pPlayer->SetDirection(dwDir);
			CNetwork::GetInstance()->SendPosPacket();
		}*/
	}
}