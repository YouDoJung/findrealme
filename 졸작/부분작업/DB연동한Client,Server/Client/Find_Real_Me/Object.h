#pragma once
#include "ModelMesh.h"
#include "Mesh.h"
#include "Camera.h"
#include "Texture.h"
#include "Material.h"
#define DIR_FORWARD					0x01
#define DIR_BACKWARD				0x02
#define DIR_LEFT					0x04
#define DIR_RIGHT					0x08
#define DIR_UP						0x10
#define DIR_DOWN					0x20
class CShader;

constexpr char MONSTER_WAKEUP_LOCK = 0;
constexpr char MONSTER_LOOK_LOCK = 1;
constexpr char MONSTER_POS_LOCK = 2;
constexpr char IS_DIE_LOCK = 3;

struct CombinedAnimationClip
{
	float m_fStartTime;
	float m_fEndTime;
	float m_fSpeed;

	CombinedAnimationClip() : m_fStartTime{ 0.f }, m_fEndTime{ 0.f }, m_fSpeed{ 0.f } {}
	CombinedAnimationClip(float start, float end, float speed) : m_fStartTime{ start }, m_fEndTime{ end }, m_fSpeed{ speed } {}
};

struct DivisionAnimationClip
{
	const aiScene* m_pAnimationModel;
	float m_fStartTime;
	float m_fEndTime;
	float m_fSpeed;
	bool m_isRoof;

	DivisionAnimationClip() : m_pAnimationModel{ nullptr }, m_fStartTime{ 0.f }, m_fEndTime{ 0.f }, m_fSpeed{ 0.f }, m_isRoof{ false } {}
	DivisionAnimationClip(const aiScene* pAnimationModel, float start, float end, float speed, bool is_roof)
		: m_pAnimationModel{ pAnimationModel }, m_fStartTime{ start }, m_fEndTime{ end }, m_fSpeed{ speed }, m_isRoof{ is_roof } {}

};

const enum e_LOCK_TYPE {
	HP_LOCK, MP_LOCK, EXP_LOCK, MONEY_LOCK, LEVEL_LOCK, MAXHP_LOCK, MAXMP_LOCK, MAXEXP_LOCK, OBJECT_TYPE_LOCK, CHARACTER_SET_LOCK
};

class CGameObject
{
public:
	CGameObject(int nMeshes, int nMaterials);
	CGameObject(int num);
	virtual ~CGameObject();

private:
	bool m_bIs_die = false;
	bool m_bWakeUp = false;
	std::shared_mutex m_rmMonsterMutex[4];
	std::shared_mutex m_rmObjectStatusMutex[10];
protected:
	CPlayer* m_pTarget;
	XMFLOAT4X4 m_xmf4x4World;
	CMesh** m_ppMeshes = nullptr;
	int m_nMeshes = 0;
	CMaterial** m_ppMaterials = nullptr;
	int m_nMaterials = 0;

	int m_iHP = 100;
	int m_iMaxHP = 0;
	int m_iHPsize = 100;
	int m_iMP = 100;
	int m_iEXP, m_iMaxMP, m_iMaxEXP = 0;
	int m_iMoney = 0;
	int m_iLevel = 1;
	char m_cObjectType = 0;
	bool m_bIs_Character_Set = false;

	XMUINT2 m_xmi2TexIndex = { 0, 0 };
	float m_fFrameTime;
	float m_fStartTime = 0.f;
	UINT m_ObjectCBIndex = -1;
	int m_ChildObjectCBIndex[3] = { 0 };
	UINT m_SkinCBIndex = -1;

	int m_CollisionBoxCBIndex = 0;
	FrameResource* m_pCurrResource = nullptr;

	// 물체의 자료가 변해서 상수 버퍼를 갱신해야 하는지의 여부를 뜻하는 '더러움'플래그. FrameResource마다
	// 물체의 cbuffer가 있으므로, FrameResource마다 갱신을 적용해야 한다. 
	// 따라서, 물체의 자료를 수정할 때는 반드시
	// m_nNumFrameDirty = gNumFrameResources 로 설정해야 한다. 그래야 각각의 프레임 자원이 갱신된다.
	int m_nNumFrameDirty = gNumFrameResources;
public:
	string m_strObjectName = "";
	BoundingOrientedBox m_xmOOBB;
	BoundingOrientedBox m_xmOOBBTransformed;
	void SetOOBB(XMFLOAT3& xmCenter, XMFLOAT3& xmExtents, XMFLOAT4& xmOrientation) {
		m_xmOOBBTransformed = m_xmOOBB = BoundingOrientedBox(xmCenter, xmExtents, xmOrientation);
	}
	XMFLOAT3 m_xmf3Rotkey = { 0.f, 0.f, 0.f };

	void SetObjectCBIndex(UINT index) { m_ObjectCBIndex = index; }
	void SetSkinCBIndex(UINT num) { m_SkinCBIndex = num; }
	int GetFrameDirty() const { return m_nNumFrameDirty; }
	void MinusFrameDirty() { m_nNumFrameDirty--; }
	void SetFrameDirty(int num) { m_nNumFrameDirty = num; }

	float GetFrameTime() const { return m_fFrameTime; }
	void SetFrameTime(float fFrameTime) { m_fFrameTime = fFrameTime; }
	float GetStartTime() const { return m_fStartTime; }
	void SetStartTime(float fTime) { m_fStartTime = fTime; }

public:
	virtual float GetAniTime() { return m_fNowTime; }
	virtual void SetMonsterPos(DirectX::XMFLOAT3& xmf3POS) { 
	std::unique_lock<std::shared_mutex>lock(m_rmMonsterMutex[MONSTER_POS_LOCK]); SetPosition(xmf3POS); }
	virtual void SetMonsterLook(float fScale,DirectX::XMFLOAT3& xmf3Look) {
		std::unique_lock<std::shared_mutex>lock(m_rmMonsterMutex[MONSTER_LOOK_LOCK]); 
		m_xmf4x4World._31 = xmf3Look.x; m_xmf4x4World._32 = xmf3Look.y; m_xmf4x4World._33 = xmf3Look.z;
		XMFLOAT3 xmf3Right = Vector3::CrossProduct(XMFLOAT3(m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23), xmf3Look, true);
		XMFLOAT3 xmf3Up = Vector3::CrossProduct(xmf3Look, xmf3Right, true);
		m_xmf4x4World._21 = xmf3Up.x; m_xmf4x4World._22 = xmf3Up.y; m_xmf4x4World._23 = xmf3Up.z;
		m_xmf4x4World._11 = xmf3Right.x; m_xmf4x4World._12 = xmf3Right.y; m_xmf4x4World._13 = xmf3Right.z;
		SetScale(fScale, fScale, fScale);
	}
	virtual void SetBossLook(DirectX::XMFLOAT3& xmf3Look) {
		std::unique_lock<std::shared_mutex>lock(m_rmMonsterMutex[MONSTER_LOOK_LOCK]);
		DirectX::XMFLOAT3 xmf3Up = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
		DirectX::XMFLOAT3 xmf3Right = Vector3::CrossProduct(xmf3Up, xmf3Look, true);
		m_xmf4x4World._11 = xmf3Right.x; m_xmf4x4World._12 = xmf3Right.y; m_xmf4x4World._13 = xmf3Right.z;
		m_xmf4x4World._21 = xmf3Up.x; m_xmf4x4World._22 = xmf3Up.y;  m_xmf4x4World._23 = xmf3Up.z;
		m_xmf4x4World._31 = xmf3Look.x; m_xmf4x4World._32 = xmf3Look.y; m_xmf4x4World._33 = xmf3Look.z;
		SetScale(5.0f, 5.0f, 5.0f);
		Rotate(90.0f, 0.0f, 0.0f);
		/*DirectX::XMFLOAT4X4 world = GetWorld();
		m_xmf4x4World._11 = world._12; m_xmf4x4World._12 = world._11; m_xmf4x4World._13 = world._13;
		m_xmf4x4World._21 = world._22; m_xmf4x4World._22 = world._21; m_xmf4x4World._23 = world._23;
		m_xmf4x4World._31 = world._32; m_xmf4x4World._32 = world._31; m_xmf4x4World._33 = world._33;
		DirectX::XMFLOAT4X4 world2 = GetWorld();*/
	}
	virtual void SetMonsterWakeUp(bool bWakeup) { std::unique_lock<std::shared_mutex>lock(m_rmMonsterMutex[MONSTER_WAKEUP_LOCK]); m_bWakeUp = bWakeup; }
	virtual bool GetMonsterWakeUp() { std::shared_lock<std::shared_mutex>lock(m_rmMonsterMutex[MONSTER_WAKEUP_LOCK]); return m_bWakeUp; }
	virtual void SetIsDie(bool bDie) { std::unique_lock<std::shared_mutex>lock(m_rmMonsterMutex[IS_DIE_LOCK]); m_bIs_die = bDie; }
	virtual bool GetIsDie() { std::shared_lock<std::shared_mutex>lock(m_rmMonsterMutex[IS_DIE_LOCK]); return m_bIs_die; }


	virtual void SetHP(int hp) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[HP_LOCK]); m_iHP = hp; }
	virtual int GetHP() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[HP_LOCK]); return m_iHP; }

	virtual void SetMaxHP(int maxHp) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MAXHP_LOCK]); m_iMaxHP = maxHp; }
	virtual int GetMaxHP() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MAXHP_LOCK]); return m_iMaxHP; }

	virtual void SetMP(int mp) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MP_LOCK]); m_iMP = mp; }
	virtual int GetMP() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MP_LOCK]); return m_iMP; }

	virtual void SetEXP(int exp) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[EXP_LOCK]); m_iEXP = exp; }
	virtual int GetEXP() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[EXP_LOCK]); return m_iEXP; }

	virtual void SetMoney(int money) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MONEY_LOCK]); m_iMoney = money; }
	virtual int GetMoney() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MONEY_LOCK]); return m_iMoney; }

	virtual void SetLevel(int level) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[LEVEL_LOCK]); m_iLevel = level; }
	virtual int GetLevel() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[LEVEL_LOCK]); return m_iLevel; }

	virtual void SetMaxMP(int mp) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MAXMP_LOCK]); m_iMaxMP = mp; }
	virtual int GetMaxMP() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MAXMP_LOCK]); return m_iMaxMP; }

	virtual void SetMaxEXP(int exp) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MAXEXP_LOCK]); m_iMaxEXP = exp; }
	virtual int GetMaxEXP() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[MAXEXP_LOCK]); return m_iMaxEXP; }

	virtual void SetObjectType(char type) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[OBJECT_TYPE_LOCK]); m_cObjectType = type; }
	virtual char GetObjectType() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[OBJECT_TYPE_LOCK]); return m_cObjectType; }
	
	virtual void SetCharacterSet(bool isSet) { std::unique_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[CHARACTER_SET_LOCK]); m_bIs_Character_Set = isSet; }
	virtual bool GetCharacterSet() { std::shared_lock<std::shared_mutex>lock(m_rmObjectStatusMutex[CHARACTER_SET_LOCK]); return m_bIs_Character_Set; }

	void SetHpSize(int size) { m_iHPsize = size; }
	int GetHpSize() { return m_iHPsize; }

	void SetMesh(int nIndex, CMesh* pMesh);
	void SetShader(CShader* pShader);
	void SetShader(int nIndex, CShader* pShader);
	void SetMaterial(int nIndex, CMaterial* pMaterial);
	void SetTextureIndex(XMUINT2 nIndex);
	void SetWorld(XMFLOAT4X4 world) { m_xmf4x4World = world; }
	void SetChilds(ID3D12Device* pd3dDevice, vector<CSkinnedModelMesh*> pMesh, vector<string>& meshName, XMFLOAT3 xmf3LocalScale = { 1.f, 1.f, 1.f });
	virtual void SetAnimClip(unordered_map<string, DivisionAnimationClip>& clip) {};

	virtual void UpdateCB(FrameResource* pFrameResource);
	virtual void UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime);

	virtual void Animate(float fTimeElapsed);
	virtual void OnPrepareRender() { }
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);
	virtual void BuildMaterials(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) { }

	XMFLOAT3 GetPosition() const;
	XMFLOAT3 GetLook() const;
	XMFLOAT3 GetUp() const;
	XMFLOAT3 GetRight() const;
	XMFLOAT4X4 GetWorld() const;

	CMesh* GetMesh(int nIndex) const { return m_ppMeshes[nIndex]; }

	CMaterial* GetMaterial(int nIndex) const { return m_ppMaterials[nIndex]; }
	XMUINT2 GetTextureIndex() const { return m_xmi2TexIndex; }

	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 xmf3Position);

	void MoveStrafe(float fDistance = 1.0f);
	void MoveUp(float fDistance = 1.0f);
	void MoveForward(float fDistance = 1.0f);

	void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);
	void Rotate(XMFLOAT3* pxmf3Axis, float fAngle);
	void SetScale(float x, float y, float z) {
		XMMATRIX mtxScale = XMMatrixScaling(x, y, z);
		m_xmf4x4World = Matrix4x4::Multiply(mtxScale, m_xmf4x4World);
	}

	XMFLOAT3 GetWidthHeightDepth(XMFLOAT3 scale = XMFLOAT3(1.f, 1.f, 1.f)) const;
	bool HeightCollisionCheck(XMFLOAT3& position, bool is_rot) const;

	virtual void SetNowAnimationForMonster(string& s) {}
	virtual void SetNowAnimation(string& s);
	virtual std::string GetNowAnimation();
	virtual void SetAnimationTime(float fTime);
protected:
	string m_strNowClip = "idle";
	float m_fNowTime = 0.f;
	vector<CGameObject*> m_vChilds;
	bool m_bEndAnimation = false;
	// 여캐 무기가 너무 작아서 조금 차이를 줄려고.
	XMFLOAT3 m_xmf3LocalScale = { 1.f, 1.f, 1.f };

public:
	XMFLOAT3 GetLocalScale() const { return m_xmf3LocalScale; }
	XMFLOAT3 m_xmf3Direction = { 0, -1, 0 };

	void SetEffectStart(XMFLOAT3 xmf3Position, bool flag) {
		SetPosition(xmf3Position);
		m_isEffect = flag;
	}
	bool GetIsEffect() const { return m_isEffect; }
protected:
	bool m_isEffect = false;

protected:
	// 충돌박스
	CGameObject* m_pCollisionBox = nullptr;

	// hpbar
	CGameObject* m_pHpBar = nullptr;
	CGameObject* m_pHpBGBar = nullptr;
public:
	static class CCollisionBoxShader* m_pCollisionBoxShader;

	// hpbar
	static class CHpShader* m_pHpBarShader;
public:
	void CreateCollisionBox(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList,
		ID3D12RootSignature* pd3dGraphicsRootSignature);
	void CollisionBoxRender(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr);

	// hpbar
	void CreateHpBar(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList,
		ID3D12RootSignature* pd3dGraphicsRootSignature);
	void HpBarRender(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CHeightMapTerrain : public CGameObject
{
public:
	CHeightMapTerrain(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, 
		ID3D12RootSignature* pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength,
		int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color);
	virtual ~CHeightMapTerrain();

private:
	CHeightMapImage* m_pHeightMapImage;

	int m_nWidth;
	int m_nLength;

	XMFLOAT3 m_xmf3Scale;

public:
	float GetHeight(float x, float z, bool bReverseQuad = false) const { return(m_pHeightMapImage->GetHeight(x, z, bReverseQuad) * m_xmf3Scale.y); } //World
	XMFLOAT3 GetNormal(float x, float z) const { return(m_pHeightMapImage->GetHeightMapNormal(int(x / m_xmf3Scale.x), int(z / m_xmf3Scale.z))); }

	int GetHeightMapWidth() const { return(m_pHeightMapImage->GetHeightMapWidth()); }
	int GetHeightMapLength() const { return(m_pHeightMapImage->GetHeightMapLength()); }

	XMFLOAT3 GetScale() const { return(m_xmf3Scale); }
	float GetWidth() const { return(m_nWidth * m_xmf3Scale.x); }
	float GetLength() const { return(m_nLength * m_xmf3Scale.z); }
};

class CSkyBox : public CGameObject
{
public:
	CSkyBox(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature);
	virtual ~CSkyBox();

	virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera = nullptr, bool is_Shadow = false);
};

class CUIObject : public CGameObject
{
public:
	CUIObject() : CGameObject(1) {};
	virtual ~CUIObject() {};
	virtual void UpdateCB(FrameResource* pFrameResource);

	virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera = nullptr, bool is_Shadow = false);
private:
	XMFLOAT2 m_xmf2XYScale = { 1,1 };
	XMFLOAT2 m_xmf2CenterPosition = { 0, 0 };
public:
	void SetXYScale(XMFLOAT2 xmf2Scale) { m_xmf2XYScale = xmf2Scale; }
	void SetCenterPosition(XMFLOAT2 xmf2Center) { m_xmf2CenterPosition = xmf2Center; }
};