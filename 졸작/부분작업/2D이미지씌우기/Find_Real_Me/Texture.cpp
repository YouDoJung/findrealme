#include "stdafx.h"
#include "Texture.h"

CTexture::CTexture(int nTextures, UINT nTextureType, int nSamplers, bool is_upload)
{
	m_nTextureType = nTextureType;
	m_nTextures = nTextures;
	if (m_nTextures > 0)
	{
		m_pRootArgumentInfos = new SRVROOTARGUMENTINFO[m_nTextures];
		if (is_upload) {
			m_ppd3dTextureUploadBuffers = new ID3D12Resource*[m_nTextures];
		}
		m_ppd3dTextures = new ID3D12Resource*[m_nTextures];
		for (int i = 0; i < m_nTextures; ++i) {
			m_ppd3dTextures[i] = nullptr;
		}
	}

	m_nSamplers = nSamplers;
	if (m_nSamplers > 0) m_pd3dSamplerGpuDescriptorHandles = new D3D12_GPU_DESCRIPTOR_HANDLE[m_nSamplers];
}

CTexture::~CTexture()
{
	if (m_ppd3dTextures)
	{
		for (int i = 0; i < m_nTextures; i++) if (m_ppd3dTextures[i]) m_ppd3dTextures[i]->Release();
		delete[] m_ppd3dTextures;
	}

	if (m_pRootArgumentInfos)
	{
		delete[] m_pRootArgumentInfos;
	}

	if (m_pd3dSamplerGpuDescriptorHandles) delete[] m_pd3dSamplerGpuDescriptorHandles;
}

ID3D12Resource *CTexture::CreateTexture(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, UINT nWidth, UINT nHeight, DXGI_FORMAT dxgiFormat, D3D12_RESOURCE_FLAGS d3dResourceFlags, D3D12_RESOURCE_STATES d3dResourceStates, D3D12_CLEAR_VALUE *pd3dClearValue, UINT nIndex)
{
	m_ppd3dTextures[nIndex] = ::CreateTexture2DResource(pd3dDevice, pd3dCommandList, nWidth, nHeight, dxgiFormat, d3dResourceFlags, d3dResourceStates, pd3dClearValue);
	return(m_ppd3dTextures[nIndex]);
}

void CTexture::SetRootArgument(int nIndex, UINT nRootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSrvGpuDescriptorHandle)
{
	m_pRootArgumentInfos[nIndex].m_nRootParameterIndex = nRootParameterIndex;
	m_pRootArgumentInfos[nIndex].m_d3dSrvGpuDescriptorHandle = d3dSrvGpuDescriptorHandle;
}

void CTexture::SetSampler(int nIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSamplerGpuDescriptorHandle)
{
	m_pd3dSamplerGpuDescriptorHandles[nIndex] = d3dSamplerGpuDescriptorHandle;
}

void CTexture::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList)
{
	if (m_nTextureType == RESOURCE_TEXTURE2D_ARRAY)
	{
		pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[0].m_nRootParameterIndex, m_pRootArgumentInfos[0].m_d3dSrvGpuDescriptorHandle);
	}
	else
	{
		for (int i = 0; i < m_nTextures; i++)
		{
			pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[i].m_nRootParameterIndex, m_pRootArgumentInfos[i].m_d3dSrvGpuDescriptorHandle);
		}
	}
}

void CTexture::UpdateShaderVariable(ID3D12GraphicsCommandList* pd3dCommandList, int nIndex)
{
	pd3dCommandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[nIndex].m_nRootParameterIndex, m_pRootArgumentInfos[nIndex].m_d3dSrvGpuDescriptorHandle);
}

void CTexture::ReleaseUploadBuffers()
{
	if (m_ppd3dTextureUploadBuffers)
	{
		for (int i = 0; i < m_nTextures; i++) if (m_ppd3dTextureUploadBuffers[i]) m_ppd3dTextureUploadBuffers[i]->Release();
		delete[] m_ppd3dTextureUploadBuffers;
		m_ppd3dTextureUploadBuffers = nullptr;
	}
}

void CTexture::LoadTextureFromFile(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, wchar_t* pszFileName, UINT nIndex)
{
	m_ppd3dTextures[nIndex] = ::CreateTextureResourceFromDDSFile(pd3dDevice, pd3dCommandList, pszFileName, &m_ppd3dTextureUploadBuffers[nIndex], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
}

/////////////////////////////
TextureManager::TextureManager(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int nTextures, UINT nTextureType)
{
	m_pTexture = new CTexture(nTextures, nTextureType);
	// 지형
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Terrain/earth.dds", 0);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Terrain/GrassyRocks.dds", 1);
	// 플레이어
	// 플레이어
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/SwordMan/SwordMan03.dds", 2);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/SwordMan/SwordMan03_NRM.dds", 3);
	// 플레이어 무기
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/SwordMan/SwordMan03_W.dds", 4);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/SwordMan/SwordMan03_W_NRM.dds", 5);
	// 마을 텍스처
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Portal/Portal.dds", 6);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Portal/PortalNormal.dds", 7);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Bridge/Bridge.dds", 8);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Bridge/BridgeNormal.dds", 9);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Fence/Fence.dds", 10);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Fence/FenceNormal.dds", 11);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Building,etc/Building_Texture.dds", 12);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Building,etc/Building_NormalTexture.dds", 13);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Tree/Pinetree.dds", 14);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Tree/Pinetreenormal.dds", 15);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Tree/Pinetree2.dds", 16);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Tree/Pinetreenormal2.dds", 17);

	// 탑 내부
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/TopInside/Top.dds", 18);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/TopInside/TopNormal.dds", 19);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/TopInside/Door.dds", 20);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/TopInside/DoorNRM.dds", 21);

	// 일반던전
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/floor2.dds", 22);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/floorNRM.dds", 23);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Metal_A.dds", 24);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Metal_A_NRM.dds", 25);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Wall3.dds", 26);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Wall3_NRM.dds", 27);

	// 보스 던전
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/BossDungeon/lava.dds", 28);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/BossDungeon/lavaNRM.dds", 29);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Metal.dds", 30);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Metal_NRM.dds", 31);

	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Wall.dds", 32);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Dungeon/Wall_NRM.dds", 33);

	// 그런트
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Grunt/Grunt.dds", 34);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Grunt/Grunt_NRM.dds", 35);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons.dds", 36);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons_NRM.dds", 37);

	// 피언
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Peon/Peon.dds", 38);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Peon/Peon_NRM.dds", 39);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons.dds", 40);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons_NRM.dds", 41);

	// 로드
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Lord/Lord.dds", 42);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Lord/Lord_NRM.dds", 43);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons.dds", 44);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons_NRM.dds", 45);

	// 그런트 Alt
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Grunt/GruntAlt.dds", 46);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Grunt/Grunt_NRM.dds", 47);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons.dds", 48);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons_NRM.dds", 49);

	// 피언 Alt
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Peon/PeonAlt.dds", 50);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Peon/Peon_NRM.dds", 51);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons.dds", 52);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons_NRM.dds", 53);

	// 로드 Alt
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Lord/LordAlt.dds", 54);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Lord/Lord_NRM.dds", 55);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons.dds", 56);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Weapons_NRM.dds", 57);

	// 보스드래곤
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Dragon/DragonBoss_DS.dds", 58);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Monster/Dragon/DragonBoss_N.dds", 59);

	// 파티클
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Particle/flame.dds", 60);
	// 파티클
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Particle/flame2.dds", 61);

	// 갈라짐
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Effect/Crack2.dds", 62);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Effect/Crack3.dds", 63);
	// 이펙트
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Effect/Lightning1.dds", 64);
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Effect/Lightning2.dds", 65);
	// 물
	m_pTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/Water/Water.dds", 66);

	m_pSkyBoxTexture = new CTexture(1, RESOURCE_TEXTURE_CUBE, 0);
	m_pSkyBoxTexture->LoadTextureFromFile(pd3dDevice, pd3dCommandList, L"../Image/SkyBox/SkyBox1.dds", 0);

	// 텍스처개수 + 그림자 + 스카이박스
	CreateSrvAndUavDescriptorHeaps(pd3dDevice, pd3dCommandList, nTextures + 2, 0);
	CreateShaderResourceViews(pd3dDevice, pd3dCommandList, m_pTexture, 3, false);

	// 스카이박스 쉐이더리소스뷰 생성
	CreateShaderResourceViews(pd3dDevice, pd3dCommandList, m_pSkyBoxTexture, 5, false);

	//UINT index = nTextures;
	//m_d3dShaderSrvGPUDescriptorHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_d3dSrvGPUDescriptorStartHandle, index, ::gnCbvSrvUavDescriptorIncrementSize);
	//auto nullSrv = CD3DX12_CPU_DESCRIPTOR_HANDLE(m_d3dSrvCPUDescriptorStartHandle, index, ::gnCbvSrvUavDescriptorIncrementSize);

	//D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	//srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	//srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	//srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//srvDesc.Texture2D.MostDetailedMip = 0;
	//srvDesc.Texture2D.MipLevels = 1;
	//srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
	//pd3dDevice->CreateShaderResourceView(nullptr, &srvDesc, nullSrv);
}

void TextureManager::ReleaseUploadBuffers()
{
	m_pTexture->ReleaseUploadBuffers();
	m_pSkyBoxTexture->ReleaseUploadBuffers();
}

TextureManager::~TextureManager()
{
	delete m_pTexture;
	delete m_pSkyBoxTexture;
}

void TextureManager::CreateSrvAndUavDescriptorHeaps(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, int nShaderResourceViews, int nUnorderedAccessViews)
{
	D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
	d3dDescriptorHeapDesc.NumDescriptors = nShaderResourceViews + nUnorderedAccessViews; //CBVs + SRVs 
	d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	d3dDescriptorHeapDesc.NodeMask = 0;
	pd3dDevice->CreateDescriptorHeap(&d3dDescriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&m_pd3dSrvUavDescriptorHeap);

	m_d3dSrvCPUDescriptorStartHandle = m_pd3dSrvUavDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	m_d3dSrvGPUDescriptorStartHandle = m_pd3dSrvUavDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
	m_d3dUavCPUDescriptorStartHandle.ptr = m_d3dSrvCPUDescriptorStartHandle.ptr + (::gnCbvSrvUavDescriptorIncrementSize * nShaderResourceViews);
	m_d3dUavGPUDescriptorStartHandle.ptr = m_d3dSrvCPUDescriptorStartHandle.ptr + (::gnCbvSrvUavDescriptorIncrementSize * nShaderResourceViews);
}

D3D12_SHADER_RESOURCE_VIEW_DESC TextureManager::GetShaderResourceViewDesc(D3D12_RESOURCE_DESC d3dResourceDesc, UINT nTextureType)
{
	D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc;
	d3dShaderResourceViewDesc.Format = d3dResourceDesc.Format;
	d3dShaderResourceViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	switch (nTextureType)
	{
	case RESOURCE_TEXTURE2D: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize == 1)
	case RESOURCE_TEXTURE2D_ARRAY:
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		d3dShaderResourceViewDesc.Texture2D.MipLevels = -1;
		d3dShaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.Texture2D.PlaneSlice = 0;
		d3dShaderResourceViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;
		break;
	case RESOURCE_TEXTURE2DARRAY: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize != 1)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
		d3dShaderResourceViewDesc.Texture2DArray.MipLevels = -1;
		d3dShaderResourceViewDesc.Texture2DArray.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.Texture2DArray.PlaneSlice = 0;
		d3dShaderResourceViewDesc.Texture2DArray.ResourceMinLODClamp = 0.0f;
		d3dShaderResourceViewDesc.Texture2DArray.FirstArraySlice = 0;
		d3dShaderResourceViewDesc.Texture2DArray.ArraySize = d3dResourceDesc.DepthOrArraySize;
		break;
	case RESOURCE_TEXTURE_CUBE: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize == 6)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
		d3dShaderResourceViewDesc.TextureCube.MipLevels = -1;
		d3dShaderResourceViewDesc.TextureCube.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.TextureCube.ResourceMinLODClamp = 0.0f;
		break;
	case RESOURCE_BUFFER: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_BUFFER)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
		d3dShaderResourceViewDesc.Buffer.FirstElement = 0;
		d3dShaderResourceViewDesc.Buffer.NumElements = 0;
		d3dShaderResourceViewDesc.Buffer.StructureByteStride = 0;
		d3dShaderResourceViewDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
		break;
	}
	return(d3dShaderResourceViewDesc);
}

void TextureManager::CreateShaderResourceViews(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, CTexture* pTexture, UINT nRootParameterStartIndex, bool bAutoIncrement)
{
	D3D12_CPU_DESCRIPTOR_HANDLE d3dSrvCPUDescriptorHandle = m_d3dSrvCPUDescriptorStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE d3dSrvGPUDescriptorHandle = m_d3dSrvGPUDescriptorStartHandle;
	int nTextures = pTexture->GetTextures();
	int nTextureType = pTexture->GetTextureType();
	for (int i = 0; i < nTextures; i++)
	{
		ID3D12Resource* pShaderResource = pTexture->GetTexture(i);
		D3D12_RESOURCE_DESC d3dResourceDesc = pShaderResource->GetDesc();
		D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc = GetShaderResourceViewDesc(d3dResourceDesc, nTextureType);
		pd3dDevice->CreateShaderResourceView(pShaderResource, &d3dShaderResourceViewDesc, d3dSrvCPUDescriptorHandle);
		d3dSrvCPUDescriptorHandle.ptr += ::gnCbvSrvUavDescriptorIncrementSize;

		pTexture->SetRootArgument(i, (bAutoIncrement) ? (nRootParameterStartIndex + i) : nRootParameterStartIndex, d3dSrvGPUDescriptorHandle);
		d3dSrvGPUDescriptorHandle.ptr += ::gnCbvSrvUavDescriptorIncrementSize;

	}
}

void TextureManager::SetDescriptorHeapsAndTextureUpdate(ID3D12GraphicsCommandList* pd3dCommandList)
{
	ID3D12DescriptorHeap* descriptorHeaps[] = { m_pd3dSrvUavDescriptorHeap.Get() };
	pd3dCommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);

	m_pTexture->UpdateShaderVariable(pd3dCommandList, 0);

}

void TextureManager::SetDescriptorHeaps(ID3D12GraphicsCommandList* pd3dCommandList)
{
	ID3D12DescriptorHeap* descriptorHeaps[] = { m_pd3dSrvUavDescriptorHeap.Get() };
	pd3dCommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);
}

void TextureManager::TexturesUpdate(ID3D12GraphicsCommandList* pd3dCommandList)
{
	m_pTexture->UpdateShaderVariable(pd3dCommandList, 0);
	m_pSkyBoxTexture->UpdateShaderVariable(pd3dCommandList, 0);
}

void TextureManager::ShadowSrvUpdate(ID3D12GraphicsCommandList* pd3dCommandList)
{
	pd3dCommandList->SetGraphicsRootDescriptorTable(4, m_d3dShaderSrvGPUDescriptorHandle);
}
