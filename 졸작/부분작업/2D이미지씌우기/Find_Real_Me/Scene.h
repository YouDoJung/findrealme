#pragma once
#include "Shader.h"
#include "Player.h"
#include "ModelShader.h"

class CScene
{
public:
	CScene();
	~CScene();

	bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	void BuildObjects(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList);
	void ReleaseObjects();

	void CreateGraphicsRootSignature(ID3D12Device* pd3dDevice);
	ID3D12RootSignature* GetGraphicsRootSignature() const { return(m_pd3dGraphicsRootSignature.Get()); }
	void SetGraphicsRootSignature(ID3D12GraphicsCommandList* pd3dCommandList) { pd3dCommandList->SetGraphicsRootSignature(m_pd3dGraphicsRootSignature.Get()); }

	bool ProcessInput(UCHAR* pKeysBuffer);
	void AnimateObjects(float fTimeElapsed);
	//
	void UpdateObjects(FrameResource* pFrameResource, float fTime);
	//
	void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);

	CHeightMapTerrain* GetTerrain() const { return(m_pTerrain); }

	CPlayer* m_pPlayer = nullptr;

	void SetPlayer()
	{
		for (int i = 0; i < m_nShaders; ++i) {
			m_ppShaders[i]->SetPlayer(m_pPlayer);
		}
	}
	void SetMapNum(int num) { m_eMaplist = num; }
protected:
	int m_eMaplist = MAP_LIST::VILLAGE;
	CShader* m_pWaterShader = nullptr;
	CShader* m_pLavaShader = nullptr;
	CShader* m_pSpurtShader = nullptr;

	CShader** m_ppShaders = nullptr;
	int m_nShaders = 0;

	CHeightMapTerrain* m_pTerrain = nullptr;
	CSkyBox* m_pSkyBox = nullptr;

	ComPtr<ID3D12RootSignature>	m_pd3dGraphicsRootSignature = nullptr;

	FrameResource* m_pCurrResource = nullptr;
private:

	D3D12_VIEWPORT mShadowViewport;
	D3D12_RECT mShadowScissorRect;

public:
	BYTE GetMapNum() { return m_eMaplist; };
};
