#include "Shaders.hlsl"

struct VS_MODEL_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float2 uv : TEXCOORD;
};

struct VS_MODEL_OUTPUT
{
	float4 position : SV_POSITION;
	float4 shadowPosition : POSITION0;
	float3 positionW : POSITION1;
	float3 normalW : NORMAL;
	float3 tangentW : TANGENT;
	float2 uv : TEXCOORD;
};

struct VS_SKINING_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float2 uv : TEXCOORD;
	uint4 index : BONEINDEX;
	float3 weight : WEIGHT;
};

VS_MODEL_OUTPUT VSSkining(VS_SKINING_INPUT input) {
	VS_MODEL_OUTPUT output = (VS_MODEL_OUTPUT)0.f;

	float3 posL = float3(0.0f, 0.0f, 0.0f);
	float3 normalL = float3(0.0f, 0.0f, 0.0f);
	float3 tangentL = float3(0.f, 0.f, 0.f);
	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = input.weight.x;
	weights[1] = input.weight.y;
	weights[2] = input.weight.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];
	if (input.index[0] == 0 && input.index[1] == 0 && input.weight.x == 0 && input.weight.y == 0)
		posL = input.position;
	else {
		for (int i = 0; i < 4; ++i) {
			posL += weights[i] * mul(float4(input.position, 1.0f),
				gmtxBoneTransforms[input.index[i]]).xyz;
			normalL += weights[i] * mul(input.normal,
				(float3x3)gmtxBoneTransforms[input.index[i]]).xyz;
			tangentL += weights[i] * mul(input.tangent,
				(float3x3)gmtxBoneTransforms[input.index[i]]).xyz;
		}
	}
	output.positionW = (float3)mul(float4(posL, 1.0f), gmtxObjectWorld);
	//output.positionW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.normalW = mul(normalL, (float3x3)gmtxObjectWorld);
	output.tangentW = mul(tangentL, (float3x3)gmtxObjectWorld);
	output.position = mul(float4(output.positionW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;
	output.shadowPosition = mul(float4(output.positionW, 1.f), gmtxShadowTransform);
	return output;
}

float4 PSSkining(VS_MODEL_OUTPUT input) : SV_TARGET{

	float4 diffuseAlbedo = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].Sample(gSamplerState, input.uv);

	input.normalW = normalize(input.normalW);
	float4 normalColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.y)].Sample(gSamplerState, input.uv);
	float3 BumpNormalW = NormalSampleToWorldSpace(normalColor.rgb, input.normalW, input.tangentW);

	float3 toEyeW = normalize(gvCameraPosition - input.positionW);
	float3 shadowFactor = float3(1.0f, 1.0f, 1.0f);
	shadowFactor[0] = CalcShadowFactor(input.shadowPosition);

	float4 directLight = ComputeLighting(gLights, gMaterial, input.positionW, input.normalW, toEyeW, shadowFactor);

	float4 ambient = diffuseAlbedo;
	float4 cColor = ambient + directLight;

	cColor.a = diffuseAlbedo.a;
	return cColor;
}

VS_MODEL_OUTPUT VSModel(VS_MODEL_INPUT input) {

	VS_MODEL_OUTPUT output = (VS_MODEL_OUTPUT)0.f;

	float4 posW = mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.positionW = posW.xyz;
	output.normalW = mul(input.normal, (float3x3)gmtxObjectWorld);
	output.tangentW = mul(input.tangent, (float3x3)gmtxObjectWorld);
	output.position = mul(float4(output.positionW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;
	output.shadowPosition = mul(posW, gmtxShadowTransform);
	return output;
}

float4 PSModel(VS_MODEL_OUTPUT input) : SV_TARGET{

	float3 toEyeW = normalize(gvCameraPosition - input.positionW);

	float3 shadowFactor = float3(1.0f, 1.0f, 1.0f);
	shadowFactor[0] = CalcShadowFactor(input.shadowPosition);

	float4 diffuseAlbedo = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);

	float4 normalColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.y)].SampleLevel(gSamplerState, input.uv, 0);

	float3 normal = NormalSampleToWorldSpace(normalColor.rgb, input.normalW, input.tangentW);

	float4 directLight = ComputeLighting(gLights, gMaterial, input.positionW, normal, toEyeW, shadowFactor);

	float4 ambient = gAmbientLight * diffuseAlbedo;

	
	float4 cColor = ambient;
	if (gTextureIndex.y != 5 && gTextureIndex.y != 37)
	{
		cColor += directLight;
	}
	cColor.a = diffuseAlbedo.a;
	return cColor;
}