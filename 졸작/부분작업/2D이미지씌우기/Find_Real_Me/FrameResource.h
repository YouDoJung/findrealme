#pragma once
#include "UploadBuffer.h"
#define MAXLIGHTS 16
struct LIGHT
{
	XMFLOAT3 m_xmf3Strength = { 0.5f, 0.5f, 0.5f };
	float m_FalloffStart = 1.0f;                          // point/spot light only
	XMFLOAT3 m_xmf3Direction = { 0.0f, -1.0f, 0.0f };// directional/spot light only
	float m_FalloffEnd = 10.0f;                           // point/spot light only
	XMFLOAT3 m_xmf3Position = { 0.0f, 0.0f, 0.0f };  // point/spot light only
	float m_SpotPower = 64.0f;                            // spot light only
};

struct PassConstants
{
	XMFLOAT4X4 m_xmf4x4View;
	XMFLOAT4X4 m_xmf4x4InverseView;
	XMFLOAT4X4 m_xmf4x4Projection;
	XMFLOAT4X4 m_xmf4x4InverseProjection;

	XMFLOAT4X4 m_xmf4x4ViewProjection;
	XMFLOAT4X4 m_xmf4x4ShadowTransform;

	XMFLOAT3 m_xmf3EyePosition;
	float m_fDeltaTime;

	XMFLOAT4 m_xmf4GlobalLight;
	LIGHT m_pLights[MAXLIGHTS];

	float m_fTotalTime;
};

struct SkinnedConstants
{
	XMFLOAT4X4 m_xmf4x4BoneTransforms[SKINNED_ANIMATION_BONES];
};

struct ObjectConstants
{
	XMFLOAT4X4 m_xmf4x4World;

	XMFLOAT4 m_xmf4DiffuseAlbedo;
	XMFLOAT3 m_xmf3FresnelR0;
	float Shininess;

	XMUINT2 m_xmi2TexIndex;
	float m_fFrameTime = 0.f;
	float m_fStartTime = 0.f;
};

// CPU가 한 프레임의 명령 리스트들을 구축하는 데 필요한 자원들을 대표하는 클래스
struct FrameResource
{
public:

	FrameResource(ID3D12Device* device, UINT passCount, UINT objectCount, UINT skinnedObjectCount);
	FrameResource(const FrameResource& rhs) = delete;
	FrameResource& operator=(const FrameResource& rhs) = delete;
	~FrameResource();

	// 명령할당자는 GPU가 명령들을 다 처리한 후 재설정해야 한다.
	// 따라서 프레임마다 할당자가 필요하다.
	ComPtr<ID3D12CommandAllocator> CmdListAlloc;

	// 상수버퍼는 참조하는 명령들을 GPU가 다 처리한 후에 갱신해야 한다. 따라서 프레임마다
	// 상수버퍼를 새로 만들어야 한다.
	// 카메라
	unique_ptr<UploadBuffer<PassConstants>> PassCB = nullptr;
	// 오브젝트
	unique_ptr<UploadBuffer<ObjectConstants>> ObjectCB = nullptr;
	// 뼈의 최종변환행렬
	unique_ptr<UploadBuffer<SkinnedConstants>> SkinnedCB = nullptr;
	// Fence는 현재 울타리 지점까지의 명령들을 표시하는 값이다.
	// 이 값은 GPU가 아직 이 프레임 자원들을 사용하고 있는지 판정하는 용도로 쓰인다.
	UINT64 Fence = 0;
};

