#include "stdafx.h"
#include "GameFramework.h"
#include <chrono>
using namespace chrono;

GameFramework::GameFramework()
{
	m_WndClientWidth = FRAME_BUFFER_WIDTH;
	m_WndClientHeight = FRAME_BUFFER_HEIGHT;
}

float GameFramework::AspectRatio()const
{
	return static_cast<float>(FRAME_BUFFER_WIDTH) / FRAME_BUFFER_HEIGHT;
}

bool GameFramework::OnCreate(HINSTANCE hInstance, HWND hMainWnd)
{
	m_hAppInst = hInstance;
	m_hMainWnd = hMainWnd;

	CreateDirect3DDevice();
	CreateCommandQueueAndList();
#ifdef _WITH_DIRECT2D
	CreateDirect2DDevice();
#endif

	CreateSwapChain();
	CreateRtvAndDsvDescriptorHeaps();

	CreateDepthStencilView();

	BuildObjects();
	return(true);
}

void GameFramework::OnDestroy()
{
	FlushCommandQueue();

	if (m_pScene) {
		m_pScene->ReleaseObjects();
		delete m_pScene;
	}

	if (m_ppMonsterObjects) {
		for (int i = 0; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (m_ppMonsterObjects[i]) {
				delete m_ppMonsterObjects[i];
				m_ppMonsterObjects[i] = nullptr;
			}
		}
		delete[] m_ppMonsterObjects;
		m_ppMonsterObjects = nullptr;
	}
	if (m_pBoss) {
		delete m_pBoss;
		m_pBoss = nullptr;
	}
	if (m_pSkinnedObjectShader) {
		m_pSkinnedObjectShader->Release();
		m_pSkinnedObjectShader = nullptr;
	}

	for (auto& data : m_mModelData) {
		if (data.second) {
			delete data.second;
			data.second = nullptr;
		}
	}
	m_mModelData.clear();

	if (m_pPlayer) {
		delete m_pPlayer;
	}

	if (CPlayer::m_pCrackEffectShader) {
		delete CPlayer::m_pCrackEffectShader;
		CPlayer::m_pCrackEffectShader = nullptr;
	}
	if (CPlayer::m_pSkinnedObjectShader) {
		delete CPlayer::m_pSkinnedObjectShader;
		CPlayer::m_pSkinnedObjectShader = nullptr;
	}

	if (m_TextureMgr) delete m_TextureMgr;

	if (m_pTestRectShader) delete m_pTestRectShader;

#ifdef _WITH_DIRECT2D
	if (m_pd2dbrBackground) m_pd2dbrBackground->Release();
	if (m_pd2dbrBorder) m_pd2dbrBorder->Release();
	if (m_pdwFont) m_pdwFont->Release();
	if (m_pdwTextLayout) m_pdwTextLayout->Release();
	if (m_pd2dbrText) m_pd2dbrText->Release();
#ifdef _WITH_DIRECT2D_IMAGE_EFFECT
	if (m_pd2dfxBitmapSource) m_pd2dfxBitmapSource->Release();
	if (m_pd2dfxGaussianBlur) m_pd2dfxGaussianBlur->Release();
	if (m_pd2dsbDrawingState) m_pd2dsbDrawingState->Release();
	if (m_pwicFormatConverter) m_pwicFormatConverter->Release();
	if (m_pwicImagingFactory) m_pwicImagingFactory->Release();
#endif

	if (m_pd2dDeviceContext) m_pd2dDeviceContext->Release();
	if (m_pd2dDevice) m_pd2dDevice->Release();
	if (m_pdWriteFactory) m_pdWriteFactory->Release();
	if (m_pd3d11On12Device) m_pd3d11On12Device->Release();
	if (m_pd3d11DeviceContext) m_pd3d11DeviceContext->Release();
	if (m_pd2dFactory) m_pd2dFactory->Release();

	for (int i = 0; i < m_SwapChainBufferCount; i++)
	{
		if (m_ppd3d11WrappedBackBuffers[i]) m_ppd3d11WrappedBackBuffers[i]->Release();
		if (m_ppd2dRenderTargets[i]) m_ppd2dRenderTargets[i]->Release();
	}
#endif
}

void GameFramework::CreateDirect3DDevice()
{
#if defined(DEBUG) || defined(_DEBUG) 
	// 디버그 층 활성화
	{
		ComPtr<ID3D12Debug> debugController;

		ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)));
		debugController->EnableDebugLayer();
	}
#endif
	HRESULT hResult = ::CreateDXGIFactory2(0, __uuidof(IDXGIFactory4), (void **)&m_pdxgiFactory);
	//ThrowIfFailed(CreateDXGIFactory2(0, IID_PPV_ARGS(&m_pDxgiFactory)));

	IDXGIAdapter1* pd3dAdapter = NULL;

	for (UINT i = 0; DXGI_ERROR_NOT_FOUND != m_pdxgiFactory->EnumAdapters1(i, &pd3dAdapter); i++)
	{
		DXGI_ADAPTER_DESC1 dxgiAdapterDesc;
		pd3dAdapter->GetDesc1(&dxgiAdapterDesc);
		if (dxgiAdapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;
		if (SUCCEEDED(D3D12CreateDevice(pd3dAdapter, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&m_pD3dDevice)))) break;
	}

	// 실패했으면 WARP device생성
	/*if (FAILED(hardwareResult))
	{
		ComPtr<IDXGIAdapter> pWarpAdapter;
		ThrowIfFailed(m_pDxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&pWarpAdapter)));

		ThrowIfFailed(D3D12CreateDevice(
			pWarpAdapter.Get(),
			D3D_FEATURE_LEVEL_11_0,
			IID_PPV_ARGS(&m_pD3dDevice)));
	}*/

	ThrowIfFailed(m_pD3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE,
		IID_PPV_ARGS(&m_pFence)));

	m_RtvDescriptorSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_DsvDescriptorSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	gnCbvSrvUavDescriptorIncrementSize = m_pD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	// Check 4X MSAA quality support for our back buffer format.
	// All Direct3D 11 capable devices support 4X MSAA for all render 
	// target formats, so we only need to check quality support.
	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msQualityLevels;
	msQualityLevels.Format = m_BackBufferFormat;
	msQualityLevels.SampleCount = 4;
	msQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	msQualityLevels.NumQualityLevels = 0;
	ThrowIfFailed(m_pD3dDevice->CheckFeatureSupport(
		D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS,
		&msQualityLevels,
		sizeof(msQualityLevels)));

	m_4xMsaaQuality = msQualityLevels.NumQualityLevels;
	assert(m_4xMsaaQuality > 0 && "Unexpected MSAA quality level.");

	//m_pD3dDevice->QueryInterface(IID_PPV_ARGS(&device));
	//device->ReportLiveDeviceObjects(D3D12_RLDO_DETAIL);
}

void GameFramework::CreateSwapChain()
{
	// 새 스왑체인을 만들기 전에 먼저 기존 스왑체인을 해제.
	m_pSwapChain.Reset();

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = m_WndClientWidth;
	sd.BufferDesc.Height = m_WndClientHeight;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = m_BackBufferFormat;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.SampleDesc.Count = m_is4xMsaaState ? 4 : 1;
	sd.SampleDesc.Quality = m_is4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = m_SwapChainBufferCount;
	sd.OutputWindow = m_hMainWnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	// Note: 스왑체인은  명령큐를 이용해 방출(flush)를 수행한다.
	ThrowIfFailed(m_pdxgiFactory->CreateSwapChain(
		m_pCommandQueue.Get(),
		&sd,	
		m_pSwapChain.GetAddressOf()));
}

void GameFramework::CreateCommandQueueAndList()
{
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	ThrowIfFailed(m_pD3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pCommandQueue)));

	ThrowIfFailed(m_pD3dDevice->CreateCommandAllocator(
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		IID_PPV_ARGS(m_pDirectCmdListAlloc.GetAddressOf())));

	ThrowIfFailed(m_pD3dDevice->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		m_pDirectCmdListAlloc.Get(), // 연관된 명령 할당자.
		nullptr,                   // 초기 파이프라인 상태 객체
		IID_PPV_ARGS(m_pCommandList.GetAddressOf())));

	// 닫힌 상태로 시작한다. 이후 명령 리스트를 처음 참조할 때 Reset을 호출하는데
	// Reset을 호출하려면 명령 리스트가 닫혀 있어야 한다.
	ThrowIfFailed(m_pCommandList->Close());
}

void GameFramework::CreateRtvAndDsvDescriptorHeaps()
{
	// 다중렌더타겟할라면 NumDescriptors 수정하셈.
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
	rtvHeapDesc.NumDescriptors = m_SwapChainBufferCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	rtvHeapDesc.NodeMask = 0;
	ThrowIfFailed(m_pD3dDevice->CreateDescriptorHeap(
		&rtvHeapDesc, IID_PPV_ARGS(m_pRtvHeap.GetAddressOf())));

	// 그림자 때문에.
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
	dsvHeapDesc.NumDescriptors = 2;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dsvHeapDesc.NodeMask = 0;
	ThrowIfFailed(m_pD3dDevice->CreateDescriptorHeap(
		&dsvHeapDesc, IID_PPV_ARGS(m_pDsvHeap.GetAddressOf())));
}

void GameFramework::CreateRenderTargetViews()
{
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_pRtvHeap->GetCPUDescriptorHandleForHeapStart());
	for (UINT i = 0; i < m_SwapChainBufferCount; ++i)
	{
		ThrowIfFailed(m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(&m_pSwapChainBuffer[i])));
		m_pD3dDevice->CreateRenderTargetView(m_pSwapChainBuffer[i].Get(), nullptr, rtvHeapHandle);
		rtvHeapHandle.Offset(1, m_RtvDescriptorSize);
	}
#ifdef _WITH_DIRECT2D
	float fxDPI, fyDPI;
	m_pd2dFactory->GetDesktopDpi(&fxDPI, &fyDPI); //UINT GetDpiForWindow(HWND hwnd);

	D2D1_BITMAP_PROPERTIES1 d2dBitmapProperties = D2D1::BitmapProperties1(D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), fxDPI, fyDPI);

	for (UINT i = 0; i < m_SwapChainBufferCount; i++)
	{
		D3D11_RESOURCE_FLAGS d3d11Flags = { D3D11_BIND_RENDER_TARGET };
		m_pd3d11On12Device->CreateWrappedResource(m_pSwapChainBuffer[i].Get(), &d3d11Flags, D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT, __uuidof(ID3D11Resource), (void **)&m_ppd3d11WrappedBackBuffers[i]);

		IDXGISurface *pdxgiSurface = NULL;
		m_ppd3d11WrappedBackBuffers[i]->QueryInterface(__uuidof(IDXGISurface), (void **)&pdxgiSurface);
		m_pd2dDeviceContext->CreateBitmapFromDxgiSurface(pdxgiSurface, &d2dBitmapProperties, &m_ppd2dRenderTargets[i]);
		if (pdxgiSurface) pdxgiSurface->Release();
	}
#endif
}

void GameFramework::CreateDepthStencilView()
{
	// Create the depth/stencil buffer and view.
	D3D12_RESOURCE_DESC depthStencilDesc;
	depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	depthStencilDesc.Alignment = 0;
	depthStencilDesc.Width = m_WndClientWidth;
	depthStencilDesc.Height = m_WndClientHeight;
	depthStencilDesc.DepthOrArraySize = 1;
	depthStencilDesc.MipLevels = 1;

	depthStencilDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;

	depthStencilDesc.SampleDesc.Count = m_is4xMsaaState ? 4 : 1;
	depthStencilDesc.SampleDesc.Quality = m_is4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
	depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE optClear;
	optClear.Format = m_DepthStencilFormat;
	optClear.DepthStencil.Depth = 1.0f;
	optClear.DepthStencil.Stencil = 0;
	ThrowIfFailed(m_pD3dDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthStencilDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&optClear,
		IID_PPV_ARGS(m_pDepthStencilBuffer.GetAddressOf())));

	// Create descriptor to mip level 0 of entire resource using the format of the resource.
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = m_DepthStencilFormat;
	dsvDesc.Texture2D.MipSlice = 0;
	m_pD3dDevice->CreateDepthStencilView(m_pDepthStencilBuffer.Get(), &dsvDesc, GetDepthStencilView());
}

void GameFramework::OnResizeBackBuffers()
{
	assert(m_pD3dDevice);
	assert(m_pSwapChain);
	assert(m_pDirectCmdListAlloc);

	// Flush before changing any resources.
	FlushCommandQueue();

	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));

	// Release the previous resources we will be recreating.
	for (int i = 0; i < m_SwapChainBufferCount; ++i)
		m_pSwapChainBuffer[i].Reset();
	m_pDepthStencilBuffer.Reset();

	// Resize the swap chain.
	ThrowIfFailed(m_pSwapChain->ResizeBuffers(
		m_SwapChainBufferCount,
		m_WndClientWidth, m_WndClientHeight,
		m_BackBufferFormat,
		DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));

	m_CurrBackBuffer = 0;

	CreateRenderTargetViews();
	CreateDepthStencilView();
	// Execute the resize commands.
	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	// Wait until resize is complete.
	FlushCommandQueue();

}

void GameFramework::FlushCommandQueue()
{
	// 현재 펜스 지점까지의 명령들을 표시하도록 펜스 값을 증가시킨다.
	m_CurrentFence++;

	// 새 펜스 지점을 설정하는 명령(Signal)을 명령 대기열에 추가한다.
	// 지금 우리는 gpu 시간선에 있으므로, 새 펜스 지점은 gpu가 Signal 명령까지의
	// 모든 명령을 처리하기 전까지는 설정되지 않는다.
	ThrowIfFailed(m_pCommandQueue->Signal(m_pFence.Get(), m_CurrentFence));

	// GPU가 이 펜스 지점까지의 명령들을 완료할 때까지 기다린다.
	if (m_pFence->GetCompletedValue() < m_CurrentFence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);

		// 도달했으면 이벤트를 발생시킨다. 
		ThrowIfFailed(m_pFence->SetEventOnCompletion(m_CurrentFence, eventHandle));

		// GPU가 현재 펜스 지점에 도달했음을 뜻하는 이벤트를 기다린다.
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}
}

ID3D12Resource* GameFramework::GetCurrentBackBuffer()const
{
	return m_pSwapChainBuffer[m_CurrBackBuffer].Get();
}

D3D12_CPU_DESCRIPTOR_HANDLE GameFramework::GetCurrentBackBufferView()const
{
	return CD3DX12_CPU_DESCRIPTOR_HANDLE(
		m_pRtvHeap->GetCPUDescriptorHandleForHeapStart(),
		m_CurrBackBuffer,
		m_RtvDescriptorSize);
}

D3D12_CPU_DESCRIPTOR_HANDLE GameFramework::GetDepthStencilView()const
{
	return m_pDsvHeap->GetCPUDescriptorHandleForHeapStart();
}

void GameFramework::CalculateFrameStats()
{
	// Code computes the average frames per second, and also the 
	// average time it takes to render one frame.  These stats 
	// are appended to the window caption bar.

	static int frameCnt = 0;
	static float timeElapsed = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if ((m_Timer.GetTotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;

		wstring fpsStr = to_wstring(fps);
		wstring mspfStr = to_wstring(mspf);

		wstring windowText = m_MainWndCaption +
			L"    fps: " + fpsStr +
			L"   mspf: " + mspfStr + 
			L"   " + to_wstring(m_pPlayer->GetPosition().x) +
			L"," + to_wstring(m_pPlayer->GetPosition().y) +
			L"," + to_wstring(m_pPlayer->GetPosition().z) + L"FrameTime : " + to_wstring(m_Timer.GetDeltaTime());

		SetWindowText(m_hMainWnd, windowText.c_str());

		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}

void GameFramework::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		::SetCapture(hWnd);
		::GetCursorPos(&m_OldCursorPos);
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		::ReleaseCapture();
		break;
	case WM_MOUSEMOVE:
		break;
	default:
		break;
	}
}

void GameFramework::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_KEYUP:
		switch (wParam)
		{
		case VK_ESCAPE:
			::PostQuitMessage(0);
			break;
		case VK_RETURN:
			break;
		case VK_F1:
		case VK_F2:
		case VK_F3:
			m_pCamera = m_pPlayer->ChangeCamera((DWORD)(wParam - VK_F1 + 1), m_Timer.GetDeltaTime());
			break;
		case VK_F9:
		{
			BOOL bFullScreenState = FALSE;
			m_pSwapChain->GetFullscreenState(&bFullScreenState, NULL);
			m_pSwapChain->SetFullscreenState(!bFullScreenState, NULL);

			OnResizeBackBuffers();
			break;
		}
		case VK_F10:
			break;
		case 0x5A:	//z
			m_pPlayer->SetNowAnimation(string("attack1"));
			break;
		case 0x58:	//x
			m_pPlayer->SetNowAnimation(string("attack2"));
			break;
		case 0x31:	//1
			m_pPlayer->SetNowAnimation(string("skill1"));
			break;
		case 0x32:	//2
			m_pPlayer->SetNowAnimation(string("skill2"));
			break;
		case 0x33:	//3
			m_pPlayer->SetNowAnimation(string("skill3"));
			break;
		case 0x34:	//4
			m_pPlayer->SetNowAnimation(string("skill4"));
			break;
		case 0x35:	//5
			m_pPlayer->SetNowAnimation(string("skill5"));
			break;
		case 0x43:	//c
			m_pPlayer->SetNowAnimation(string("stun"));
			break;
		case 0x56:	//v
			m_pPlayer->SetNowAnimation(string("damage"));
			break;
		case 0x42:	//b
			m_pPlayer->SetNowAnimation(string("die"));
			break;
		case VK_SHIFT:
			++m_eMaplist %= 4;

			if (m_eMaplist) {
				m_pPlayer->SetPlayerUpdatedContext(nullptr);
			}
			else {
				m_pPlayer->SetPlayerUpdatedContext(nullptr);
			}
			m_pPlayer->SetPosition(XMFLOAT3(5000.f, 516.f, 5000.f));
			
			m_pScene->SetMapNum(m_eMaplist);
			break;
		default:
			m_pScene->OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
			break;
		}
		break;
	default:
		break;
	}
}

LRESULT CALLBACK GameFramework::OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (nMessageID)
	{
	case WM_ACTIVATE:
	{
		if (LOWORD(wParam) == WA_INACTIVE)
			m_Timer.Stop();
		else
			m_Timer.Start();
		break;
	}
	case WM_SIZE:
	{
		m_WndClientWidth = LOWORD(lParam);
		m_WndClientHeight = HIWORD(lParam);

		if (m_WndClientWidth != 0 && m_WndClientHeight != 0)
			OnResizeBackBuffers();
		break;
	}
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam);
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
		break;
	}
	return(0);
}

void GameFramework::ModelLoad()
{
	ModelData* pData = new ModelData();
	pData->ModelLoadAndMeshLoad("../Model/Character/SwordMan/SwordMan3.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Run_.fbx", "walk", 0.8f, true);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Idle2.fbx", "idle2", 0.5f, true);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Attack1.fbx", "attack1", 1.f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Attack2.fbx", "attack2", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill1.fbx", "skill1", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill2.fbx", "skill2", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill3.fbx", "skill3", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill4.fbx", "skill4", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Skill5.fbx", "skill5", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Stun_.fbx", "walk", 0.8f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Damage.fbx", "damage", 0.5f, false);
	pData->AnimationLoad("../Model/Character/SwordMan/Animation/SwordMan@Die_.fbx", "die", 0.5f, false);
	m_mModelData["SwordMan"] = pData;

	// 몬스터들
	string strModelFilenames[] = {
"../Model/Monster/Peon/peon.fbx", "../Model/Monster/Grunt/Grunt.fbx", "../Model/Monster/Lord/Lord.fbx"
	};

	// 내가 수정했다 이말이다 이개쌔이야~
	string strAnimFilenames[] = {
		"../Model/Monster/Peon/peon@idle.fbx", "../Model/Monster/Grunt/Grunt@idle.fbx", "../Model/Monster/Lord/Lord@idle.fbx",
		"../Model/Monster/Peon/peon@run.fbx", "../Model/Monster/Grunt/Grunt@run.fbx", "../Model/Monster/Lord/Lord@run.fbx",
		"../Model/Monster/Peon/peon@attack01.fbx", "../Model/Monster/Grunt/Grunt@attack01.fbx", "../Model/Monster/Lord/Lord@attack01.fbx",
		"../Model/Monster/Peon/peon@hit.fbx", "../Model/Monster/Grunt/Grunt@hit.fbx", "../Model/Monster/Lord/Lord@hit.fbx",
		"../Model/Monster/Peon/peon@die.fbx", "../Model/Monster/Grunt/Grunt@die.fbx", "../Model/Monster/Lord/Lord@die.fbx"
	};
	string strMonsternames[] = { "Peon", "Grunt", "Lord" };
	/*for (int i = 0; i < 3; ++i)
	{
		ModelData* pData = new ModelData();
		pData->ModelLoadAndMeshLoad(strModelFilenames[i], m_pD3dDevice.Get(), m_pCommandList.Get());
		pData->AnimationLoad(strAnimFilenames[i], "walk", 0.8f, true);
		if (i == 0) {
			pData->AnimationLoad(strAnimFilenames[3], "run", 0.8f, true);
			pData->AnimationLoad(strAnimFilenames[4], "attack", 0.8f, true);
		}

		m_mModelData.insert(pair<string, ModelData*>(strMonsternames[i], pData));
	}*/
	for (int i = 0; i < 3; ++i)
	{
		ModelData* pData = new ModelData();
		pData->ModelLoadAndMeshLoad(strModelFilenames[i], m_pD3dDevice.Get(), m_pCommandList.Get());
		pData->AnimationLoad(strAnimFilenames[i], "idle", 0.8f, true);
		pData->AnimationLoad(strAnimFilenames[i + 3], "run", 0.8f, true);
		pData->AnimationLoad(strAnimFilenames[i + 6], "attack", 0.8f, true);
		pData->AnimationLoad(strAnimFilenames[i + 9], "hit", 0.1f, false);
		pData->AnimationLoad(strAnimFilenames[i + 12], "die", 0.8f, false);

		m_mModelData.insert(pair<string, ModelData*>(strMonsternames[i], pData));
	}
}

// 내가 수정했다 이말이다 이개쌔이야~
void GameFramework::BuildMonsters()
{
	// 몬스터
	CMaterial* pMaterial = new CMaterial();
	m_nNormalDungeonObjects = 244;
	m_nBossDungeonObjects = 48;
	m_ppMonsterObjects = new CGameObject*[m_nNormalDungeonObjects + m_nBossDungeonObjects];
	m_pBoss = new CBossDragonObject();

	// 일반던전 몬스터.
	ifstream in("test/peon.txt");
	XMFLOAT3 pos;
	int index = 0;
	for (int i = 0; i < 96; ++i) {
		CSkinAndDivisionObject* pPeon = new CSkinAndDivisionObject();
		pPeon->SetTextureIndex(XMUINT2(38, 39));
		pPeon->SetMesh(0, m_mModelData["Peon"]->m_pParentMesh);
		pPeon->SetChilds(m_pD3dDevice.Get(), m_mModelData["Peon"]->m_vChildMesh, m_mModelData["Peon"]->m_vNames);
		pPeon->SetAnimClip(m_mModelData["Peon"]->m_mDivisionAnimationClip);
		in >> pos.x >> pos.y >> pos.z;
	
		pPeon->SetScale(8.f, 8.f, 8.f);
		pPeon->SetPosition(pos);
		pPeon->SetTarget(m_pPlayer);
		pPeon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pPeon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pPeon->SetMaterial(0, pMaterial);
		pPeon->m_strObjectName = "Peon";
		pPeon->SetObjectCBIndex(::gnObjCBindex++);
		pPeon->SetSkinCBIndex(::gnSkinCBindex++);
		m_ppMonsterObjects[index++] = pPeon;
	}
	in.close();
	in.open("test/grunt.txt");
	for (int i = 0; i < 85; ++i) {
		CSkinAndDivisionObject* pGrunt = new CSkinAndDivisionObject();
		pGrunt->SetTextureIndex(XMUINT2(34, 35));
		pGrunt->SetMesh(0, m_mModelData["Grunt"]->m_pParentMesh);
		pGrunt->SetChilds(m_pD3dDevice.Get(), m_mModelData["Grunt"]->m_vChildMesh, m_mModelData["Grunt"]->m_vNames);
		pGrunt->SetAnimClip(m_mModelData["Grunt"]->m_mDivisionAnimationClip);
		in >> pos.x >> pos.y >> pos.z;
		pGrunt->SetScale(10.f, 10.f, 10.f);
		pGrunt->SetPosition(pos);
		pGrunt->SetTarget(m_pPlayer);
		pGrunt->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pGrunt->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pGrunt->SetMaterial(0, pMaterial);
		pGrunt->m_strObjectName = "Grunt";
		pGrunt->SetObjectCBIndex(::gnObjCBindex++);
		pGrunt->SetSkinCBIndex(::gnSkinCBindex++);
		m_ppMonsterObjects[index++] = pGrunt;
	}
	in.close();


	in.open("test/lord.txt");
	for (int i = 0; i < 63; ++i) {
		CSkinAndDivisionObject* pLord = new CSkinAndDivisionObject();
		pLord->SetTextureIndex(XMUINT2(42, 43));
		pLord->SetMesh(0, m_mModelData["Lord"]->m_pParentMesh);
		pLord->SetChilds(m_pD3dDevice.Get(), m_mModelData["Lord"]->m_vChildMesh, m_mModelData["Lord"]->m_vNames);
		pLord->SetAnimClip(m_mModelData["Lord"]->m_mDivisionAnimationClip);
		in >> pos.x >> pos.y >> pos.z;
		pLord->SetScale(12.f, 12.f, 12.f);
		pLord->SetPosition(pos);
		pLord->SetTarget(m_pPlayer);
		pLord->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pLord->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pLord->SetMaterial(0, pMaterial);
		pLord->m_strObjectName = "Lord";
		pLord->SetObjectCBIndex(::gnObjCBindex++);
		pLord->SetSkinCBIndex(::gnSkinCBindex++);
		m_ppMonsterObjects[index++] = pLord;
	}
	in.close();


	std::ofstream out("BossNPC.txt");
	out << m_nBossDungeonObjects << '\t';


	// 보스던전 몬스터
	in.open("test/peonAlt.txt");
	for (int i = 0; i < 24; ++i) {
		CSkinAndDivisionObject* pPeon = new CSkinAndDivisionObject();
		pPeon->SetTextureIndex(XMUINT2(50, 51));
		pPeon->SetMesh(0, m_mModelData["Peon"]->m_pParentMesh);
		pPeon->SetChilds(m_pD3dDevice.Get(), m_mModelData["Peon"]->m_vChildMesh, m_mModelData["Peon"]->m_vNames);
		pPeon->SetAnimClip(m_mModelData["Peon"]->m_mDivisionAnimationClip);
		in >> pos.x >> pos.y >> pos.z;
		pPeon->SetScale(8.f, 8.f, 8.f);
		pPeon->SetPosition(pos);
		pPeon->SetTarget(m_pPlayer);
		pPeon->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pPeon->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pPeon->SetMaterial(0, pMaterial);
		pPeon->m_strObjectName = "Peon";
		pPeon->SetObjectCBIndex(::gnObjCBindex++);
		pPeon->SetSkinCBIndex(::gnSkinCBindex++);
		m_ppMonsterObjects[index++] = pPeon;

		out << pPeon->m_strObjectName << '\t' << pPeon->m_xmOOBB.Center.x << '\t' << pPeon->m_xmOOBB.Center.y << '\t' << pPeon->m_xmOOBB.Center.z << '\t' << pPeon->m_xmOOBB.Extents.x << '\t' << pPeon->m_xmOOBB.Extents.y << '\t' << pPeon->m_xmOOBB.Extents.z << '\t' << pos.x << '\t' << pos.y << '\t' << pos.z << '\t';
	}
	in.close();

	in.open("test/gruntAlt.txt");
	for (int i = 0; i < 14; ++i) {
		CSkinAndDivisionObject* pGrunt = new CSkinAndDivisionObject();
		pGrunt->SetTextureIndex(XMUINT2(46, 47));
		pGrunt->SetMesh(0, m_mModelData["Grunt"]->m_pParentMesh);
		pGrunt->SetChilds(m_pD3dDevice.Get(), m_mModelData["Grunt"]->m_vChildMesh, m_mModelData["Grunt"]->m_vNames);
		pGrunt->SetAnimClip(m_mModelData["Grunt"]->m_mDivisionAnimationClip);
		in >> pos.x >> pos.y >> pos.z;
		pGrunt->SetScale(10.f, 10.f, 10.f);
		pGrunt->SetPosition(pos);
		pGrunt->SetTarget(m_pPlayer);
		pGrunt->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pGrunt->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pGrunt->SetMaterial(0, pMaterial);
		pGrunt->m_strObjectName = "Grunt";
		pGrunt->SetObjectCBIndex(::gnObjCBindex++);
		pGrunt->SetSkinCBIndex(::gnSkinCBindex++);
		m_ppMonsterObjects[index++] = pGrunt;
		out << pGrunt->m_strObjectName << '\t' << pGrunt->m_xmOOBB.Center.x << '\t' << pGrunt->m_xmOOBB.Center.y << '\t' << pGrunt->m_xmOOBB.Center.z << '\t' << pGrunt->m_xmOOBB.Extents.x << '\t' << pGrunt->m_xmOOBB.Extents.y << '\t' << pGrunt->m_xmOOBB.Extents.z << '\t' << pos.x << '\t' << pos.y << '\t' << pos.z << '\t';
	}
	in.close();

	in.open("test/lordAlt.txt");
	for (int i = 0; i < 10; ++i) {
		CSkinAndDivisionObject* pLord = new CSkinAndDivisionObject();
		pLord->SetTextureIndex(XMUINT2(54, 55));
		pLord->SetMesh(0, m_mModelData["Lord"]->m_pParentMesh);
		pLord->SetChilds(m_pD3dDevice.Get(), m_mModelData["Lord"]->m_vChildMesh, m_mModelData["Lord"]->m_vNames);
		pLord->SetAnimClip(m_mModelData["Lord"]->m_mDivisionAnimationClip);
		in >> pos.x >> pos.y >> pos.z;
		pLord->SetScale(12.f, 12.f, 12.f);
		pLord->SetPosition(pos);
		pLord->SetTarget(m_pPlayer);
		pLord->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, pLord->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
		pLord->SetMaterial(0, pMaterial);
		pLord->m_strObjectName = "Lord";
		pLord->SetObjectCBIndex(::gnObjCBindex++);
		pLord->SetSkinCBIndex(::gnSkinCBindex++);
		m_ppMonsterObjects[index++] = pLord;
		out << pLord->m_strObjectName << '\t' << pLord->m_xmOOBB.Center.x << '\t' << pLord->m_xmOOBB.Center.y << '\t' << pLord->m_xmOOBB.Center.z << '\t' << pLord->m_xmOOBB.Extents.x << '\t' << pLord->m_xmOOBB.Extents.y << '\t' << pLord->m_xmOOBB.Extents.z << '\t' << pos.x << '\t' << pos.y << '\t' << pos.z << '\t';
	}
	in.close();

	m_pBoss->LoadModelAndInitCombinedAnimationClip("../Model/Monster/Dragon/DragonBoss.fbx", m_pD3dDevice.Get(), m_pCommandList.Get());
	m_pBoss->SetTextureIndex(XMUINT2(58, 59));
	m_pBoss->SetPosition(XMFLOAT3(5000.f, 1000.f, 26500.f));
	m_pBoss->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, m_pBoss->GetWidthHeightDepth()), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	m_pBoss->SetMaterial(0, pMaterial);
	m_pBoss->m_strObjectName = "Boss";
	m_pBoss->SetScale(5.f, 5.f, 5.f);
	m_pBoss->Rotate(90.f, 0, 0);
	m_pBoss->SetObjectCBIndex(::gnObjCBindex++);
	m_pBoss->SetSkinCBIndex(::gnSkinCBindex++);
	out << m_pBoss->m_strObjectName << '\t' << m_pBoss->m_xmOOBB.Center.x << '\t' << m_pBoss->m_xmOOBB.Center.y << '\t' << m_pBoss->m_xmOOBB.Center.z << '\t' << m_pBoss->m_xmOOBB.Extents.x << '\t' << m_pBoss->m_xmOOBB.Extents.y << '\t' << m_pBoss->m_xmOOBB.Extents.z << '\t' << pos.x << '\t' << pos.y << '\t' << pos.z << '\t';
	out.close();
}

void GameFramework::BuildFrameResources()
{
	cout << "상수버퍼 개수 : "<< gnObjCBindex << endl;

	for (int i = 0; i < gNumFrameResources; ++i)
	{
		mFrameResources.push_back(std::make_unique<FrameResource>(m_pD3dDevice.Get(),
			2, ::gnObjCBindex, ::gnSkinCBindex));
	}
}

void GameFramework::BuildObjects()
{
	ThrowIfFailed(m_pCommandList->Reset(m_pDirectCmdListAlloc.Get(), nullptr));
	
	m_pScene = new CScene();
	m_pScene->BuildObjects(m_pD3dDevice.Get(), m_pCommandList.Get());
	// 모델 정보 로드.
	ModelLoad();

	// 스키닝 모델들을 그려줄 파이프라인상태객체.
	m_pSkinnedObjectShader = new CSkinnedObjectShader();
	m_pSkinnedObjectShader->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), true);

	m_pScene->m_pPlayer = m_pPlayer = new CTerrainPlayer(m_pD3dDevice.Get(), m_pCommandList.Get(), m_pScene->GetGraphicsRootSignature(), m_pScene->GetTerrain());
	m_pPlayer->SetMesh(0, m_mModelData["SwordMan"]->m_pParentMesh);
	m_pPlayer->SetChilds(m_pD3dDevice.Get(), m_mModelData["SwordMan"]->m_vChildMesh, m_mModelData["SwordMan"]->m_vNames);
	m_pPlayer->SetAnimClip(m_mModelData["SwordMan"]->m_mDivisionAnimationClip);
	m_pPlayer->SetSkinCBIndex(::gnSkinCBindex++);
	m_pPlayer->SetOOBB(XMFLOAT3(0.f, 0.f, 0.f), Vector3::Multiply(0.5f, m_pPlayer->GetWidthHeightDepth(XMFLOAT3(1.f, 0.6f, 1.f))), XMFLOAT4(0.f, 0.f, 0.f, 1.f));
	m_pCamera = m_pPlayer->GetCamera();
	m_pScene->SetPlayer();

	BuildMonsters();

	m_nTextures = 67;
	m_TextureMgr = new TextureManager(m_pD3dDevice.Get(), m_pCommandList.Get(), m_nTextures, RESOURCE_TEXTURE2D_ARRAY);
	
	mShadowMap = make_unique<ShadowMap>(
		m_pD3dDevice.Get(), 2048,2048);

	auto srvCpuStart = m_TextureMgr->GetCPUDescriptorHandleForHeapStart();
	auto srvGpuStart = m_TextureMgr->GetGPUDescriptorHandleForHeapStart();
	auto dsvCpuStart = GetDepthStencilView();
	
	mShadowMap->BuildDescriptors(
		CD3DX12_CPU_DESCRIPTOR_HANDLE(srvCpuStart, m_nTextures, ::gnCbvSrvUavDescriptorIncrementSize),
		CD3DX12_GPU_DESCRIPTOR_HANDLE(srvGpuStart, m_nTextures, ::gnCbvSrvUavDescriptorIncrementSize),
		CD3DX12_CPU_DESCRIPTOR_HANDLE(dsvCpuStart, 1, m_DsvDescriptorSize));

	m_pTestRectShader = new CPostProcessingShader();
	m_pTestRectShader->CreateShader(m_pD3dDevice.Get(), m_pScene->GetGraphicsRootSignature(), false);

	BuildFrameResources();

	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	FlushCommandQueue();

	if (m_TextureMgr) m_TextureMgr->ReleaseUploadBuffers();

	m_Timer.Reset();
}

void GameFramework::MoveToTargetPlayer(unsigned short usID, float fUpdateTime)
{
	XMFLOAT3 xmf3Up = XMFLOAT3(0, 1, 0);      // 업벡터.
	XMFLOAT3 xmf3Look = m_ppMonsterObjects[usID]->GetLook();      // 룩벡터 갖고있음.
	XMFLOAT3 xmf3Right = Vector3::CrossProduct(xmf3Up, xmf3Look, true);      // 업, 룩으로 라이트벡터 구함.

	XMFLOAT3 xmf3Position = m_ppMonsterObjects[usID]->GetPosition();   // 몬스터 좌표.
	XMFLOAT3 xmf3TargetPosition = m_pPlayer->GetPosition();   // 플레이어 좌표
	XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 플레이어를 향한 방향벡터

	float fElapsedTime = fUpdateTime;               // deltatime.

	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	//aοb 내적연산 => |a|*|b|*cosθ 를 구할수있다. =>a, b둘다 정규화를 했기 때문에 cosθ값이 fDotProduct로 들어감
	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);

	XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3Look, xmf3ToTarget);                     //외적으로 회전축을 구한다.       

	XMFLOAT4X4 xmf4x4World = Matrix4x4::Identity();

	xmf4x4World._11 = xmf3Right.x; xmf4x4World._12 = xmf3Right.y; xmf4x4World._13 = xmf3Right.z;
	xmf4x4World._21 = xmf3Up.x;   xmf4x4World._22 = xmf3Up.y; xmf4x4World._23 = xmf3Up.z;
	xmf4x4World._31 = xmf3Look.x; xmf4x4World._32 = xmf3Look.y; xmf4x4World._33 = xmf3Look.z;
	xmf4x4World._41 = xmf3Position.x; xmf4x4World._42 = xmf3Position.y; xmf4x4World._43 = xmf3Position.z;

	if (fAngle != 0.0f) {
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f),
			XMConvertToRadians(fAngle * fElapsedTime * ((xmf3CrossProduct.y > 0.0f) ? 1.0f : -1.0f)), XMConvertToRadians(0.f));

		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	}

	xmf4x4World._41 += 100 * fUpdateTime;  xmf4x4World._43 += 100 * fUpdateTime; // 이동시키기

	// 해당 몬스터의 충돌박스 업데이트 하기.
	/*m_xmOOBB.Transform(this->m_xmOOBB, XMLoadFloat4x4(&this->m_xmf4x4World));
	XMStoreFloat4(&this->m_xmOOBB.Orientation, XMQuaternionNormalize(XMLoadFloat4(&this->m_xmOOBB.Orientation)));*/

	// 최종 몬스터의 룩벡터, 포지션.
	xmf3Look = XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33);
	xmf3Position = XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);

	m_ppMonsterObjects[usID]->SetLook(xmf3Look);
	m_ppMonsterObjects[usID]->SetPosition(xmf3Position);
}

// 내가 수정했다 이말이다 이개쌔이야~
void GameFramework::AnimateObjects()
{
#ifdef _WITH_DIRECT2D_IMAGE_EFFECT
	static UINT64 i = 0;
	if (++i % 15) return;

	static float fColors[4] = { 0.0f, 0.478f, 0.8f, 1.0f };
	fColors[1] += 0.01f;
	if (fColors[1] > 1.0f) fColors[1] = 0.0f;
	m_pd2dfxGaussianBlur->SetValue(D2D1_GAUSSIANBLUR_PROP_STANDARD_DEVIATION, 0.3f + fColors[1] * 10.0f);
#endif
	if (m_pScene) {
		m_pScene->AnimateObjects(m_Timer.GetDeltaTime());
	}
	if (m_eMaplist == MAP_LIST::그냥DUNGEON) {
		// 일반던전 애니메이트
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			m_ppMonsterObjects[i]->Animate(m_Timer.GetDeltaTime());
			m_ppMonsterObjects[i]->MoveToTaget(m_Timer.GetDeltaTime());
		}
		//m_ppMonsterObjects[0]->MoveToTaget(m_Timer.GetDeltaTime());
		//cout << m_ppMonsterObjects[0]->GetNowAnimation() << endl;
	}
	if (m_eMaplist == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			m_ppMonsterObjects[i]->Animate(m_Timer.GetDeltaTime());
			m_ppMonsterObjects[i]->MoveToTaget(m_Timer.GetDeltaTime());
		}
		m_pBoss->Animate(m_Timer.GetDeltaTime());
	}
	m_pPlayer->Animate(m_Timer.GetDeltaTime());

}

void GameFramework::Update()
{
	ProcessInput();
	AnimateObjects();

	// 순환적으로 자원 프레임 배열의 다음 원소에 접근한다.
	mCurrFrameResourceIndex = (mCurrFrameResourceIndex + 1) % gNumFrameResources;
	mCurrFrameResource = mFrameResources[mCurrFrameResourceIndex].get();
	// GPU가 현재 프레임 자원의 명령들을 다 처리했는지 확인한다.
	// 아직 다 처리하지 않았으면 GPU가 이 울타리 지점까지의 명령들을 처리할 때 까지 기다린다.
	if (mCurrFrameResource->Fence != 0 && m_pFence->GetCompletedValue() < mCurrFrameResource->Fence)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);
		ThrowIfFailed(m_pFence->SetEventOnCompletion(mCurrFrameResource->Fence, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	m_pCamera->UpdateShadowTransform(m_Timer.GetDeltaTime());
	m_pCamera->UpdateCB(mCurrFrameResource, m_Timer.GetDeltaTime());
	m_pCamera->UpdateShadowPassCB(mCurrFrameResource, m_Timer.GetDeltaTime());
	//shader객체들, 터레인 상수버퍼 업데이트
	m_pScene->UpdateObjects(mCurrFrameResource, m_Timer.GetDeltaTime());

	// 몬스터들 상수버퍼 업데이트. (해당 맵이 활성화 되있을경우 && 카메라에 보이는 경우에만 업데이트)
	if (m_eMaplist == MAP_LIST::그냥DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (m_ppMonsterObjects[i] && m_ppMonsterObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMonsterObjects[i]->SetFrameDirty(::gNumFrameResources);
				m_ppMonsterObjects[i]->UpdateCB(mCurrFrameResource);
				m_ppMonsterObjects[i]->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
			}
		}
	}
	if (m_eMaplist == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {

			if (m_ppMonsterObjects[i] && m_ppMonsterObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMonsterObjects[i]->SetFrameDirty(::gNumFrameResources);
				m_ppMonsterObjects[i]->UpdateCB(mCurrFrameResource);
				m_ppMonsterObjects[i]->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
			}
		}
		if (m_pBoss && m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			m_pBoss->SetFrameDirty(::gNumFrameResources);
			m_pBoss->UpdateCB(mCurrFrameResource);
			m_pBoss->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
		}
	}

	// 플레이어 상수버퍼업데이트
	m_pPlayer->UpdateCB(mCurrFrameResource);
	m_pPlayer->SetFrameDirty(::gNumFrameResources);
	// 플레이어 스킨정보 업데이트
	m_pPlayer->UpdateSkinCB(mCurrFrameResource, m_Timer.GetDeltaTime());
}

void GameFramework::DrawSceneToShadowMap()
{
	m_pCommandList->RSSetViewports(1, &mShadowMap->Viewport());
	m_pCommandList->RSSetScissorRects(1, &mShadowMap->ScissorRect());

	// Change to DEPTH_WRITE.
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mShadowMap->Resource(),
		D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_DEPTH_WRITE));

	UINT passCBByteSize = (sizeof(PassConstants) + 255) & ~255;

	// Clear the back buffer and depth buffer.
	m_pCommandList->ClearDepthStencilView(mShadowMap->Dsv(),
		D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	m_pCommandList->OMSetRenderTargets(0, nullptr, false, &mShadowMap->Dsv());

	// Bind the pass constant buffer for the shadow map pass.
	auto passCB = mCurrFrameResource->PassCB->Resource();
	D3D12_GPU_VIRTUAL_ADDRESS passCBAddress = passCB->GetGPUVirtualAddress() + 1 * passCBByteSize;
	m_pCommandList->SetGraphicsRootConstantBufferView(0, passCBAddress);

	m_pScene->Render(m_pCommandList.Get(), m_pCamera, true);
	// 스키닝 객체 그려줄때 파이프라인 상태객체를 셋해주는 역할.
	m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, true);
	// 몬스터들 렌더. 해당맵이 활성화 되있을 경우 && 카메라에 보일경우만.
	if (m_eMaplist == MAP_LIST::그냥DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (m_ppMonsterObjects[i] && m_ppMonsterObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMonsterObjects[i]->Render(m_pCommandList.Get(), m_pCamera, true);
			}
		}
	}

	if (m_eMaplist == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (m_ppMonsterObjects[i] && m_ppMonsterObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMonsterObjects[i]->Render(m_pCommandList.Get(), m_pCamera, true);
			}
		}
		if (m_pBoss && m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			m_pBoss->Render(m_pCommandList.Get(), m_pCamera, true);
		}
	}

	m_pPlayer->Render(m_pCommandList.Get(), m_pCamera, true);

	// Change back to GENERIC_READ so we can read the texture in a shader.
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mShadowMap->Resource(),
		D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_GENERIC_READ));

}

void GameFramework::FrameAdvance()
{
	//auto t = high_resolution_clock::now();

	m_Timer.Tick();

	Update();
	auto cmdListAlloc = mCurrFrameResource->CmdListAlloc;
	
	ThrowIfFailed(cmdListAlloc->Reset());
	ThrowIfFailed(m_pCommandList->Reset(cmdListAlloc.Get(), nullptr));

	m_pScene->SetGraphicsRootSignature(m_pCommandList.Get());
	m_TextureMgr->SetDescriptorHeaps(m_pCommandList.Get());
	m_TextureMgr->TexturesUpdate(m_pCommandList.Get());

	DrawSceneToShadowMap();

	m_pCamera->SetViewportsAndScissorRects(m_pCommandList.Get());

	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	m_pCommandList->ClearRenderTargetView(GetCurrentBackBufferView(), Colors::LightSteelBlue, 0, nullptr);
	m_pCommandList->ClearDepthStencilView(GetDepthStencilView(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	m_pCommandList->OMSetRenderTargets(1, &GetCurrentBackBufferView(), true, &GetDepthStencilView());


#ifndef _WITH_DIRECT2D
	d3dResourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	d3dResourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	d3dResourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	m_pd3dCommandList->ResourceBarrier(1, &d3dResourceBarrier);
#endif

	// 그림자패스에서 얻은 텍스처정보를 gpu에 올림.
	m_pCommandList->SetGraphicsRootDescriptorTable(4, mShadowMap->Srv());
	// 카메라 정보 gpu전달.
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = mCurrFrameResource->PassCB->Resource()->GetGPUVirtualAddress();
	m_pCommandList->SetGraphicsRootConstantBufferView(0, d3dGpuVirtualAddress);

	m_pScene->Render(m_pCommandList.Get(), m_pCamera, false);
	// 스키닝 객체 그려줄때 파이프라인 상태객체를 셋해주는 역할.
	m_pSkinnedObjectShader->Render(m_pCommandList.Get(), m_pCamera, false);
	// 몬스터들 렌더. 해당맵이 활성화 되있을 경우 && 카메라에 보일경우만.
	if (m_eMaplist == MAP_LIST::그냥DUNGEON) {
		for (int i = 0; i < m_nNormalDungeonObjects; ++i) {
			if (m_ppMonsterObjects[i] && m_ppMonsterObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMonsterObjects[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
	}

	if (m_eMaplist == MAP_LIST::BOSS_DUNGEON) {
		for (int i = m_nNormalDungeonObjects; i < m_nNormalDungeonObjects + m_nBossDungeonObjects; ++i) {
			if (m_ppMonsterObjects[i] && m_ppMonsterObjects[i]->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
				m_ppMonsterObjects[i]->Render(m_pCommandList.Get(), m_pCamera, false);
			}
		}
		if (m_pBoss && m_pBoss->m_xmOOBB.Intersects(m_pPlayer->GetCamera()->GetFrustum())) {
			m_pBoss->Render(m_pCommandList.Get(), m_pCamera, false);
		}
	}
	m_pPlayer->Render(m_pCommandList.Get(), m_pCamera, false);
	m_pTestRectShader->Render(m_pCommandList.Get(), m_pCamera, false);

#ifndef _WITH_DIRECT2D
	m_pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GetCurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
#endif

	ThrowIfFailed(m_pCommandList->Close());
	ID3D12CommandList* cmdsLists[] = { m_pCommandList.Get() };
	m_pCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);
	
#ifdef _WITH_DIRECT2D
	//Direct2D Drawing
	m_pd3d11On12Device->AcquireWrappedResources(&m_ppd3d11WrappedBackBuffers[m_CurrBackBuffer], 1);
	m_pd2dDeviceContext->SetTarget(m_ppd2dRenderTargets[m_CurrBackBuffer]);

	m_pd2dDeviceContext->BeginDraw();

	m_pd2dDeviceContext->SetTransform(D2D1::Matrix3x2F::Identity());
#ifdef _WITH_DIRECT2D_IMAGE_EFFECT
	//m_pd2dDeviceContext->DrawImage(m_pd2dfxBitmapSource);
	//	m_pd2dDeviceContext->DrawRectangle(rcText, m_pd2dbrBackground);

	//	m_pd2dDeviceContext->DrawRectangle(&rcText, m_pd2dbrBorder);
	//	m_pd2dDeviceContext->FillRectangle(&rcText, m_pd2dbrBackground);
#endif
	D2D1_SIZE_F szRenderTarget = m_ppd2dRenderTargets[m_CurrBackBuffer]->GetSize();
	//m_Timer.GetDeltaTime(m_pszFrameRate + 12, 37);
	D2D1_RECT_F rcUpperText = D2D1::RectF(0, 0, szRenderTarget.width, szRenderTarget.height * 0.5f);
	//m_pd2dDeviceContext->DrawTextW(m_pszFrameRate, (UINT32)wcslen(m_pszFrameRate), m_pdwFont, &rcUpperText, m_pd2dbrText);

	D2D1_RECT_F rcLowerText = D2D1::RectF(0, szRenderTarget.height * 0.5f, szRenderTarget.width, szRenderTarget.height);
	m_pd2dDeviceContext->DrawTextW(L"한글 테스트", (UINT32)wcslen(L"한글 테스트"), m_pdwFont, &rcLowerText, m_pd2dbrText);


	m_pd2dDeviceContext->EndDraw();

	m_pd3d11On12Device->ReleaseWrappedResources(&m_ppd3d11WrappedBackBuffers[m_CurrBackBuffer], 1);

	m_pd3d11DeviceContext->Flush();
#endif
	ThrowIfFailed(m_pSwapChain->Present(0, 0));
	m_CurrBackBuffer = (m_CurrBackBuffer + 1) % m_SwapChainBufferCount;

	CalculateFrameStats();
	//auto d = high_resolution_clock::now() - t;
	//t = high_resolution_clock::now();
	//FlushCommandQueue();
	mCurrFrameResource->Fence = ++m_CurrentFence;
	m_pCommandQueue->Signal(m_pFence.Get(), m_CurrentFence);
	//auto s = high_resolution_clock::now() - t;

	//cout << "cpu : " << duration_cast<milliseconds>(d).count() << endl;
	//cout << "gpu : " << duration_cast<milliseconds>(s).count() << endl;
}

void GameFramework::ProcessInput()
{
	static UCHAR pKeysBuffer[256];
	bool bProcessedByScene = false;
	if (GetKeyboardState(pKeysBuffer) && m_pScene) 
		bProcessedByScene = m_pScene->ProcessInput(pKeysBuffer);

	if (!bProcessedByScene)
	{
		DWORD dwDirection = 0;
		if (pKeysBuffer[VK_UP] & 0xF0 || pKeysBuffer[0x57] & 0xF0) dwDirection |= DIR_FORWARD;
		if (pKeysBuffer[VK_DOWN] & 0xF0 || pKeysBuffer[0x53] & 0xF0) dwDirection |= DIR_BACKWARD;
		if (pKeysBuffer[VK_LEFT] & 0xF0 || pKeysBuffer[0x41] & 0xF0) dwDirection |= DIR_LEFT;
		if (pKeysBuffer[VK_RIGHT] & 0xF0 || pKeysBuffer[0x44] & 0xF0) dwDirection |= DIR_RIGHT;
		if (pKeysBuffer[VK_PRIOR] & 0xF0 || pKeysBuffer[0x45] & 0xF0) dwDirection |= DIR_UP;
		if (pKeysBuffer[VK_NEXT] & 0xF0 || pKeysBuffer[0x51] & 0xF0) dwDirection |= DIR_DOWN;

		float cxDelta = 0.0f, cyDelta = 0.0f;
		POINT ptCursorPos;
		if (GetCapture() == m_hMainWnd)
		{
			SetCursor(NULL);
			GetCursorPos(&ptCursorPos);
			cxDelta = (float)(ptCursorPos.x - m_OldCursorPos.x) / 3.0f;
			cyDelta = (float)(ptCursorPos.y - m_OldCursorPos.y) / 3.0f;
			SetCursorPos(m_OldCursorPos.x, m_OldCursorPos.y);
		}

		if ((dwDirection != 0) || (cxDelta != 0.0f) || (cyDelta != 0.0f))
		{
			if (cxDelta || cyDelta)
			{
				if (pKeysBuffer[VK_RBUTTON] & 0xF0)
					m_pPlayer->Rotate(cyDelta, 0.0f, -cxDelta);
				else
					m_pPlayer->Rotate(cyDelta, cxDelta, 0.0f);
			}
			if (dwDirection) {
				//cout << "무브중" << endl;
				if (m_pPlayer->GetNowAnimation() == "idle2" || m_pPlayer->GetNowAnimation() == "walk" || m_pPlayer->GetNowAnimation() == "skill2") {
					if (m_pPlayer->GetNowAnimation() != "skill2")
						m_pPlayer->SetNowAnimation(string("walk"));
					m_pPlayer->Move(dwDirection, PLAYER_WORKSPEED * m_Timer.GetDeltaTime(), false);
					m_pPlayer->SetDirection(dwDirection);
				}
			}
		}
		if (!dwDirection) {
			if (m_pPlayer->GetNowAnimation() == "walk")
				m_pPlayer->SetNowAnimation(string("idle2"));

			//cout << "안무브중" << endl;
		}
	}
	m_pPlayer->Update(m_Timer.GetDeltaTime());
}

GameFramework::~GameFramework()
{
	if (m_pD3dDevice != nullptr)
		FlushCommandQueue();
}


//2D추가
#define _WITH_DIRECT2D_IMAGE_EFFECT
#ifdef _WITH_DIRECT2D
void GameFramework::CreateDirect2DDevice()
{
	UINT nD3D11DeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

#if defined(_DEBUG)
	nD3D11DeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	
	ID3D11Device *pd3d11Device = NULL;

	HRESULT hResult = ::D3D11On12CreateDevice(m_pD3dDevice.Get(), nD3D11DeviceFlags, NULL, 0, (IUnknown **)m_pCommandQueue.GetAddressOf(), 1, 0, &pd3d11Device, &m_pd3d11DeviceContext, NULL);

	hResult = pd3d11Device->QueryInterface(__uuidof(ID3D11On12Device), (void **)&m_pd3d11On12Device);

	if (pd3d11Device) pd3d11Device->Release();

	D2D1_FACTORY_OPTIONS nD2DFactoryOptions = { D2D1_DEBUG_LEVEL_NONE };
#if defined(_DEBUG)
	nD2DFactoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif
	hResult = ::D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory3), &nD2DFactoryOptions, (void **)&m_pd2dFactory);

	IDXGIDevice *pdxgiDevice = NULL;
	hResult = m_pd3d11On12Device->QueryInterface(__uuidof(IDXGIDevice), (void **)&pdxgiDevice);
	hResult = m_pd2dFactory->CreateDevice(pdxgiDevice, &m_pd2dDevice);
	hResult = m_pd2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &m_pd2dDeviceContext);
	hResult = ::DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), (IUnknown **)&m_pdWriteFactory);

	if (pdxgiDevice) pdxgiDevice->Release();

	m_pd2dDeviceContext->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_GRAYSCALE);

	m_pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(0.3f, 0.0f, 0.0f, 0.5f), &m_pd2dbrBackground);
	m_pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF(0x9ACD32, 1.0f)), &m_pd2dbrBorder);

	hResult = m_pdWriteFactory->CreateTextFormat(L"궁서체", NULL, DWRITE_FONT_WEIGHT_DEMI_BOLD, DWRITE_FONT_STYLE_ITALIC, DWRITE_FONT_STRETCH_NORMAL, 48.0f, L"en-US", &m_pdwFont);
	hResult = m_pdwFont->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	hResult = m_pdwFont->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	m_pd2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Purple, 1.0f), &m_pd2dbrText);
	hResult = m_pdWriteFactory->CreateTextLayout(L"텍스트 레이아웃", 8, m_pdwFont, 4096.0f, 4096.0f, &m_pdwTextLayout);

#ifdef _WITH_DIRECT2D_IMAGE_EFFECT
	/*CoInitialize(NULL);
	hResult = ::CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, __uuidof(IWICImagingFactory), (void **)&m_pwicImagingFactory);

	hResult = m_pd2dFactory->CreateDrawingStateBlock(&m_pd2dsbDrawingState);
	hResult = m_pd2dDeviceContext->CreateEffect(CLSID_D2D1BitmapSource, &m_pd2dfxBitmapSource);
	hResult = m_pd2dDeviceContext->CreateEffect(CLSID_D2D1GaussianBlur, &m_pd2dfxGaussianBlur);

	IWICBitmapDecoder *pwicBitmapDecoder;
	hResult = m_pwicImagingFactory->CreateDecoderFromFilename(L"ScoreBoard.png", NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pwicBitmapDecoder);
	IWICBitmapFrameDecode *pwicFrameDecode;
	pwicBitmapDecoder->GetFrame(0, &pwicFrameDecode);
	m_pwicImagingFactory->CreateFormatConverter(&m_pwicFormatConverter);
	m_pwicFormatConverter->Initialize(pwicFrameDecode, GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0.0f, WICBitmapPaletteTypeCustom);
	m_pd2dfxBitmapSource->SetValue(D2D1_BITMAPSOURCE_PROP_WIC_BITMAP_SOURCE, m_pwicFormatConverter);
	m_pd2dfxGaussianBlur->SetInputEffect(0, m_pd2dfxBitmapSource);
	if (pwicBitmapDecoder) pwicBitmapDecoder->Release();
	if (pwicFrameDecode) pwicFrameDecode->Release();*/
#endif
}
#endif