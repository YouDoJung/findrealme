#pragma once
#include "ModelMesh.h"
#include "Mesh.h"
#include "Camera.h"
#include "Texture.h"
#include "Material.h"
#define DIR_FORWARD					0x01
#define DIR_BACKWARD				0x02
#define DIR_LEFT					0x04
#define DIR_RIGHT					0x08
#define DIR_UP						0x10
#define DIR_DOWN					0x20
class CShader;

class CGameObject
{
public:
	CGameObject(int nMeshes, int nMaterials);
	CGameObject(int num);
	virtual ~CGameObject();

protected:
	CPlayer* m_pTarget;
	XMFLOAT4X4 m_xmf4x4World;
	CMesh** m_ppMeshes = nullptr;
	int m_nMeshes = 0;
	CMaterial** m_ppMaterials = nullptr;
	int m_nMaterials = 0;

	XMUINT2 m_xmi2TexIndex = { 0, 0 };
	float m_fFrameTime;
	float m_fStartTime = 0.f;
	UINT m_ObjectCBIndex = -1;
	UINT m_SkinCBIndex = -1;
	FrameResource* m_pCurrResource = nullptr;

	// 물체의 자료가 변해서 상수 버퍼를 갱신해야 하는지의 여부를 뜻하는 '더러움'플래그. FrameResource마다
	// 물체의 cbuffer가 있으므로, FrameResource마다 갱신을 적용해야 한다. 
	// 따라서, 물체의 자료를 수정할 때는 반드시
	// m_nNumFrameDirty = gNumFrameResources 로 설정해야 한다. 그래야 각각의 프레임 자원이 갱신된다.
	int m_nNumFrameDirty = gNumFrameResources;
public:
	string m_strObjectName = "";
	BoundingOrientedBox m_xmOOBB;
	BoundingOrientedBox m_xmOOBBTransformed;
	void SetOOBB(XMFLOAT3& xmCenter, XMFLOAT3& xmExtents, XMFLOAT4& xmOrientation) {
		m_xmOOBBTransformed = m_xmOOBB = BoundingOrientedBox(xmCenter, xmExtents, xmOrientation);
	}
	XMFLOAT3 m_xmf3Rotkey = { 0.f, 0.f, 0.f };

	void SetTarget(CPlayer* pPlayer) { m_pTarget = pPlayer; }
	void SetObjectCBIndex(UINT index) { m_ObjectCBIndex = index; }
	void SetSkinCBIndex(UINT num) { m_SkinCBIndex = num; }
	int GetFrameDirty() const { return m_nNumFrameDirty; }
	void MinusFrameDirty() { m_nNumFrameDirty--; }
	void SetFrameDirty(int num) { m_nNumFrameDirty = num; }

	float GetFrameTime() const { return m_fFrameTime; }
	void SetFrameTime(float fFrameTime) { m_fFrameTime = fFrameTime; }
	float GetStartTime() const { return m_fStartTime; }
	void SetStartTime(float fTime) { m_fStartTime = fTime; }
public:
	void SetMesh(int nIndex, CMesh* pMesh);
	void SetShader(CShader* pShader);
	void SetShader(int nIndex, CShader* pShader);
	void SetMaterial(int nIndex, CMaterial* pMaterial);
	void SetTextureIndex(XMUINT2 nIndex);
	void SetWorld(XMFLOAT4X4 world) { m_xmf4x4World = world; }
	void SetChilds(ID3D12Device* pd3dDevice, vector<CSkinnedModelMesh*> pMesh, vector<string>& meshName);
	virtual void UpdateCB(FrameResource* pFrameResource);
	virtual void UpdateSkinCB(FrameResource* pFrameResource, float fElapsedTime);

	virtual void Animate(float fTimeElapsed);
	virtual void OnPrepareRender() { }
	virtual void Render(ID3D12GraphicsCommandList* pd3dCommandList, CCamera* pCamera = nullptr, bool is_Shadow = false);
	virtual void BuildMaterials(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList) { }

	XMFLOAT3 GetPosition() const;
	XMFLOAT3 GetLook() const;
	XMFLOAT3 GetUp() const;
	XMFLOAT3 GetRight() const;
	XMFLOAT4X4 GetWorld() const;

	CMaterial* GetMaterial(int nIndex) const { return m_ppMaterials[nIndex]; }
	XMUINT2 GetTextureIndex() const { return m_xmi2TexIndex; }

	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 xmf3Position);
	void SetLook(XMFLOAT3 xmf3Look);

	void MoveStrafe(float fDistance = 1.0f);
	void MoveUp(float fDistance = 1.0f);
	void MoveForward(float fDistance = 1.0f);
	void MoveToTaget(float fTimeElapsed);

	void Rotate(float fPitch = 10.0f, float fYaw = 10.0f, float fRoll = 10.0f);
	void Rotate(XMFLOAT3* pxmf3Axis, float fAngle);

	void SetScale(float x, float y, float z) {
		XMMATRIX mtxScale = XMMatrixScaling(x, y, z);
		m_xmf4x4World = Matrix4x4::Multiply(mtxScale, m_xmf4x4World);
	}

	XMFLOAT3 GetWidthHeightDepth(XMFLOAT3 scale = XMFLOAT3(1.f, 1.f, 1.f)) const;
	bool HeightCollisionCheck(XMFLOAT3& position, bool is_rot) const;

	virtual void SetNowAnimation(string& s);
	string GetNowAnimation() const { return m_strNowClip; }
protected:
	string m_strNowClip = "walk";
	float m_fNowTime = 0.f;
	vector<CGameObject*> m_vChilds;

	// 내가 수정했다 이말이다 이개쌔이야~
public:
	CGameObject* GetChild(int n) { return m_vChilds[n]; }

};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
class CHeightMapTerrain : public CGameObject
{
public:
	CHeightMapTerrain(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, 
		ID3D12RootSignature* pd3dGraphicsRootSignature, LPCTSTR pFileName, int nWidth, int nLength,
		int nBlockWidth, int nBlockLength, XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color);
	virtual ~CHeightMapTerrain();

private:
	CHeightMapImage* m_pHeightMapImage;

	int m_nWidth;
	int m_nLength;

	XMFLOAT3 m_xmf3Scale;

public:
	float GetHeight(float x, float z, bool bReverseQuad = false) const { return(m_pHeightMapImage->GetHeight(x, z, bReverseQuad) * m_xmf3Scale.y); } //World
	XMFLOAT3 GetNormal(float x, float z) const { return(m_pHeightMapImage->GetHeightMapNormal(int(x / m_xmf3Scale.x), int(z / m_xmf3Scale.z))); }

	int GetHeightMapWidth() const { return(m_pHeightMapImage->GetHeightMapWidth()); }
	int GetHeightMapLength() const { return(m_pHeightMapImage->GetHeightMapLength()); }

	XMFLOAT3 GetScale() const { return(m_xmf3Scale); }
	float GetWidth() const { return(m_nWidth * m_xmf3Scale.x); }
	float GetLength() const { return(m_nLength * m_xmf3Scale.z); }
};

class CSkyBox : public CGameObject
{
public:
	CSkyBox(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList, ID3D12RootSignature *pd3dGraphicsRootSignature);
	virtual ~CSkyBox();

	virtual void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera = nullptr, bool is_Shadow = false);
};
