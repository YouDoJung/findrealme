#include "Shaders.hlsl"
struct VS_TEXTURED_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VS_TEXTURED_OUTPUT VS_TextureEffect(VS_TEXTURED_INPUT input)
{
	VS_TEXTURED_OUTPUT output;

	float3 posW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;
	return(output);
}

float4 PS_TextureEffect(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
	float2 newUV = input.uv - float2(0.5f, 0.5f);

	float distance = length(newUV);

	float4 color1;
	float4 color2;
	float4 cColor = { 0.f, 0.f, 0.f, 0.f };

	if (gMaterial.DiffuseAlbedo.r == 0.f) {
		if (distance < gFrameTime) {
			color1 = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);
			color2 = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.y)].SampleLevel(gSamplerState, input.uv, 0);

			cColor = (color1 + color2) * gMaterial.DiffuseAlbedo;
		}
	}
	else {
		if (distance > 0.5f)
			discard;

		if (distance < gFrameTime + 0.2f && distance > gFrameTime) {
			color1 = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);
			color2 = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.y)].SampleLevel(gSamplerState, input.uv, 0);

			cColor = lerp(color1, color2, 0.5f) * gMaterial.DiffuseAlbedo;
			cColor.a = color1.a;
		}
	}
	return(cColor);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct VS_FLOWING_ANIMATION_INPUT
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
	float value : VALUE;
};

struct VS_FLOWING_ANIMATION_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VS_FLOWING_ANIMATION_OUTPUT VS_FlowingAnimation(VS_FLOWING_ANIMATION_INPUT input, uint nVertexID : SV_VertexID)
{
	VS_FLOWING_ANIMATION_OUTPUT output;

	if (nVertexID % 3 == 0) {
		input.position.x += 20 * sin(gfTotalTime * input.value * 2);
		input.position.y += 20 * sin(gfTotalTime * input.value * 3);
		input.position.z += 20 * sin(gfTotalTime * input.value * 2);
	}
	input.uv.x -= sin(gfTotalTime / 10.f);
	input.uv.y += gfTotalTime / 10.f;

	float3 posW = (float3)mul(float4(input.position, 1.0f), gmtxObjectWorld);
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);
	output.uv = input.uv;
	return(output);
}

float4 PS_FlowingAnimation(VS_FLOWING_ANIMATION_OUTPUT input) : SV_TARGET
{
	float4 cColor = { 0.f, 0.f, 0.f, 0.f };

	cColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);
	
	//color1.a = 0.7f;
	return(cColor);
}

struct VS_Lava_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
	float value : VALUE;
};

struct VS_Lava_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VS_Lava_OUTPUT VSLava(VS_Lava_INPUT input)
{
	VS_Lava_OUTPUT output;
	float3 newPos = input.position;
	float newTime = gFrameTime - input.value;

	if (input.uv.x == 0.5f && input.uv.y == 0.5f) {
		if (newTime >= 0)
			newPos.y += sin(newTime) * input.value * 20;
	}

	output.uv = float2(input.uv.x - sin(gFrameTime / 20.f), input.uv.y + gFrameTime / 20);
	float3 posW = (float3)mul(float4(newPos, 1.0f), gmtxObjectWorld);
	output.position = mul(float4(posW, 1.0f), gmtxViewProjection);
	return(output);
}

float4 PSLava(VS_Lava_OUTPUT input) : SV_TARGET
{
	float4 cColor = { 0.f, 0.f, 0.f, 0.f };

	cColor = gtxtAllTextures[NonUniformResourceIndex(gTextureIndex.x)].SampleLevel(gSamplerState, input.uv, 0);

	return(cColor);
}