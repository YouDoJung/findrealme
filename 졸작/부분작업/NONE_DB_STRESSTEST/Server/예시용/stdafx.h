// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#pragma comment(lib, "ws2_32")

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC

#include <winsock2.h>
#include <Windows.h>
#include <sql.h>
#include <sqlext.h>
#include <tchar.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include <crtdbg.h>

#include <malloc.h>
#include <tchar.h>
#include <math.h>

#include <string>
#include <shellapi.h>

#include <Mmsystem.h>

#include <assert.h>
#include <algorithm>
#include <memory.h>
#include <wrl.h>
#include <comdef.h>

#include <ppl.h>
#include <concurrent_unordered_map.h>
#include <concurrent_unordered_set.h>
#include <concurrent_priority_queue.h>
#include <concurrent_vector.h>
#include <concurrent_queue.h>

#include <thread>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <future>

#include <set>
#include <vector>
#include <map>
#include <unordered_set>
#include <unordered_map>

#include <chrono>
#include <iostream>
#include <fstream>
#include <random>
#include <functional>

/*///////////////////////////////
	다렉 관련 필요 헤더 파일.
*////////////////////////////////
#include <d3d12.h>
#include <dxgi1_4.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>


#include "targetver.h"

// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.

#include "util.h"
#include "sigleton.h"

#include "directXmath.h"

#include "enum.h"
#include "protocol.h"
#include "struct.h"