#include "stdafx.h"
#include "playerprocess.h"
#include "network.h"
#include "manager.h"
#include "monsterprocess.h"

CPlayerProcess::CPlayerProcess()
{
	if (!CProcess::m_pPlayerPool) {
		CProcess::m_pPlayerPool = new class CPlayerPool();
		BindPacketProcess();
		BindUpdateViewListFuntion();
	}
}

CPlayerProcess::~CPlayerProcess()
{
	if (m_pPlayerPool)
		SAFE_DELETE(m_pPlayerPool);
}

void CPlayerProcess::AcceptClient(const SOCKET& sSOCKET, unsigned short usID)
{
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPlayerSocket(sSOCKET);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPlayerID(usID);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetRecvState();

	/*/////////////////////////////
	이건 지워야함
	////////////////////////////*/
	s_DataBaseEvent sample_event;
	sample_event.client_num_ = usID;
	sample_event.exp_ = 100;
	sample_event.hp_ = 100;
	sample_event.level_ = 2;
	sample_event.map_ = rand()%4;
	sample_event.money_ = 0;
	sample_event.mp_ = 100;
	sample_event.posx_ = 5000.0f;
	sample_event.posy_ = 516.0f;
	sample_event.posz_ = 5000.0f;
	sample_event.charcter_ = rand() % 2;
	PlayerLogIn(sample_event);
}

void CPlayerProcess::Recv(unsigned short usID, DWORD dwSize, UCHAR* ucPacket)
{
	UCHAR* packet = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->RecvEvent(dwSize, ucPacket);
	if (packet != NULL) {
		m_fpPacketProcess[ucPacket[1]](usID, ucPacket);
		CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetRecvState();
	}
	else
		return;
}

void CPlayerProcess::PlayerMapSwap(unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ClearListElement();

	DeleteListElement(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap(), usID);
	
	for (auto& au : viewlist) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPlayerConnection();
		if (!is_connect) continue;
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
		}
		CManager::SendPlayerRemoveData(usID, au);
	}
}

void CPlayerProcess::PlayerDisconnect(unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPlayerConnect(false);

	/*///////////////////////////////////////////////
	이거 다시 살려야함
	//////////////////////////////////////////////*/
	/*if (!CData_Base::GetInstance()->GetIsLogin(usID)) {
		return;
	}*/

	if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetState() == e_ObjectState::e_Object_Down) {
		CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ReSpawn();
	}
	DirectX::XMFLOAT3 pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	char map = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap();

	s_DataBaseEvent data_event{ e_Data_Base_State::e_LogOut,usID,CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel(),CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp(), CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetHP() ,CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMP(), pos.x , pos.y, pos.z, map,  CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMoney(),  CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetIsPlayer()};

	/*///////////////////////////////////////////////
	이거 다시 살려야함
	//////////////////////////////////////////////*/
	//CData_Base::GetInstance()->InsertToDataBaseQueue(data_event);



	DeleteListElement(map, usID);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CManager::SendPlayerDisconnect(usID);
	closesocket(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerSokcet());
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InitClientData();

}

void CPlayerProcess::PlayerLogInFail(unsigned short usID)
{
	s_SC_LOGIN_STATE_PACKET packet;
	packet.m_bSize = sizeof(s_SC_LOGIN_STATE_PACKET);
	packet.m_bType = SC_LOGIN_FAIL;
	CManager::SendEvent(usID, &packet);
}

void CPlayerProcess::PlayerLogIn(s_DataBaseEvent& event)
{
	s_SC_LOGIN_STATE_PACKET packet;
	packet.m_bSize = sizeof(s_SC_LOGIN_STATE_PACKET);
	packet.m_bType = SC_LOGIN_SUCCESS;
	CManager::SendEvent(event.client_num_, &packet);

	concurrency::concurrent_unordered_set<unsigned short>sample_list;

	InsertListElement(event.map_, event.client_num_);

	s_SC_FIRST_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_FIRST_STATUS_PACKET);
	status_packet.m_bType = SC_LOGIN;
	status_packet.m_usId = event.client_num_;
	status_packet.m_cMap = event.map_;
	status_packet.m_fX = event.posx_;
	status_packet.m_fY = event.posy_;
	status_packet.m_fZ = event.posz_;
	status_packet.m_iExp = event.exp_;
	status_packet.m_iHP = event.hp_;
	status_packet.m_iLevel = event.level_;
	status_packet.m_iMoney = event.money_;
	status_packet.m_iMP = event.mp_;

	status_packet.m_iMaxHP = event.hp_ * event.level_;
	status_packet.m_iMaxMP = event.mp_ * event.level_;
	status_packet.m_iMaxEXP = event.exp_ * event.level_;

	status_packet.m_cObjectType = event.charcter_;
	CManager::SendEvent(event.client_num_, &status_packet);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetPos(DirectX::XMFLOAT3(event.posx_, event.posy_, event.posz_));
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLevel(event.level_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetMP(event.mp_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetHP(event.hp_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetMap(event.map_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetMoney(event.money_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetExp(event.exp_);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLimitHP(status_packet.m_iMaxHP);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetLimitMP(status_packet.m_iMaxMP);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetOOBB(DirectX::XMFLOAT3(event.posx_, event.posy_, event.posz_));
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetIsPlayer(event.charcter_);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[event.client_num_]->SetPlayerConnect(true);
	CopyBefore(event.map_, sample_list);
	CManager::SendPlayerAcceptData(sample_list, event.client_num_);

	//m_fpUpdateViewList[event.map_](event.client_num_);
}

void CPlayerProcess::ClientLoginInput(unsigned short usID, UCHAR* ucPacket)
{
	s_pCS_LOGIN_PACKET packet = reinterpret_cast<s_pCS_LOGIN_PACKET>(ucPacket);
	wchar_t input_id[MAX_STR_LEN];
	wcsncpy_s(input_id, MAX_STR_LEN, packet->m_wcPASS, (UINT32)wcslen(packet->m_wcPASS));
	s_DataBaseEvent data_event{ e_Data_Base_State::e_Login,usID,packet->m_wcID, input_id };
	CData_Base::GetInstance()->InsertToDataBaseQueue(data_event);
}

void CPlayerProcess::PlayerAcceptMap(char cMap, unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>sample_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->StatResetForSwapMap();
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetMap(cMap);
	CManager::SendMapSwap(cMap, usID);
	InsertListElement(cMap, usID);
	CopyBefore(cMap, sample_list);
	CManager::SendPlayerAcceptData(sample_list, usID);
	m_fpUpdateViewList[cMap](usID);
}

void CPlayerProcess::PlayerPos(unsigned short usID, UCHAR* ucPacket)
{
	s_pCS_POS_PACKET packet = reinterpret_cast<s_pCS_POS_PACKET>(ucPacket);
	DirectX::XMFLOAT3 sample_pos = DirectX::XMFLOAT3(packet->m_fX, packet->m_fY, packet->m_fZ);
	BoundingOrientedBox sample_box = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetOOBB();
	
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetPos(sample_pos);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetOOBB(sample_pos);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLookVec(DirectX::XMFLOAT3(packet->m_Dir));
	sample_box.Center = DirectX::XMFLOAT3(packet->m_fX, packet->m_fY, packet->m_fZ);
	m_fpUpdateViewList[CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap()](usID);
}

void CPlayerProcess::PlayerLook(unsigned short usID, UCHAR* ucPacket)
{
	s_pCS_PLAYER_DIR_PACKET packet = reinterpret_cast<s_pCS_PLAYER_DIR_PACKET>(ucPacket);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLookVec(packet->m_Dir);
	m_fpUpdateViewList[CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap()](usID);
}

void CPlayerProcess::PlayerAttack(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Attack_1)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Attack);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendAttackData(usID);
}

void CPlayerProcess::PlayerAttack2(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Attack_2)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Attack2);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendAttack2Data(usID);
}

void CPlayerProcess::PlayerSkill1(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_1)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill1);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill1Data(usID);
}

void CPlayerProcess::PlayerSkill2(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_2)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill2);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill2Data(usID);
}

void CPlayerProcess::PlayerSkill3(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_3)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill3);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill3Data(usID);
}

void CPlayerProcess::PlayerSkill4(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_4)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill4);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill4Data(usID);
}

void CPlayerProcess::PlayerSkill5(unsigned short usID)
{
	if (!CalculateInteraction(usID, e_PlayerAttackSkillType::e_Skill_5)) return;
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Skill5);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	CManager::SendSkill5Data(usID);
}

void CPlayerProcess::PlayerIdle(unsigned short usID)
{
	concurrency::concurrent_unordered_set<unsigned short>viewlist;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(viewlist);
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetState(e_ObjectState::e_Object_Idle);
	CManager::SendIdleData(usID);
}

void CPlayerProcess::UpdateViewListInVillage(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	CopyBefore(MAP_VILLAGE, login);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::UpdateViewListInLoby(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	CopyBefore(MAP_LOBY, login);

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::UpdateViewListInDungeon(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	CopyBefore(MAP_DUNGEON, login);

	char what_map = 0;

	for (auto& au : CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER]) {
		if (au.second->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 other_pos = au.second->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, MONSTER_RANGE)) {
			after.insert(au.first + MAX_PLAYER);
			continue;
		}
	}

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}

	s_SC_ANIMATION_PACKET state_packet;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;

	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			if (au >= MAX_PLAYER) {
				int monster_id = NOMALDUNGEON_MONSTER_NUM + au - MAX_PLAYER;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->GetWakeUp()) {
					state_packet.m_usId = au - MAX_PLAYER;
					CManager::SendEvent(usID, &state_packet);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->SetWakeUp(true);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->SetState(e_ObjectState::e_Object_Idle);
					CProcess::PostEvent(DUNGEON_MONSTER, au - MAX_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
				CManager::SendNpcLogInInPlayer(usID, au - MAX_PLAYER);
				continue;
			}
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (au >= MAX_PLAYER) {
				CManager::SendNpcStatusInPlayer(DUNGEON_MONSTER, usID, au - MAX_PLAYER);
				if (CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->GetFollow() != NO_TARGET_PLAYER) continue;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->GetWakeUp()) {
					state_packet.m_usId = au - MAX_PLAYER;
					CManager::SendEvent(usID, &state_packet);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->SetWakeUp(true);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->SetState(e_ObjectState::e_Object_Idle);
					CProcess::PostEvent(DUNGEON_MONSTER, au - MAX_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
				if (Vector3::ObjectRangeCheck(sample_pos, CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->GetPos(), MONSTER_DETECT_PLAYER)) {
					//Battle Range -> Move
					CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->SetFollow(usID);
					if (CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->GetState() != e_ObjectState::e_Object_Follow)
						CProcess::m_pMonsterPool->m_ccumMonsterPool[DUNGEON_MONSTER][au - MAX_PLAYER]->SetState(e_ObjectState::e_Object_Follow);
				}
				continue;
			}
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				if (au >= MAX_PLAYER) {
					CManager::SendNpcRemoveInPlayer(usID, au - MAX_PLAYER);
					continue;
				}
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::UpdateViewListInBoss(unsigned short usID)
{
	DirectX::XMFLOAT3 sample_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	concurrency::concurrent_unordered_set<unsigned short>login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	CopyBefore(MAP_BOSS, login);

	char what_map = 0;

	for (auto& au : CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER]) {
		if (au.second->GetState() == e_ObjectState::e_Object_Down) continue;
		DirectX::XMFLOAT3 other_pos = au.second->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, MONSTER_RANGE)) {
			after.insert(au.first + MAX_PLAYER);
			continue;
		}
	}

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyBefore(before);

	for (auto& au : login) {
		DirectX::XMFLOAT3 other_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(sample_pos, other_pos, PLAYER_AND_PLAYER_RANGE)) {
			after.insert(au);
			continue;
		}
	}
	s_SC_ANIMATION_PACKET state_packet;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;


	for (auto au : after) {
		//한번도 등장하지 않았던 아이디.
		if (before.count(au) == 0) {
			CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->InsertListElement(au);
			if (au >= MAX_PLAYER) {
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->GetWakeUp()) {
					state_packet.m_usId = au - MAX_PLAYER;
					CManager::SendEvent(usID, &state_packet);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->SetWakeUp(true);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->SetState(e_ObjectState::e_Object_Idle);
					CProcess::PostEvent(BOSSMAP_MONSTER, au - MAX_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
				CManager::SendNpcLogInInPlayer(usID, au - MAX_PLAYER);
				continue;
			}
			CManager::SendPlayerStatusData(au, usID);
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			else {
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			continue;
		}
		else {
			if (au >= MAX_PLAYER) {
				CManager::SendNpcStatusInPlayer(BOSSMAP_MONSTER, usID, au - MAX_PLAYER);
				if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->GetFollow() != NO_TARGET_PLAYER) continue;
				if (!CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->GetWakeUp()) {
					state_packet.m_usId = au - MAX_PLAYER;
					CManager::SendEvent(usID, &state_packet);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->SetWakeUp(true);
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->SetState(e_ObjectState::e_Object_Idle);
					CProcess::PostEvent(BOSSMAP_MONSTER, au - MAX_PLAYER, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
				}
				if (Vector3::ObjectRangeCheck(sample_pos, CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->GetPos(), MONSTER_DETECT_PLAYER)) {
					//Battle Range -> Move
					CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->SetFollow(usID);
					if (CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->GetState() != e_ObjectState::e_Object_Follow)
						CProcess::m_pMonsterPool->m_ccumMonsterPool[BOSSMAP_MONSTER][au - MAX_PLAYER]->SetState(e_ObjectState::e_Object_Follow);
				}
				continue;
			}
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->InsertListElement(usID);
				CManager::SendPlayerStatusData(usID, au);
				continue;
			}
			else {
				////////////////////////////////////////
				//FIXXX: 다시 방향은 위치데이터에 한번에
				///////////////////////////////////////
				//CManager::SendPlayerLook(CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPlayerID(), au);
				CManager::SendPlayerPosData(usID, au);
				continue;
			}
			continue;
		}
		continue;
	}

	for (auto au : before) {
		if (after.count(au) == 0) {
			if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ExistListElement(au)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->DeleteListElement(au);
				if (au >= MAX_PLAYER) {
					CManager::SendNpcRemoveInPlayer(usID, au - MAX_PLAYER);
					continue;
				}
				CManager::SendPlayerRemoveData(au, usID);
				if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->ExistListElement(usID)) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->DeleteListElement(usID);
					CManager::SendPlayerRemoveData(usID, au);
					continue;
				}
				continue;
			}
		}
	}
}

void CPlayerProcess::PlayerUpdate(char cState, unsigned short usID)
{
	switch (cState)
	{
	case e_ObjectState::e_Object_Down:
	{
		PlayerReSpawn(usID);
		break;
	}
	default:
		break;
	}
}

void CPlayerProcess::PlayerReSpawn(unsigned short usID)
{
	char map_num = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap();

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ClearListElement();	//이걸 해야하나?

	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->ReSpawn();

	s_SC_PLAYER_RESPAWN_PACKET packet;
	packet.m_bSize = sizeof(s_SC_PLAYER_RESPAWN_PACKET);
	packet.m_bType = SC_PLAYER_RESPAWN;
	CManager::SendEvent(usID, &packet);

	//원래 뷰리스트 업뎃을 여기다 넣었음
	m_fpUpdateViewList[CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap()](usID);
}

bool CPlayerProcess::CalculateInteraction(unsigned short usID, char cType)
{
	//나중에 스탯(hp) 변화도 보내주는거 추가 하기
	//레벨과 exp 또한...
	//
	//

	char map = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMap();
	if (map != MAP_DUNGEON && map != MAP_BOSS) {
		return true;
	}
	
	map -= 2;

	concurrency::concurrent_unordered_set<unsigned short>monster_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyMonsterList(monster_list);

	concurrency::concurrent_unordered_set<unsigned short>player_list;
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->CopyPlayerList(player_list);

	s_SC_ANIMATION_PACKET monster_state_packet;
	monster_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	int player_atk = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAtk();
	int player_mp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetMP();
	int monster_amr;
	int monster_hp;
	int after_monster_hp;
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetPos();
	unsigned short monster_id;

	switch (cType)
	{
	case e_PlayerAttackSkillType::e_Attack_1:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_ATTACK1_TIME_LIMIT >= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (!Vector3::ObjectRangeCheck(CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetPos(), player_pos, PLAYER_ATTACK_RANGE))continue;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerAttack(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr) - 100;
			if (after_monster_hp <= 0) {

				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}

				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	case e_PlayerAttackSkillType::e_Attack_2:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_ATTACK2_TIME_LIMIT >= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerAttack(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr);
			if (after_monster_hp <= 0) {
				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	case e_PlayerAttackSkillType::e_Skill_1:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_SKILL1_TIME_LIMIT>= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerSkill(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr);
			if (after_monster_hp <= 0) {
				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	case e_PlayerAttackSkillType::e_Skill_2:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_SKILL2_TIME_LIMIT >= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerSkill(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr);
			if (after_monster_hp <= 0) {
				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	case e_PlayerAttackSkillType::e_Skill_3:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_SKILL3_TIME_LIMIT >= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerSkill(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr);
			if (after_monster_hp <= 0) {
				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	case e_PlayerAttackSkillType::e_Skill_4:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_SKILL4_TIME_LIMIT >= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerSkill(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr);
			if (after_monster_hp <= 0) {
				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	case e_PlayerAttackSkillType::e_Skill_5:
	{
		if (CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetAniTime() + PLAYER_SKILL5_TIME_LIMIT >= std::chrono::high_resolution_clock::now()) return false;
		for (auto& monster : monster_list) {
			monster_id = monster - MAX_PLAYER;
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetState() == e_ObjectState::e_Object_Down) continue;
			if (!CheckCollisionPlayerSkill(map, usID, monster_id)) continue;
			monster_amr = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetAmr();
			monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetHP();
			after_monster_hp = monster_hp - (player_atk - monster_amr);
			if (after_monster_hp <= 0) {
				int monster_exp = CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->GetExp();
				int player_exp = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetExp();
				int player_level = CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->GetLevel();

				if (player_exp + monster_exp >= player_level * 100) {
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetExp(player_exp + monster_exp - (player_level * 100));
					CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetLevel(player_level + 1);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(0);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DOWN;
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetState(e_ObjectState::e_Object_Down);
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
				CProcess::PostEvent(map, monster_id, UPDATE_EVENT, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::microseconds(5000));
			}
			else {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[map][monster_id]->SetHP(after_monster_hp);
				monster_state_packet.m_usId = monster_id;
				monster_state_packet.m_bType = SC_NPC_DAMAGED;
				for (auto& player : player_list) {
					CManager::SendEvent(player, &monster_state_packet);
				}
			}
		}
		break;
	}
	default:
		break;
	}
	CProcess::m_pPlayerPool->m_ccumPlayerPool[usID]->SetAniTime();
	return true;
}