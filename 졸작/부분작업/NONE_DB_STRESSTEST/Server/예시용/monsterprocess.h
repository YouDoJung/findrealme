#pragma once
#include "stdafx.h"
#include "process.h"

class CMonsterProcess: public CProcess {
private:

	std::function<void(char, unsigned short, float)>m_fpMonsterUpdate[5];
public:
	explicit CMonsterProcess();
	virtual ~CMonsterProcess();

public:
	//Inline은 알아서 판단해서 해준다고는 하지만 뭔가 이건 Inline을 명시적으로 해놓고서 쓰자.
	inline void BindMonsterUpdate()
	{
		m_fpMonsterUpdate[e_ObjectState::e_Object_Attack] = [&](char cMap, unsigned short usID, float fUpdateTime) { this->AttackEvent(cMap, usID, fUpdateTime); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Down] = [&](char cMap, unsigned short usID, float fUpdateTime) { this->DownEvent(cMap, usID, fUpdateTime); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Follow] = [&](char cMap, unsigned short usID, float fUpdateTime) { this->FollowEvent(cMap, usID, fUpdateTime); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_Idle] = [&](char cMap, unsigned short usID, float fUpdateTime) { this->IdleEvent(cMap, usID, fUpdateTime); };
		m_fpMonsterUpdate[e_ObjectState::e_Object_MoveToFirstPos] = [&](char cMap, unsigned short usID, float fUpdateTime) { this->MoveFirstPosEvent(cMap, usID, fUpdateTime); };
	}

public:
	inline void UpdateMonster(char cState, char cMap, unsigned short usID, float fUpdateTime) { m_fpMonsterUpdate[cState](cMap, usID, fUpdateTime); }

	void MoveToTargetPlayer(char cMap, unsigned short usTargetID, unsigned short usID, float fUpdateTime);
	void MoveToFirstPos(char cMap, unsigned short usID, float fUpdateTime);
	void MoveToNoTargetPlayer(char cMap, unsigned short usID, float fUpdateTime);

	void CheckPlayerInRange(char cMap, concurrency::concurrent_unordered_set<unsigned short>& ccusLogin, concurrency::concurrent_unordered_set<unsigned short>& ccusList, unsigned short usID);

	void FollowEvent(char cMap, unsigned short usID, float fUpdateTime);
	void MoveFirstPosEvent(char cMap, unsigned short usID, float fUpdateTime);
	void MoveNoTargetEvent(char cMap, unsigned short usID, float fUpdateTime);
	void IdleEvent(char cMap, unsigned short usID, float fUpdateTime);
	void AttackEvent(char cMap, unsigned short usID, float fUpdateTime);
	void DownEvent(char cMap, unsigned short usID, float fUpdateTime);
};