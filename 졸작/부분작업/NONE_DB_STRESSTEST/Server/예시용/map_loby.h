#pragma once
#include "stdafx.h"

class CMap_Loby : public CTemplateSingleton<CMap_Loby> {

private:
	int m_iNumObject = 0;
	std::multimap<unsigned short, DirectX::BoundingOrientedBox>m_mmObjectBoundingBox;
public:
	explicit CMap_Loby();
	~CMap_Loby();

public:
	bool CheckMapObjectCollision(const DirectX::BoundingOrientedBox& ObjectOOBB);
};