#include "stdafx.h"
#include "map_boss.h"

CMap_Boss::CMap_Boss()
{
	unsigned short sample_id;
	DirectX::XMFLOAT3 sample_extent;
	DirectX::XMFLOAT3 sample_pos;

	m_mmObjectBoundingBox.clear();

	std::ifstream in("../MapData/BossDungeonObject.txt");
	if (!in.is_open()) {
		std::cout << "BossObject Open Failed" << std::endl;
		return;
	}

	in >> m_iNumObject;
	for (int i = 0; i < m_iNumObject; ++i) {
		if (in.eof())
			break;
		else {
			in >> sample_id;
			in >> sample_pos.x;
			in >> sample_pos.z;
			in >> sample_extent.x;
			in >> sample_extent.y;
			in >> sample_extent.z;
			sample_pos.y = 516.0f;
			m_mmObjectBoundingBox.insert(std::make_pair(sample_id, DirectX::BoundingOrientedBox(sample_pos, sample_extent, DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f))));
		}
	}
	in.close();
}

CMap_Boss::~CMap_Boss()
{
	m_mmObjectBoundingBox.clear();
}

bool CMap_Boss::CheckMapObjectCollision(DirectX::BoundingOrientedBox& ObjectOOBB)
{
	for (auto& au : m_mmObjectBoundingBox) {
		if (Vector3::ObjectRangeCheck(ObjectOOBB.Center, au.second.Center, 1000.f)) {
			if (au.second.Intersects(ObjectOOBB)) {
				return true;
			}
		}
	}
	return false;
}