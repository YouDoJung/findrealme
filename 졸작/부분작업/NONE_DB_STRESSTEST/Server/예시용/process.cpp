#include "stdafx.h"
#include "process.h"

class CPlayerPool* CProcess::m_pPlayerPool = nullptr;
class CMonsterPool* CProcess::m_pMonsterPool = nullptr;
concurrency::concurrent_unordered_set<unsigned short>CProcess::m_ccusLoginList[4];
concurrency::concurrent_priority_queue<s_ObjectEvent, CEventOperator>CProcess::m_ccpqEventQueue;

CProcess::CProcess()
{
}

CProcess::~CProcess()
{
	InitProcessData();
}

void CProcess::InitProcessData()
{
	for (int i = 0; i < 4; ++i) {
		SAFE_DELETE_CON_SET(CProcess::m_ccusLoginList[i]);
	}
	m_ccpqEventQueue.clear();
}

bool CProcess::CheckCollisionPlayerAttack(char cMap, unsigned short usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	//범위 설정
	player.Extents.x = 100.0f;
	player.Extents.z = 100.0f;


	if (player.Intersects(monster)) {
		std::cout << "충돌 처리\n";
		return true;
	}
	else return false;

	//광선 충돌 어케 해야하냐
	/*
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetPos();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetPos();
	DirectX::XMFLOAT3 monster_look = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetLookVector();

	DirectX::XMVECTOR origin = DirectX::XMLoadFloat3(&monster_pos);
	DirectX::XMVECTOR dir = DirectX::XMLoadFloat3(&monster_look);
	float range = PLAYER_ATTACK_RANGE;

	if (player.Intersects(origin, dir, range)) {
		std::cout << "충돌\n";
		return true;
	}
	else return false;*/
}

bool CProcess::CheckCollisionPlayerSkill(char cMap, unsigned short usPlayerID, unsigned short usMonsterID)
{
	DirectX::BoundingOrientedBox monster = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usMonsterID]->GetOOBB();
	DirectX::BoundingOrientedBox player = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetOOBB();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();

	DirectX::XMFLOAT3 for_attack_player_pos = player.Center;

	//플레이어가 보는 방향 앞쪽을 임의 거리로 설정하고 OOBB를 재조정한다.
	XMFLOAT3 xmf3Shift = XMFLOAT3(0, 0, 0);

	xmf3Shift = Vector3::Add(xmf3Shift, player_look, PLAYER_ATTACK_RANGE);

	player.Center = Vector3::Add(for_attack_player_pos, xmf3Shift);	//거리
	//범위 설정
	player.Extents.x = 500.0f;
	player.Extents.z = 500.0f;

	if (player.Intersects(monster)) {
		std::cout << "충돌 처리\n";
		return true;
	}
	else return false;

	//광선 충돌 어케 해야하냐
	/*
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetPos();
	DirectX::XMFLOAT3 player_look = CProcess::m_pPlayerPool->m_ccumPlayerPool[usPlayerID]->GetLookVector();
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetPos();
	DirectX::XMFLOAT3 monster_look = CProcess::m_pMonsterPool->m_ccumMonsterPool[usMonsterID]->GetLookVector();

	DirectX::XMVECTOR origin = DirectX::XMLoadFloat3(&monster_pos);
	DirectX::XMVECTOR dir = DirectX::XMLoadFloat3(&monster_look);
	float range = PLAYER_ATTACK_RANGE;

	if (player.Intersects(origin, dir, range)) {
		std::cout << "충돌\n";
		return true;
	}
	else return false;*/
}