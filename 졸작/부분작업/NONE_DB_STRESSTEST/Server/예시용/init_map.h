#pragma once
#include "stdafx.h"
#include "map_boss.h"
#include "map_dungeon.h"
#include "map_loby.h"
#include "map_village.h"

class CInit_Map : public CTemplateSingleton<CInit_Map> {
public:
	explicit CInit_Map()
	{
		CMap_Boss::GetInstance();
		CMap_Dungeon::GetInstance();
		CMap_Loby::GetInstance();
		CMap_Village::GetInstance();
		std::cout << "MAP INIT\n";
	}
	~CInit_Map()
	{
		CMap_Boss::DestoryInstance();
		CMap_Dungeon::DestoryInstance();
		CMap_Loby::DestoryInstance();
		CMap_Village::DestoryInstance();
	}
};