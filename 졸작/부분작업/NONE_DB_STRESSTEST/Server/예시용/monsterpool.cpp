#include "stdafx.h"
#include "monsterpool.h"

concurrency::concurrent_unordered_map<unsigned short, class CMonster*>CMonsterPool::m_ccumMonsterPool[2];

CMonsterPool::CMonsterPool()
{
	std::string sample_id;
	DirectX::XMFLOAT3 sample_center;
	DirectX::XMFLOAT3 sample_extent;
	DirectX::XMFLOAT3 sample_pos;
	DirectX::XMFLOAT4X4 sample_world;
	CMonster* sample_monster;

	for (char c = 0; c < 2; ++c) {
		m_ccumMonsterPool[c].clear();
	}

	std::ifstream dungeonfile("../NpcData/Dungeon/DungeonNPC.txt");
	if (!dungeonfile.is_open()) {
		std::cout << "DungeonNPC Open Failed" << std::endl;
		return;
	}
	else {
		std::cout << "DungeonNPC Init\n";
	}

	dungeonfile >> m_usNumObject;
	for (unsigned short us = 0; us < m_usNumObject; ++us) {
		dungeonfile >> sample_id;
		dungeonfile >> sample_center.x;
		dungeonfile >> sample_center.y;
		dungeonfile >> sample_center.z;
		dungeonfile >> sample_extent.x;
		dungeonfile >> sample_extent.y;
		dungeonfile >> sample_extent.z;
		dungeonfile >> sample_pos.x;
		dungeonfile >> sample_pos.y;
		dungeonfile >> sample_pos.z;
		sample_monster = new CMonster();
		sample_monster->SetWakeUp(false);

		if (sample_id == std::string("Peon")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Peon);
		}
		else if (sample_id == std::string("Lord")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Lord);
		}
		else if (sample_id == std::string("Grunt")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Grunt);
		}
		else if (sample_id == std::string("Boss")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Boss);
		}
		
		sample_monster->SetOOBB(sample_center);
		sample_monster->SetOOBBExtern(sample_extent);
		sample_monster->SetPos(sample_pos);
		sample_monster->SetFirstPos(sample_pos);
		sample_monster->SetMap(MAP_DUNGEON);


		m_ccumMonsterPool[0].insert(std::make_pair(us, sample_monster));
	}
	dungeonfile.close();

	std::ifstream bossfile("../NpcData/BossMap/BossNPC.txt");
	if (!bossfile.is_open()) {
		std::cout << "BossNPC Open Failed" << std::endl;
		return;
	}
	else {
		std::cout << "BossNPC Init\n";
	}

	bossfile >> m_usNumObject;
	for (unsigned short us = 0; us < m_usNumObject; ++us) {
		bossfile >> sample_id;
		bossfile >> sample_center.x;
		bossfile >> sample_center.y;
		bossfile >> sample_center.z;
		bossfile >> sample_extent.x;
		bossfile >> sample_extent.y;
		bossfile >> sample_extent.z;
		bossfile >> sample_pos.x;
		bossfile >> sample_pos.y;
		bossfile >> sample_pos.z;
		sample_monster = new CMonster();
		sample_monster->SetWakeUp(false); 
		if (sample_id == std::string("Peon")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Peon);
		}
		else if (sample_id == std::string("Lord")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Lord);
		}
		else if (sample_id == std::string("Grunt")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Grunt);
		}
		else if (sample_id == std::string("Boss")) {
			sample_monster->SetIsPlayer(e_ObjectType::e_Monster_Boss);
		}
		sample_monster->SetOOBB(sample_center);
		sample_monster->SetOOBBExtern(sample_extent);
		sample_monster->SetPos(sample_pos);
		sample_monster->SetFirstPos(sample_pos);
		sample_monster->SetMap(MAP_BOSS);

		m_ccumMonsterPool[1].insert(std::make_pair(us, sample_monster));
	}
	bossfile.close();
}

CMonsterPool::~CMonsterPool()
{

}