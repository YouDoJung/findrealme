#pragma once

typedef enum _Object_State_Type
{
	e_Object_Attack,
	e_Object_Down,//쳐맞는중
	e_Object_Follow,
	e_Object_Idle,	//기본 상태
	e_Object_MoveToFirstPos,
	e_Object_MoveToNoTarget,
	e_Object_Damaged,
	e_Object_Attack2,
	e_Object_Skill1,
	e_Object_Skill2,
	e_Object_Skill3,
	e_Object_Skill4,
	e_Object_Skill5
}e_ObjectState;

typedef enum _Object_Dir_Type
{
	e_Ob_Left,
	e_Ob_Up,
	e_Ob_Right,
	e_Ob_Down
}e_ObjectDir;

typedef enum _Object_Type
{
	e_Player_Man,
	e_Player_Woman,
	e_Monster_Grunt,
	e_Monster_Lord,
	e_Monster_Peon,
	e_Monster_Boss,
	e_NPC_0
}e_ObjectType;

typedef enum e_Player_Attack_Skill
{
	e_Attack_1 = 0,
	e_Attack_2,
	e_Skill_1,
	e_Skill_2,
	e_Skill_3,
	e_Skill_4,
	e_Skill_5
}e_PlayerAttackSkillType;


typedef enum e_Data_Base_State {
	e_Login,
	e_LogOut,
	e_ItemGet,
	e_ItemGive,
	e_ChangeState
}e_DBState;

typedef enum e_Data_Base_Save_State {
	e_LoginFail,
	e_LoginSuccess,
	e_LogOutFail,
	e_LogOutSuccess,
	e_ItemGetFail,
	e_ItemGetSuccess,
	e_ItemGiveFail,
	e_ItemGiveSuccess,
	e_ChangeStateFail,
	e_ChangeStateSuccess
}e_DBSaveState;