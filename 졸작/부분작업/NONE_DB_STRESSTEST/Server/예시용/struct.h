#pragma once
#include "enum.h"



/*////////////////////////////////
통신을 위한 구조체
*/////////////////////////////////
typedef struct _Context
{
	WSAOVERLAPPED m_sOverlapped;
	WSABUF m_sWsabuf;
	BYTE m_bType;
	UCHAR m_cBuf[BUFSIZE];
	char m_cState;
	char m_cMap;
}s_Context, *s_pContext;


/*////////////////////////////////
클라이언트 정보 저장용.
*/////////////////////////////////
typedef struct _Client
{
	s_Context m_sContext;
	int m_iPredata;	//이전
	int m_iCursize;	//현재

	UCHAR m_ucPacketBuf[MAX_PACKET_SIZE];

}s_Client, *s_pClient;

/*////////////////////////////////
In Client Object Stat
*/////////////////////////////////
typedef struct _InClient_Object
{
	BYTE m_eObjectType;
	int m_iLevel;
	int m_iExp;
	int m_iMoney;
	int m_iHP, m_iLimitHP;
	int m_iMP, m_iLimitMP;
	int m_iATK;//공격력
	int m_iAMR;//방어력
	e_ObjectState m_eState;
	BYTE m_bMap;
}s_InClientObject, *s_pInClientObject;

/*////////////////////////////////
오브젝트용
*/////////////////////////////////
typedef struct _Object
{
	BYTE m_eObjectType;
	DirectX::XMFLOAT4X4 m_xmf4World;
	DirectX::XMFLOAT3 m_xmf3FirstPos;
	e_ObjectDir m_eDir;	//캐릭터가 향하고있는 방향은 어디 화면이든 같아야하니깐
	int m_iLevel;
	int m_iExp;
	int m_iMoney;
	int m_iHP, m_iLimitHP;
	int m_iMP, m_iLimitMP;
	int m_iATK;//공격력
	int m_iAMR;//방어력
	e_ObjectState m_eState;
	std::chrono::high_resolution_clock::time_point m_tStartTime;
	BYTE m_bMap;
}s_Object, *s_pObject;


/*////////////////////////////////
Timer Event
*/////////////////////////////////
typedef struct _Object_Event {
	char m_cMap;
	std::chrono::high_resolution_clock::time_point   m_hrcTimePoint;
	char m_cEventType;
	char m_cOverType;
	unsigned short m_usID;
}s_ObjectEvent, *s_pObjectEvent;


/*////////////////////////////////
Object Event
*/////////////////////////////////
typedef struct _Monster_Event {
	DirectX::XMFLOAT3 m_xmf3ToTarget;
	float m_fRotate;
	e_ObjectState m_eState;
	_Monster_Event() {}
	_Monster_Event(DirectX::XMFLOAT3 xmf3ToTarget, float fRotate, e_ObjectState eState) {
		m_xmf3ToTarget = xmf3ToTarget,m_fRotate = fRotate, m_eState = eState;
	}
}s_MonsterEvent, *s_pMonsterEvent;


/*////////////////////////////////
DataBase Event
*/////////////////////////////////
typedef struct _DataBase_Event {
	char state_;
	unsigned short client_num_;
	wchar_t id_[MAX_STR_LEN];
	wchar_t password_[MAX_STR_LEN];
	int level_, exp_, hp_, mp_;
	int money_;
	float posx_, posy_, posz_;
	char map_, charcter_;
	_DataBase_Event() {}
	_DataBase_Event(char state, unsigned short client_num) : state_(state), client_num_(client_num){}
	_DataBase_Event(char state, unsigned short client_num, int level, int exp, int hp, int mp, float x, float y, float z, char map, int money, char character) : state_(state), client_num_(client_num), level_(level), exp_(exp), hp_(hp), mp_(mp), posx_(x), posy_(y), posz_(z), map_(map), money_(money), charcter_(character) {} //로그아웃때
	_DataBase_Event(e_Data_Base_State state, unsigned short client_num, wchar_t* id, wchar_t* password) : state_(state), client_num_(client_num){
		wcscpy_s(id_, id);
		wcscpy_s(password_, password);
	}
	_DataBase_Event(char state, unsigned short client_num, wchar_t* id, wchar_t* password, int level, int exp, int hp, int mp, float x, float y, float z, char map,char character) : state_(state),client_num_(client_num),level_(level), exp_(exp), hp_(hp), mp_(mp), posx_(x), posy_(y), posz_(z), map_(map), charcter_(character){
		wcscpy_s(id_, id);
		wcscpy_s(password_, password);
	}
	_DataBase_Event(char state, unsigned short client_num, int level, int exp, int hp, int mp, float x, float y, float z, char map, char character) : state_(state), client_num_(client_num), level_(level), exp_(exp), hp_(hp), mp_(mp), posx_(x), posy_(y), posz_(z), map_(map), charcter_(character) {}
}s_DataBaseEvent, *s_pDataBaseEvent;

typedef struct _User_Data {
	std::string m_sID;
	unsigned short m_usID;

	_User_Data(std::string sID, unsigned short usNum) {
		m_sID = sID;
		m_usID = usNum;
	}
}s_UserData;