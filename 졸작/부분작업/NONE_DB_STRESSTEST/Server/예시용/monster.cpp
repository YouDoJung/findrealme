#include "stdafx.h"
#include "monster.h"

CMonster::CMonster()
{

}

CMonster::~CMonster()
{

}

void CMonster::Move(const float& fTimeDistance)
{
	this->SetPos(Vector3::Add(this->GetPos(), Vector3::Add(DirectX::XMFLOAT3(0, 0, 0), this->GetLookVector(), MONSTER_IDLE_MOVE_SPEED * fTimeDistance)));
	
}

void CMonster::ReSpawn()
{
	DirectX::XMFLOAT3 first_pos = GetFirstPos();
	SetPos(first_pos);
	SetOOBB(first_pos);
	SetState(e_ObjectState::e_Object_Idle);
	SetWakeUp(false);
	SetFollow(NO_TARGET_PLAYER);
	ClearEvent();
	DirectX::XMFLOAT3 look = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
	SetLookVec(look);
}