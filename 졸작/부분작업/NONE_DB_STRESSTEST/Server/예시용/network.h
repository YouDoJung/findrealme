#pragma once
#include "stdafx.h"
#include "iocp.h"
#include "playerprocess.h"
#include "monsterprocess.h"
#include "manager.h"
#include "gametimer.h"
#include "data_base.h"

class CNetwork: public CTemplateSingleton<CNetwork>{
private:
	SOCKET m_Listensock = INVALID_SOCKET;
	CPlayerProcess* m_pPlayerProcess = nullptr;
	CMonsterProcess* m_pMonsterProcess = nullptr;
	volatile bool m_bRunningServer = true;
private:
	volatile unsigned short m_usUserID = 0;
public:
	explicit CNetwork();
	~CNetwork();
public:
	BOOL InitWinSock();
	BOOL InitCompletionPort();
	BOOL InitSock();

	void Initialize();
	void Disconnect();

	void WorkerThread();
	void AcceptThread();
	void UpdateThread();
	void DataBaseThread();

	void EndServer() { m_bRunningServer = false; }
	bool GetServerState() { return m_bRunningServer; }

public:
	static void Err_quit(char *msg);
	static void Err_display(char *msg);

};