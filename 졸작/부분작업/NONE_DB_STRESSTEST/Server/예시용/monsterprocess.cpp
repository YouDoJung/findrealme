#include "stdafx.h"
#include "monsterprocess.h"
#include "manager.h"
#include "map_dungeon.h"
#include "map_boss.h"

CMonsterProcess::CMonsterProcess()
{
	if (!CProcess::m_pMonsterPool) {
		CProcess::m_pMonsterPool = new class CMonsterPool();
		BindMonsterUpdate();
	}
}

CMonsterProcess::~CMonsterProcess()
{
	if (m_pMonsterPool)
		SAFE_DELETE(m_pMonsterPool);
}

void CMonsterProcess::CheckPlayerInRange(char cMap, concurrency::concurrent_unordered_set<unsigned short>& ccusLogin, concurrency::concurrent_unordered_set<unsigned short>& ccusList, unsigned short usID)
{
	DirectX::XMFLOAT3 monster_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	for (auto& au : ccusLogin) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPlayerConnection();
		if (!is_connect) continue;
		//if (CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetState() == e_ObjectState::e_Object_Down)continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[au]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_RANGE)) {
			ccusList.insert(au);
		}
	}
}

void CMonsterProcess::MoveToTargetPlayer(char cMap, unsigned short usTargetID, unsigned short usID, float fUpdateTime)
{
	s_MonsterEvent sample_event;
	
	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pPlayerPool->m_ccumPlayerPool[usTargetID]->GetPos();   // 플레이어 좌표

	if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
		DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
		float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
		float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
		DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
		float fNextRotate = 0.0f;
		if (fDotProduct > -0.95) {
			fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
			//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
			XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
			//몬스터를 회전시킨다.
			xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		}
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);

		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
		
	}

	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
		float monster_rotate;
		DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
		monster_rotate = sample_event.m_fRotate;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetLookVec(xmf3Look);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetPos(xmf3Position);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetOOBB(xmf3Position);
	}
}

void CMonsterProcess::MoveToFirstPos(char cMap, unsigned short usID, float fUpdateTime)
{
	s_MonsterEvent sample_event;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();   // 플레이어 좌표

	if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, 10)) {
		DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)
		float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
		float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
		DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
		float fNextRotate = 0.0f;
		if (fDotProduct > -0.95) {
			fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
			//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
			XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
			//몬스터를 회전시킨다.
			xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		}
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);

		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
	}

	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
		float monster_rotate;
		DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
		monster_rotate = sample_event.m_fRotate;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetLookVec(xmf3Look);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetPos(xmf3Position);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetOOBB(xmf3Position);
	}
}


void CMonsterProcess::FollowEvent(char cMap, unsigned short usID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	//쫓는 와중에 플레이어가 죽으면 어택과 동시에 일어나면 어택부분에서 NO_TARGET으로 바꿔준 상태라 이상한 곳을 참조하게 된다.
	//여기서 체크해주기.
	//그니깐 뭔가 동시성 떄문에? shared_mutex를 쓰니 이렇게 되는 거임 동시에 읽을 수는 있어서.
	unsigned short target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow();
	if (target_id == NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}
	else {
		target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow();
	}

	char target_state = CProcess::m_pPlayerPool->m_ccumPlayerPool[target_id]->GetState();
	//얘는 아직 한번도 때려보지는 못했는데 쫓는 와주에 타겟이 죽어버리면
	if (target_state == e_ObjectState::e_Object_Down) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}
	concurrency::concurrent_unordered_set<unsigned short>before_login;
	concurrency::concurrent_unordered_set<unsigned short>after_login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	s_SC_NPC_LOGIN_PACKET login_packet;
	s_SC_NPC_STATUS_PACKET status_packet;
	s_SC_DISCONNECT_PACKET remove_packet;
	s_SC_ANIMATION_PACKET state_packet;

	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;

	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, before_login);
	CheckPlayerInRange(cMap, before_login, before, usID);

	MoveToTargetPlayer(cMap, target_id, usID, fUpdateTime);

	CopyBefore(this_map, after_login);
	CheckPlayerInRange(cMap, after_login, after, usID);

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	status_packet.m_fX = sample_pos.x;
	status_packet.m_fZ = sample_pos.z;
	status_packet.m_iHP = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLookVector();
	DirectX::XMFLOAT3 target_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[target_id]->GetPos();   // 플레이어 좌표

	for (auto& pu : after) {
		if (before.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(usID + MAX_PLAYER)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->InsertListElement(usID + MAX_PLAYER);
				CManager::SendEvent(pu, &login_packet);
				CManager::SendEvent(pu, &status_packet);
			}
			else {
				CManager::SendEvent(pu, &status_packet);
			}
		}
		else {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &status_packet);
		}
	}

	for (auto& pu : before) {
		if (after.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->DeleteListElement(usID + MAX_PLAYER);
			CManager::SendEvent(pu, &remove_packet);
		}
	}

	if (after.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(false);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
	}
	else {
		if (Vector3::ObjectRangeCheck(sample_pos, target_pos, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
			state_packet.m_bType = SC_NPC_ATTACK;
			state_packet.m_usId = usID;
			for (auto& pu : after) {
				CManager::SendEvent(pu, &state_packet);
			}
			CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else if (!Vector3::ObjectRangeCheck(sample_pos, target_pos, MONSTER_DETECT_PLAYER)) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
			CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
}

void CMonsterProcess::IdleEvent(char cMap, unsigned short usID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	if (range_list.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(false);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		return;
	}

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	
	for (auto& pu : range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
		if (!is_connect) continue;
		DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPos();
		if (Vector3::ObjectRangeCheck(player_pos, sample_pos, MONSTER_DETECT_PLAYER)) {
			if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow() == NO_TARGET_PLAYER) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Follow);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(pu);
				break;
			}
		}
	}

	unsigned short target_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow();
	if (target_id != NO_TARGET_PLAYER) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		state_packet.m_bType = SC_NPC_FOLLOW;
		state_packet.m_usId = usID;
		for (auto& pu : range_list) {
			CManager::SendEvent(pu, &state_packet);
		}
		CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
	}
	else {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1000));
	}
}

void CMonsterProcess::AttackEvent(char cMap, unsigned short usID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	//여기서 충돌 체크를 한 다음에 맞았다고 판정할건가?
	//그럼 만약에 때리는 순간에 뒤로 물러나면 안맞은걸로 될텐데 RPG에서 그렇게 되는게 맞는가?
	int player_id = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFollow();

	if (player_id == NO_TARGET_PLAYER) return;
	
	e_ObjectState target_state = CProcess::m_pPlayerPool->m_ccumPlayerPool[player_id]->GetState();
	if (target_state == e_ObjectState::e_Object_Down) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		return;
	}

	int player_hp = CProcess::m_pPlayerPool->m_ccumPlayerPool[player_id]->GetHP();
	int player_amr = CProcess::m_pPlayerPool->m_ccumPlayerPool[player_id]->GetAmr();
	DirectX::XMFLOAT3 player_pos = CProcess::m_pPlayerPool->m_ccumPlayerPool[player_id]->GetPos();

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(player_pos, monster_pos, true);   // 내가 타겟을 향하는 방향벡터(단위)

	float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
	float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
	DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
	float fNextRotate = 0.0f;
	if (fDotProduct > -0.95) {
		fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
	}
	//회전시킨 후의 Look Vec을 설정한다.
	xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));

	s_SC_NPC_DIR_PACKET npc_dir_packet;
	npc_dir_packet.m_bSize = sizeof(s_SC_NPC_DIR_PACKET);
	npc_dir_packet.m_bType = SC_NPC_DIR;
	npc_dir_packet.m_usId = usID;
	npc_dir_packet.m_xmf3Dir = xmf3Look;

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	s_SC_ANIMATION_PACKET player_state_packet;
	player_state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);


	int monster_atk = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetAtk();
	int after_player_hp = player_hp - (monster_atk - player_amr);
	if (after_player_hp <= 0) {
		CProcess::m_pPlayerPool->m_ccumPlayerPool[player_id]->SetState(e_ObjectState::e_Object_Down);

		player_state_packet.m_bType = SC_PLAYER_DOWN;
		player_state_packet.m_usId = player_id;

		for (auto& pu : range_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &npc_dir_packet);
			CManager::SendEvent(pu, &player_state_packet);
		}
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();

		CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		//여기서 부활 큐 ㄱㄱ
		CProcess::PostEvent(cMap, player_id, PLAYER_UPDATE, e_ObjectState::e_Object_Down, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000));

	}
	else {
		CProcess::m_pPlayerPool->m_ccumPlayerPool[player_id]->SetHP(after_player_hp);
		player_state_packet.m_bType = SC_PLAYER_DAMAGED;
		player_state_packet.m_usId = player_id;

		for (auto& pu : range_list) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &npc_dir_packet);
			CManager::SendEvent(pu, &player_state_packet);
		}

		if (range_list.empty()) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(false);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		}
		else {
			if (Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Attack, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(800));
			}
			else if (!Vector3::ObjectRangeCheck(player_pos, monster_pos, MONSTER_DETECT_PLAYER)) {
				state_packet.m_bType = SC_NPC_FOLLOW;
				state_packet.m_usId = usID;
				for (auto& pu : range_list) {
					CManager::SendEvent(pu, &state_packet);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_MoveToFirstPos);
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
				CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
			}
			else {
				state_packet.m_bType = SC_NPC_FOLLOW;
				state_packet.m_usId = usID;
				for (auto& pu : range_list) {
					CManager::SendEvent(pu, &state_packet);
				}
				CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
				CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Follow, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(30));
			}
		}
	}

	
}

void CMonsterProcess::DownEvent(char cMap, unsigned short usID, float fUpdateTime)
{
	concurrency::concurrent_unordered_set<unsigned short>login_list;
	concurrency::concurrent_unordered_set<unsigned short>range_list;

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, login_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	s_SC_DISCONNECT_PACKET remove_packet;
	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	//시체가 사라지도록
	for (auto& player : range_list) {
		CManager::SendEvent(player, &remove_packet);
	}

	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ReSpawn();
	//리스폰 후 위치 재설정 하고 다시 범위에 있는 플레이어
	SAFE_DELETE_CON_SET(range_list);
	CheckPlayerInRange(cMap, login_list, range_list, usID);

	//리스폰 할때 전부 상태 바꿔 줬으니 이것만.
	if (range_list.empty())return;

	//근처에 플레이어가 있는 상태니깐
	CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(true);

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 monster_pos = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));

	s_SC_NPC_LOGIN_PACKET login_packet;
	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	s_SC_NPC_STATUS_PACKET status_packet;
	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;
	status_packet.m_fX = monster_pos.x;
	status_packet.m_fZ = monster_pos.z;
	status_packet.m_iHP = 0;
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = xmf3Look;

	s_SC_ANIMATION_PACKET state_packet;
	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);
	state_packet.m_bType = SC_NPC_IDLE;
	state_packet.m_usId = usID;


	for (auto& player: range_list) {
		bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[player]->GetPlayerConnection();
		if (!is_connect) continue;
		CManager::SendEvent(player, &login_packet);
		CManager::SendEvent(player, &status_packet);
		CManager::SendEvent(player, &state_packet);
	}

	CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
}

void CMonsterProcess::MoveFirstPosEvent(char cMap, unsigned short usID, float fUpdateTime)
{
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetState() == e_ObjectState::e_Object_Down) return;

	concurrency::concurrent_unordered_set<unsigned short>before_login;
	concurrency::concurrent_unordered_set<unsigned short>after_login;
	concurrency::concurrent_unordered_set<unsigned short>before;
	concurrency::concurrent_unordered_set<unsigned short>after;

	s_SC_NPC_LOGIN_PACKET login_packet;
	s_SC_NPC_STATUS_PACKET status_packet;
	s_SC_DISCONNECT_PACKET remove_packet;
	s_SC_ANIMATION_PACKET state_packet;

	login_packet.m_bSize = sizeof(s_SC_NPC_LOGIN_PACKET);
	login_packet.m_bType = SC_NPC_LOG_IN;
	login_packet.m_usId = usID;

	status_packet.m_bSize = sizeof(s_SC_NPC_STATUS_PACKET);
	status_packet.m_bType = SC_NPC_STATUS;
	status_packet.m_usId = usID;

	remove_packet.m_bSize = sizeof(s_SC_DISCONNECT_PACKET);
	remove_packet.m_bType = SC_NPC_REMOVE;
	remove_packet.m_usId = usID;

	state_packet.m_bSize = sizeof(s_SC_ANIMATION_PACKET);

	char this_map = cMap + 2;
	//던전 몬스터인지 보스맵 몬스터인지 구별하기.
	CopyBefore(this_map, before_login);
	CheckPlayerInRange(cMap, before_login, before, usID);

	MoveToFirstPos(cMap, usID, fUpdateTime);

	CopyBefore(this_map, after_login);
	CheckPlayerInRange(cMap, after_login, after, usID);

	int monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetHP();
	int limit_monster_hp = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLimitHP();

	//돌아가는 중에 hp 리셋 시키기
	if (monster_hp < limit_monster_hp) {
		double percent_hp = limit_monster_hp * 0.5;
		monster_hp += (int)percent_hp;
		if (monster_hp >= limit_monster_hp) {
			monster_hp = limit_monster_hp;
		}
	}

	DirectX::XMFLOAT3 sample_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetPos();
	DirectX::XMFLOAT3 first_pos = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetFirstPos();
	status_packet.m_fX = sample_pos.x;
	status_packet.m_fZ = sample_pos.z;
	status_packet.m_iHP = monster_hp;
	status_packet.m_iLevel = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLevel();
	status_packet.m_xmf3Dir = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetLookVector();


	for (auto& pu : after) {
		if (before.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			if (!CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->ExistListElement(usID + MAX_PLAYER)) {
				CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->InsertListElement(usID + MAX_PLAYER);
				CManager::SendEvent(pu, &login_packet);
				CManager::SendEvent(pu, &status_packet);
			}
			else {
				CManager::SendEvent(pu, &status_packet);
			}
		}
		else {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CManager::SendEvent(pu, &status_packet);
		}
	}

	for (auto& pu : before) {
		if (after.count(pu) == 0) {
			bool is_connect = CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->GetPlayerConnection();
			if (!is_connect) continue;
			CProcess::m_pPlayerPool->m_ccumPlayerPool[pu]->DeleteListElement(usID + MAX_PLAYER);
			CManager::SendEvent(pu, &remove_packet);
		}
	}

	if (after.empty()) {
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWakeUp(false);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetPos(first_pos);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(limit_monster_hp);
	}
	else {
		if (Vector3::ObjectRangeCheck(sample_pos, first_pos, 10.0f)) {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->ClearEvent();
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetState(e_ObjectState::e_Object_Idle);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetFollow(NO_TARGET_PLAYER);
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(limit_monster_hp);
			state_packet.m_bType = SC_NPC_IDLE;
			state_packet.m_usId = usID;
			for (auto& pu : after_login) {
				CManager::SendEvent(pu, &state_packet);
			}
			CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_Idle, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
		else {
			CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetHP(monster_hp);
			CProcess::PostEvent(cMap, usID, UPDATE_EVENT, e_ObjectState::e_Object_MoveToFirstPos, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(1));
		}
	}
}

void CMonsterProcess::MoveNoTargetEvent(char cMap, unsigned short usID, float fUpdateTime)
{
	s_MonsterEvent sample_event;

	//월드 행렬을 가져온다.
	DirectX::XMFLOAT4X4 xmf4x4World = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetWorld();
	//월드 행렬을 통해서 좌표를 가져온다.
	DirectX::XMFLOAT3 xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41, xmf4x4World._42, xmf4x4World._43);
	DirectX::XMFLOAT3 xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
	DirectX::XMFLOAT3 xmf3TargetPosition = CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetRandomPos();
	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetDirTime() + std::chrono::milliseconds(5000) <= std::chrono::high_resolution_clock::now()) {

	}

	if (!Vector3::ObjectRangeCheck(xmf3TargetPosition, xmf3Position, MONSTER_DETECT_PLAYER_AND_ATTACK)) {
		DirectX::XMFLOAT3 xmf3ToTarget = Vector3::Subtract(xmf3TargetPosition, xmf3Position, true);   // 내가 타겟을 향하는 방향벡터(단위)

		float fDotProduct = Vector3::DotProduct(xmf3Look, xmf3ToTarget);
		float fAngle = ::IsEqual(fDotProduct, 1.0f) ? 0.0f : ((fDotProduct > 0.0f) ? XMConvertToDegrees(acos(fDotProduct)) : 90.0f);
		DirectX::XMFLOAT3 xmf3CrossProduct = Vector3::CrossProduct(xmf3ToTarget, xmf3Look);                 //외적으로 회전축을 구한다.
		float fNextRotate = 0.0f;
		if (fDotProduct > -0.95) {
			fNextRotate = xmf3CrossProduct.y * fUpdateTime * MONSTER_TURN_SPEED;
			//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
			XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(fNextRotate), XMConvertToRadians(0.f));
			//몬스터를 회전시킨다.
			xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		}
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);

		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SaveEvent(xmf3ToTarget, fNextRotate, e_ObjectState::e_Object_Follow);
	}

	if (CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->GetEvent(sample_event)) {
		float monster_rotate;
		DirectX::XMFLOAT3 xmf3ToTarget = sample_event.m_xmf3ToTarget;
		monster_rotate = sample_event.m_fRotate;
		//여기서 다음 순서 Rotate값을 가져와서 회전값으로 설정한다.
		XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(0.f), XMConvertToRadians(monster_rotate), XMConvertToRadians(0.f));
		//몬스터를 회전시킨다.
		xmf4x4World = Matrix4x4::Multiply(mtxRotate, xmf4x4World);
		//회전시킨 후의 Look Vec을 설정한다.
		xmf3Look = Vector3::Normalize(DirectX::XMFLOAT3(xmf4x4World._31, xmf4x4World._32, xmf4x4World._33));
		//여기서 좌표를 설정 한다.
		xmf3Position = DirectX::XMFLOAT3(xmf4x4World._41 + xmf3ToTarget.x * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._42 + xmf3ToTarget.y * MONSTER_TARGET_MOVE_SPEED * fUpdateTime, xmf4x4World._43 + xmf3ToTarget.z * MONSTER_TARGET_MOVE_SPEED * fUpdateTime);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetWorld(xmf4x4World);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetLookVec(xmf3Look);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetPos(xmf3Position);
		CProcess::m_pMonsterPool->m_ccumMonsterPool[cMap][usID]->SetOOBB(xmf3Position);
	}
}