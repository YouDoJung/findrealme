#pragma once
#include "stdafx.h"

class CMap_Dungeon : public CTemplateSingleton<CMap_Dungeon> {

private:
	int m_iNumObject = 0;
	std::multimap<unsigned short, DirectX::BoundingOrientedBox>m_mmObjectBoundingBox;
public:
	explicit CMap_Dungeon();
	~CMap_Dungeon();

public:
	bool CheckMapObjectCollision(DirectX::BoundingOrientedBox& ObjectOOBB);
};