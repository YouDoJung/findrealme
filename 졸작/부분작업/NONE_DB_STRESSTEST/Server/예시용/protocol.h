#pragma once

///////////////////////////////////////\
FOR NETWORK							   
///////////////////////////////////////
constexpr size_t MAX_PACKET_SIZE = 255;
constexpr int SERVER_PORT = 9000;
constexpr unsigned short MAX_PLAYER = 4000;
constexpr int NOMALDUNGEON_MONSTER_NUM = 244;
constexpr int BOSSMAP_MONSTER_NUM = 48;
constexpr int MAX_MONSTER = NOMALDUNGEON_MONSTER_NUM + BOSSMAP_MONSTER_NUM;
constexpr size_t BUFSIZE = 6000;
//#define SERVERIP  "127.0.0.1"
constexpr size_t MAX_STR_LEN = 50;

extern char SERVERIP[20];

///////////////////////////////////////\
FOR OVERLAPPED EVENT
///////////////////////////////////////
constexpr char RECV_EVENT = 1;
constexpr char SEND_EVENT = 2;
constexpr char UPDATE_EVENT = 3;
constexpr char PLAYER_UPDATE = 4;
constexpr char DATABASE_EVENT = 5;


///////////////////////////////////////\
FOR PROTOCOL						   
///////////////////////////////////////
constexpr char CS_LOOK = 0;
constexpr char CS_POS = 1;
constexpr char CS_ATTACK = 2;
constexpr char CS_ATTACK2 = 3;
constexpr char CS_SKILL1 = 4;
constexpr char CS_SKILL2 = 5;
constexpr char CS_SKILL3 = 6;
constexpr char CS_SKILL4 = 7;
constexpr char CS_SKILL5 = 8;
constexpr char CS_IDLE = 9;
constexpr char CS_MAP_SWAP_TO_VILLAGE = 10;
constexpr char CS_MAP_SWAP_TO_LOBY = 11;
constexpr char CS_MAP_SWAP_TO_DUNGEON = 12;
constexpr char CS_MAP_SWAP_TO_BOSS = 13;
constexpr char CS_LOGIN = 14;

constexpr char SC_POS = 1;
constexpr char SC_ATTACK = 2;
constexpr char SC_DIR = 3;
constexpr char SC_CONNECT = 4;
constexpr char SC_DISCONNECT = 5;
constexpr char SC_FIRST_DATA = 6;
constexpr char SC_IDLE = 7;
constexpr char SC_LOGIN = 8;
constexpr char SC_MAP_SWAP = 9;
constexpr char SC_ATTACK2 = 10;
constexpr char SC_SKILL1 = 11;
constexpr char SC_SKILL2 = 12;
constexpr char SC_SKILL3 = 13;
constexpr char SC_SKILL4 = 14;
constexpr char SC_SKILL5 = 15;

constexpr char SC_NPC_LOG_IN = 16;
constexpr char SC_NPC_STATUS = 17;
constexpr char SC_NPC_REMOVE = 18;
constexpr char SC_NPC_DIR = 19;
constexpr char SC_NPC_ATTACK = 20;
constexpr char SC_NPC_FOLLOW = 21;
constexpr char SC_NPC_IDLE = 22;
constexpr char SC_NPC_DOWN = 23;
constexpr char SC_NPC_DAMAGED = 24;


constexpr char SC_PLAYER_DAMAGED = 25;
constexpr char SC_PLAYER_DOWN = 26;
constexpr char SC_PLAYER_RESPAWN = 27;


constexpr char SC_LOGIN_SUCCESS = 28;	//아이디 로그인 의미
constexpr char SC_LOGIN_FAIL = 29;


///////////////////////////////////////\
FOR MAP									
///////////////////////////////////////
constexpr char MAP_VILLAGE = 0;
constexpr char MAP_LOBY = 1;
constexpr char MAP_DUNGEON = 2;
constexpr char MAP_BOSS = 3;

///////////////////////////////////////\
FOR MAP	IN MONSTER								
///////////////////////////////////////
constexpr char DUNGEON_MONSTER = 0;
constexpr char BOSSMAP_MONSTER = 1;


///////////////////////////////////////\
FOR KEY DIR							   
///////////////////////////////////////
#define DIR_FORWARD				0x01
#define DIR_BACKWARD			0x02
#define DIR_LEFT				0x04
#define DIR_RIGHT				0x08
#define DIR_UP					0x10
#define DIR_DOWN				0x20


/*
For Player
*/
constexpr float PLAYER_MOVE_SPEED = 800.f;
constexpr float PLAYER_DISTANCE = 1000.f;
constexpr float PLAYER_AND_PLAYER_RANGE = 2500.f;
constexpr float PLAYER_ATTACK_RANGE = 300.0f;
constexpr float PLAYER_SKILL_RANGE_1 = 100.0f;
constexpr float PLAYER_SKILL_RANGE_2 = 1000.0f;

constexpr std::chrono::milliseconds PLAYER_ATTACK1_TIME_LIMIT = std::chrono::milliseconds(500);
constexpr std::chrono::milliseconds PLAYER_ATTACK2_TIME_LIMIT = std::chrono::milliseconds(500);
constexpr std::chrono::milliseconds PLAYER_SKILL1_TIME_LIMIT = std::chrono::milliseconds(500);
constexpr std::chrono::milliseconds PLAYER_SKILL2_TIME_LIMIT = std::chrono::milliseconds(500);
constexpr std::chrono::milliseconds PLAYER_SKILL3_TIME_LIMIT = std::chrono::milliseconds(500);
constexpr std::chrono::milliseconds PLAYER_SKILL4_TIME_LIMIT = std::chrono::milliseconds(500);
constexpr std::chrono::milliseconds PLAYER_SKILL5_TIME_LIMIT = std::chrono::milliseconds(500);




/*
For Monster
*/
constexpr float MONSTER_IDLE_MOVE_SPEED = 500.f;
constexpr float MONSTER_TARGET_MOVE_SPEED = 1000.0f;
constexpr float MONSTER_RANGE = 2000.0f;	//View List Check
constexpr float MONSTER_DETECT_PLAYER = 1000.0f;	//Monster Detect Player Range -> FOLLOW
constexpr float MONSTER_DETECT_PLAYER_AND_ATTACK = 200.0f;
constexpr float MONSTER_AND_TARGET_DISTANCE = 800.f;
constexpr float MONSTER_TURN_SPEED = 1000.0f;

constexpr short NO_TARGET_PLAYER = MAX_PLAYER;



#pragma pack(push, 1)
typedef struct _SC_Login_State_Packet		//아이디 비번 입력후 결과 패킷
{
	BYTE m_bSize;
	BYTE m_bType;
}s_SC_LOGIN_STATE_PACKET, *s_pSC_LOGIN_STATE_PACKET;

typedef struct _SC_First_Status_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
	float m_fX, m_fY, m_fZ;
	int m_iHP, m_iMP, m_iLevel, m_iExp, m_iMoney;
	int m_iMaxHP, m_iMaxMP, m_iMaxEXP;
	char m_cMap;
	char m_cObjectType;
}s_SC_FIRST_STATUS_PACKET, *s_pSC_FIRST_STATUS_PACKET;

typedef struct _SC_Login_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
}s_SC_LOGIN_PACKET, *s_pSC_LOGIN_PACKET;

typedef struct _SC_Npc_Login_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
}s_SC_NPC_LOGIN_PACKET, *s_pSC_NPC_LOGIN_PACKET;

typedef struct _SC_Npc_Status_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
	float m_fX, m_fZ;
	int m_iHP, m_iLevel;
	DirectX::XMFLOAT3 m_xmf3Dir;
}s_SC_NPC_STATUS_PACKET, *s_pSC_NPC_STATUS_PACKET;

typedef struct _SC_Npc_Dir_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
	DirectX::XMFLOAT3 m_xmf3Dir;
}s_SC_NPC_DIR_PACKET, *s_pSC_NPC_DIR_PACKET;

typedef struct _SC_Accept_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
	float m_fX, m_fY, m_fZ;
	DirectX::XMFLOAT3 m_Dir;
	int m_iHP, m_iLevel, m_iMaxHP;
	wchar_t m_wcID[MAX_STR_LEN];
	char m_cObjectType;
}s_SC_ACCEPT_PACKET, *s_pSC_ACCEPT_PACKET;

typedef struct _SC_Dir_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
	DirectX::XMFLOAT3 m_Dir;
}s_SC_DIR_PACKET, *s_pSC_DIR_PACKET;

typedef struct _SC_Pos_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
	float m_fX, m_fY, m_fZ;
	DirectX::XMFLOAT3 m_Dir;
}s_SC_POS_PACKET, *s_pSC_POS_PACKET;

typedef struct _SC_Disconnect_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
}s_SC_DISCONNECT_PACKET, *s_pSC_DISCONNECT_PACKET;

typedef struct _SC_Animation_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	unsigned short m_usId;
}s_SC_ANIMATION_PACKET, *s_pSC_ANIMATION_PACKET;

typedef struct _SC_Player_Respawn_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
}s_SC_PLAYER_RESPAWN_PACKET, *s_pSC_PLAYER_RESPAWN_PACKET;

typedef struct _SC_Player_Map_Swap_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	BYTE m_bMapType;
}s_SC_PLAYER_MAP_SWAP_PACKET, *s_pSC_PLAYER_MAP_SWAP_PACKET;

/*//////////////////////////////////////////////////
클라이언트에서 서버로.
*///////////////////////////////////////////////////
typedef struct _CS_Login_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	wchar_t m_wcID[MAX_STR_LEN];
	wchar_t m_wcPASS[MAX_STR_LEN];
}s_CS_LOGIN_PACKET, *s_pCS_LOGIN_PACKET;

typedef struct _CS_Pos_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	float m_fX, m_fY, m_fZ;
	DirectX::XMFLOAT3 m_Dir;
}s_CS_POS_PACKET, *s_pCS_POS_PACKET;

typedef struct _CS_Player_Dir_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
	DirectX::XMFLOAT3 m_Dir;
}s_CS_PLAYER_DIR_PACKET, *s_pCS_PLAYER_DIR_PACKET;

typedef struct _CS_Player_Animation_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
}s_CS_PLAYER_ANIMATION_PACKET, *s_pCS_PLAYER_ANIMATION_PACKET;

typedef struct _CS_Player_Map_Swap_Packet
{
	BYTE m_bSize;
	BYTE m_bType;
}s_CS_PLAYER_MAP_SWAP_PACKET, *s_pCS_PLAYER_MAP_SWAP_PACKET;
#pragma pack(pop)