#include "stdafx.h"
#include "map_village.h"

CMap_Village::CMap_Village()
{
	std::string sample_id;
	DirectX::XMFLOAT3 sample_center;
	DirectX::XMFLOAT3 sample_extent;
	DirectX::XMFLOAT3 sample_pos;
	DirectX::XMFLOAT3 sample_rot;

	m_mmObjectBoundingBox.clear();

	std::ifstream in("../MapData/VillageObject.txt");
	if (!in.is_open()) {
		std::cout << "Village Open Failed" << std::endl;
		return;
	}

	in >> m_iNumObject;
	for (int i = 0; i < m_iNumObject - 2; ++i) {
		in >> sample_id;
		in >> sample_center.x;
		in >> sample_center.y;
		in >> sample_center.z;
		in >> sample_extent.x;
		in >> sample_extent.y;
		in >> sample_extent.z;
		in >> sample_pos.x;
		in >> sample_pos.y;
		in >> sample_pos.z;
		in >> sample_rot.x;
		in >> sample_rot.y;
		in >> sample_rot.z;
		m_mmObjectBoundingBox.insert(std::make_pair(sample_id, DirectX::BoundingOrientedBox(sample_pos, sample_extent, DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f))));
	}
	in.close();
}

CMap_Village::~CMap_Village()
{
	m_mmObjectBoundingBox.clear();
}

bool CMap_Village::CheckMapObjectCollision(DirectX::BoundingOrientedBox& ObjectOOBB)
{
	for (auto& au : m_mmObjectBoundingBox) {
		if (Vector3::ObjectRangeCheck(ObjectOOBB.Center, au.second.Center, 100.0f)) {
			if (au.second.Intersects(ObjectOOBB)) {
				return true;
			}
		}
	}
	return false;
}