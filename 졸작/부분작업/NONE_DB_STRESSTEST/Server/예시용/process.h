#pragma once
#include "stdafx.h"
#include "monsterpool.h"
#include "playerpool.h"

constexpr char g_cProcessLockNum = 4;

//concurrency_priority_queue ������
class CEventOperator {
public:
	CEventOperator() {}
	~CEventOperator() {}
	bool operator()(const s_ObjectEvent& eLeftEvent, const s_ObjectEvent& eRightEvent) {
		return (eLeftEvent.m_hrcTimePoint > eRightEvent.m_hrcTimePoint);
	}
};

class CProcess {
private:
	std::recursive_mutex m_rmProcessMutex[g_cProcessLockNum];
public:
	explicit CProcess();
	virtual ~CProcess();
public:
	static class CPlayerPool* m_pPlayerPool;
	static class CMonsterPool* m_pMonsterPool;
	static concurrency::concurrent_unordered_set<unsigned short>m_ccusLoginList[4];	//MAP_NUM == 4

	static concurrency::concurrent_priority_queue<s_ObjectEvent, CEventOperator>m_ccpqEventQueue;
	
public:
	void InitProcessData();
	concurrency::concurrent_unordered_set<unsigned short>& GetLonginList(char cMapNum)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		auto au = m_ccusLoginList[cMapNum];
		return au;
	}
	void CopyBefore(char cMapNum, concurrency::concurrent_unordered_set<unsigned short>& ccusCopyList)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		ccusCopyList = m_ccusLoginList[cMapNum];
	}
	void DeleteListElement(char cMapNum, unsigned short bId)
	{
		concurrency::concurrent_unordered_set<unsigned short>list;
		CopyBefore(cMapNum, list);
		for (auto au = list.begin(); au != list.end();) {
			if (*au == bId) {
				au = list.unsafe_erase(au);
			}
			else {
				++au;
			}
		}
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		m_ccusLoginList[cMapNum] = list;
	}
	void InsertListElement(char cMapNum, unsigned short bId)
	{
		m_ccusLoginList[cMapNum].insert(bId);
	}
	BOOL ExistListElement(char cMapNum, unsigned short bId)
	{
		std::lock_guard<std::recursive_mutex>lock(m_rmProcessMutex[cMapNum]);
		if (m_ccusLoginList[cMapNum].count(bId) != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	static void InitializeBeforeStart()
	{
		CProcess::m_ccpqEventQueue.clear();
		for (int i = 0; i < 4; ++i) {
			m_ccusLoginList[i].clear();
		}
	}

	static void PostEvent(char cMap, unsigned short usId, char cOverEvent,char cEvent, const std::chrono::high_resolution_clock::time_point& hrcTimePoint) 
	{
		m_ccpqEventQueue.push(s_ObjectEvent{ cMap, hrcTimePoint, cEvent, cOverEvent, usId });
	}
	static bool TryPopEvent(s_ObjectEvent& eEvent){	return m_ccpqEventQueue.try_pop(eEvent);}
	static bool StateEventQueue() { return m_ccpqEventQueue.empty(); }
	static bool CheckEventStart(s_ObjectEvent& eEvent)
	{
		if (m_ccpqEventQueue.try_pop(eEvent)) {
			if (eEvent.m_hrcTimePoint >= std::chrono::high_resolution_clock::now()) {
				CProcess::PostEvent(eEvent.m_cMap, eEvent.m_usID, eEvent.m_cOverType, eEvent.m_cEventType, eEvent.m_hrcTimePoint);
				return false;
			}
			else return true;
		}
		else return false;
	}

public:
	bool CheckCollisionPlayerAttack(char cMap, unsigned short usPlayerID, unsigned short usMonsterID);
	bool CheckCollisionPlayerSkill(char cMap, unsigned short usPlayerID, unsigned short usMonsterID);

};