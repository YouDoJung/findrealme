#include "stdafx.h"
#include "map_loby.h"

CMap_Loby::CMap_Loby()
{
	unsigned short sample_id;
	DirectX::XMFLOAT3 sample_extent;
	DirectX::XMFLOAT3 sample_pos;

	m_mmObjectBoundingBox.clear();

	std::ifstream in("../MapData/TopInsideObject.txt");
	if (!in.is_open()) {
		std::cout << "TopObejct Open Failed" << std::endl;
		return;
	}

	in >> m_iNumObject;
	for (int i = 0; i < m_iNumObject; ++i) {
		if (in.eof())
			break;
		else {
			in >> sample_id;
			in >> sample_pos.x;
			in >> sample_pos.y;
			in >> sample_pos.z;
			in >> sample_extent.z;
			in >> sample_extent.y;
			in >> sample_extent.x;
			m_mmObjectBoundingBox.insert(std::make_pair(sample_id, DirectX::BoundingOrientedBox(sample_pos, sample_extent, DirectX::XMFLOAT4(0.f, 0.f, 0.f, 1.f))));
		}
	}
	in.close();
}

CMap_Loby::~CMap_Loby()
{
	m_mmObjectBoundingBox.clear();
}

bool CMap_Loby::CheckMapObjectCollision(const DirectX::BoundingOrientedBox& ObjectOOBB)
{
	DirectX::BoundingOrientedBox object = ObjectOOBB;
	for (auto& au : m_mmObjectBoundingBox) {
		if (Vector3::ObjectRangeCheck(object.Center, au.second.Center, 500.f)) {
			if (au.second.Intersects(object)) {
				return true;
			}
		}
	}
	return false;
}