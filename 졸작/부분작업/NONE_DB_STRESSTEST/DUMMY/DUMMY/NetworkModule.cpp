#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#include <winsock.h>
#include <Windows.h>
#include <iostream>
#include <thread>
#include <vector>
#include <unordered_set>
#include <mutex>
#include <atomic>
#include <chrono>
#include <queue>
#include <random>
#include <array>
#include <unordered_map>

using namespace std;
using namespace chrono;

extern HWND		hWnd;

const static int MAX_TEST = 3000;
const static int INVALID_ID = -1;
const static int MAX_BUFF_SIZE = 255;

#pragma comment (lib, "ws2_32.lib")


#include "../../Server/예시용/directXmath.h"
#include "../../Server/예시용/protocol.h"

HANDLE g_hiocp;
void SendPacket(int cl, void *packet);
enum OPTYPE { OP_SEND, OP_RECV, OP_DO_MOVE };

high_resolution_clock::time_point last_connect_time;

struct OverlappedEx {
	WSAOVERLAPPED over;
	WSABUF wsabuf;
	unsigned char IOCP_buf[MAX_BUFF_SIZE];
	OPTYPE event_type;
	int event_target;
};

struct CLIENT {
	int id_;
	char map_ = 0;
	high_resolution_clock::time_point last_move_time_;
	bool connect_;
	DirectX::XMFLOAT3 pos_;
	DirectX::XMFLOAT3 dir_;

	SOCKET client_socket_;
	OverlappedEx recv_over_;
	unsigned char packet_buf_[MAX_PACKET_SIZE];
	int prev_packet_data_;
	int curr_packet_size_;
};


struct NPC {
	int id_;
	char map_ = 0;
	bool connect_;
	DirectX::XMFLOAT3 pos_;
};

array<CLIENT, MAX_PLAYER> g_clients;
array<NPC, MAX_MONSTER> g_npcs;
atomic_int num_connections;

vector <thread *> worker_threads;
thread test_thread;

float point_cloud[MAX_PLAYER * 2];
float point_cloud_NPC[MAX_MONSTER * 2];

// 나중에 NPC까지 추가 확장 용
struct ALIEN {
	int id;
	int x, y;
	int visible_count;
};

void error_display(const char *msg, int err_no)
{
	WCHAR *lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	std::cout << msg;
	std::wcout << L"에러" << lpMsgBuf << std::endl;

	MessageBox(hWnd, lpMsgBuf, L"ERROR", 0);
	LocalFree(lpMsgBuf);
	while (true);
}

void DisconnectClient(int ci)
{
	closesocket(g_clients[ci].client_socket_);
	g_clients[ci].connect_ = false;
	cout << "Client [" << ci << "] Disconnected!\n";
}

void ProcessPacket(int ci, unsigned char packet[])
{
	switch (packet[1])
	{
	case SC_LOGIN:
	{
		s_pSC_FIRST_STATUS_PACKET login_packet = reinterpret_cast<s_pSC_FIRST_STATUS_PACKET>(packet);
		g_clients[ci].id_ = login_packet->m_usId;
		g_clients[ci].map_ = login_packet->m_cMap;
		//g_clients[ci].pos_ = DirectX::XMFLOAT3(login_packet->m_fX, login_packet->m_fY, login_packet->m_fZ);
		break;
	}
	case SC_MAP_SWAP:
	{
		s_pSC_PLAYER_MAP_SWAP_PACKET map_swap_packet = reinterpret_cast<s_pSC_PLAYER_MAP_SWAP_PACKET>(packet);
		g_clients[ci].map_ = map_swap_packet->m_bMapType;
		break;
	}
	case SC_NPC_LOG_IN:
	{
		if (g_clients[ci].map_ == MAP_DUNGEON) {
			s_pSC_NPC_LOGIN_PACKET login_packet = reinterpret_cast<s_pSC_NPC_LOGIN_PACKET>(packet);
			g_npcs[login_packet->m_usId].connect_ = true;
		}
		else if (g_clients[ci].map_ == MAP_BOSS) {
			s_pSC_NPC_LOGIN_PACKET login_packet = reinterpret_cast<s_pSC_NPC_LOGIN_PACKET>(packet);
			g_npcs[login_packet->m_usId + NOMALDUNGEON_MONSTER_NUM].connect_ = true;
		}

		break;
	}
	case SC_NPC_STATUS:
	{
		if (g_clients[ci].map_ == MAP_DUNGEON) {
			s_pSC_NPC_STATUS_PACKET status_packet = reinterpret_cast<s_pSC_NPC_STATUS_PACKET>(packet);
			g_npcs[status_packet->m_usId].pos_ = DirectX::XMFLOAT3(status_packet->m_fX, 516.0f, status_packet->m_fZ);
		}
		else if (g_clients[ci].map_ == MAP_BOSS) {
			s_pSC_NPC_STATUS_PACKET status_packet = reinterpret_cast<s_pSC_NPC_STATUS_PACKET>(packet);
			int id = status_packet->m_usId + NOMALDUNGEON_MONSTER_NUM;
			g_npcs[id].pos_ = DirectX::XMFLOAT3(status_packet->m_fX, 516.0f, status_packet->m_fZ);
		}
		break;
	}
	case SC_NPC_REMOVE:
	{
		if (g_clients[ci].map_ == MAP_DUNGEON) {
			s_pSC_NPC_LOGIN_PACKET logout_packet = reinterpret_cast<s_pSC_NPC_LOGIN_PACKET>(packet);
			g_npcs[logout_packet->m_usId].connect_ = false;
		}
		else if (g_clients[ci].map_ == MAP_BOSS) {
			s_pSC_NPC_LOGIN_PACKET logout_packet = reinterpret_cast<s_pSC_NPC_LOGIN_PACKET>(packet);
			g_npcs[logout_packet->m_usId + NOMALDUNGEON_MONSTER_NUM].connect_ = false;
		}
		break;
	}
	default:
		break;
	}
}

void Worker_Thread()
{
	while (true) {
		DWORD io_size;
		unsigned long long ci;
		OverlappedEx *over;
		BOOL ret = GetQueuedCompletionStatus(g_hiocp, &io_size, &ci,
			reinterpret_cast<LPWSAOVERLAPPED *>(&over), INFINITE);
		// std::cout << "GQCS :";
		if (FALSE == ret) {
			int err_no = WSAGetLastError();
			if (64 == err_no) DisconnectClient(ci);
			else error_display("GQCS : ", WSAGetLastError());
		}
		if (0 == io_size) {
			DisconnectClient(ci);
			continue;
		}
		if (OP_RECV == over->event_type) {
			//std::cout << "RECV from Client :" << ci;
			//std::cout << "  IO_SIZE : " << io_size << std::endl;
			unsigned char *buf = g_clients[ci].recv_over_.IOCP_buf;
			unsigned psize = g_clients[ci].curr_packet_size_;
			unsigned pr_size = g_clients[ci].prev_packet_data_;
			while (io_size > 0) {
				if (0 == psize) psize = buf[0];
				if (io_size + pr_size >= psize) {
					// 지금 패킷 완성 가능
					unsigned char packet[MAX_PACKET_SIZE];
					memcpy(packet, g_clients[ci].packet_buf_, pr_size);
					memcpy(packet + pr_size, buf, psize - pr_size);
					ProcessPacket(static_cast<int>(ci), packet);
					io_size -= psize - pr_size;
					buf += psize - pr_size;
					psize = 0; pr_size = 0;
				}
				else {
					memcpy(g_clients[ci].packet_buf_ + pr_size, buf, io_size);
					pr_size += io_size;
					io_size = 0;
				}
			}
			g_clients[ci].curr_packet_size_ = psize;
			g_clients[ci].prev_packet_data_ = pr_size;
			DWORD recv_flag = 0;
			int ret = WSARecv(g_clients[ci].client_socket_,
				&g_clients[ci].recv_over_.wsabuf, 1,
				NULL, &recv_flag, &g_clients[ci].recv_over_.over, NULL);
			if (SOCKET_ERROR == ret) {
				int err_no = WSAGetLastError();
				if (err_no != WSA_IO_PENDING)
				{
					error_display("RECV ERROR", err_no);
				}
			}
		}
		else if (OP_SEND == over->event_type) {
			if (io_size != over->wsabuf.len) {
				std::cout << "Send Incomplete Error!\n";
				closesocket(g_clients[ci].client_socket_);
				g_clients[ci].connect_ = false;
			}
			delete over;
		}
		else if (OP_DO_MOVE == over->event_type) {
			// Not Implemented Yet
			delete over;
		}
		else {
			std::cout << "Unknown GQCS event!\n";
			while (true);
		}
	}
}

void Adjust_Number_Of_Client()
{
	if (num_connections >= MAX_TEST) return;

	if (high_resolution_clock::now() < last_connect_time + 100ms) return;
	last_connect_time = high_resolution_clock::now();

	g_clients[num_connections].client_socket_ = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN ServerAddr;
	ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(SERVER_PORT);
	ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");


	int Result = WSAConnect(g_clients[num_connections].client_socket_, (sockaddr *)&ServerAddr, sizeof(ServerAddr), NULL, NULL, NULL, NULL);
	if (0 != Result) {
		error_display("WSAConnect : ", GetLastError());
	}

	g_clients[num_connections].curr_packet_size_ = 0;
	g_clients[num_connections].prev_packet_data_ = 0;
	ZeroMemory(&g_clients[num_connections].recv_over_, sizeof(g_clients[num_connections].recv_over_));
	g_clients[num_connections].recv_over_.event_type = OP_RECV;
	g_clients[num_connections].recv_over_.wsabuf.buf =
		reinterpret_cast<CHAR *>(g_clients[num_connections].recv_over_.IOCP_buf);
	g_clients[num_connections].recv_over_.wsabuf.len = sizeof(g_clients[num_connections].recv_over_.IOCP_buf);

	
	




	DWORD recv_flag = 0;
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(g_clients[num_connections].client_socket_), g_hiocp, num_connections, 0);

	g_clients[num_connections].connect_ = true;


	int ret = WSARecv(g_clients[num_connections].client_socket_, &g_clients[num_connections].recv_over_.wsabuf, 1,
		NULL, &recv_flag, &g_clients[num_connections].recv_over_.over, NULL);
	if (SOCKET_ERROR == ret) {
		int err_no = WSAGetLastError();
		if (err_no != WSA_IO_PENDING)
		{
			error_display("RECV ERROR", err_no);
		}
	}
	

	num_connections++;
}

void SendPacket(int cl, void *packet)
{
	int psize = reinterpret_cast<unsigned char *>(packet)[0];
	int ptype = reinterpret_cast<unsigned char *>(packet)[1];
	OverlappedEx *over = new OverlappedEx;
	over->event_type = OP_SEND;
	memcpy(over->IOCP_buf, packet, psize);
	ZeroMemory(&over->over, sizeof(over->over));
	over->wsabuf.buf = reinterpret_cast<CHAR *>(over->IOCP_buf);
	over->wsabuf.len = psize;
	int ret = WSASend(g_clients[cl].client_socket_, &over->wsabuf, 1, NULL, 0,
		&over->over, NULL);
	if (0 != ret) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			error_display("Error in SendPacket:", err_no);
	}
	//std::cout << "Send Packet [" << ptype << "] To Client : " << cl << std::endl;
}

void Test_Thread()
{
	while (true) {
		Adjust_Number_Of_Client();
		random_device rand;
		default_random_engine dre{ rand() };
		uniform_real_distribution<float> ramdom_pos(-50.0f, 50.0f);
		Sleep(10);
		for (int i = 0; i < num_connections; ++i) {
			if (false == g_clients[i].connect_) continue;
			if (g_clients[i].last_move_time_ + 500ms > high_resolution_clock::now()) continue;
			g_clients[i].last_move_time_ = high_resolution_clock::now();

			s_CS_POS_PACKET my_packet;
			my_packet.m_bSize = sizeof(s_CS_POS_PACKET);
			my_packet.m_bType = CS_POS;

		
			
			my_packet.m_fX = g_clients[i].pos_.x += ramdom_pos(dre);
			my_packet.m_fY = 516.0f;
			my_packet.m_fZ = g_clients[i].pos_.z += ramdom_pos(dre);

			/*if (i == 0) {
				std::cout << g_clients[i].pos_.x << '\t' << g_clients[i].pos_.z << '\n';
			}*/

			SendPacket(i, &my_packet);
		}
	}
}

void InitializeNetwork()
{
	random_device rand;
	default_random_engine dre{ rand() };
	uniform_real_distribution<float> ramdom_pos_x(100.0f, 10000.0f);
	uniform_real_distribution<float> ramdom_pos_z(55.0f, 75100.0f);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		g_clients[i].connect_ = false;
		g_clients[i].id_ = INVALID_ID;
		g_clients[i].pos_ = DirectX::XMFLOAT3(ramdom_pos_x(dre), 516.0f, ramdom_pos_z(dre));
	}
	for (int i = 0; i < MAX_MONSTER; ++i) {
		g_npcs[i].connect_ = false;
		g_npcs[i].id_ = INVALID_ID;
	}
	num_connections = 0;
	last_connect_time = high_resolution_clock::now();

	WSADATA	wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);

	g_hiocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, NULL, 0);

	for (int i = 0; i < 6; ++i)
		worker_threads.push_back(new std::thread{ Worker_Thread });

	test_thread = thread{ Test_Thread };
}

void ShutdownNetwork()
{
	test_thread.join();
	for (auto& pth : worker_threads) {
		pth->join();
		delete pth;
	}
}

void Do_Network()
{
	return;
}

void GetPointCloud(char map,int* all_size, int *size, float **points, float **points_npc)
{
	int cnt = 0;
	for (int i = 0; i < num_connections; ++i) {
		if (g_clients[i].map_ != map) continue;
		point_cloud[i * 2] = g_clients[i].pos_.x;
		point_cloud[i * 2 + 1] = g_clients[i].pos_.z;
		++cnt;
	}
	int npc_cnt;

	if (map == MAP_BOSS) {
		npc_cnt = 0;
		for (int i = 0; i < NOMALDUNGEON_MONSTER_NUM; ++i) {
			if (!g_npcs[i].connect_) continue;
			point_cloud_NPC[i * 2] = g_npcs[i].pos_.x;
			point_cloud_NPC[i * 2 + 1] = g_npcs[i].pos_.z;
			++npc_cnt;
		}
	}
	else if (map == MAP_BOSS) {
		npc_cnt = 0;
		for (int i = NOMALDUNGEON_MONSTER_NUM; i < MAX_MONSTER; ++i) {
			if (!g_npcs[i].connect_) continue;
			point_cloud_NPC[i * 2] = g_npcs[i].pos_.x;
			point_cloud_NPC[i * 2 + 1] = g_npcs[i].pos_.z;
			++npc_cnt;
		}
	}
	*all_size = num_connections;
	*size = cnt;
	*points = point_cloud;
	*points_npc = point_cloud_NPC;
}

